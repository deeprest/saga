#!/bin/bash

# --if-changed
macos=$(ls -dtU build/MacOS/Saga* | head -1)
butler push --fix-permissions $macos deeprest/saga:macos

linux=$(ls -dtU build/Linux/Saga* | head -1)
rm $linux/*.debug
butler push --fix-permissions $linux deeprest/saga:linux

windows=$(ls -dtU build/Windows/Saga* | head -1)
butler push $windows deeprest/saga:windows

# zipwebgl=saga2020-webgl.zip
# rm $zipwebgl
# webgl=$(ls -td build/WebGL/Saga* | head -1)
# echo $webgl
# ditto -c -k --sequesterRsrc $webgl $zipwebgl
# butler push $zipwebgl deeprest/saga2020:webgl
