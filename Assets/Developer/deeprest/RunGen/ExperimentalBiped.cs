﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class ExperimentalBiped : MonoBehaviour
{
  // Component References
  public Animator animator;

  public void ApplyBipedState( BipedState BS )
  {
    // play sounds, etc
    animator.Play( BS.animationState );
    transform.position = BS.MS.pos;
  }
}