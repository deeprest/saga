﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(RunGen), true )]
public class RunGenEditor : Editor
{
  public override void OnInspectorGUI()
  {
    RunGen obj = target as RunGen;
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Run" ) )
    {
      obj.Run();
      /*Selection.activeObject = obj.runner.controller;*/
    }
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Stop" ) )
    {
      obj.Stop();
    }
    DrawDefaultInspector();
  }
}
#endif

public class RunGen : MonoBehaviour
{
  public PlayerBiped runner;
  public LineRenderer lineRenderer;

  public float unit = 1;
  public float grid = 0.125f;
  const int count = 100;
  Vector3[] pos = new Vector3[count];
  int index;
  Vector3 oldTrail;
  public float trailUnit = 0.1f;

  public List<GameObject> gos = new List<GameObject>();
  Vector3 oldBottom = Vector3.zero;
  public GameObject bottom;
  Vector3 oldRight = Vector3.zero;
  public GameObject right;

  bool running;

  void Start()
  {
    for( int i = 0; i < count; i++ )
      pos[i] = default;
    /*Global.instance.Controls.DevActions.PlaceCollider.performed += PlaceCollider;*/
  }

  void Update()
  {
    //runner = (PlayerBiped) Global.instance.CurrentPlayer;

    if( Vector3.SqrMagnitude( runner.transform.position - oldTrail ) > trailUnit )
    {
      oldTrail = runner.transform.position;
      pos[index] = runner.transform.position;
      lineRenderer.SetPosition( index, pos[index] );
      index = (index + 1) % count;
    }
  }

  public void Run()
  {
    runner.UpdateCollision = Collision;
    running = true;
    RunGenController runGenController = (RunGenController) runner.controller;
    runGenController.Generate();
    runGenController.Run();
  }

  public void Stop()
  {
    runner.UpdateCollision = runner.UpdateBipedCollision;
    running = false;
  }

  void Collision()
  {
    runner.collideBottom = !(runner.input.MoveRight || runner.input.MoveLeft || runner.input.MoveDown || runner.input.MoveUp) || (!runner.input.MoveDown && !runner.input.Jump);
    if( runner.collideBottom )
      PlaceBottom();

    runner.collideRight = !runner.pinput.Jump && runner.input.Jump && runner.input.MoveRight;
    runner.collideLeft = !runner.pinput.Jump && runner.input.Jump && runner.input.MoveLeft;
  }

  Vector3 Snap( Vector3 vec )
  {
    vec.x = Mathf.Round( vec.x / grid ) * grid;
    vec.y = Mathf.Round( vec.y / grid ) * grid;
    return vec;
  }

  void PlaceCollider( InputAction.CallbackContext context )
  {
    Vector2 dir = context.ReadValue<Vector2>();
    if( dir.x > 0 )
      PlaceRight();
    if( dir.y < 0 )
      PlaceBottom();
  }

  void PlaceBottom()
  {
    Vector3 newBottom = Snap( runner.transform.position + Vector3.down * (runner.box.size.y * 0.5f + runner.DownOffset) );
    if( Vector3.SqrMagnitude( newBottom - oldBottom ) > unit * unit )
    {
      GameObject go = Instantiate( bottom, newBottom, Quaternion.identity, null );
      gos.Add( go );
      oldBottom = newBottom;
    }
  }

  void PlaceRight()
  {
    Vector3 newPos = Snap( runner.transform.position + Vector3.right * (runner.box.size.x * 0.5f) );
    if( Vector3.SqrMagnitude( newPos - oldRight ) > unit * unit )
    {
      gos.Add( Instantiate( right, newPos, Quaternion.identity, null ) );
      oldRight = newPos;
    }
  }
}