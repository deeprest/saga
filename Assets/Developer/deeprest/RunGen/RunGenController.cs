﻿using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(RunGenController) )]
public class RunGenControllerEditor : Editor
{
  public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();
    RunGenController pc = target as RunGenController;
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Generate" ) )
      pc.Generate();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Run" ) )
      pc.Run();
  }
}
#endif

public class RunGenController : PlayerController
{
  public struct RunAction
  {
    public InputState input;
    public float duration;
  }

  List<RunAction> actions = new List<RunAction>();
  public int actionCount = 30;
  int index;
  bool running;
  Timer timer = new Timer();

  public void Generate()
  {
    Random.InitState( System.DateTime.Now.Millisecond );
    actions.Clear();
    for( int i = 0; i < actionCount; i++ )
    {
      RunAction action = new RunAction();
      action.input = new InputState();

      action.input.MoveRight = Random.Range( 0, 2 ) > 0;
      action.input.MoveLeft = !action.input.MoveRight;
      action.input.Jump = Random.Range( 0, 5 ) > 0;
      if( action.input.Jump )
        action.duration = Random.Range( 0.1f, 0.25f );
      else
        action.duration = Random.Range( 0.25f, 0.5f );
      action.input.Dash = Random.Range( 0, 3 ) > 0;
      actions.Add( action );
    }
  }

  public void Run()
  {
    running = true;
    index = 0;
  }

  public void Stop() { running = false; }

  public override void Update()
  {
    if( pawn != null )
    {
      if( index == actionCount )
        running = false;
      if( running )
      {
        if( !timer.IsActive )
          timer.Start( this, actions[index].duration, null, () => { index++; } );
        input = actions[index].input;
        pawn.ApplyInput( input );
      }
    }
    input = default;
  }
}