﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Gizmos = Popcron.Gizmos;

[System.Serializable]
public class BipedAttributes
{
  // collision
  public Rect box = new Rect( 0, 0, .2f, .3f );
  public float Scale = 1;
  public float raylength = 0.025f;
  public float contactSeparation = 0.02f;
  public float DownOffset = 0.114f;
  // smaller head box allows for easier jump out and up onto an overhead wall when standing on a ledge.
  public const float raydown = 0.2f;
  public Vector2 headbox = new Vector2( .1f, .1f );
  public float headboxy = -0.1f;
  //private const float corner = 0.707106769f;
  public const float collisionCornerTop = -0.658504546f;
  public const float collisionCornerBottom = 0.658504546f;
  public const float collisionCornerSide = 0.752576649f;
  public float ignorePlatformDuration = 0.2f;

  // movement
  public float speedFactorNormalized;
  public float moveVelMin = 1.5f;
  public float moveVelMax = 3;
  public float jumpVelMin = 5;
  public float jumpVelMax = 10;
  public float jumpDuration = 0.4f;
  public float jumpRepeatInterval = 0.1f;
  public float dashVelMin = 3;
  public float dashVelMax = 10;
  public float dashDuration = 1;
  public float wallJumpPushVelocity = 1.5f;
  public float wallJumpPushDuration = 0.1f;
  public float wallSlideFriction = 0.5f;
  public float wallSlideFactorGravity = 1f;
  public float wallSlideRotateSpeed = 1;
  public float wallSlideDownY = 1;
  public float wallSlideHardAngleThreshold = 25;
  public float landDuration = 0.1f;
  public float friction = 0.05f;

  public float MoveSpeed()
  {
    return (moveVelMin + (moveVelMax - moveVelMin) * speedFactorNormalized);
  }
  public float JumpSpeed()
  {
    return (jumpVelMin + (jumpVelMax - jumpVelMin) * speedFactorNormalized);
  }

  public int MaxHealth = 5;
}

[System.Serializable]
public class CollisionState
{
  public void Reset()
  {
    bottomCount = 0;
    topCount = 0;
    leftCount = 0;
    rightCount = 0;
    ignoreTwowayPlatform = false;
  }

  public int bottomCount;
  public int topCount;
  public int leftCount;
  public int rightCount;
  public int bottomIndex;
  public int topIndex;
  public int leftIndex;
  public int rightIndex;
  // store raycast hits to process afterward, for things like crush detection.
  public RaycastHit2D[] bottomHits = new RaycastHit2D[4];
  public RaycastHit2D[] topHits = new RaycastHit2D[4];
  public RaycastHit2D[] leftHits = new RaycastHit2D[4];
  public RaycastHit2D[] rightHits = new RaycastHit2D[4];
  
  public bool ignoreTwowayPlatform;
};

[System.Serializable]
public class MoveState
{
  public Vector2 pos;
  public Vector2 velocity;
  public Vector2 inertia;
  public Vector2 up;
  public float rot;
  public bool flying;
  
  void CalcMoveState( InputState IS, BipedAttributes BA, WorldState WS )
  {
    // simlate movement from input
    if( IS.MoveRight )
      pos += Vector2.right * BA.MoveSpeed() * WS.deltaTime;
    if( IS.MoveLeft )
      pos += Vector2.left * BA.MoveSpeed() * WS.deltaTime;
    if( IS.Jump )
      velocity.y = BA.JumpSpeed();

    velocity += WS.gravity * WS.deltaTime;
    velocity.y = Mathf.Max( velocity.y, -Global.MaxVelocity );
    pos = pos + velocity * WS.deltaTime;
    up = Vector2.up;
  }
}

[System.Serializable]
public class BipedState
{
  public BipedState()
  {
    BA = new BipedAttributes();
    MS = new MoveState();
    CS = new CollisionState();
  }
  public BipedAttributes BA;
  public MoveState MS;
  public CollisionState CS;

  // example
  public string animationState;
  public int soundId;

  // MIGRATED
  public bool dead;
  public int Health;

  public bool facingRight;
  public bool onGround;
  public bool jumping;
  public bool walljumping;
  public bool wallsliding;
  public bool landing;
  public bool dashing;
  public bool rotating;
  public float rotateTarget;
  public float rotateSpeed;
  // Wall sliding
  private bool previousWallsliding;
  Vector2 previousWallSlideTargetNormal;
  Vector2 wallSlideNormal;
  Vector2 wallSlideTargetNormal;
  // auto wallslide directional flag
  public bool lastMoveWasRight;
  
  // REFERENCES
  // moving platforms / stacking characters
  public Entity carryCharacter;
  public List<Collider2D> IgnoreCollideObjects = new List<Collider2D>();
  // Timers
  [SerializeField] Timer landTimer= new Timer();
  Timer ignoreOnewayPlatformMinimum = new Timer();
  
  public void CalcBipedState( InputState IS, WorldState WS, UpdateFunctions UF )
  {
    if( dead || Health <= 0 )
      return;

    UF.Mover( IS, WS );
    UF.Collision( WS );
    
    bool collideBottom = CS.bottomCount > 0;
    bool collideLeft = CS.leftCount > 0;
    bool collideRight = CS.rightCount > 0;
    onGround = collideBottom || (collideLeft && CS.leftHits[CS.leftIndex].normal.y > 0 && collideRight && CS.rightHits[CS.rightIndex].normal.y > 0);

    if( collideBottom )
    {
      MS.velocity.y = Mathf.Max( MS.velocity.y, 0 );
      MS.inertia.x = 0;
    }
    
    animationState = "idle";
  }
  
  public void CalcMoveState( InputState IS, WorldState WS )
  {
    // simlate movement from input
    if( IS.MoveRight )
      MS.pos += Vector2.right * BA.MoveSpeed() * WS.deltaTime;
    if( IS.MoveLeft )
      MS.pos += Vector2.left * BA.MoveSpeed() * WS.deltaTime;
    if( IS.Jump )
      MS.velocity.y = BA.JumpSpeed();

    MS.velocity += WS.gravity * WS.deltaTime;
    MS.velocity.y = Mathf.Max( MS.velocity.y, -Global.MaxVelocity );
    MS.pos = MS.pos + MS.velocity * WS.deltaTime;
    MS.up = Vector2.up;
  }

  public void CalcCollisionState( WorldState WS )
  {
    // reuse collision state
    CS.Reset();

    Vector2 adjust = MS.pos;
    float down = MS.velocity.y > 0 ? BipedAttributes.raydown - BA.DownOffset : BipedAttributes.raydown;
    down = Mathf.Max( down * BA.Scale, -MS.velocity.y * WS.deltaTime );

    Bounds bounds = new Bounds( adjust + Vector2.down * down * 0.5f, BA.box.size + Vector2.up * Mathf.Max( down, -MS.velocity.y * WS.deltaTime ) );
    Gizmos.Line( new Vector3( bounds.min.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.max.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.min.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.max.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.max.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.min.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.max.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.min.y ), Color.red );

    int onewayLayer = LayerMask.NameToLayer( "twowayPlatform" );
    int downMask = Global.CharacterCollideLayers;
    if( CS.ignoreTwowayPlatform || MS.flying )
      downMask = Global.CharacterCollideLayers & ~LayerMask.GetMask( "twowayPlatform" );
    
    float prefer = float.MinValue;
    int bottomHitCount = Physics2D.BoxCastNonAlloc( adjust, BA.box.size * BA.Scale, MS.rot, MS.up, CS.bottomHits, down, downMask );
    for( int i = 0; i < bottomHitCount; i++ )
    {
      RaycastHit2D hit = CS.bottomHits[i];
      if( IgnoreCollideObjects.Contains( hit.collider ) )
         continue;
      if( hit.normal.y > 0 && hit.normal.y > BipedAttributes.collisionCornerBottom && hit.point.y > prefer &&
        ((onewayLayer != hit.transform.gameObject.layer) || hit.point.y < MS.pos.y - BA.box.size.y * 0.5f * BA.Scale) )
      {
        prefer = hit.point.y;
        CS.bottomIndex = i;
        CS.bottomCount = 1;
        MS.velocity.y = Mathf.Max( MS.velocity.y, 0 );
        MS.inertia.x = 0;
        adjust.y = hit.point.y + BA.box.size.y * 0.5f * BA.Scale + BA.DownOffset * BA.Scale;
      }
    }

    MS.pos = adjust;
  }
}

public class WorldState
{
  public float deltaTime;
  public Vector2 gravity;
}

[System.Serializable]
public class BipedStateGroup
{
  public BipedState BS;
  public MoveState MS;
  public CollisionState CS;
  public BipedAttributes BA;
  public WorldState WS;
}

public delegate void CalcMoveFunction( InputState IS, WorldState WS );
public delegate void CalcCollisionFunction( WorldState WS );
public class UpdateFunctions 
{
  public CalcMoveFunction Mover;
  public CalcCollisionFunction Collision;
}

public class ExperimentalController : ScriptableObject
{
  public ExperimentalBiped biped;
  // Controller calculates a BipedState from these structs/states, and then applies it to a biped.
  public WorldState mWorldState;
  public InputState mInputState;
  
  /*public BipedAttributes mBipedAttributes;
  public MoveState mMoveState;
  public CollisionState mCollisionState;*/
  public BipedState mBipedState;

  /*public BipedStateGroup mBipedStateGroup;*/
  UpdateFunctions mUpdateFunctions;

  public void Initialize()
  {
    /*mBipedStateGroup = new BipedStateGroup()
    {
      BS = new BipedState(),
      BA = new BipedAttributes(),
      MS = new MoveState(),
      CS = new CollisionState()
    };*/
    mWorldState = new WorldState();
    mInputState = new InputState();  
    
    mBipedState = new BipedState();
    mUpdateFunctions = new UpdateFunctions()
    {
      Mover = mBipedState.CalcMoveState,
      Collision = mBipedState.CalcCollisionState
    };
      
    
  }

  public void Update()
  {
    mWorldState.deltaTime = Time.deltaTime;
    mWorldState.gravity = Vector2.down * Global.Gravity;
    
    /*
    // Can calculate movement paths based on changing input or differing biped attributes.
    // Can apply input state from a device or recorded input, for instance.
    CalcMoveState( mMoveState, mInputState, mBipedAttributes, worldState );
    // RunGen may bypass this call and create its own collision state. 
    CalcCollisionState( ref mCollisionState, ref mMoveState, mBipedAttributes, worldState, biped.IgnoreCollideObjects );
*/
    /* CalcBipedState( mBipedState, mMoveState, mCollisionState, mBipedAttributes );*/
    /*CalcBipedState( mBipedStateGroup, mUpdateFunctions );*/
    
    mBipedState.CalcBipedState( mInputState, mWorldState, mUpdateFunctions );
    biped.ApplyBipedState( mBipedState );
  }
  
  
  /*
  void CalcMoveState( MoveState MS, InputState IS, BipedAttributes BA, WorldState WS )
  {
    // simlate movement from input
    if( IS.MoveRight )
      MS.pos += Vector2.right * BA.MoveSpeed() * WS.deltaTime;
    if( IS.MoveLeft )
      MS.pos += Vector2.left * BA.MoveSpeed() * WS.deltaTime;
    if( IS.Jump )
      MS.velocity.y = BA.JumpSpeed();

    MS.velocity += WS.gravity * WS.deltaTime;
    MS.velocity.y = Mathf.Max( MS.velocity.y, -Global.MaxVelocity );
    MS.pos = MS.pos + MS.velocity * WS.deltaTime;
    MS.up = Vector2.up;
  }

  void CalcCollisionState( ref CollisionState CS, ref MoveState MS, BipedAttributes BA, WorldState WS, List<Collider2D> IgnoreCollideObjects )
  {
    // reuse collision state
    CS.Reset();

    Vector2 adjust = MS.pos;
    float down = MS.velocity.y > 0 ? BipedAttributes.raydown - BA.DownOffset : BipedAttributes.raydown;
    down = Mathf.Max( down * BA.Scale, -MS.velocity.y * WS.deltaTime );

    Bounds bounds = new Bounds( adjust + Vector2.down * down * 0.5f, BA.box.size + Vector2.up * Mathf.Max( down, -MS.velocity.y * WS.deltaTime ) );
    Gizmos.Line( new Vector3( bounds.min.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.max.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.min.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.max.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.max.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.min.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.max.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.min.y ), Color.red );

    int onewayLayer = LayerMask.NameToLayer( "twowayPlatform" );
    int downMask = Global.CharacterCollideLayers;
    if( CS.ignoreOnewayPlatform || MS.flying )
      downMask = Global.CharacterCollideLayers & ~LayerMask.GetMask( "twowayPlatform" );
    
    float prefer = float.MinValue;
    int bottomHitCount = Physics2D.BoxCastNonAlloc( adjust, BA.box.size * BA.Scale, MS.rot, MS.up, CS.bottomHits, down, downMask );
    for( int i = 0; i < bottomHitCount; i++ )
    {
      RaycastHit2D hit = CS.bottomHits[i];
      if( IgnoreCollideObjects.Contains( hit.collider ) )
         continue;
      if( hit.normal.y > 0 && hit.normal.y > BipedAttributes.collisionCornerBottom && hit.point.y > prefer &&
        ((onewayLayer != hit.transform.gameObject.layer) || hit.point.y < MS.pos.y - BA.box.size.y * 0.5f * BA.Scale) )
      {
        prefer = hit.point.y;
        CS.bottomIndex = i;
        CS.bottomCount = 1;
        MS.velocity.y = Mathf.Max( MS.velocity.y, 0 );
        MS.inertia.x = 0;
        adjust.y = hit.point.y + BA.box.size.y * 0.5f * BA.Scale + BA.DownOffset * BA.Scale;
      }
    }

    MS.pos = adjust;
  }
  */
/*
  void CalcBipedState( BipedState BS, MoveState MS, CollisionState CS, BipedAttributes BA )
  {
    if( BS.dead || BS.Health <= 0 )
      return;

    bool collideBottom = CS.bottomCount > 0;
    bool collideLeft = CS.leftCount > 0;
    bool collideRight = CS.rightCount > 0;
    BS.onGround = collideBottom || (collideLeft && CS.leftHits[CS.leftIndex].normal.y > 0 && collideRight && CS.rightHits[CS.rightIndex].normal.y > 0);

    if( BS.onGround )
    {
      MS.velocity.y = Mathf.Max( MS.velocity.y, 0 );
      MS.inertia.x = 0;
    }

    BS.animationState = "idle";
  }

  void CalcBipedState( BipedStateGroup BSG, UpdateFunctions UF ) 
  {
    if( BSG.BS.dead || BSG.BS.Health <= 0 )
      return;

    bool collideBottom = BSG.CS.bottomCount > 0;
    bool collideLeft = BSG.CS.leftCount > 0;
    bool collideRight = BSG.CS.rightCount > 0;
    BSG.BS.onGround = collideBottom || (collideLeft && BSG.CS.leftHits[BSG.CS.leftIndex].normal.y > 0 && collideRight && BSG.CS.rightHits[BSG.CS.rightIndex].normal.y > 0);

    if( BSG.BS.onGround )
    {
      BSG.MS.velocity.y = Mathf.Max( BSG.MS.velocity.y, 0 );
      BSG.MS.inertia.x = 0;
    }

    BSG.BS.animationState = "idle";
  
  }
  */
}