﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(ChainGen), true )]
public class ChainGenEditor : Editor
{
  public override void OnInspectorGUI()
  {
    ChainGen level = target as ChainGen;
    EditorGUILayout.BeginHorizontal();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Generate" ) ) level.Generate();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "+1" ) )
    {
      level.seed++;
      level.Generate();
    }
    EditorGUILayout.EndHorizontal();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Destroy" ) ) level.DestroyAll();
    DrawDefaultInspector();
  }
}
#endif

public class ChainGen : MonoBehaviour
{
  public bool DebugBounds = false;
  public int MaxLinkPasses = 30;
  public int seed;
  public Vector2Int dimension = new Vector2Int( 20, 8 );
  private Vector2Int cellsize = new Vector2Int( 10, 10 );
  List<GameObject> gens = new List<GameObject>();
  List<Bounds> established = new List<Bounds>();
  List<Bounds> debug = new List<Bounds>();
  public GameObject[] ChainLinks;
  public float fudge = 0.15f;

  void Start() { Generate(); }

  public void Generate()
  {
    DestroyAll();
    if( !Application.isPlaying || Application.isEditor )
      Random.InitState( seed );
    else
      Random.InitState( Global.instance.Seed );
    
    GenerateChain( ChainLinks, Vector2.zero, true );

    foreach( var go in gens )
    {
      GenerationVariant[] vars = go.GetComponentsInChildren<GenerationVariant>();
      foreach( var cmp in vars ) cmp.Generate();
    }
  }

  void GenerateChain( GameObject[] gos, Vector2 pos, bool overlapCheck )
  {
    established.Clear();
    debug.Clear();
    List<NodeLink> links = new List<NodeLink>();
    List<NodeLink> newLinks = new List<NodeLink>();
    List<NodeLink> removeLinks = new List<NodeLink>();
    // first link
    GameObject first = CreateChainLink( gos[0], pos );
    links.AddRange( first.GetComponentsInChildren<NodeLink>() );
    established.Add( first.GetComponent<PrefabAABB>().bounds );

    for( int i = 0; i < MaxLinkPasses; i++ )
    {
      links.AddRange( newLinks );
      removeLinks.ForEach( ( item ) => { links.Remove( item ); } );
      newLinks.Clear();
      removeLinks.Clear();
      foreach( var link in links )
      {
        int count = link.linkSet.AllowedToLink.Length;
        int[] indices = Util.ShuffledIndexList( count );
        bool prefabFound = false;
        for( int prefabIndex = 0; prefabIndex < count; prefabIndex++ )
        {
          GameObject prefab = link.linkSet.AllowedToLink[indices[prefabIndex]];
          Bounds pbounds = prefab.GetComponent<PrefabAABB>().bounds;
          Bounds checkBounds = pbounds;
          checkBounds.center = link.transform.position + pbounds.center;
          checkBounds.extents = new Vector3( checkBounds.extents.x - fudge, checkBounds.extents.y - fudge, 1 );
          if( DebugBounds ) debug.Add( checkBounds );
          bool conflict = false;
          for( int idx = 0; idx < established.Count; idx++ )
          {
            if( established[idx].Intersects( checkBounds ) &&
              link.transform.root.GetComponent<PrefabAABB>().bounds.GetHashCode() != established[idx].GetHashCode() )
            {
              conflict = true;
              break;
            }
          }
          if( !conflict )
          {
            prefabFound = true;
            removeLinks.Add( link );
            link.gameObject.SetActive( false );
            GameObject go = CreateChainLink( prefab, link.transform.position );
            PrefabAABB aabb = go.GetComponent<PrefabAABB>();
            aabb.RecalculateBounds();
            newLinks.AddRange( go.GetComponentsInChildren<NodeLink>() );
            established.Add( aabb.bounds );
            break;
          }
        }
        // spawn endcap if no prefab can be used
        if( !prefabFound && link.endCap != null )
        {
          Instantiate( link.endCap, link.transform.position, Quaternion.identity, link.transform.parent );
          removeLinks.Add( link );
          link.gameObject.SetActive( false );
        }
      }
    }
    
    links.AddRange( newLinks );
    removeLinks.ForEach( ( item ) => { links.Remove( item ); } );
    newLinks.Clear();
    removeLinks.Clear();
    foreach( var link in links )
    {
      // spawn endcap if no prefab can be used
      if( link.endCap != null )
      {
        Instantiate( link.endCap, link.transform.position, Quaternion.identity, link.transform.parent );
        removeLinks.Add( link );
        link.gameObject.SetActive( false );
      }
    }
  }

  GameObject CreateChainLink( GameObject prefab, Vector3 pos )
  {
    GameObject go = null;
    if( Application.isPlaying )
    {
      go = Instantiate( prefab, pos, Quaternion.identity );
    }
    else
    {
#if UNITY_EDITOR
      go = (GameObject) PrefabUtility.InstantiatePrefab( prefab );
      go.transform.position = pos;
#endif
    }
    go.name = prefab.name;
    gens.Add( go );
    return go;
  }

  public void DestroyAll()
  {
    for( int i = 0; i < gens.Count; i++ ) Util.Destroy( gens[i] );
    gens.Clear();
    established.Clear();
  }

  void OnDrawGizmos()
  {
    Gizmos.color = Color.green;
    foreach( var bound in established ) Gizmos.DrawWireCube( bound.center, bound.size );
    if( DebugBounds )
    {
      Gizmos.color = new Color( 1, 0, 1, 1 );
      foreach( var bound in debug ) Gizmos.DrawWireCube( bound.center, bound.size );
    }
  }
}