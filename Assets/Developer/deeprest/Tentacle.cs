﻿using UnityEngine.U2D;
using UnityEngine;

public class Tentacle : Entity
{
  [System.Serializable]
  public struct AnimatedSpline
  {
    public Vector2 pos0;
    public Vector2 tanR;
    public Vector2 pos1;
    public Vector2 tanL;
  }

  public AnimatedSpline aspline;
  public Vector2 ppos;
  public DamageDefinition damage;
  public CircleCollider2D collider;
  public SpriteShapeController spriteShape;

  public bool stabby;
  public Timer stabbyTimer = new Timer();
  public float stabbySpeed = 5;
  public float stabbyRange = 5;
  public float stabbyTimeout = 3;

  // Start is called before the first frame update
  protected override void Start()
  {
    // empty
  }
  public void Stab( int tentacleIndex ) { stabby = true; }
  // Update is called once per frame
  public void TentacleUpdate()
  {
    if( stabby )
    {
      if( !stabbyTimer.IsActive )
      {
        stabbyTimer.Start( this, stabbyTimeout, null, () => stabby = false );
      }
      if( stabbyTimer.IsActive )
      {
        /*collider.transform.position = Vector2.MoveTowards( collider.transform.position, targetpos, Time.deltaTime * stabbySpeed );
        collider.transform.rotation = Quaternion.LookRotation( Vector3.forward, delta );
        */
        /*if( collider.transform.localPosition.magnitude > stabbyRange || Vector3.Distance( collider.transform.position, targetpos ) < .1f )
        {
          stabbyTimer.Stop( true );
        }*/
      }
    }

    spriteShape.spline.SetPosition( 0, aspline.pos0 );
    spriteShape.spline.SetRightTangent( 0, aspline.tanR );
    spriteShape.spline.SetPosition( 1, collider.transform.localPosition );
    spriteShape.spline.SetLeftTangent( 1, collider.transform.localRotation * -Vector3.up );
    ppos = collider.transform.position;
    //collider.transform.localPosition = aspline.pos1;
    //collider.transform.rotation = Quaternion.LookRotation( Vector3.forward, -aspline.tanL );
    // tentacle hit
    if( collider.enabled )
    {
      hitCount = Physics2D.CircleCastNonAlloc( ppos, collider.radius, (Vector2)collider.transform.position - ppos, Global.RaycastHits, ((Vector2)collider.transform.position - ppos).magnitude, Global.DefaultProjectileCollideLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];
        if( hit.transform.root == transform.root )
          continue;
        IDamage dam = hit.transform.GetComponent<IDamage>();
        if( dam != null )
        {
          Damage dmg = new Damage();
          dmg.def = damage;
          dmg.instigator = this;
          dmg.damageSource = transform;
          dmg.hit = hit;
          dam.TakeDamage( dmg );
        }
        break;
      }
    }
  }

 

  
}