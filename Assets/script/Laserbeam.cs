﻿using System.Reflection;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Laserbeam : Projectile
{
  [SerializeField] GameObject hitPrefab;
  GameObject hitObject;
  public float BeamDistance = 10;
  [SerializeField] float beamRadius = 0.2f;
  public Timer timeoutTimer;
  public LineRenderer lineRenderer;
  [SerializeField] Light2D lineLight;
  Vector3[] positions = new Vector3[2];

  void OnDestroy() { timeoutTimer.Stop( false ); }

  void Start()
  {
    /*timeoutTimer = new Timer( this, data.Timeout, null, delegate()
    {
      if( gameObject != null )
        Destroy( gameObject );
    } );*/

    mesh = new Mesh();
    vertices = new Vector3[4];
    vertices[0] = new Vector3(0,0,0);
    vertices[1] = new Vector3(1,0,0);
    vertices[2] = new Vector3(1,1,0);
    vertices[3] = new Vector3(0,1,0);
    mesh.vertices = vertices;
    colors = new Color[4];
    colors[0] = Color.white;
    colors[1] = Color.white;
    colors[2] = Color.white;
    colors[3] = Color.white;
    mesh.colors = colors;
    int[] indices = new int[6];
    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
    indices[3] = 1;
    indices[4] = 0;
    indices[5] = 3;
    mesh.SetIndices( indices, MeshTopology.Triangles, 0);
  }

  void Hit( Vector3 position )
  {
    if( hitPrefab != null && hitObject == null )
      hitObject = Instantiate( hitPrefab, position, transform.rotation );
  }
  
  public override void UpdateProjectile()
  {
    if( instigator is Pawn )
      positions[0] = (instigator as Pawn).GetShotOriginPosition();
    else
      positions[0] = shotOriginPosition;
    Vector3 endPoint = (Vector2)positions[0] + velocity.normalized * BeamDistance;
    int hitCount = Physics2D.CircleCastNonAlloc( transform.position, beamRadius, velocity, Global.RaycastHits, BeamDistance, Global.DefaultProjectileCollideLayers );
    // The hit results seem to be sorted by distance. Relying on that here.
    for( int i = 0; i < hitCount; i++ )
    {
      RaycastHit2D hit = Global.RaycastHits[i];
      if( hit.transform != null && hit.transform != transform ) /*&& !ignoreColliders.Contains( hit.collider ) )*/
      {
        if( instigator == null || !hit.transform.IsChildOf( instigator.transform ) )
        {
          IDamage dam = hit.transform.GetComponent<IDamage>();
          if( dam != null )
          {
            Damage dmg = new Damage
            {
              def = data.ContactDamageDefinition,
              instigator = instigator,
              damageSource = transform,
              hit = hit
            };
            DamageResult dmr = dam.CalculateDamageResult( dmg );
            if( !dmr.ignore )
            {
              dmr.entity.TakeDamage( dmg );
              endPoint = hit.point;
              Hit( hit.point );
            }
            else
            {
              endPoint = hit.point;
              Hit( hit.point );
              // hit something solid
              break;
            }
          }
          else
          {
            endPoint = hit.point;
            Hit( hit.point );
            // hit something solid
            break;
          }
        }
      }
    }
    positions[1] = endPoint;
    lineRenderer.SetPositions( positions );
    
    Vector2 line = ((Vector2)endPoint - shotOriginPosition).normalized;
    line = new Vector2( -line.y, line.x );
    /*
    // HACK because Light2D.shapePath is not public, and I need to update the light shape for the laser. 
    Vector3[] shapePath = new Vector3[4]; 
    shapePath[0] = shotOriginPosition + line * beamRadius;
    shapePath[1] = shotOriginPosition - line * beamRadius;
    shapePath[2] = (Vector2)endPoint - line * beamRadius;
    shapePath[3] = (Vector2)endPoint + line * beamRadius;
    // System.Reflection.FieldInfo fi = lineLight.GetType().GetField( "m_ShapePath", System.Reflection.BindingFlags.Instance|System.Reflection.BindingFlags.NonPublic );
    // fi?.SetValue(lineLight, shapePath );
    // System.Reflection.MethodInfo mi = lineLight.GetType().GetMethod( "UpdateMesh", System.Reflection.BindingFlags.Instance );
    // mi?.Invoke( lineLight, null );
    lineLight.m_ShapePath = shapePath;
    lineLight.UpdateMesh();
    // m_ShapePath
    // m_ShapeLightFalloffSize
    // m_LocalBounds
    vertices[0] = new Vector3(0,0,0);
    vertices[1] = new Vector3(1,0,0);
    vertices[2] = new Vector3(1,1,0);
    vertices[3] = new Vector3(0,1,0);
    mesh.vertices = vertices;
    lineLight.m_Mesh = mesh;
    lineLight.m_LocalBounds = CalculateBoundingSphere( ref vertices, ref colors, lineLight.m_ShapeLightFalloffSize );
    */
  }

  Vector3[] vertices;
  Color[] colors;
  Mesh mesh;
  
  
  
  // COPIED FROM UnityEngine.Experimental.Rendering.Universal.LightUtility
  public static Bounds CalculateBoundingSphere(ref Vector3[] vertices, ref Color[] colors, float falloffDistance)
  {
    Bounds localBounds = new Bounds();

    Vector3 minimum = new Vector3(float.MaxValue, float.MaxValue);
    Vector3 maximum = new Vector3(float.MinValue, float.MinValue);
    for (int i = 0; i < vertices.Length; i++)
    {
      Vector3 vertex = vertices[i];
      vertex.x += falloffDistance * colors[i].r;
      vertex.y += falloffDistance * colors[i].g;

      minimum.x = vertex.x < minimum.x ? vertex.x : minimum.x;
      minimum.y = vertex.y < minimum.y ? vertex.y : minimum.y;
      maximum.x = vertex.x > maximum.x ? vertex.x : maximum.x;
      maximum.y = vertex.y > maximum.y ? vertex.y : maximum.y;
    }

    localBounds.max = maximum;
    localBounds.min = minimum;

    return localBounds;
  }
}