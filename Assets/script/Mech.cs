using System;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Serialization;
using Gizmos = Popcron.Gizmos;
using Random = UnityEngine.Random;

public class Mech : BossPrototype, ITalker
{
  [Header( "Jabber-Talky" )]
  public CharacterIdentity identity;
  public JabberPlayer jabber;

  public void OnSay( string say )
  {
    animator.Play( "talk" );
    jabber.Play( say );
    // face the player when talking to them
    // todo check if the player still exists
    transform.localScale = new Vector3( Mathf.Sign( Global.instance.CurrentPlayer.transform.position.x - transform.position.x ), 1, 1 );
  }

  public void OnSpeakEnd()
  {
    // HACK called after destruction leads to null refs
    if( animator == null || jabber == null )
      return;
    if( animator.gameObject.activeInHierarchy )
      animator.Play( "idle" );
    jabber.Stop();
  }

  public TalkerCameraInfo GetCameraInfo() { return new TalkerCameraInfo() {focus = (Vector2) Head.transform.position + HeadCamOffset, orthoSize = 0.15f}; }
  public CharacterIdentity GetIdentity() { return identity; }

  [Header( "Mech" )]
  [SerializeField] Entity Head;
  [SerializeField] Vector2 HeadCamOffset;
  public float Scale = 1;
  public AudioSource audio;
  public bool facingRight;
  public BoxCollider2D fist;
  public BoxCollider2D torso;

  [SerializeField] float withinpunchRange = 3;
  [SerializeField] float small = 0.1f;
  [SerializeField] float moveSpeed = 0.5f;
  [SerializeField] float jumpSpeed = 5;
#if UNITY_EDITOR
  float c_jumpSpeed = 5;
#endif
  [ReadOnly, SerializeField] public float JumpHeight;
  [SerializeField] float dashSpeed = 5;
  [SerializeField] float shootUp = 5;
  // durations
  public float jumpDuration = 0.4f;
  public float dashDuration = 1;
  public float landDuration = 0.1f;
  bool onGround;
  Timer jumpTimer = new Timer();
  Timer jumpCooldownTimer = new Timer();
  Timer jumpRepeat = new Timer();
  Timer dashTimer = new Timer();
  Timer dashCooldownTimer = new Timer();

  public AudioClip soundJump;
  public AudioClip soundDash;
  public AudioClip soundReflect;

  [SerializeField] WeaponDefinition weaponDefinition;
  Weapon weapon;
  [SerializeField] Transform shotOrigin;

  // fist
  int FistDamageLayers;
  public DamageDefinition FistDamageDef;
  Vector3 pposFist = Vector3.zero;

  // sight, target
  [SerializeField] Transform sightOrigin;
  [SerializeField] Entity visibleTarget;
  /* [SerializeField] float sightRange = 6;*/
  Timer SightPulseTimer = new Timer();
  SortedList<float, EntityColliderPair> nearby = new SortedList<float, EntityColliderPair>();

  [SerializeField] Entity[] Legs;
  int destroyedLegCount;
  bool legsBroken;
  bool berserk;

  [FormerlySerializedAs( "dashSmoke" ), SerializeField]
  ParticleSystem dashPuff;
  [SerializeField] ParticleSystem HealthWarningSmoke;
  [SerializeField] Light2D DamageWarningLight;

  [SerializeField] GameObject brokenHeadSparks;

  protected override void Start()
  {
    base.Start();
    /*if( controller == null && assignedController != null )
    {
      controller = ScriptableObject.Instantiate( assignedController );
      controller.AssignPawn( this );
    }*/

    UpdateLogic = MechUpdate;
    UpdateHit = MechHit;
    Physics2D.IgnoreCollision( box, fist );
    Physics2D.IgnoreCollision( box, torso );
    Physics2D.IgnoreCollision( torso, fist );
    FistDamageLayers = LayerMask.GetMask( "entity", "destructible" );
    pposFist = fist.transform.position;
    Head.EventDestroyed.AddListener( () =>
    {
      berserk = true;
      brokenHeadSparks.SetActive( true );
      HealthWarningSmoke.Play();
    } );

    foreach( Entity leg in Legs )
    {
      leg.EventDestroyed.AddListener( () =>
      {
        if( ++destroyedLegCount == 1 )
        {
          legsBroken = true;
          moveSpeed = 0;
          StopDash();
          DownOffset = -0.143f;
          Idle();
          for( int i = 0; i < Legs.Length; i++ )
          {
            if( Legs[i] != null )
              Legs[i].Die( default );
          }
        }
      } );
    }
    if( weaponDefinition != null )
      weapon = weaponDefinition.GetNewWeapon();
  }

  protected override void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    base.OnDestroy();
    jumpTimer.Stop( false );
    jumpRepeat.Stop( false );
    dashTimer.Stop( false );
    dashCooldownTimer.Stop( false );

    SightPulseTimer.Stop( false );
    if( Head )
      Head.EventDestroyed.RemoveAllListeners();
    foreach( Entity leg in Legs )
      if( leg )
        leg.EventDestroyed.RemoveAllListeners();
  }

  void SightPulse()
  {
    nearby.Clear();
    int count = Physics2D.OverlapCircleNonAlloc( transform.position, sightRange, Global.ColliderResults,
      Global.EnemyInterestLayers );
    for( int i = 0; i < count; i++ )
    {
      Collider2D cld = Global.ColliderResults[i];
      Entity potentialTarget = Global.ColliderResults[i].GetComponent<Entity>();
      if( potentialTarget != null && IsEnemyTeam( potentialTarget.TeamFlags ) )
      {
        float dist = Vector3.Distance( transform.position, potentialTarget.transform.position );
        if( !nearby.ContainsKey( dist ) )
          nearby.Add( dist, new EntityColliderPair {entity = potentialTarget, collider = cld} );
      }
    }

    visibleTarget = null;

    foreach( var pair in nearby )
    {
      if( pair.Value.collider == null )
        continue;

      // assign target without line of sight.
      AssignTarget( pair.Value.entity );
      break;

      /* // check line of sight to potential target
      hitCount = Physics2D.LinecastNonAlloc( sightOrigin.position, pair.Value.collider.bounds.center, Global.RaycastHits, Global.SightObstructionLayers );
      if( hitCount == 0 )
      {
        AssignTarget( pair.Value.entity );
        break;
      }*/
    }
  }

  void MechUpdate()
  {
#if UNITY_EDITOR
    if( Math.Abs( c_jumpSpeed - jumpSpeed ) > .001f )
    {
      JumpHeight = CalcJumpHeight();
      c_jumpSpeed = jumpSpeed;
    }
#endif

    if( !berserk )
    {
      if( !SightPulseTimer.IsActive )
      {
        SightPulse();
        SightPulseTimer.Start( this, 1 );
      }

      if( velocity.y < 0 )
        StopJump();

      if( !visibleTarget && Instigator )
      {
        AssignTarget( Instigator );
      }
      else if( nearby.Count == 0 )
      {
        Idle();
      }

      if( visibleTarget )
        Attack( visibleTarget );
      else
        Idle();
    }
    else
      Berserk();

    transform.localScale = new Vector3( facingRight ? Scale : -Scale, Scale, 1 );
  }

  void AssignTarget( Entity target ) { visibleTarget = target; }

  void Idle()
  {
    velocity.x = 0;
    // todo Animator.StringToHash
    animator.Play( "idle" );
  }

  void Attack( Entity Target )
  {
    Vector3 targetpos = Target.transform.position;
    Vector3 delta = targetpos - transform.position;

    //  do not change directions while dashing
    if( !dashTimer.IsActive )
      facingRight = delta.x >= 0;

    if( collideBottom && delta.sqrMagnitude < sightRange * sightRange )
    {
      if( !dashTimer.IsActive && !jumpTimer.IsActive )
      {
        if( ((delta.y > 1f && delta.y < 3 && Mathf.Abs( delta.x ) < 3) || (collideRight && rightHits[0].point.y < transform.position.y) || (collideLeft && leftHits[0].point.y < transform.position.y)) && !jumpCooldownTimer.IsActive && !jumpRepeat.IsActive && !jumpTimer.IsActive )
        {
          // see if I can clear the jump
          Vector2 origin = (Vector2) transform.position + box.offset + Vector2.down * box.size * 0.5f + Vector2.up * JumpHeight;
          Vector2 direction = facingRight ? Vector2.right : Vector2.left;
          if( !Physics2D.Raycast( origin, direction, 0.1f, collideMask ) )
          {
            Gizmos.Line( origin, origin + direction * 0.1f, Color.red );
            if( !legsBroken )
              StartJump();
          }
        }
        else if( !dashCooldownTimer.IsActive && Mathf.Abs( delta.y ) < 1 && Mathf.Abs( delta.x ) < withinpunchRange && Mathf.Approximately( Mathf.Sign( transform.localScale.x ), Mathf.Sign( delta.x ) ) )
        {
          StartDash();
        }
        else
        {
          if( legsBroken || Mathf.Abs( delta.x ) < small )
            Idle();
          else if( (Mathf.Sign( delta.x ) > 0 && !collideRight) || (Mathf.Sign( delta.x ) < 0 && !collideLeft) )
          {
            // if not colliding with a wall, walk towards enemy
            animator.Play( "walk" );
            velocity.x = Mathf.Sign( delta.x ) * moveSpeed;
          }
          else
            Idle();

          if( !dead && Head && weaponDefinition ) // && !shootRepeatTimer.IsActive )
          {
            Shoot( targetpos - shotOrigin.position + Vector3.up * shootUp );
            /*
            // todo check line of site to target
            // todo have a "ready to fire" animation play to warn player
            //hit = Physics2D.LinecastAll( shotOrigin.position, player );
            hit = Physics2D.Linecast( shotOrigin.position, targetpos, LayerMask.GetMask( new string[] { "Default", "entity" } ) );
            if( hit.transform == Target.transform )
              Shoot( targetpos - shotOrigin.position + Vector3.up * shootUp );
            
            
            Entity visibleTarget = null;
            // check line of sight to potential target
            hitCount = Physics2D.LinecastNonAlloc( shotOrigin.position, targetpos,  Global.RaycastHits, Global.CharacterCollideLayers );
            if( hitCount == 0 ) 
            {
              
            }
            */
          }
        }
      }
    }
  }

  void Berserk()
  {
    if( !dashCooldownTimer.IsActive )
    {
      facingRight = Random.Range( 0, 2 ) > 0;
      StartDash();
      dashCooldownTimer.Start( this, 1 );
    }
  }

  float CalcJumpHeight()
  {
    // calculate height at which the jump velocity reaches zero.
    float dT = Time.smoothDeltaTime;
    float vel = jumpSpeed;
    float pos = 0;
    // set a max to avoid infinite loop when gravity is zero, etc.
    for( int i = 0; i < 10000 && vel > 0; i++ )
    {
      vel -= Global.Gravity * dT;
      pos += vel * dT;
    }
    return pos;
  }

  void StartJump()
  {
    velocity.y = jumpSpeed;
    audio.PlayOneShot( soundJump );
    //dashSmoke.Stop();
    animator.Play( "jump" );
    jumpRepeat.Start( this, 2, null, null );
    jumpTimer.Start( this, jumpDuration, null, delegate { StopJump(); } );
  }

  void StopJump()
  {
    jumpTimer.Stop( false );
    velocity.y = Mathf.Min( velocity.y, 0 );
  }

  void StartDash()
  {
    animator.Play( "punchdash" );
    if( !legsBroken )
    {
      audio.PlayOneShot( soundDash );
      dashPuff.Play();
      dashTimer.Start( this, dashDuration, ( x ) => { velocity.x = (facingRight ? 1 : -1) * dashSpeed; },
        delegate { StopDash(); } );
    }
  }

  void StopDash()
  {
    // if leg with dashpuff particle system is destroyed, is null here
    if( dashPuff != null )
      dashPuff.Stop();
    dashCooldownTimer.Start( this, 2 );
    dashTimer.Stop( false );
  }

  // cached shoot vector
  Vector2 c_Shoot;

  void Shoot( Vector3 shoot )
  {
    //shootRepeatTimer.Start( this, shootInterval, null, null );
    if( !Physics2D.Linecast( transform.position, shotOrigin.position, Global.ProjectileNoShootLayers ) )
    {
      c_Shoot = shoot;
      weapon.FireWeapon( this, shotOrigin.position, shoot );
    }
  }

  void MechHit()
  {
    if( ContactDamageDefinition == null )
      return;
    // body hit
    hitCount = Physics2D.BoxCastNonAlloc( (Vector2) box.transform.position + box.offset, box.size, 0, velocity, Global.RaycastHits, raylength, Global.CharacterDamageLayers );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      DamageOther( hit.transform.GetComponent<IDamage>(), ContactDamageDefinition, transform, hit );
    }

    hitCount = Physics2D.BoxCastNonAlloc( torso.transform.position, torso.size, 0, velocity, Global.RaycastHits, raylength, Global.CharacterDamageLayers );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      DamageOther( hit.transform.GetComponent<IDamage>(), ContactDamageDefinition, transform, hit );
    }

    // fist hit
    Vector2 fistMag = fist.transform.position - pposFist;
    hitCount = Physics2D.BoxCastNonAlloc( pposFist, fist.size, 0, fistMag, Global.RaycastHits, fistMag.magnitude, FistDamageLayers );
    Gizmos.Bounds( fist.bounds, Color.red );
    Gizmos.Bounds( new Bounds( fist.transform.position, fist.size ), Color.red );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      DamageOther( hit.transform.GetComponent<IDamage>(), ContactDamageDefinition, fist.transform, hit );
    }
    pposFist = fist.transform.position;
  }


  public override void TakeDamage( Damage damage )
  {
    base.TakeDamage( damage );
    if( Health < MaxHealth * 0.3f )
    {
      DamageWarningLight.enabled = true;
      // TODO Remove the optional damage warning light
      Animation optionalWarningAnimation = DamageWarningLight.GetComponent<Animation>();
      if( optionalWarningAnimation != null )
        optionalWarningAnimation.Play();
      HealthWarningSmoke.Play();
    }
    else
      HealthWarningSmoke.Stop();
  }

  public override void Die( Damage damage )
  {
    if( dead )
      return;
    animator.Play( "idle" );
    base.Die( damage );
  }

  public override Vector2 GetShotOriginPosition() { return shotOrigin.position; }

  public override Vector2 GetAimVector()
  {
    if( visibleTarget )
      return visibleTarget.transform.position - shotOrigin.position + Vector3.up * shootUp;
    else
      return c_Shoot;
  }
}