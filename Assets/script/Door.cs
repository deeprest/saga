﻿using UnityEngine;
using UnityEngine.AI;

public class Door : MonoBehaviour, ITrigger
{
  public Animator animator;
  // solid collider, optional
  [SerializeField] Collider2D cd;
  [SerializeField] AudioSource audio;
  [SerializeField] NavMeshObstacle obstacle;
  [SerializeField] CameraZone CameraIn;
  [SerializeField] AudioLoop Music;
  [SerializeField] IndexedColors indexedColors;

  public Door Connected;

  public bool TriggerOnContact;
  public bool isLocked;
  public bool isOpen;
  public bool InsideToTheRight;
  public bool MMXStyleDoorTransition;
  public bool MMXStyleBossDoor;
  public int MMXStyleBossIndex;
  public bool LockAfterEntering;
  const float openDuration = 2;
  [EnumFlag] public TeamFlags OpenForTeams;
  const float animationRate = 16;
  const int openFrame = 19;
  const int closeFrame = 3;

  public AudioClip soundOpen;
  public AudioClip soundClose;
  public AudioClip soundDenied;

  // transient
  Transform instigator;
  TimerParams tp;
  bool busy;
  bool deniedDelay;
  Timer waitToCloseTimer = new Timer();
  Timer timer = new Timer();
  Color doorColor;

  protected System.Action OnOpen;
  protected System.Action OnClose;

  public virtual void Start()
  {
    LockDoor( isLocked );

    if( MMXStyleDoorTransition )
    {
      OnOpen = OnOpenSideDoor;
      OnClose = OnCloseSideDoor;
    }
    else
    {
      OnOpen = null;
      OnClose = null;
    }

    if( isOpen )
    {
      animator.Play( "opened" );
      isOpen = true;
      if( cd != null )
        cd.enabled = false;
      if( obstacle != null )
        obstacle.enabled = false;
    }
    else
    {
      animator.Play( "closed" );
      isOpen = false;
      if( cd != null )
        cd.enabled = true;
      if( obstacle != null )
        obstacle.enabled = true;
    }
  }

  void OnDestroy()
  {
    timer.Stop( false );
    waitToCloseTimer.Stop( false );
  }

  // ITrigger
  public void Trigger( Collider2D trigger, Transform instigator )
  {
    if( !TriggerOnContact || deniedDelay || busy )
      return;
    if( isOpen )
    {
      if( ShouldOpen( instigator ) )
      {
        // hold the door open longer
        waitToCloseTimer.Start( this, new TimerParams
        {
          unscaledTime = Global.Paused,
          repeat = false,
          duration = openDuration,
          CompleteDelegate = delegate
          {
            Close( null );
            if( OnClose != null )
              OnClose();
          }
        } );
        if( Connected != null )
          Connected.waitToCloseTimer.Start( this, openDuration, null, () =>
          {
            Connected.Close();
            if( Connected.OnClose != null )
              Connected.OnClose();
          } );
      }
    }
    else
      OpenDoor( instigator );
  }

  public bool ShouldOpen( Transform instigator )
  {
    Entity check = instigator.GetComponent<Entity>();
    if( check != null )
    {
      bool OpenForThisCharacter = (OpenForTeams & check.TeamFlags) > 0;
      if( isLocked || !OpenForThisCharacter )
        return false;
    }
    return true;
  }

  public virtual void OpenDoor( Transform instigator )
  {
    this.instigator = instigator;

    if( !ShouldOpen( instigator ) )
    {
      Global.instance.AudioOneShot( soundDenied, transform.position );
      deniedDelay = true;
      timer.Start( this, 2, null, delegate { deniedDelay = false; } );
      indexedColors.colors[0] = Color.red;
      indexedColors.ExplicitUpdate();
      animator.Play( "denied" );
      return;
    }

    indexedColors.colors[0] = doorColor;
    indexedColors.ExplicitUpdate();

    if( MMXStyleDoorTransition )
    {
      animator.updateMode = AnimatorUpdateMode.UnscaledTime;
      if( Connected != null )
        Connected.animator.updateMode = AnimatorUpdateMode.UnscaledTime;
      Global.instance.Pause();
      Global.instance.Controls.BipedActions.Disable();
    }

    OpenAndClose();
  }

  public void OpenAndClose()
  {
    Open( delegate
    {
      if( OnOpen != null )
        OnOpen();
      if( Connected != null && Connected.OnOpen != null )
      {
        Connected.instigator = instigator;
        Connected.OnOpen();
      }
      waitToCloseTimer.Start( this, new TimerParams
      {
        unscaledTime = Global.Paused,
        repeat = false,
        duration = openDuration,
        CompleteDelegate = delegate
        {
          Close( null );
          if( OnClose != null )
            OnClose();
        }
      } );
    } );

    if( Connected != null )
      Connected.Open( delegate
      {
        Connected.waitToCloseTimer.Start( this, new TimerParams
          {
            unscaledTime = Global.Paused,
            repeat = false,
            duration = openDuration,
            CompleteDelegate = delegate
            {
              Connected.Close( null );
              if( Connected.OnClose != null )
                Connected.OnClose();
            }
          }
        );
      } );
  }


  public void Open( System.Action onOpen = null )
  {
    if( busy || isOpen )
      return;
    waitToCloseTimer.Stop( false );
    busy = true;
    animator.Play( "opening" );
    audio.PlayOneShot( soundOpen );
    TimerParams timerParams = new TimerParams
    {
      unscaledTime = Global.Paused,
      repeat = false,
      duration = (1.0f / animationRate) * openFrame,
      UpdateDelegate = null,
      CompleteDelegate = delegate
      {
        busy = false;
        isOpen = true;
        if( cd != null )
          cd.enabled = false;
        if( obstacle != null )
          obstacle.enabled = false;
        if( onOpen != null )
          onOpen();
      }
    };
    timer.Start( this, timerParams );
  }

  // for access from UnityEvent
  public void Close() { Close( null ); }

  void Close( System.Action onClosed = null )
  {
    if( busy || !isOpen )
      return;
    waitToCloseTimer.Stop( false );
    busy = true;
    animator.Play( "closing" );
    audio.PlayOneShot( soundClose );
    timer.Start( this, (1.0f / animationRate) * closeFrame, null, delegate
    {
      isOpen = false;
      if( cd != null )
        cd.enabled = true;
      if( obstacle != null )
        obstacle.enabled = true;
      timer.Start( this, 2, null, delegate
      {
        busy = false;
        if( onClosed != null )
          onClosed();
      } );
    } );
  }

  void OnOpenSideDoor()
  {
    SceneScript sceneScript = Global.instance.sceneScript;
    bool Entering;
    Vector2 targetPosition;
    Vector2 instigatorDelta = instigator.position - transform.position;
    float yOffset = Mathf.Abs( instigatorDelta.y ) < 0.2f ? instigatorDelta.y : 0;
    if( Vector3.Dot( transform.right, transform.position - instigator.position ) > 0 )
    {
      targetPosition = transform.position + transform.right * 0.5f + Vector3.up * yOffset;
      Entering = InsideToTheRight;
    }
    else
    {
      targetPosition = transform.position - transform.right * 0.5f + Vector3.up * yOffset;
      Entering = !InsideToTheRight;
    }

    // door is now open
    if( sceneScript != null )
    {
      if( Entering )
      {
        if( CameraIn != null )
        {
          Global.instance.OverrideCameraZone( CameraIn );
          Global.instance.CameraController.Lerp( CameraIn.CameraBounds.center, CameraIn.CameraBounds.extents.y, openDuration );
        }
        if( MMXStyleBossDoor )
          sceneScript.FadeOutMusic( 2 );
        else if( Music != null )
          Global.instance.MusicCrossFadeTo( Music, 2 );
      }
      else
      {
        if( CameraIn != null )
        {
          Global.instance.OverrideCameraZone( null );
          Global.instance.CameraController.Lerp( targetPosition, Global.instance.FloatSetting["Zoom"].Value, openDuration );
        }
        if( sceneScript.music != null )
          Global.instance.MusicCrossFadeTo( sceneScript.music, 2 );
      }
    }

    PlayerBiped player = instigator.GetComponentInParent<PlayerBiped>();
    if( player != null )
      player.DoorTransition( targetPosition, openDuration );
  }

  void OnCloseSideDoor()
  {
    if( MMXStyleDoorTransition )
    {
      // door is now closed
      if( LockAfterEntering )
      {
        isLocked = true;
        doorColor = Color.red;
      }
      if( MMXStyleBossDoor )
      {
        if( Global.instance.sceneScript != null )
        {
          if( MMXStyleBossDoor )
          {
            Global.instance.Unpause();
            Global.instance.sceneScript.StartBossIntro( MMXStyleBossIndex );
          }
        }
      }
      else
      {
        Global.instance.Controls.BipedActions.Enable();
        Global.instance.Unpause();
      }
    }
  }

  public void LockDoor( bool locked )
  {
    isLocked = locked;
    if( isLocked )
      doorColor = Color.red;
    else
    {
      Team team = Global.instance.GetTeam( OpenForTeams );
      if( team != null )
        doorColor = team.color[0];
      else
        doorColor = Color.grey;
    }
    if( indexedColors != null )
    {
      indexedColors.colors[0] = doorColor;
      indexedColors.ExplicitUpdate();
    }
  }
}