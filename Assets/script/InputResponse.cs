﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputResponse : MonoBehaviour, Controls.IBipedActionsActions
{
  [SerializeField] bool AutoEnable;
  [SerializeField] WorldText nameTxt;
  [SerializeField] Animator[] anim;
  Dictionary<string, int> lookup = new Dictionary<string, int>();
  
  void Start()
  {
    if( AutoEnable )
      Enable();
  }

  void OnDestroy()
  {
    if( AutoEnable )
      Disable();
  }

  public void Enable()
  {
    Global.instance.Controls.BipedActions.SetCallbacks( this );
    lookup.Clear();
    int i = 0;
    foreach( var animator in anim )
      lookup.Add( animator.name, i++ );
  }

  public void Disable()
  {
    Global.instance.Controls.BipedActions.SetCallbacks( null );
  }

  void DoTheThing( InputAction.CallbackContext context )
  {
    if( !lookup.ContainsKey( context.action.name ) ) 
      return;
    if( context.started ) anim[lookup[context.action.name]].Play( "on" );
    if( context.canceled ) anim[lookup[context.action.name]].Play( "off" );
    if( nameTxt != null )
    {
      nameTxt.text = context.action.name;
      nameTxt.ExplicitUpdate();
    }
  }

  public void OnMoveRight( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnMoveLeft( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnMove( InputAction.CallbackContext context ) { }

  public void OnJump( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnDash( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnAim( InputAction.CallbackContext context ) { }

  public void OnFire( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnAbility( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnInteract( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnNextWeapon( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnNextAbility( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnCharge( InputAction.CallbackContext context ) { }

  public void OnDown( InputAction.CallbackContext context )
  {
    DoTheThing( context );
  }

  public void OnMinimap( InputAction.CallbackContext context ) 
  { 
    DoTheThing( context );
  }
}