﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Serialization;

[CreateAssetMenu]
public class AbilityDefinition : ScriptableObject
{
  public virtual Ability GetNewAbility() { return new Ability( this ); }
  [FormerlySerializedAs( "icon" )]
  public Sprite Icon;
  public GameObject AnimatedIcon;
  [FormerlySerializedAs( "cursor" )]
  public Sprite Cursor;
  [FormerlySerializedAs( "prefab" )]
  public GameObject Prefab;
  [FormerlySerializedAs( "mountType" )]
  public Ability.MountType MountType;
  [FormerlySerializedAs( "pickupPrefab" )]
  public Pickup PickupPrefab;
  public bool FireArmAlwaysActive;
  [FormerlySerializedAs( "armFireSprite" )]
  public Sprite FireArmSprite;
  [FormerlySerializedAs( "fireArmTimeout" )]
  public float FireArmTimeout = 0.25f;
}


public class Ability
{
  public enum MountType
  {
    NONE,
    ARM_BACK,
    ARM_FRONT,
    BACK
  }
  
  public Ability( AbilityDefinition rhsDef ) { abilityDef = rhsDef; }
  public AbilityDefinition abilityDef;
  
  public Entity owner;
  public PlayerBiped biped;
  public bool IsActive;
  
  // transient
  protected GameObject go;

  // instance ID, sorting layer
  Dictionary<int, int> AbilityRendererLayerOffset = new Dictionary<int, int>();
  SpriteRenderer[] abilityRenderers;

  public virtual void OnAcquire( Entity ownerEntity )
  {
    owner = ownerEntity;
    if( owner is PlayerBiped )
      biped = owner as PlayerBiped;
  }

  public virtual void Equip( Transform parentTransform )
  {
    if( abilityDef.Prefab != null )
    {
      go = Object.Instantiate( abilityDef.Prefab, parentTransform.position, Quaternion.identity, parentTransform );
      go.transform.localRotation = Quaternion.identity;

      AbilityRendererLayerOffset.Clear();
      abilityRenderers = go.transform.GetComponentsInChildren<SpriteRenderer>();
      if( abilityRenderers != null )
        for( int i = 0; i < abilityRenderers.Length; i++ )
          AbilityRendererLayerOffset.Add( abilityRenderers[i].GetInstanceID(), abilityRenderers[i].sortingOrder - abilityRenderers[0].sortingOrder );
    }
  }

  public virtual void Unequip()
  {
    if( abilityDef.Prefab != null )
      Object.Destroy( go );
  }

  public virtual void Activate( Vector2 origin, Vector2 aim ) { }

  public virtual void UpdateAbility() { }

  public virtual void UpdateAbilityLate()
  {
    if( abilityDef.MountType == MountType.ARM_BACK )
    {
      if( abilityDef.FireArmAlwaysActive || biped.fireArmTimer.IsActive )
      {
        // must set the sprite in LateUpdate, after the animator has updated.
        biped.arm.rotation = Quaternion.LookRotation( Vector3.forward, biped.GetAimVector() );
        biped.armMount.localPosition = Vector3.up * 0.29f;
        biped.armMount.localRotation = Quaternion.identity;
        if( abilityDef.FireArmSprite != null )
          biped.partArmBack.renderer.sprite = abilityDef.FireArmSprite;
      }
    }
  }

  public virtual void Deactivate() { }
  public virtual void PreSceneTransition() { }
  public virtual void PostSceneTransition() { }

  public void RenderLayerLateUpdate( int CharacterLayer, int AssociatedAbilityPartLayerAnimated )
  {
    // keep the mounted sprite renderer sorting layers synchronized with the parent sorting layer while animating.
    if( abilityRenderers != null )
      for( int i = 0; i < abilityRenderers.Length; i++ )
        abilityRenderers[i].sortingOrder = CharacterLayer + AssociatedAbilityPartLayerAnimated + AbilityRendererLayerOffset[abilityRenderers[i].GetInstanceID()] + 1;
  }
}