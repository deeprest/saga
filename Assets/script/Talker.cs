﻿using UnityEngine;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using Random = UnityEngine.Random;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor( typeof(Talker) )]
public class TalkerEditor : Editor
{
  public override void OnInspectorGUI()
  {
    if( Application.isPlaying )
    {
      Talker obj = target as Talker;
      if( GUI.Button( EditorGUILayout.GetControlRect(), "Test" ) )
        obj.Select( null );
    }
    DrawDefaultInspector();
  }
}
#endif

[RequireComponent( typeof(JabberPlayer), typeof(Animator) )]
public class Talker : WorldSelectable, ITalker
{
  public CharacterIdentity identity;
  public JabberPlayer jabber;
  public Animator animator;
  [SerializeField] Transform headTransform;
  [SerializeField] Vector2 headOffset;
  public bool FacePlayer;

  bool isDead;
  int stateHighlight;

  void Start()
  {
    if( headTransform == null )
      headTransform = transform.Find( "head" );

    if( identity == null )
    {
      Debug.LogWarning( "null identity for " + name, gameObject );
      return;
    }
    if( identity.TextAsset == null )
    {
      Debug.LogWarning( "identity has no text asset " + name, gameObject );
      return;
    }
    if( identity.TextAsset.text.Length == 0 )
    {
      Debug.LogWarning( "identity text asset is empty " + name, gameObject );
      return;
    }

    stateHighlight = Animator.StringToHash( "highlight" );
  }
  
  void Update()
  {
    if( FacePlayer && Global.instance.CurrentPlayer != null )
      transform.localScale = new Vector3( Mathf.Sign( Global.instance.CurrentPlayer.transform.position.x - transform.position.x), 1, 1 );
  }

  private void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    OnSpeakEnd();
    isDead = true;
  }

  public override void Highlight()
  {
    base.Highlight();
    if( !isDead && animator.HasState( 0, stateHighlight ) )
      animator.Play( stateHighlight );
  }

  public override void Unhighlight()
  {
    base.Unhighlight();
    if( !isDead )
      animator.Play( "idle" );
  }

  public override Vector2 GetPosition()
  {
    if( headTransform != null )
      return headTransform.GetComponent<SpriteRenderer>().bounds.center;
    return transform.position;
  }

  public override void Select( Entity instigator )
  {
    if( instigator == null )
      return;
    // todo read this once on initialize
    var deserializer = new DeserializerBuilder().WithNamingConvention( new CamelCaseNamingConvention() ).Build();
    ConvoDocument doc = deserializer.Deserialize<ConvoDocument>( new StringReader( identity.TextAsset.text ) );
    // pick a random remark to say
    string say = string.Copy( doc.random[Random.Range( 0, doc.random.Length )] );
    Global.instance.Speak( this, say, 4, PriorityEnum.Remark );
  }

  public override void Unselect() { }

  public void OnSay( string say )
  {
    animator.Play( "talk" );
    jabber.Play( say ); 
    // face the player when talking to them
    // todo check if the player still exists
    transform.localScale = new Vector3( Mathf.Sign( Global.instance.CurrentPlayer.transform.position.x - transform.position.x ), 1, 1 );
  }

  public TalkerCameraInfo GetCameraInfo()
  {
    return new TalkerCameraInfo() {focus = (Vector2)headTransform.position + headOffset, orthoSize = 0.15f};
  }

  public CharacterIdentity GetIdentity()
  {
    return identity;
  }

  public void OnSpeakEnd()
  {
    // HACK called after destruction leads to null refs
    if( animator == null || jabber == null )
      return;
    if( animator.gameObject.activeInHierarchy )
      animator.Play( "idle" );
    jabber.Stop();
  }
}