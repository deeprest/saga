﻿using UnityEngine;
using LitJson;

namespace deeprest
{
  public static class JsonUtil
  {
    public static void SerializeValue( JsonWriter writer, System.Type type, object value )
    {
      if( type == typeof(System.String) )
        writer.Write( (string) value );
      else if( type == typeof(System.Int32) )
        writer.Write( (System.Int32) value );
      else if( type == typeof(System.Boolean) )
        writer.Write( (System.Boolean) value );
      else if( type == typeof(System.Single) )
        writer.Write( System.Math.Round( (System.Single) value, 3 ) );
      else if( type == typeof(Vector3) )
        JsonUtil.WriteVector( writer, (Vector3) value );
      else
        writer.Write( null );
    }

    public static void WriteVector( JsonWriter writer, Vector3 vec )
    {
      writer.WriteArrayStart();
      writer.Write( System.Math.Round( vec.x, 3 ) );
      writer.Write( System.Math.Round( vec.y, 3 ) );
      writer.Write( System.Math.Round( vec.z, 3 ) );
      writer.WriteArrayEnd();
    }

    public static Vector3 ReadVector( JsonData data )
    {
      return new Vector3( (float) data[0].GetReal(), (float) data[1].GetReal(), (float) data[2].GetReal() );
    }

    public static Vector3 ReadVector( Vector3 defaultValue, JsonData data, string key )
    {
      if( data.Keys.Contains( key ) )
        return new Vector3( (float) data[key][0].GetReal(), (float) data[key][1].GetReal(), (float) data[key][2].GetReal() );
      return defaultValue;
    }

    public static T Read<T>( T defaultValue, JsonData data, params string[] list )
    {
      if( data == null )
        return defaultValue;
      JsonData obj = data;
      for( int i = 0; i < list.Length; i++ )
      {
        string key = list[i];
        if( obj.Keys.Contains( key ) )
        {
          if( i == list.Length - 1 )
          {
            JsonData value = obj[key];
            if( value == null )
              return (T) System.Convert.ChangeType( null, typeof(T) );
            ;
            JsonType type = value.GetJsonType();
            switch( type )
            {
              case JsonType.None:
                Debug.LogError( "should be value" );
                break;
              case JsonType.Object:
                return (T) System.Convert.ChangeType( value, typeof(T) );
              case JsonType.Array:
                Debug.LogError( "should be value" );
                break;
              case JsonType.String:
                return (T) System.Convert.ChangeType( value.GetString(), typeof(T) );
              case JsonType.Natural:
                return (T) System.Convert.ChangeType( value.GetNatural(), typeof(T) );
              case JsonType.Real:
                return (T) System.Convert.ChangeType( value.GetReal(), typeof(T) );
              case JsonType.Boolean:
                return (T) System.Convert.ChangeType( value.GetBoolean(), typeof(T) );
            }
          }
          else
            obj = obj[key];
        }
      }
      return defaultValue;
    }
  }
}