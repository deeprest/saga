﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(BackgroundWindow) )]
[CanEditMultipleObjects]
public class BackgroundWindowEditor : Editor
{
    public override void OnInspectorGUI()
    {
        BackgroundWindow window = target as BackgroundWindow;
        if( window.Connected != null )
        {
            if( window.EnforceOffsetInEditor )
            {
                window.Connected.transform.position = (Vector2) window.transform.position +
                    ((window.OffsetDownward ? Vector2.down : Vector2.up) * Global.GameplayLayerOffset);
                // disable other door offset to avoid recursive shennanigans
                window.Connected.EnforceOffsetInEditor = false;
            }
        }
        DrawDefaultInspector();
    }
}
#endif

public class BackgroundWindow : MonoBehaviour
{
    public Vector2Int textureDimension;
#if UNITY_EDITOR
    public bool EnforceOffsetInEditor;
    public bool OffsetDownward = true;
#endif
    
    public BackgroundWindow Connected;

    [SerializeField] MeshRenderer portalMeshRenderer;
    Global.ViewPortal portal;
    public void PreSceneTransition() { Global.instance.DoneWithViewPortal( portal ); }
    

    void OnWillRenderObject()
    {
        if( Connected != null && (portal == null || portal.owner != this) &&
            Global.instance.RequestViewPortal( ref portal ) )
        {
            portal.owner = this;

            if( portal.texture.width != textureDimension.x || portal.texture.height != textureDimension.y )
            {
                portal.texture.DiscardContents();
                portal.texture.Release();
                portal.texture = new RenderTexture( textureDimension.x, textureDimension.y, 24, RenderTextureFormat.Default, 0 );
                portal.texture.filterMode = FilterMode.Point;
                portal.camera.GetComponent<Camera>().targetTexture = portal.texture;
                portal.camera.GetComponent<Camera>().orthographicSize = 0.5f * textureDimension.y / Global.PixelDensity;
            }
            portalMeshRenderer.material.mainTexture = portal.texture;
            portal.camera.transform.position = Connected.transform.position;
            // Used in CityWrap, to avoid "dangling" portals
            portal.camera.transform.parent = Connected.transform;
        }
    }
}