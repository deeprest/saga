
public class CaptainSmirk : Talker
{
  
  // serialize if persistent
  bool Condition_BoxDestroyed;
  bool Condition_BoxDestroyed_Reacted;
  public Talker ConvoCondition_BoxDestroyed_OtherTalker;

  public int highlightCount;
  bool Condition_Highlight;

/*
  public struct TalkerCondition
  {
    public TalkerCondition( string tag, System.Action<bool> handler )
    {
      Tag = tag;
      Handler = handler;
      Condition = false;
      Handled = false;
      Global.instance.RegisterProgressFlag( Tag, Handler );
    }

    public string Tag;
    public bool Condition;
    public bool Handled;
    public System.Action<bool> Handler;
  }

  // TalkerCondition boxDest;
  Dictionary<string,TalkerCondition> Conditions = new Dictionary<string,TalkerCondition>();
  */

  void Start()
  {
    
    Global.instance.RegisterProgressFlag( "boxDestroyed", ProgressFlag_BoxDestroyed );
    Global.instance.RegisterProgressFlag( "youin", ProgressFlag_YouIn );
    Global.instance.RegisterProgressFlag( "teachBossIntro", ProgressFlag_TeachBossIntro );

    /*//boxDest = new TalkerCondition( "boxDestroyed", ProgressFlag_BoxDestroyed );
    Conditions.Add( "boxDestroyed", new TalkerCondition( "boxDestroyed", ProgressFlag_BoxDestroyed2 ) );*/
  }


  public override void Highlight()
  {
    base.Highlight();
    /*if( boxDest.Condition && !boxDest.Handled ) boxDest.Handler( true );*/

    // In case the conversation could not start when the condition was triggered, do it now.
    if( Condition_BoxDestroyed && !Condition_BoxDestroyed_Reacted ) 
      ProgressFlag_BoxDestroyed( true );
    
    if( !Condition_Highlight && ++highlightCount > 5 )
    {
      Condition_Highlight = true;
      // SetProgressFlag will only trigger the result one time
      Global.instance.SetProgressFlag( "youin" );
    }
  }

  public override void Select( Entity instigator )
  {
    // TEMP
    ProgressFlag_TeachBossIntro( true );

    // SetProgressFlag will only trigger the result one time
    //if( Global.instance.SetProgressFlag( "teachBossIntro" ) ) return;


    /*foreach( var condition in Conditions.Values )
      if( condition.Condition && !condition.Handled )
      {
        condition.Handler( true );
        break;
      }*/
  }

  void ProgressFlag_TeachBossIntro( bool flag )
  {
    Global.instance.StartConversation( new[] { this, Global.instance.CurrentPlayer.GetComponent<ITalker>()
    }, "convo_intro_to_boss_battles", true );
  }

  void ProgressFlag_BoxDestroyed( bool flag )
  {
    Condition_BoxDestroyed = flag;
    if( Condition_BoxDestroyed_Reacted || !flag )
      return;
    ITalker player = Global.instance.PlayerController.pawn.GetComponent<ITalker>();
    ITalker[] talkers = new ITalker[] {this, player, ConvoCondition_BoxDestroyed_OtherTalker};
    // 
    Condition_BoxDestroyed_Reacted = Global.instance.StartConversation( talkers, "convo_boxDestroyed", true );
  }
  
  
  void ProgressFlag_BoxDestroyed2( bool flag )
  {
    if( Condition_BoxDestroyed_Reacted || !flag )
      return;
    ITalker player = Global.instance.PlayerController.pawn.GetComponent<ITalker>();
    ITalker[] talkers = new ITalker[] {this, player, ConvoCondition_BoxDestroyed_OtherTalker};
    // 
    Condition_BoxDestroyed_Reacted = Global.instance.StartConversation( talkers, "convo_boxDestroyed", true );
  }

  void ProgressFlag_YouIn( bool flag )
  {
    if( Global.instance.CurrentPlayer != null )
    {
      ITalker player = Global.instance.CurrentPlayer.GetComponent<ITalker>();
      if( Global.instance.StartConversation( new[] {this, player}, "convo_youin", true ) )
        Global.instance.CurrentPlayer.InteractIndicator.SetActive( false );
    }
  }
}