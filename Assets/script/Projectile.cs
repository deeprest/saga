﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
  public Animator animator;
  public CircleCollider2D circle;
  public WeaponDefinition.ProjectileData data;
  
  /*
  The weapon may have been destroyed by the time this is accessed, so don't do this.
  public Weapon weapon;
  */
  public WeaponDefinition.WeaponCategory weaponCategory;
  public Entity instigator;
  // keep the team flag separate from the instigator in case the instigator dies before projectile contact.
  public TeamFlags teamFlags;
  public Vector2 shotOriginPosition;
  public Vector2 velocity;

  void Update() { UpdateProjectile(); }
  
  public virtual void UpdateProjectile() { }
}