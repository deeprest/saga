﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
  static int Active;
  const int MaxExplosionSoundsAtOnce = 2;

  [SerializeField] float timeout = 0.35f;
  public bool playSound = true;

  public bool playRandomSound;
  public AudioClip[] sounds;

  void Start()
  {
    Destroy( gameObject, timeout );
    if( Active <= MaxExplosionSoundsAtOnce )
    {
      if( playSound )
      {
        if( playRandomSound )
          GetComponent<AudioSource>().clip = sounds[Random.Range( 0 , sounds.Length )];
        GetComponent<AudioSource>().Play();
      }
      Active++;
    }
  }

  private void OnDestroy()
  {
    Active--;
  }
}