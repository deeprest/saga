﻿using UnityEngine;
using UnityEngine.Serialization;

// Allows for tuning damage settings globally with ScriptableObjects
[CreateAssetMenu]
public class DamageDefinition : ScriptableObject
{
  [FormerlySerializedAs( "type" )]
  public Damage.DamageTypeEnum typeEnum = Damage.DamageTypeEnum.Generic;
  public int amount = 1;
}

public struct DamageResult
{
  // the actual entity hit; could be a subentity/child entity.
  public IDamage entity;
  // ignore damage, or (take damage)
  public bool ignore;
  // pass through, or (absorb the hit)
  public bool pass;
  // reflects
  public bool reflect;

  /*public bool reflectBackAtInstigator;
   public bool reflectChangeTeam; */
  public DamageResult( IDamage entity )
  {
    this.entity = entity;
    ignore = false;
    pass = false;
    reflect = false;
  }
}

public struct Damage
{
  public enum DamageTypeEnum
  {
    Generic,
    Fire,
    Crush, 
    Melee
  }

  public DamageDefinition def;
  // optional instigator
  public Entity instigator;
  // the projectile, etc
  public Transform damageSource;

  public RaycastHit2D hit;
  
  // public Vector2 point;
  // public Collider2D collider;

  public Damage( DamageDefinition ddef )
  {
    def = ddef;
    instigator = null;
    damageSource = null;
    hit = default;
  }
}