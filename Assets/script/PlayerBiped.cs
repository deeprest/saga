﻿#define HEALTH_AUDIO

using System.Collections.Generic;
using UnityEngine;
using Gizmos = Popcron.Gizmos;
using Random = UnityEngine.Random;

[SelectionBase]
public class PlayerBiped : Pawn
{
  public Vector2 Velocity
  {
    get
    {
      if( carryEntity != null )
        return velocity + carryEntity.Velocity;
      else if( carryConveyorBelt != null )
        return velocity + conveyorMove;
      else
        return velocity + inertia;
    }
  }

  [Header( "PlayerBiped" )]
  public Controller assignedController;
  public bool ResetInputOnLateUpdate;

  /*public float slidingOffset = 1;
  public float slidingOffsetTarget = 1;
  public float slidingOffsetRate = 4;*/
  
  // smaller head box allows for easier jump out and up onto an overhead wall when standing on a ledge.
  const float raydown = 0.2f;
  /*public Vector2 headbox = new Vector2( .1f, .1f );
  public float headboxy = -0.1f;*/
  const float downslopefudge = 0.2f;
  //private const float corner = 0.707106769f;
  const float collisionCornerTop = -0.658504546f;
  const float collisionCornerBottom = 0.658504546f;
  const float collisionCornerSide = 0.752576649f;

  Vector2 shoot;
  bool victorySting;

  [Header( "State" )]
  // Move State
  public bool facingRight = true;
  public bool onGround;
  public bool flying;
  bool jumping;
  bool walljumping;
  bool wallsliding;
  bool landing;
  bool dashing;
  /* bool crouching;*/
  public bool rotating;
  public float rotateTarget;
  public float rotateSpeed;
  // Wall sliding
  private bool previousWallsliding;
  Vector2 previousWallSlideTargetNormal;
  Vector2 wallSlideNormal;
  Vector2 wallSlideTargetNormal;
  // auto wallslide directional flag
  public bool lastMoveWasRight;
  Timer dashTimer = new Timer();
  Timer jumpTimer = new Timer();
  Timer jumpRepeatTimer = new Timer();
  Timer landTimer = new Timer();
  Timer walljumpTimer = new Timer();
  // temporary variable for position calculation
  private Vector2 pos;
  [SerializeField] IndexedColors indexedColors;

  [Header( "DEV" )]
  public float MoveScale = 0.5f;
  public float JumpScale = 0.5f;
  public AnimationCurve OrthoScale;

  [Header( "Setting" )]
  public float speedFactorNormalized = 1;

  // movement
  public float moveVelMin = 1.5f;
  public float moveVelMax = 3;

  public float moveSpeed { get { return (moveVelMin + (moveVelMax - moveVelMin) * speedFactorNormalized); } }

  public float jumpVelMin = 5;
  public float jumpVelMax = 10;

  public float jumpSpeed { get { return (jumpVelMin + (jumpVelMax - jumpVelMin) * speedFactorNormalized); } }

  public float jumpDuration = 0.4f;
  public float jumpRepeatInterval = 0.1f;
  public float dashVelMin = 3;
  public float dashVelMax = 10;

  public float dashSpeed { get { return (dashVelMin + (jumpVelMax - jumpVelMin) * speedFactorNormalized); } }

  public float dashDuration = 1;
  public float wallJumpPushVelocity = 1.5f;
  public float wallJumpPushDuration = 0.1f;
  public float wallSlideFriction = 0.5f;
  public float wallSlideFactorGravity = 1f;
  public float wallSlideRotateSpeed = 1;
  public float wallSlideDownY = 1;
  public float wallSlideHardAngleThreshold = 25;
  public float landDuration = 0.1f;


  public ParticleSystem dashSmoke;
  public GameObject dashflashPrefab;
  public GameObject walljumpEffect;
  public Timer fireArmTimer = new Timer();

  [Header( "Cursor" )]
  [SerializeField] Transform Cursor;
  public Transform CursorSnapped;
  public Transform CursorAutoAim;
  public float CursorScale = 2;
  public float SnapAngleDivide = 8;
  public float SnapCursorDistance = 1;
  public float AutoAimCircleRadius = 1;
  public float AutoAimDistance = 5;
  public float DirectionalMinimum = 0.3f;
  bool CursorAboveMinimumDistance;
  bool CursorShowAuto = true;

  // show aim path
  [SerializeField] LineRenderer lineRenderer;

  public List<WeaponDefinition> weaponDefs;
  public int CurrentWeaponIndex;
  public Weapon weapon;
  public List<Weapon> weapons;

  // charged shots
  ParticleSystem chargeEffect;
  public float chargeMin = 0.3f;
  public float armRadius = 0.3f;
  Timer chargePulse = new Timer();
  Timer chargeStartDelay = new Timer();
  public float chargeDelay = 0.2f;
  public bool chargePulseOn = true;
  public float chargePulseInterval = 0.1f;
  public Color chargeColor = Color.white;
  float chargeStartTime;
  GameObject chargeEffectGO;

  Ability ability;
  public Transform arm;
  public Transform armMount;
  public Transform armMountFront;
  public Transform backMount;
  // transient
  Transform abilityMount;
  CharacterPart AssociatedAbilityPart;

  public int CurrentAbilityIndex;
  [SerializeField] List<Ability> abilities;
  [SerializeField] List<AbilityDefinition> abilityDefs;

  [Header( "Sound" )]
  public AudioSource audio;
  public AudioSource audio2;
  public AudioClip soundJump;
  public AudioClip soundDash;
  public AudioClip soundDamage;
  public AudioClip soundPickup;
  public AudioClip soundDenied;
  public AudioClip soundWeaponFail;


  [Header( "PlayerBiped Damage" )]
  [SerializeField] float damageDuration = 0.5f;
  public bool takingDamage;
  Timer damageTimer = new Timer();
  public Color damagePulseColor = Color.white;
  bool damagePulseOn;
  Timer damagePulseTimer = new Timer();
  public float damagePulseInterval = .1f;
  public float damageBlinkDuration = 1f;
  const float damageLift = 0;
  public float damagePushAmount = 1f;
  [SerializeField] private AnimationCurve damageShakeCurve;
  public ParticleSystem damageSmoke;

  // Collision: cached for optimization
  const int BipedDirectionalRaycastHits = 8;
  int HitLayers;
  RaycastHit2D hitTop;
  RaycastHit2D hitBottom;
  RaycastHit2D hitRight;
  RaycastHit2D hitLeft;

  // TWOWAY PLATFORM
  bool ignoreTwowayPlatform;
  Timer ignoreTwowayPlatformMinimum = new Timer();
  public float ignorePlatformDuration = 0.2f;

  WarpDoor warpDoor;

  // World Selection
  [SerializeField] float selectRange = 3;
  IWorldSelectable closestISelect;
  IWorldSelectable WorldSelection;
  List<Component> pups = new List<Component>();

  public bool IsPlayer { get { return Global.instance.CurrentPlayer == this; } }

  public bool IsBiped { get { return partLegs.enabled; } }

  public bool grapPulling { get { return ability != null && ability is GraphookAbility && ability.IsActive; } }

  public bool jumpStart { get { return input.Jump && !pinput.Jump; } }

  public bool jumpStop { get { return !input.Jump && pinput.Jump; } }

  public bool dashStart { get { return (input.Dash && !pinput.Dash); } }

  public bool dashStop { get { return (!input.Dash && pinput.Dash); } }

  public bool chargeStart { get { return input.Charge && !pinput.Charge; } }

  public bool chargeStop { get { return !input.Charge && pinput.Charge; } }

  public bool abilityStart { get { return input.Ability && !pinput.Ability; } }

  public bool abilityEnd { get { return !input.Ability && pinput.Ability; } }

  // CLIMB
  bool climbing;
  bool climbStart;
  public float climbSpeed = 1;
  float climbTime;
  public float climbAnimSpeed = 1;
  public float climbAnimDuration = 0.12f;
  Timer climbStartTimer = new Timer();
  ClimbableBackground ClimbableBackground;

  string pAnim = "idle";
  public bool OverrideAnimation;
  public string OverrideAnimationName;

  protected override void Awake()
  {
    base.Awake();
    InitializeParts();
    bottomHits = new RaycastHit2D[BipedDirectionalRaycastHits];
    topHits = new RaycastHit2D[BipedDirectionalRaycastHits];
    leftHits = new RaycastHit2D[BipedDirectionalRaycastHits];
    rightHits = new RaycastHit2D[BipedDirectionalRaycastHits];
  }

  protected override void Start()
  {
    HitLayers = Global.TriggerLayers | Global.CharacterDamageLayers;
    spriteRenderers.AddRange( GetComponentsInChildren<SpriteRenderer>() );

    // unpack
    InteractIndicator.SetActive( false );
    InteractIndicator.transform.SetParent( null );
    // acquire abilities serialized into the prefab
    abilities = new List<Ability>( abilityDefs.Count );
    for( int i = 0; i < abilityDefs.Count; i++ )
    {
      Ability alt = abilityDefs[i].GetNewAbility();
      abilities.Add( alt );
      alt.OnAcquire( this );
    }
    weapons = new List<Weapon>( weaponDefs.Count );
    for( int i = 0; i < weaponDefs.Count; i++ )
    {
      Weapon wpn = weaponDefs[i].GetNewWeapon();
      wpn.OnAcquire( this );
      weapons.Add( wpn );
    }
    if( weapons.Count > 0 && CurrentWeaponIndex < weapons.Count )
      AssignWeapon( weapons[CurrentWeaponIndex] );
    if( abilities.Count > 0 && CurrentAbilityIndex < abilities.Count )
      AssignAbility( abilities[CurrentAbilityIndex] );

    if( controller == null && assignedController != null )
    {
      controller = ScriptableObject.Instantiate( assignedController );
      controller.AssignPawn( this );
    }

    if( controller == Global.instance.PlayerController )
    {
      if( Global.instance.HealthAudioPitch )
        UpdateHealthAudio();
      if( Global.instance.HealthTimescale )
        UpdateHealthTimescale();
    }
  }

  public void UpdateHealthAudio()
  {
    float healthNormalized = ((float) Health / (float) MaxHealth);
    Global.instance.MusicPitch( 0.5f + 0.5f * healthNormalized );
  }

  public void UpdateHealthTimescale()
  {
    float healthNormalized = ((float) Health / (float) MaxHealth);
    Global.instance.CurrentTimescale = 0.5f + 0.5f * healthNormalized;
    Time.timeScale = Global.instance.CurrentTimescale;
  }

  public override void OnControllerAssigned()
  {
    // settings are read before player is created, so set player settings here.
    speedFactorNormalized = Global.instance.FloatSetting["PlayerSpeedFactor"].Value;
  }

  public override void PreSceneTransition()
  {
    velocity = Vector2.zero;
    StopCharge();
    InteractIndicator.transform.SetParent( gameObject.transform );
    if( ability != null )
      ability.PreSceneTransition();
    WorldSelection = null;
    closestISelect = null;
  }

  public override void PostSceneTransition()
  {
    InteractIndicator.SetActive( false );
    InteractIndicator.transform.SetParent( null );
    if( ability != null )
      ability.PostSceneTransition();
    // prevent latent state variables from affecting logic (through state variables, such as inertia)
    collideBottom = collideLeft = collideRight = collideTop = false;
  }

  protected override void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    base.OnDestroy();
    damageTimer.Stop( false );
    damagePulseTimer.Stop( false );
    climbStartTimer.Stop( false );
    if( chargeEffect != null )
    {
      audio2.Stop();
      Destroy( chargeEffectGO );
    }
    chargeStartDelay.Stop( false );
    chargePulse.Stop( false );
    if( weapon != null )
      weapon.Deactivate();
    if( ability != null )
      ability.Deactivate();
  }

  bool AddWeapon( WeaponDefinition def )
  {
    Weapon wpn = weapons.Find( x => x.def == def );
    if( wpn != null )
    {
      audio.PlayOneShot( soundDenied );
      return false;
    }
    wpn = def.GetNewWeapon();
    weapons.Add( wpn );
    wpn.OnAcquire( this );
    CurrentWeaponIndex = weapons.Count - 1;
    AssignWeapon( weapons[CurrentWeaponIndex] );
    return true;
  }

  void AssignWeapon( Weapon wpn )
  {
    if( weapon != null )
      weapon.Unequip();
    weapon = wpn;

    Transform weaponMount = transform;
    if( weapon.abilityDef.MountType == Ability.MountType.ARM_BACK )
      weaponMount = armMount;
    if( weapon.abilityDef.MountType == Ability.MountType.ARM_FRONT )
      weaponMount = armMountFront;
    if( weapon.abilityDef.MountType == Ability.MountType.BACK )
      weaponMount = backMount;
    weapon.Equip( weaponMount );

    Global.instance.weaponIcon.sprite = weapon.abilityDef.Icon;
    Cursor.GetComponent<SpriteRenderer>().sprite = weapon.abilityDef.Cursor;
    StopCharge();

    indexedColors.colors[0] = weapon.def.Color0;
    indexedColors.colors[1] = weapon.def.Color1;
    indexedColors.ExplicitUpdate();
  }

  void NextWeapon()
  {
    if( weapons.Count == 0 )
      return;
    CurrentWeaponIndex = (CurrentWeaponIndex + 1) % weapons.Count;
    AssignWeapon( weapons[CurrentWeaponIndex] );
  }

  bool AddAbility( AbilityDefinition def )
  {
    Ability alt = abilities.Find( x => x.abilityDef == def );
    if( alt != null )
    {
      audio.PlayOneShot( soundDenied );
      return false;
    }
    alt = def.GetNewAbility();
    abilities.Add( alt );
    alt.OnAcquire( this );
    CurrentAbilityIndex = abilities.Count - 1;
    AssignAbility( abilities[CurrentAbilityIndex] );
    return true;
  }

  void AssignAbility( Ability alt )
  {
    if( ability != null )
      ability.Unequip();
    ability = alt;
    switch( ability.abilityDef.MountType )
    {
      case Ability.MountType.ARM_BACK:
        abilityMount = armMount;
        AssociatedAbilityPart = partArmBack;
        break;

      case Ability.MountType.ARM_FRONT:
        abilityMount = armMountFront;
        AssociatedAbilityPart = partArmFront;
        break;

      case Ability.MountType.BACK:
        abilityMount = backMount;
        AssociatedAbilityPart = partArmBack;
        break;
    }
    ability.Equip( abilityMount );
    Global.instance.abilityIcon.sprite = ability.abilityDef.Icon;
    if( ability.abilityDef.Cursor != null )
      Cursor.GetComponent<SpriteRenderer>().sprite = ability.abilityDef.Cursor;
  }

  void NextAbility()
  {
    if( abilities.Count == 0 )
      return;
    CurrentAbilityIndex = (CurrentAbilityIndex + 1) % abilities.Count;
    AssignAbility( abilities[CurrentAbilityIndex] );
  }

  public override void UnselectWorldSelection()
  {
    if( WorldSelection != null )
    {
      WorldSelection.Unselect();
      WorldSelection = null;
    }
  }

  new void UpdateHit( float dT )
  {
    ClimbableBackground = null;

    pups.Clear();
    hitCount = Physics2D.BoxCastNonAlloc( pos, box.size + Vector2.up * DownOffset * 2 + Vector2.right * 0.1f, 0,
      velocity, Global.RaycastHits, Mathf.Max( raylength, velocity.magnitude * dT ),
      HitLayers );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      ITrigger tri = hit.transform.GetComponent<ITrigger>();
      if( tri != null )
      {
        tri.Trigger( hit.collider, transform );
      }
      IDamage dam = hit.transform.GetComponent<IDamage>();
      if( dam != null )
      {
        // maybe only damage other if charging weapon or has active powerup?
        if( ContactDamageDefinition != null )
        {
          Damage dmg = new Damage();
          dmg.def = ContactDamageDefinition;
          dmg.instigator = this;
          dmg.damageSource = transform;
          dmg.hit = hit;
          dam.TakeDamage( dmg );
        }
      }
      Pickup pickup = hit.transform.GetComponent<Pickup>();
      if( pickup != null && pickup.SelectOnContact )
      {
        if( pickup.unique != UniquePickupType.None )
        {
          switch( pickup.unique )
          {
            case UniquePickupType.Health:
              AddHealth( pickup.uniqueInt0 );
              break;

            case UniquePickupType.SpeedFactorNormalized:
              speedFactorNormalized += pickup.uniqueFloat0;
              break;
          }
        }
        // this should destroy the pickup
        pickup.Select( this );
      }
      WarpDoor warpd = hit.transform.GetComponent<WarpDoor>();
      if( warpd != null )
        warpDoor = warpd;

      ClimbableBackground = hit.transform.GetComponent<ClimbableBackground>();
    }

    if( IsPlayer )
    {
      pups.Clear();
      hitCount = Physics2D.CircleCastNonAlloc( transform.position, selectRange, Vector3.zero, Global.RaycastHits, 0,
        Global.WorldSelectableLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];
        if( hit.transform.gameObject == null || hit.transform == transform || hit.transform.root == transform.root )
          continue;
        IWorldSelectable pup = hit.transform.GetComponent<IWorldSelectable>();
        if( pup != null )
        {
          pups.Add( (Component) pup );
          /*if( !highlightedPickups.Contains( pup ) )
          {
            pup.Highlight();
            highlightedPickups.Add( pup );
          }*/
        }
      }

      IWorldSelectable closest = (IWorldSelectable) Util.FindSmallestAngle( transform.position, shoot, pups.ToArray() );
      if( closest == null )
      {
        if( closestISelect != null )
        {
          closestISelect.Unhighlight();
          closestISelect = null;
          /*InteractIndicator.SetActive( false );*/
        }
        if( WorldSelection != null )
        {
          WorldSelection.Unselect();
          WorldSelection = null;
        }
      }
      else if( closest != closestISelect )
      {
        if( closestISelect != null )
        {
          closestISelect.Unhighlight();
          /*InteractIndicator.SetActive( false );*/
        }
        closestISelect = closest;
        closestISelect.Highlight();
        // Not everything is indicated the same way
        /*InteractIndicator.SetActive( true );
        InteractIndicator.transform.position = closestISelect.GetPosition();*/
      }
      else
      {
        InteractIndicator.transform.position = closestISelect.GetPosition();
      }
      /*highlightedPickupsRemove.Clear();
      foreach( var pup in highlightedPickups )
        if( !pups.Contains( pup ) )
        {
          pup.Unhighlight();
          highlightedPickupsRemove.Add( pup );
        }
      foreach( var pup in highlightedPickupsRemove )
        highlightedPickups.Remove( pup );
      */
    }
  }

  public void UpdateBipedCollision()
  {
    collideRight = false;
    collideLeft = false;
    collideTop = false;
    collideBottom = false;
    adjust = pos;

    bottomHitCount = 0;
    topHitCount = 0;
    rightHitCount = 0;
    leftHitCount = 0;
    /*
        Vector2 rightFoot;
        Vector2 leftFoot;
        // Avoid the (box-to-box) standing-on-a-corner-and-moving-means-momentarily-not-on-ground issue by 'sampling' the ground at multiple points
        RaycastHit2D right = Physics2D.Raycast( adjust + Vector2.right * box.size.x, Vector2.down, Mathf.Max( down, -velocity.y * dT ), Global.CharacterCollideLayers );
        RaycastHit2D left = Physics2D.Raycast( adjust + Vector2.left * box.size.x, Vector2.down, Mathf.Max( down, -velocity.y * dT ), Global.CharacterCollideLayers );
        if( right.transform != null )
          rightFoot = right.point;
        else
          rightFoot = adjust + ( Vector2.right * box.size.x ) + ( Vector2.down * Mathf.Max( raylength, -velocity.y * dT ) );
        if( left.transform != null )
          leftFoot = left.point;
        else
          leftFoot = adjust + ( Vector2.left * box.size.x ) + ( Vector2.down * Mathf.Max( raylength, -velocity.y * dT ) );
    
        if( right.transform != null || left.transform != null )
        {
          Vector2 across = rightFoot - leftFoot;
          Vector3 sloped = Vector3.Cross( across.normalized, Vector3.back );
          if( sloped.y > corner )
          {
            collideBottom = true;
            adjust.y = ( leftFoot + across * 0.5f ).y + downOffset;
            hitBottomNormal = sloped;
          }
        }
    
        if( left.transform != null )
          Popcron.Gizmos.Line( adjust + Vector2.left * box.size.x, leftFoot, Color.green );
        else
          Popcron.Gizmos.Line( adjust + Vector2.left * box.size.x, leftFoot, Color.grey );
        if( right.transform != null )
          Popcron.Gizmos.Line( adjust + Vector2.right * box.size.x, rightFoot, Color.green );
        else
          Popcron.Gizmos.Line( adjust + Vector2.right * box.size.x, rightFoot, Color.grey );
    */

    // BOTTOM
    // slidingOffsetTarget = 1;
    float down = Mathf.Max( velocity.y > 0 ? raydown - DownOffset : raydown, -velocity.y * Time.deltaTime );

    Bounds bounds = new Bounds( adjust + Vector2.down * down * 0.5f, box.size + Vector2.up * Mathf.Max( down, -velocity.y * Time.deltaTime ) );
    Gizmos.Square( adjust, box.size, Color.blue );
    Gizmos.Square( adjust + Vector2.down * down, box.size , Color.cyan );
    Gizmos.Line( new Vector3( bounds.min.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.max.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.min.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.max.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.max.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.min.y ), Color.red );
    Gizmos.Line( new Vector3( bounds.max.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.min.y ), Color.red );

    int twowayLayer = LayerMask.NameToLayer( "twowayPlatform" );
    int downMask = Global.CharacterCollideLayers;
    if( ignoreTwowayPlatform || flying )
      downMask = Global.CharacterCollideLayers & ~LayerMask.GetMask( "twowayPlatform" );

    float prefer = float.MinValue;
    // todo reproduce and fix the exception that occurs after death
    bottomHitCount = Physics2D.BoxCastNonAlloc( adjust, box.size , transform.rotation.eulerAngles.z, -(Vector2) transform.up, bottomHits, down, downMask );
    for( int i = 0; i < bottomHitCount; i++ )
    {
      hit = bottomHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      if( hit.normal.y > collisionCornerBottom && hit.point.y > prefer && ((twowayLayer != hit.transform.gameObject.layer) || hit.point.y < transform.position.y - box.size.y * 0.5f) )
      {
        prefer = hit.point.y;
        collideBottom = true;
        hitBottom = hit;
        velocity.y = Mathf.Max( velocity.y, 0 );
        /*
                // sliding offset. This moves the player downward a little bit on slopes to
                // close the gap created by the corners of the box during boxcast.
                if( Mathf.Abs( hit.normal.x ) > 0 )
                {
                  slidingOffsetTarget = 1 - Mathf.Abs( hit.normal.x ) / hit.normal.y;
                  slidingOffset = Mathf.MoveTowards( slidingOffset, slidingOffsetTarget, slidingOffsetRate * dT );
                }
                else
                {
                  slidingOffset = 1;
                }
                adjust.y = hit.point.y + box.size.y * 0.5f + slidingOffset * downOffset;
        */
        adjust.y = hit.point.y + box.size.y * 0.5f + DownOffset;

        // moving platforms
        Entity cha = hit.transform.GetComponent<Entity>();
        if( cha != null )
          carryEntity = cha;

        ConveyorBelt belt = hit.transform.GetComponent<ConveyorBelt>();
        if( belt != null && belt.enabled )
        {
          carryConveyorBelt = belt;
          if( carryConveyorBelt.CW )
            conveyorMove = new Vector2( hit.normal.y, -hit.normal.x ) * carryConveyorBelt.Speed;
          else
            conveyorMove = new Vector2( -hit.normal.y, hit.normal.x ) * carryConveyorBelt.Speed;
        }
      }
    }

    // TOP
    topHitCount = Physics2D.BoxCastNonAlloc( adjust, box.size, 0, Vector2.up, topHits, Mathf.Max( raylengthUP, velocity.y * Time.deltaTime ), Global.CharacterCollideLayersNoTwoWayPlatform );
    for( int i = 0; i < topHitCount; i++ )
    {
      hit = topHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      if( hit.normal.y < collisionCornerTop )
      {
        collideTop = true;
        hitTop = hit;
        velocity.y = Mathf.Min( velocity.y, 0 );
        adjust.y = hit.point.y - box.size.y * 0.5f - HeadOffset;

        // moving platforms
        /*Entity cha = hit.transform.GetComponent<Entity>();
        if( cha != null )
          carryEntity = cha;*/
      }
    }
    
    // LEFT
    // Prefer more-horizontal walls for wall sliding. Start beyond normal range.
    prefer = 2;
    leftHitCount = Physics2D.BoxCastNonAlloc( adjust, box.size, 0, Vector2.left, leftHits, Mathf.Max( raylength, -velocity.x * Time.deltaTime ), Global.CharacterCollideLayersNoTwoWayPlatform );
    for( int i = 0; i < leftHitCount; i++ )
    {
      hit = leftHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      if( hit.normal.x > collisionCornerSide && hit.normal.x < prefer )
      {
        prefer = hit.normal.x;
        collideLeft = true;
        hitLeft = hit;

        velocity.x = Mathf.Max( velocity.x, 0 );
        adjust.x = hit.point.x + box.size.x * 0.5f + contactSeparation;
        wallSlideTargetNormal = hit.normal;
        // prevent clipping through angled walls when falling fast.
        // velocity.y -= Util.Project2D( velocity, hit.normal ).y;
        velocity -= Util.Project2D( velocity.normalized, hit.normal ) * velocity.magnitude;

        // moving platforms
        Entity cha = hit.transform.GetComponent<Entity>();
        if( cha != null )
          carryEntity = cha;
      }
    }

    // RIGHT
    prefer = -2;
    rightHitCount = Physics2D.BoxCastNonAlloc( adjust, box.size, 0, Vector2.right, rightHits, Mathf.Max( raylength, velocity.x * Time.deltaTime ), Global.CharacterCollideLayersNoTwoWayPlatform );
    for( int i = 0; i < rightHitCount; i++ )
    {
      hit = rightHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      if( hit.normal.x < -collisionCornerSide && hit.normal.x > prefer )
      {
        prefer = hit.normal.x;
        collideRight = true;
        hitRight = hit;

        velocity.x = Mathf.Min( velocity.x, 0 );
        adjust.x = hit.point.x - box.size.x * 0.5f - contactSeparation;
        wallSlideTargetNormal = hit.normal;
        // prevent clipping through angled walls when falling fast.
        // velocity.y -= Util.Project2D( velocity, hit.normal ).y;
        velocity -= Util.Project2D( velocity.normalized, hit.normal ) * velocity.magnitude;

        // moving platforms
        Entity cha = hit.transform.GetComponent<Entity>();
        if( cha != null )
          carryEntity = cha;
      }
    }

    // Detect pinch point below box and prevent entity from tunneling downward.
    // Normally, this would be handled by downward raycast, but the angles must be too steep.
    // DO THIS BEFORE DetectCrushDamage()
    if( collideLeft && collideRight && hitRight.point.x - hitLeft.point.x < box.size.x &&
      hitRight.normal.y > 0 && hitRight.normal.y < collisionCornerBottom &&
      hitLeft.normal.y > 0 && hitLeft.normal.y < collisionCornerBottom )
    {
      collideBottom = true;
      collideLeft = false;
      collideRight = false;
      velocity.y = Mathf.Max( velocity.y, 0 );
      adjust.y = (hitRight.point.y + hitLeft.point.y) * 0.5f + box.size.y * 0.5f + DownOffset;
    }

    pos = adjust;
  }

  public override Vector2 GetShotOriginPosition() { return (Vector2) arm.position + shoot.normalized * armRadius + Velocity * Time.deltaTime; }

  public override Vector2 GetAimVector() { return shoot; }

  void ShootCharged()
  {
    if( weapon == null || weapon.def.ChargeVariant == null )
    {
      StopCharge();
      return;
    }
    // HACK chargeEffect is used to indicate charged state
    if( chargeEffect != null )
    {
      audio.Stop();
      if( (Time.time - chargeStartTime) > chargeMin )
        weapon.FireChargeVariant( this, GetShotOriginPosition(), shoot );
    }
    StopCharge();
  }

  void UpdateCursor()
  {
    // AimPosition is where the projectile is going.
    // CursorWorldPosition is where the cursor is. 
    // When auto-aim or aim-snapping is on, these positions differ.
    Vector2 AimPosition = Vector2.zero;
    Vector2 cursorOrigin = transform.position;
    inputAim = input.Aim;

    CursorAboveMinimumDistance = inputAim.magnitude > DirectionalMinimum;
    if( CursorShowAuto )
      Cursor.gameObject.SetActive( CursorAboveMinimumDistance );

    if( Global.instance.AimSnap )
    {
      if( CursorAboveMinimumDistance )
      {
        // set cursor
        CursorSnapped.gameObject.SetActive( true );
        float angle = Mathf.Atan2( inputAim.x, inputAim.y ) / Mathf.PI;
        float snap = Mathf.Round( angle * SnapAngleDivide ) / SnapAngleDivide;
        Vector2 snapped = new Vector2( Mathf.Sin( snap * Mathf.PI ), Mathf.Cos( snap * Mathf.PI ) );
        AimPosition = cursorOrigin + snapped * SnapCursorDistance;
        CursorSnapped.position = AimPosition;
        CursorSnapped.rotation = Quaternion.LookRotation( Vector3.forward, snapped );
      }
      else
      {
        CursorSnapped.gameObject.SetActive( false );
      }
    }
    else if( Global.instance.AutoAim )
    {
      RaycastHit2D[] hits = Physics2D.CircleCastAll( transform.position, AutoAimCircleRadius, inputAim, AutoAimDistance,
        LayerMask.GetMask( new string[] {"enemy"} ) );
      float distance = Mathf.Infinity;
      Transform closest = null;
      foreach( var hit in hits )
      {
        float dist = Vector2.Distance( cursorOrigin + inputAim, hit.transform.position );
        if( dist < distance )
        {
          closest = hit.transform;
          distance = dist;
        }
      }

      if( closest == null )
      {
        CursorAutoAim.gameObject.SetActive( false );
      }
      else
      {
        CursorAutoAim.gameObject.SetActive( true );
        // todo adjust for flight path
        AimPosition = closest.position;
        CursorAutoAim.position = AimPosition;
      }
    }
    else
    {
      CursorAutoAim.gameObject.SetActive( false );
      CursorSnapped.gameObject.SetActive( false );
      AimPosition = cursorOrigin + inputAim;
    }

    if( inputAim.sqrMagnitude < 0.005f )
      shoot = facingRight ? Vector2.right : Vector2.left;
    else
      shoot = AimPosition - (Vector2) arm.position;
  }

  public override void EntityUpdate()
  {
    if( dead || Health <= 0 || victorySting )
      return;

    bool previousGround = onGround;
    onGround = collideBottom || (collideLeft && hitLeft.normal.y > 0 && collideRight && hitRight.normal.y > 0);
    if( onGround && !previousGround )
    {
      landing = true;
      landTimer.Start( this, landDuration, null, delegate { landing = false; } );
    }

    if( wallsliding )
    {
      float angle = Vector2.Angle( previousWallSlideTargetNormal, wallSlideTargetNormal );
      if( previousWallsliding && angle < wallSlideHardAngleThreshold )
        wallSlideNormal =
          Vector2.MoveTowards( wallSlideNormal, wallSlideTargetNormal, wallSlideRotateSpeed * Time.deltaTime );
      else
        wallSlideNormal = wallSlideTargetNormal;
    }
    previousWallsliding = wallsliding;
    previousWallSlideTargetNormal = wallSlideTargetNormal;
    wallsliding = false;

    if( input.MoveDown )
    {
      hanging = false;
      if( !ignoreTwowayPlatform )
      {
        ignoreTwowayPlatform = true;
        // ensure a minimum time that platforms are ignore after pressing down
        ignoreTwowayPlatformMinimum.Start( this, ignorePlatformDuration );
      }
      /*if( onGround )
        crouching = true;*/
    }

    // stop the ignore timer after releasing down button, to prevent accidently falling through multiple platforms
    if( ignoreTwowayPlatform && !ignoreTwowayPlatformMinimum.IsActive && !input.MoveDown )
      ignoreTwowayPlatform = false;

    // WarpDoor
    if( warpDoor != null && input.MoveDown && !pinput.MoveDown )
    {
      warpDoor.EnterDoor( new Entity[] {this} );
      input.Jump = false;
      new Timer( this, Global.instance.WarpDoorTransitionDuration, timer => input.Jump = false, null );
    }
    warpDoor = null;

    if( input.Interact )
    {
      if( WorldSelection != null && WorldSelection != closestISelect )
        WorldSelection.Unselect();
      WorldSelection = closestISelect;
      if( WorldSelection != null )
      {
        WorldSelection.Select( this );
        if( WorldSelection is Pickup )
        {
          Pickup pickup = (Pickup) WorldSelection;
          if( pickup.weaponDefinition != null )
          {
            if( AddWeapon( pickup.weaponDefinition ) )
              pickup.Select( this );
          }
          else if( pickup.abilityDefinition != null )
          {
            if( AddAbility( pickup.abilityDefinition ) )
              pickup.Select( this );
          }
        }
      }
    }

    if( partLegs.enabled )
    {
      if( takingDamage )
      {
        velocity.y = 0;
      }
      else
      {
        if( input.MoveRight && input.MoveLeft )
        {
          velocity.x = 0;
        }
        else if( input.MoveRight )/*&& !crouching )*/
        {
          facingRight = true;
          // move along floor if angled downwards
          Vector3 hitnormalCross = Vector3.Cross( hitBottom.normal, Vector3.forward );
          if( onGround && hitnormalCross.y < 0 )
            // add a small downward vector for curved surfaces
            velocity = hitnormalCross * moveSpeed + Vector3.down * downslopefudge;
          else
            velocity.x = moveSpeed;
          if( !facingRight && onGround )
            StopDash();
        }
        else if( input.MoveLeft )/*&& !crouching )*/
        {
          facingRight = false;
          // move along floor if angled downwards
          Vector3 hitnormalCross = Vector3.Cross( hitBottom.normal, Vector3.back );
          if( onGround && hitnormalCross.y < 0 )
            // add a small downward vector for curved surfaces
            velocity = hitnormalCross * moveSpeed + Vector3.down * downslopefudge;
          else
            velocity.x = -moveSpeed;
          if( facingRight && onGround )
            StopDash();
        }
        else
        {
          // must have input (or inertia) to move horizontally, so allow no persistent horizontal velocity (without inertia)
          velocity.x = 0;
        }

        if( dashStart && (onGround || collideLeft || collideRight) )
          StartDash();

        if( dashStop && !jumping )
          StopDash();

        if( dashing )
        {
          if( onGround && !previousGround )
          {
            StopDash();
          }
          else if( facingRight )
          {
            if( onGround || input.MoveRight )
              velocity.x = dashSpeed;
            if( (onGround || collideRight) && !dashTimer.IsActive )
              StopDash();
          }
          else
          {
            if( onGround || input.MoveLeft )
              velocity.x = -dashSpeed;
            if( (onGround || collideLeft) && !dashTimer.IsActive )
              StopDash();
          }
        }

        bool autoWallslide = Global.instance.BoolSetting["AutoWallslide"].Value;
        if( input.MoveRight )
          lastMoveWasRight = true;
        if( input.MoveLeft )
          lastMoveWasRight = false;

        if( onGround && jumpStart )
          StartJump();
        else if( (jumping || walljumping) && jumpStop )
          StopJump();
        else if( collideRight && hitRight.normal.y >= 0 &&
          ((autoWallslide && (!collideLeft || lastMoveWasRight)) || (!autoWallslide && input.MoveRight)) )
        {
          if( jumpStart && input.MoveRight )
          {
            walljumping = true;
            //velocity.y = jumpSpeed;
            OverrideVelocity( new Vector2( -(input.Dash ? dashSpeed : wallJumpPushVelocity), jumpSpeed ), wallJumpPushDuration );
            jumpRepeatTimer.Start( this, jumpRepeatInterval );
            walljumpTimer.Start( this, wallJumpPushDuration, null, delegate { walljumping = false; } );
            audio.PlayOneShot( soundJump );
            Instantiate( walljumpEffect, transform.position + new Vector3( 0.209f, -0.227f, 0 ), Quaternion.identity );
          }
          else if( !jumping && !walljumping && !climbing && !onGround && velocity.y <= 0 )
          {
            facingRight = false;
            Wallslide();
          }
        }
        else if( collideLeft && hitLeft.normal.y >= 0 &&
          ((autoWallslide && (!collideRight || !lastMoveWasRight)) || (!autoWallslide && input.MoveLeft)) )
        {
          if( jumpStart && input.MoveLeft )
          {
            walljumping = true;
            // velocity.y = jumpSpeed;
            OverrideVelocity( new Vector2( (input.Dash ? dashSpeed : wallJumpPushVelocity), jumpSpeed ), wallJumpPushDuration );
            jumpRepeatTimer.Start( this, jumpRepeatInterval );
            walljumpTimer.Start( this, wallJumpPushDuration, null, delegate { walljumping = false; } );
            audio.PlayOneShot( soundJump );
            Instantiate( walljumpEffect, transform.position + new Vector3( -0.209f, -0.227f, 0 ), Quaternion.identity );
          }
          else if( !jumping && !walljumping && !climbing && !onGround && velocity.y <= 0 )
          {
            facingRight = true;
            Wallslide();
          }
        }
      }

      if( wallsliding && input.MoveDown )
      {
        Vector2 wsvel;
        wsvel = -(Vector2) transform.up * wallSlideDownY;
        /*wsvel = Util.Project2D( -(Vector2) transform.up, wallSlideNormal.x > 0 ? Vector2.right : Vector2.left ) * wallSlideDownX + -(Vector2) transform.up * wallSlideDownY;*/
        Popcron.Gizmos.Line( (Vector2) transform.position, (Vector2) transform.position + wsvel, Color.yellow );
        velocity += wsvel;
      }

      if( velocity.y < 0 )
      {
        jumping = false;
        walljumping = false;
        walljumpTimer.Stop( false );
      }

      if( !((onGround && dashing) || wallsliding) )
        dashSmoke.Stop();

      if( ClimbableBackground != null )
      {
        if( !onGround && input.MoveUp && !pinput.MoveUp || input.MoveDown && !pinput.MoveDown )
        {
          if( !climbing )
          {
            climbStart = true;
            if( ClimbableBackground.SnapToPath )
            {
              velocity.x = 0;
              transform.position = (Vector2) ClimbableBackground.transform.position + Util.FindClosestPointOnPath( transform.position - ClimbableBackground.transform.position, ClimbableBackground.localPath );
            }
          }
          climbing = true;
          climbStartTimer.Start( this, 2f / 60f, null, () => { climbStart = false; } );
        }

        if( climbing )
        {
          if( input.MoveUp )
          {
            velocity.y = climbSpeed;
            climbTime = Mathf.Repeat( climbTime + climbAnimSpeed * Time.deltaTime, climbAnimDuration );
          }
          else if( input.MoveDown )
          {
            velocity.y = -climbSpeed;
            climbTime = Mathf.Repeat( climbTime - climbAnimSpeed * Time.deltaTime, climbAnimDuration );
          }
          else
            velocity.y = 0;

          // NOTE: JumpStop() sets vertical velocity to zero, but only for upward motion
          if( pinput.MoveDown && !input.MoveDown )
            velocity.y = 0;

          if( ClimbableBackground.SnapToPath )
          {
            velocity.x = 0;
            Vector2 nearPoint = (Vector2) ClimbableBackground.transform.position + Util.FindClosestPointOnPath( transform.position - ClimbableBackground.transform.position, ClimbableBackground.localPath );
            if( Vector2.Distance( nearPoint, transform.position ) > 0.0156f )
              climbing = false;
          }
        }
      }
      else
      {
        climbTime = 0;
      }

      // add gravity before velocity limits
      if( UseGravity && !climbing )
        velocity.y -= Global.Gravity * Time.deltaTime;

      if( overrideVelocityTimer.IsActive )
        velocity = overrideVelocity;

      // "hanging" means immobile
      if( hanging )
        velocity = Vector3.zero;

      if( !takingDamage )
      {
        // WEAPONS / ABILITIES
        if( weapon != null )
        {
          if( input.Fire && (weapon.def.HoldFullAuto || !pinput.Fire) )
          {
#if UNITY_EDITOR
            // assign the pawn each time so we can drop in new weapons in the editor at runtime for testing
            weapon.owner = this;
#endif
            // play sound to indicate weapon will not fire starting within a wall.
            if( Physics2D.Linecast( transform.position, GetShotOriginPosition(), Global.ProjectileNoShootLayers ) )
              Global.instance.AudioOneShot( Global.instance.SoundWeaponFail, transform.position );
            // fire weapon either way because the player may be pinched between a wall and an entity.
            weapon.Activate( GetShotOriginPosition(), shoot );
          }

          // keep the "aiming arm" aimed at the cursor
          if( input.Fire || weapon.def.weaponCategory == WeaponDefinition.WeaponCategory.Melee )
            fireArmTimer.Start( this, weapon.abilityDef.FireArmTimeout );

          weapon.UpdateAbility();

          if( weapon.def.HasChargeVariant )
          {
            if( chargeStart )
              StartCharge();
            if( chargeStop )
              ShootCharged();
          }

          if( pinput.Fire && !input.Fire )
            weapon.Deactivate();
        }

        if( ability != null )
        {
          if( abilityStart )
            ability.Activate( GetShotOriginPosition(), shoot );

          if( ability.abilityDef.MountType == Ability.MountType.ARM_BACK )
            fireArmTimer.Start( this, ability.abilityDef.FireArmTimeout );

          ability.UpdateAbility();

          if( abilityEnd )
            ability.Deactivate();
        }

        if( input.NextWeapon )
          NextWeapon();

        if( input.NextAbility )
          NextAbility();
      }
      if( collideRight )
      {
        velocity.x = Mathf.Min( velocity.x, 0 );
        inertia.x = 0;
        inertia.y -= inertia.y * Mathf.Min( 1, friction * Time.deltaTime );
      }

      if( collideLeft )
      {
        velocity.x = Mathf.Max( velocity.x, 0 );
        inertia.x = 0;
        inertia.y -= inertia.y * Mathf.Min( 1, friction * Time.deltaTime );
      }

      if( onGround && !climbing )
      {
        velocity.y = Mathf.Max( velocity.y, 0 );
        inertia = Vector2.zero;
      }

      if( collideTop )
      {
        velocity.y = Mathf.Min( velocity.y, 0 );
        inertia.x -= inertia.x * Mathf.Min( 1, friction * Time.deltaTime );
        inertia.y = 0;
      }
      velocity.x = Mathf.Max( velocity.x, -Global.MaxVelocity );
      velocity.x = Mathf.Min( velocity.x, Global.MaxVelocity );
      velocity.y = Mathf.Max( velocity.y, -Global.MaxVelocity );
      velocity.y = Mathf.Min( velocity.y, Global.MaxVelocity );

      float DT = Time.deltaTime;
      // value is adjusted later by collision
      pos = transform.position + (Vector3) Velocity * DT;
      Entity previousCarry = carryEntity;
      carryEntity = null;
      carryConveyorBelt = null;
      // update collision flags, and adjust position
      if( !climbing )
        UpdateBipedCollision();
      transform.position = pos;
      // preserve inertia when jumping from moving platforms
      if( previousCarry != null && carryEntity == null )
        inertia = previousCarry.Velocity;

      /*if( collideBottom && collideTop )
        crouching = true;
      else
        crouching = false;*/
      
      // Update crush detection after UpdateCollision in this frame to guarantee that
      // the hits are still valid, and still reference alive objects.
      if( !damageGraceActive )
        DetectCrushDamage();

      UpdateHit( DT );
      UpdateCursor();

      string anim = "idle";
      if( OverrideAnimation )
        anim = OverrideAnimationName;
      else if( takingDamage )
        anim = "damage";
      else if( walljumping )
        anim = "walljump";
      else if( jumping )
        anim = "jump";
      else if( wallsliding )
        anim = "wallslide";
      else if( climbing )
      {
        if( climbStart )
          anim = "climb_start";
        else
          anim = "climb";
      }
      else if( onGround )
      {
        if( dashing )
          anim = "dash";
        else if( (input.MoveRight || input.MoveLeft) && Mathf.Abs( velocity.x ) > 0 )
          anim = "run";
        else if( landing )
          anim = "land";
        /*else if( crouching )
          anim = "crouch";*/
        else
        {
          anim = "idle";
          // when idling, always face aim direction
          float face = inputAim.x;
          if( Mathf.Abs( face ) > 0.005f )
            facingRight = face > 0;
        }
      }
      else if( !jumping )
      {
        if( flying )
          anim = "propellerfly";
        else
          anim = "fall";
      }

      // todo Animator.StringToHash
      if( !anim.Equals( pAnim ) )
        Play( anim );
      pAnim = anim;
      if( climbing )
      {
        if( partLegs.animator.GetCurrentAnimatorStateInfo( 0 ).IsName( "climb" ) )
          partLegs.animator.SetFloat( "climbTime", climbTime );
      }
      transform.localScale = (new Vector3( facingRight ? 1 : -1, 1, 1 ));
      renderer.material.SetInt( "_FlipX", facingRight ? 0 : 1 );

      if( wallsliding && wallSlideNormal.x < 0 )
        transform.rotation =
          Quaternion.LookRotation( Vector3.forward, new Vector3( wallSlideNormal.y, -wallSlideNormal.x ) );
      else if( wallsliding && wallSlideNormal.x > 0 )
        transform.rotation =
          Quaternion.LookRotation( Vector3.forward, new Vector3( -wallSlideNormal.y, wallSlideNormal.x ) );
      else if( rotating )
        transform.rotation = Quaternion.Euler( 0, 0,
          Mathf.MoveTowardsAngle( transform.rotation.eulerAngles.z, rotateTarget, rotateSpeed * Time.deltaTime ) );
      else
        transform.rotation = Quaternion.Euler( 0, 0, 0 );

      if( !ResetInputOnLateUpdate )
        ResetInput();

#if debugdraw || true
      Popcron.Gizmos.Line( (Vector2) transform.position, (Vector2) transform.position + velocity, Color.magenta );
#endif
    }
  }

  public override void EntityLateUpdate()
  {
    if( !Global.instance.Updating || dead || victorySting )
      return;

    // sprite layers
    UpdateParts();
    if( ability != null && ability.abilityDef.MountType != Ability.MountType.NONE )
      ability.RenderLayerLateUpdate( CharacterLayer, AssociatedAbilityPart.layerAnimated );

    // must set the sprite in LateUpdate, after the animator has updated.
    if( weapon != null )
      weapon.UpdateAbilityLate();
    if( ability != null )
      ability.UpdateAbilityLate();

    Cursor.position = transform.position + (Vector3) inputAim;
    if( weapon != null && weapon.def.MountType == Ability.MountType.ARM_BACK && (!weapon.abilityDef.FireArmAlwaysActive && !fireArmTimer.IsActive) )
    {
      arm.localScale = Vector3.one;
      arm.rotation = Quaternion.identity;
    }

    if( weapon != null && Global.instance.ShowAimPath && CursorAboveMinimumDistance )
    {
      lineRenderer.enabled = true;
      Vector3[] trail = weapon.GetTrajectory( GetShotOriginPosition(), shoot );
      lineRenderer.positionCount = trail.Length;
      lineRenderer.SetPositions( trail );
    }
    else
    {
      lineRenderer.enabled = false;
    }

    if( ResetInputOnLateUpdate )
      ResetInput();
  }

  void Wallslide()
  {
    wallsliding = true;
    // subtract gravity here, because gravity is added after this call is made
    velocity.y += Global.Gravity * Time.deltaTime * wallSlideFactorGravity +
      Mathf.Min( -velocity.y, -velocity.y * wallSlideFriction * Time.deltaTime );
    dashSmoke.transform.localPosition = new Vector3( -0.2f, -0.2f, 0 );
    if( !dashSmoke.isPlaying )
      dashSmoke.Play();
  }

  void StartJump()
  {
    jumping = true;
    jumpTimer.Start( this, jumpDuration, null, StopJump );
    jumpRepeatTimer.Start( this, jumpRepeatInterval );
    velocity.y = jumpSpeed;
    audio.PlayOneShot( soundJump );
    dashSmoke.Stop();
  }

  void StopJump()
  {
    jumping = false;
    // air brakes
    velocity.y = Mathf.Min( velocity.y, 0 );
    jumpTimer.Stop( false );
  }

  void StartDash()
  {
    if( !dashing )
    {
      dashing = true;
      dashTimer.Start( this, dashDuration );
      if( onGround )
        audio.PlayOneShot( soundDash, 0.5f );
      dashSmoke.transform.localPosition = new Vector3( -0.38f, -0.22f, 0 );
      dashSmoke.Play();
      if( onGround )
      {
        GameObject go = Instantiate( dashflashPrefab,
          transform.position + new Vector3( facingRight ? -0.25f : 0.25f, -0.25f, 0 ), Quaternion.identity );
        go.transform.localScale = facingRight ? Vector3.one : new Vector3( -1, 1, 1 );
      }
    }
  }

  void StopDash()
  {
    dashing = false;
    dashSmoke.Stop();
    dashTimer.Stop( false );
  }

  void StartCharge()
  {
    if( weapon == null )
      return;
    if( weapon.def.ChargeVariant != null )
    {
      chargeStartDelay.Start( this, chargeDelay, null, delegate
      {
        audio.clip = weapon.def.soundCharge;
        audio.loop = false;
        audio.Play();
        audio2.clip = weapon.def.soundChargeLoop;
        audio2.loop = true;
        audio2.PlayScheduled( AudioSettings.dspTime + weapon.def.soundCharge.length );
        foreach( var sr in spriteRenderers )
          // todo Shader.PropertyToID
          sr.material.SetColor( "_FlashColor", chargeColor );
        ChargePulseFlip();
        if( chargeEffectGO != null )
          Destroy( chargeEffectGO );
        chargeEffectGO = Instantiate( weapon.def.ChargeEffect, transform );
        chargeEffect = chargeEffectGO.GetComponent<ParticleSystem>();
        chargeStartTime = Time.time;
      } );
    }
  }

  void StopCharge()
  {
    if( chargeEffect != null )
    {
      audio.Stop();
      audio2.Stop();
      Destroy( chargeEffectGO );
    }
    chargeStartDelay.Stop( false );
    chargePulse.Stop( false );
    foreach( var sr in spriteRenderers )
      sr.material.SetFloat( "_FlashAmount", 0 );
    chargeEffect = null;
  }

  void ChargePulseFlip()
  {
    chargePulse.Start( this, chargePulseInterval, null, delegate
    {
      chargePulseOn = !chargePulseOn;
      if( chargePulseOn )
        foreach( var sr in spriteRenderers )
          sr.material.SetFloat( "_FlashAmount", 1 );
      else
        foreach( var sr in spriteRenderers )
          sr.material.SetFloat( "_FlashAmount", 0 );
      ChargePulseFlip();
    } );
  }

  void DamagePulseFlip()
  {
    damagePulseTimer.Start( this, damagePulseInterval, null, delegate
    {
      damagePulseOn = !damagePulseOn;
      if( damagePulseOn )
        foreach( var sr in spriteRenderers )
          sr.enabled = true;
      else
        foreach( var sr in spriteRenderers )
          sr.enabled = false;
      DamagePulseFlip();
    } );
  }

#if false
  public void ScaleChange( float scale )
  {
    Scale *= scale;
    
    DownOffset = 0.1f * Scale;
    raylength = 0.05f * Scale;
    contactSeparation = 0.01f * Scale;
    SetAnimatorSpeed( 1f + (1f - Scale) * MoveScale );
    
    Health = MaxHealth;
    dashSmoke.transform.localScale = Vector3.one * Scale;
    damageSmoke.transform.localScale = Vector3.one * Scale;
    Global.instance.CameraController.orthoTarget = OrthoScale.Evaluate( Scale );
    // HACK hardcoded value
    Global.instance.CameraController.yHalfWidth = 0.5f * Scale; 
    // damage smoke
    ParticleSystem.MainModule damageSmokeMain = damageSmoke.main;
    ParticleSystem.MinMaxCurve rateCurve = damageSmokeMain.startSpeed;
    // HACK hardcoded value
    rateCurve.constant = 0.5f * Scale;
    damageSmokeMain.startSpeed = rateCurve;
    
    // dash effect
    // no jump
    // scale debris
    // projectile speed
  }
#endif

  public override GameObject[] GetDeathSpawnObjects()
  {
    List<GameObject> gos = new List<GameObject>();
    foreach( var wpn in weapons )
      if( wpn.abilityDef.PickupPrefab != null )
        gos.Add( wpn.abilityDef.PickupPrefab.gameObject );

    if( SpawnOnDeath.Length > 0 )
    {
      int spawnChanceWeightTotal = 0;
      for( int i = 0; i < SpawnOnDeath.Length; i++ )
        spawnChanceWeightTotal += SpawnOnDeath[i].weight;
      int index = 0;
      int randy = Random.Range( 0, spawnChanceWeightTotal + 1 );
      int runningTotal = 0;
      for( index = 0; index < SpawnOnDeath.Length; index++ )
      {
        if( SpawnOnDeath[index].alwaysSpawn )
        {
          if( SpawnOnDeath[index].prefab != null )
            gos.Add( SpawnOnDeath[index].prefab );
          continue;
        }
        runningTotal += SpawnOnDeath[index].weight;
        if( runningTotal > randy )
        {
          if( SpawnOnDeath[index].prefab != null )
            gos.Add( SpawnOnDeath[index].prefab );
          break;
        }
      }
    }
    return gos.ToArray();
  }

  public override void Die( Damage damage )
  {
    GameObject[] prefab = GetDeathSpawnObjects();
    for( int i = 0; i < prefab.Length; i++ )
      Instantiate( prefab[i], transform.position, Quaternion.identity );

    Global.instance.AudioOneShot( soundDeath, transform.position );

    // RESPAWN
    controller.RemovePawn();
    Destroy( gameObject );
    if( controller is PlayerController )
      new Timer( Global.instance, 5, null, Global.instance.SpawnPlayer );
  }

  public override void AddHealth( int amount )
  {
    base.AddHealth( amount );
    if( Health >= MaxHealth )
    {
      damageSmoke.Stop();
    }
    else
    {
      damageSmoke.gameObject.SetActive( true );
      damageSmoke.Play();
      ParticleSystem.EmissionModule damageSmokeEmission = damageSmoke.emission;
      ParticleSystem.MinMaxCurve rateCurve = damageSmokeEmission.rateOverTime;
      rateCurve.constant = 20.0f - 20.0f * ((float) Health / (float) MaxHealth);
      damageSmokeEmission.rateOverTime = rateCurve;
    }

    if( controller == Global.instance.PlayerController )
    {
      if( Global.instance.HealthAudioPitch )
        UpdateHealthAudio();
      if( Global.instance.HealthTimescale )
        UpdateHealthTimescale();
    }
  }

  public override void TakeDamage( Damage damage )
  {
    if( damageGraceActive )
      return;
    AddHealth( -damage.def.amount );
    if( Health <= 0 )
    {
      flashTimer.Stop( false );
      Die( damage );
      return;
    }
    StopCharge();
    //partHead.transform.localScale = Vector3.one * (1 + (Health / MaxHealth) * 10);
    audio.PlayOneShot( soundDamage );
    if( controller == Global.instance.PlayerController )
    {
      CameraShake shaker = Global.instance.CameraController.GetComponent<CameraShake>();
      shaker.amplitude = 0.3f;
      shaker.duration = 0.5f;
      shaker.rate = 100;
      shaker.intensityCurve = damageShakeCurve;
      shaker.enabled = true;
    }
    Play( "damage" );
    float sign = 0;
    if( damage.damageSource != null )
      sign = Mathf.Sign( damage.damageSource.position.x - transform.position.x );
    facingRight = sign > 0;
    velocity.y = 0;
    OverrideVelocity( new Vector2( -sign * damagePushAmount, damageLift ), damageDuration );
    arm.gameObject.SetActive( false );
    takingDamage = true;
    damageGraceActive = true;
    if( ability != null )
      ability.Deactivate();
    if( weapon != null )
      weapon.Deactivate();
    damageTimer.Start( this, damageDuration, null, delegate()
    {
      takingDamage = false;
      arm.gameObject.SetActive( true );
      DamagePulseFlip();
      damageTimer.Start( this, damageBlinkDuration, null, delegate()
      {
        foreach( var sr in spriteRenderers )
          sr.enabled = true;
        damageGraceActive = false;
        damagePulseTimer.Stop( false );
      } );
    } );
  }

  public override void DetectCrushDamage()
  {
     // todo the problem is the velocity of the crushing entity is zero because it has already updated this frame and collided with the floor.
    const float crushMinSpeed = 0.005f;
    if( collideRight && collideLeft )
    {
      bool crushed = false;
      Entity entity;
      for( int i = 0; i < leftHitCount; i++ )
        if( leftHits[i].transform.TryGetComponent( out entity ) && entity.Velocity.x > crushMinSpeed )
        {
          crushed = true;
          TakeCrushDamage( entity );
          break;
        }
      if( !crushed )
      {
        for( int i = 0; i < rightHitCount; i++ )
          if( rightHits[i].transform.TryGetComponent( out entity ) && entity.Velocity.x < -crushMinSpeed )
          {
            TakeCrushDamage( entity );
            break;
          }
      }
    }
    if( collideTop && collideBottom )
    {
      bool crushed = false;
      Entity entity;
      for( int i = 0; i < topHitCount; i++ )
        if( topHits[i].transform.TryGetComponent( out entity ) && entity.Velocity.y < -crushMinSpeed )
        {
          crushed = true;
          TakeCrushDamage( entity );
          break;
        }
      if( !crushed )
      {
        for( int i = 0; i < bottomHitCount; i++ )
          if( bottomHits[i].transform.TryGetComponent( out entity ) && entity.Velocity.y > crushMinSpeed )
          {
            TakeCrushDamage( entity );
            break;
          }
      }
    }
  }

  public void DoorTransition( Vector2 targetPosition, float duration )
  {
    enabled = false;
    damageTimer.Stop( true );
    Vector2 delta = targetPosition - (Vector2) transform.position;
    facingRight = delta.x > 0;
    transform.localScale = new Vector3( facingRight ? 1 : -1, 1, 1 );
    renderer.material.SetInt( "_FlipX", facingRight ? 0 : 1 );
    SetAnimatorUpdateMode( AnimatorUpdateMode.UnscaledTime );
    if( onGround && !dashing && Mathf.Abs( delta.y ) < 0.1f )
      Play( "run" );
    else if( Mathf.Abs( delta.y ) > 0.5f && !jumping )
      Play( "fall" );
    LerpToTarget lerp = gameObject.GetComponent<LerpToTarget>();
    if( lerp == null )
      lerp = gameObject.AddComponent<LerpToTarget>();
    lerp.moveTransform = transform;
    lerp.WorldTarget = true;
    lerp.targetPositionWorld = targetPosition;
    lerp.duration = duration;
    lerp.unscaledTime = true;
    lerp.enabled = true;
    lerp.OnLerpEnd = delegate
    {
      enabled = true;
      SetAnimatorUpdateMode( AnimatorUpdateMode.Normal );
    };

    // HACK to set the correct back-arm sprite while moving through the door.
    // EntityLateUpdate() (which is called after animator update) seems to set the correct sprite during the transition,
    // so I DO NOT KNOW why the animation sprite remains, and why it's only during the transition!
    // This fixes it, though.
    lerp.OnLerpUpdate = delegate
    {
      // must set the sprite in LateUpdate, after the animator has updated.
      if( weapon != null )
        weapon.UpdateAbilityLate();
    };

    Global.instance.CameraController.Lerp( targetPosition, Global.instance.CameraController.orthoTarget, 3, true, () => { } );
  }

  // Character parts exist to allow for individual sprites' layers to be animated.
  [Header( "Character Parts player biped" )]
  public CharacterPart partLegs;
  public CharacterPart partHead;
  public CharacterPart partArmBack;
  public CharacterPart partArmFront;

  // Call from Awake
  public override void InitializeParts()
  {
    AnimatedCharacterParts = new List<CharacterPart> {partLegs, partHead, partArmBack, partArmFront};
    // testing
    for( int i = 0; i < AnimatedCharacterParts.Count; i++ )
    {
      CharacterPart part = AnimatedCharacterParts[i];
      part.enabled = true;
      AnimatedCharacterParts[i] = part;
    }
  }

  /*
    // Call from LateUpdate
    void UpdateParts()
    {
      foreach( var part in CharacterParts )
        part.renderer.sortingOrder = CharacterLayer + part.layerAnimated;
    }
  */
  void Play( string anim )
  {
    partLegs.animator.Play( anim );
    /*
    foreach( var part in CharacterParts )
      if( part.animator != null && part.animator.HasState( 0, Animator.StringToHash( anim ) ) )
        part.animator.Play( anim );
        
    #if UNITY_EDITOR
          else
            Debug.LogWarning( "anim " + anim + " not found on " + part.transform.name );
    #endif*/
  }

  void SetAnimatorUpdateMode( AnimatorUpdateMode mode )
  {
    // for changing the time scale to/from unscaled
    foreach( var part in AnimatedCharacterParts )
      if( part.animator != null )
        part.animator.updateMode = mode;
  }

  void SetAnimatorSpeed( float speed )
  {
    foreach( var part in AnimatedCharacterParts )
      if( part.animator != null )
        part.animator.speed = speed;
  }

  /*
    void EnablePart( CharacterPart part )
    {
      part.enabled = true;
  
      if( part.animator != null )
        part.animator.enabled = true;
  
      if( part.transform == partLegs.transform )
      {
        part.transform.GetComponent<BoxCollider2D>().size = new Vector2( 0.2f, 0.3f );
        part.renderer.enabled = true;
        //Global.instance.CameraController.orthoTarget = 3;
        Global.instance.CameraController.UseVerticalRange = true;
        CanTakeDamage = true;
        Health = MaxHealth;
        UseGravity = true;
      }
      part.transform.gameObject.SetActive( true );
    }
  
    void DisablePart( CharacterPart part )
    {
      part.enabled = false;
  
      if( part.animator != null )
        part.animator.enabled = false;
  
      if( part.transform == partLegs.transform )
      {
        part.transform.GetComponent<BoxCollider2D>().size = new Vector2( 0.15f, 0.15f );
        part.renderer.enabled = false;
        //Global.instance.CameraController.orthoTarget = 1;
        Global.instance.CameraController.UseVerticalRange = false;
        CanTakeDamage = false;
      }
      else
      {
        part.transform.gameObject.SetActive( false );
      }
    }
  */

  public override void Teleport( Vector2 newPos ) { transform.position = newPos; }

  // Modify the Definition in the Instance
  public bool ModifyWeapon( Weapon wpn, WeaponMod mod, ref string message )
  {
    if( !weapons.Contains( wpn ) )
    {
      Debug.LogError( "player does not have weapon to mod." );
      return false;
    }
    if( !mod.CanApplyMod( wpn, ref message ) )
    {
      Debug.LogError( "Weapon mod cannot be applied." );
      return false;
    }
    mod.ApplyMod( wpn );
    return true;
  }


#region Serialization

  public struct SZ_Weapon
  {
    public string name;
    public string[] mods;
  }

  [deeprest.Serialize]
  List<SZ_Weapon> szWeapons;

  /*public override void BeforeSerialize()
  {
    
    SZ_Weapon szWeapon = new SZ_Weapon();
    szWeapon.name = weapon.name;
    szWeapon.mods = new string[weapon.mods.Length];
    for( int i = 0; i < weapon.mods.Length; i++ )
    {
      string json = JsonUtility.ToJson( weapon.mods[i] );
      szWeapon.mods[i] = json;
    }
    
  }
  
  public override void AfterDeserialize() { }
  */

#endregion

  public void PreConversation()
  {
    Cursor.gameObject.SetActive( false );
    InteractIndicator.SetActive( false );
    inputAim = default;
    CursorShowAuto = false;
  }

  public void PostConversation()
  {
    Cursor.gameObject.SetActive( true );
    CursorShowAuto = true;
  }

  public void VictorySequenceBegin()
  {
    // Boss explosions are occurring.
    CursorShowAuto = false;
    InteractIndicator.SetActive( false );
    Cursor.gameObject.SetActive( false );
    CanTakeDamage = false;
  }

  public void VictoryMusicStart()
  {
    Global.instance.Controls.BipedActions.Aim.Disable();
    // Boss has disappeard, music is playing.
    Vector2 aimStart = input.Aim;
    new Timer( this, 1, ( timer ) => { input.Aim = Vector2.Lerp( aimStart, pos + (facingRight ? Vector2.right : Vector2.left), timer.ProgressNormalized ); }, null );
  }

  public void VictoryStingBegin()
  {
    // The moment the character does a thumbs-up
    victorySting = true;
    arm.localScale = Vector3.one;
    arm.rotation = Quaternion.identity;
    Play( "victory" );
  }

  public void VictorySequenceEnd()
  {
    victorySting = false;
    CursorShowAuto = true;
    CanTakeDamage = true;
    Global.instance.Controls.BipedActions.Enable();
  }
}