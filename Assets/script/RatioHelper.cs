﻿using Unity.Collections;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

#endif

[ExecuteInEditMode]
public class RatioHelper : MonoBehaviour
{
#if UNITY_EDITOR
  [System.Serializable]
  public enum SupportedRatios
  {
    R169,
    R1610,R43
  };

  public Camera cam;
  public float width;
  public SupportedRatios Ratio;

  [ReadOnly]
  public float resultHeight_169;
  public float resultHeight_169_Half;

  [ReadOnly]
  public float resultHeight_1610;
  public float resultHeight_1610_Half;

  public void Update()
  {
    Bounds bounds;
    Color color;
    Bounds r169 = new Bounds( transform.position, new Vector2( width, (9f / 16f) * width ) );
    Bounds r1610 = new Bounds( transform.position, new Vector2( width, (10f / 16f) * width ) );
    resultHeight_1610 = r1610.size.y;
    resultHeight_1610_Half = r1610.extents.y;
    resultHeight_169 = r169.size.y;
    resultHeight_169_Half = r169.extents.y;

    bounds = r169;
    color = Color.yellow;
    Popcron.Gizmos.Line( new Vector3( bounds.min.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.max.y ), color );
    Popcron.Gizmos.Line( new Vector3( bounds.min.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.max.y ), color );
    Popcron.Gizmos.Line( new Vector3( bounds.max.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.min.y ), color );
    Popcron.Gizmos.Line( new Vector3( bounds.max.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.min.y ), color );

    bounds = r1610;
    color = Color.magenta;
    Popcron.Gizmos.Line( new Vector3( bounds.min.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.max.y ), color );
    Popcron.Gizmos.Line( new Vector3( bounds.min.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.max.y ), color );
    Popcron.Gizmos.Line( new Vector3( bounds.max.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.min.y ), color );
    Popcron.Gizmos.Line( new Vector3( bounds.max.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.min.y ), color );

    if( cam != null )
    {
      switch( Ratio )
      {
        case SupportedRatios.R169:
          cam.orthographicSize = resultHeight_169_Half;
          break;

        case SupportedRatios.R1610:
          cam.orthographicSize = resultHeight_1610_Half;
          break;
      }
    }
  }
#endif
}