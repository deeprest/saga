using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Experimental.U2D.Animation;
using Gizmos = Popcron.Gizmos;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;

#endif


#if UNITY_EDITOR
[CustomEditor( typeof(WalkerSystem) )]
public class WalkerSystemEditor : Editor
{
  public override void OnInspectorGUI()
  {
    WalkerSystem ws = target as WalkerSystem;
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Generate" ) )
      ws.Generate();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Destroy" ) )
      ws.DestroyWalkerStates();
    DrawDefaultInspector();
  }

  void OnSceneGUI()
  {
    Handles.color = Color.magenta;
    WalkerSystem ws = target as WalkerSystem;
    if( ws.segs != null )
    {
      for( int i = 0; i < ws.segs.Count; i++ )
        Handles.DrawLine( ws.segs[i].a, ws.segs[i].b );
    }
  }
}
#endif

public class WalkerSystem : MonoBehaviour
{
  /*public bool GenerateOnStart = true;*/
  public int count = 10;
  public GameObject WalkerPrefab;
  public SpriteLibraryAsset[] LibraryAssets;

  [Header( "group vars" )]
  public float speed;
  public bool UseWaitDuration = true;
  public float waitDurationMin = 1;
  public float waitDurationMax = 10;

  public Vector2 direction = Vector2.down;
  public Vector2 dimension = Vector2.one;
  public float incrementX = 1;
  public float incrementY = 1;
  public float breakThreshold = 0.1f;
  public float levelAngleThreshold = 2;
  public float minSegmentLength = 0.2f;
  RaycastHit2D[] hits = new RaycastHit2D[8];
  Vector2 prevPoint;
  Vector2 startPoint;

  public List<WalkerState> state = new List<WalkerState>();
  public List<LineSegment> segs = new List<LineSegment>();
  Dictionary<LineSegment, float> segWeights = new Dictionary<LineSegment, float>();
  float totalWeight;

  /* 
 #if UNITY_EDITOR
   // WARNING
   // In a build, Start() is called before the scene has loaded!!!!!!!
   // Call Generate from the scene script instead
   void Start() 
   {
     if( Application.isPlaying )
       if( GenerateOnStart )
         Generate();
   }
 #endif
   */
  public void DestroyWalkerStates()
  {
    for( int i = 0; i < state.Count; i++ )
      state[i].Destruct();
    state.Clear();
  }

  void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    if( Application.isEditor )
      DestroyWalkerStates();
  }

  void Update()
  {
    for( int i = 0; i < state.Count; i++ )
      state[i].Update();
    foreach( var seg in segs )
      Gizmos.Line( seg.a, seg.b, Color.magenta );
  }
  
  public void Generate()
  {
    DestroyWalkerStates();
    CreateSegments();
    if( segs.Count == 0 )
    {
      Debug.LogWarning( "WalkerSystem generated no segments", this );
      return;
    }
    // Spawn Walkers
    CalculateSegmentSpawnWeights();
    for( int i = 0; i < count; i++ )
    {
      LineSegment segment = GetRandomSegmentWeighted();
      Vector2 atob = segment.b - segment.a;
      WalkerState ws = new WalkerState();
      ws.go = Instantiate( WalkerPrefab, segment.a + atob * Random.value, Quaternion.identity, transform );
      ws.go.name = "walker_" + i;
      ws.go.GetComponent<SpriteLibrary>().spriteLibraryAsset = LibraryAssets[Random.Range( 0, LibraryAssets.Length )];
      ws.animator = ws.go.GetComponent<Animator>();
      ws.renderer = ws.go.GetComponent<SpriteRenderer>();
      // use offset from city loop area position
      segment.a -= (Vector2) transform.root.position; 
      segment.b -= (Vector2) transform.root.position;
      ws.path = new[]
      {
        Vector2.Lerp( segment.a, segment.b, Random.value ),
        Vector2.Lerp( segment.a, segment.b, Random.value ),
        Vector2.Lerp( segment.a, segment.b, Random.value ),
        Vector2.Lerp( segment.a, segment.b, Random.value ),
      };
      ws.speed = Mathf.Max( 0.1f, speed + (Random.value * 2 - 1) * 0.5f );
      ws.UseWaitDuration = UseWaitDuration;
      ws.waitDurationMin = waitDurationMin;
      ws.waitDurationMax = waitDurationMax;
      state.Add( ws );
    }
  }

  void CalculateSegmentSpawnWeights()
  {
    totalWeight = 0;
    segWeights.Clear();
    for( int i = 0; i < segs.Count; i++ )
    {
      float weight = (segs[i].a - segs[i].b).magnitude;
      segWeights.Add( segs[i], weight );
      totalWeight += weight;
    }
  }

  public LineSegment GetRandomSegmentWeighted()
  {
    float randy = Random.Range( 0, totalWeight );
    float runningTotal = 0;
    for( int i = 0; i < segs.Count; i++ )
    {
      runningTotal += segWeights[segs[i]];
      if( runningTotal > randy )
        return segs[i];
    }
    return segs[0];
  }

  void CreateSegments()
  {
    segs.Clear();
    //int mask = Physics2D.AllLayers;
    int mask = LayerMask.GetMask( new string[] {"Default", "triggerAndCollision", "twowayPlatform"} );
    Vector2 pos = transform.position;
    Vector2 pointHigh;
    Vector2 min;
    Vector2 prevDelta = Vector2.right;
    int steps = Mathf.FloorToInt( dimension.x / incrementX );
    int rows = Mathf.FloorToInt( dimension.y / incrementY );
    bool firstHit = true;
    for( int y = 0; y < rows; y++ )
    {
      min = pos + Vector2.down * y * incrementY + Vector2.left * dimension.x * 0.5f;
      for( int x = 0; x < steps; x++ )
      {
        pointHigh = min + Vector2.right * x * incrementX;
        Vector2 point = Vector2.zero;
        int hitCount = Physics2D.RaycastNonAlloc( pointHigh, direction, hits, incrementY, mask );
        // empty space is a delimiter
        if( hitCount == 0 )
        {
          // force a break
          if( startPoint != prevPoint )
            segs.Add( new LineSegment() {a = startPoint, b = prevPoint} );
          prevPoint = Vector2.zero;
          startPoint = prevPoint;
          prevDelta = Vector2.down;
          firstHit = true;

          continue;
        }
        bool hit = false;
        for( int h = 0; h < hitCount; h++ )
        {
          // ignore if inside of colliders
          if( hits[h].collider.OverlapPoint( pointHigh ) )
            break;
          if( hits[h].transform != null && hits[h].rigidbody == null && Mathf.Abs( hits[h].normal.x ) < 0.5f /*&& hits[h].transform.gameObject.isStatic*/ )
          {
            hit = true;
            point = hits[h].point;
            break;
          }
        }
        if( !hit )
        {
          // force a break
          if( startPoint != prevPoint )
            segs.Add( new LineSegment() {a = startPoint, b = prevPoint} );
          prevPoint = Vector2.zero;
          startPoint = prevPoint;
          prevDelta = Vector2.down;
          firstHit = true;

          continue;
        }

        if( firstHit )
        {
          firstHit = false;
          startPoint = point;
        }
        else
        {
          float newAngle = Vector2.Angle( point - prevPoint, prevDelta );
          // end a segment if there's a break, sharp angle, or if this is the last point
          if( Mathf.Abs( point.y - prevPoint.y ) > breakThreshold || x == steps - 1 || newAngle > levelAngleThreshold )
          {
            if( startPoint != prevPoint )
              segs.Add( new LineSegment() {a = startPoint, b = prevPoint} );
            startPoint = point;
          }
        }

        prevDelta = point - prevPoint;
        prevPoint = point;
      }
    }
  }
}


[System.Serializable]
public class WalkerState
{
  public GameObject go;
  public Animator animator;
  public SpriteRenderer renderer;

  public float speed;
  bool facingRight;
  public bool UseWaitDuration = true;
  [FormerlySerializedAs( "waitDuration" )]
  public float waitDurationMin = 1;
  public float waitDurationMax = 10;
  public bool pingpong;

  int pathIndex;
  public Vector2[] path;
  Timer timeout = new Timer();
  public Vector2 velocity;
  int indexIncrement = 1;

  const float closeEnough = 0.05f;

  public void Destruct()
  {
    timeout.Stop( false );
    Util.Destroy( go );
  }

  public void NextWaypoint()
  {
    int next = pathIndex + indexIncrement;
    if( pingpong && (next >= path.Length || next < 0) )
      indexIncrement = -indexIncrement;
    pathIndex = (pathIndex + indexIncrement) % path.Length;
  }

  public void Update()
  {
    if( go != null )
    {
      if( path.Length > 0 )
      {
        Vector2 delta = (Vector2)go.transform.root.position + path[pathIndex] - (Vector2) go.transform.position;
        if( delta.sqrMagnitude < closeEnough * closeEnough )
        {
          velocity = Vector2.zero;
          animator.Play( "idle" );
          if( UseWaitDuration )
          {
            if( !timeout.IsActive )
            {
              if( Random.value > 0.5f )
                animator.Play( "laugh" );
              else
                animator.Play( "idle" );
              timeout.Start( this, Random.Range( waitDurationMin, waitDurationMax ), null, NextWaypoint );
            }
          }
          else
            NextWaypoint();
        }
        else
        {
          velocity = delta.normalized * speed;
          animator.Play( "walk" );
        }
      }
      if( velocity.x < -0.001f )
        facingRight = false;
      if( velocity.x > 0.001f )
        facingRight = true;
      // the ff6 sprites all face left for walking
      go.transform.localScale = new Vector3( facingRight ? -1 : 1, 1, 1 );
      go.transform.position += (Vector3) velocity * Time.deltaTime;
    }
  }
}