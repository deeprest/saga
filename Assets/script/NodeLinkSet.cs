﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(NodeLinkSet)), CanEditMultipleObjects]
public class NodeLinkSetEditor : Editor
{
  public override void OnInspectorGUI()
  {
    NodeLinkSet ms = target as NodeLinkSet;
    /*if( GUI.Button( EditorGUILayout.GetControlRect(), "prefab to cell" ) ) ms.PrefabsToCells();*/
    if( GUI.Button( EditorGUILayout.GetControlRect(), "populate from selected folder" ) )
    {
      ms.FromFolder();
      ms.PrefabsToCells();
    }
    DrawDefaultInspector();
  }
}
#endif


[System.Serializable]
public enum NodeLinkSetDirectionEnum
{
  NONE,
  R,
  L,
  UL,
  UR,
  NODE_L,
  NODE_R
}

[System.Serializable]
public struct NodeLinkSetDirection
{
  public NodeLinkSetDirectionEnum direction;
  public GameObject[] gos;
}

// https://forum.unity.com/threads/prefab-with-reference-to-itself.412240/
// A prefab cannot have a reference to itself! It gets serialized internally as a local reference.
// So I keep a list of prefabs in a ScriptedObject (NodeLinkSet).
[CreateAssetMenu]
public class NodeLinkSet : ScriptableObject
{
  public NodeLinkSetDirectionEnum direction;
  public GameObject[] AllowedToLink;

  //public NodeLinkSetDirection[] Organized;
  //Dictionary<NodeLinkSetDirectionEnum, GameObject[]> Organized = new Dictionary<NodeLinkSetDirectionEnum, GameObject[]>();
  
  GameObject[] Prefabs;

#if UNITY_EDITOR
  public void PrefabsToCells()
  {
    if( Prefabs != null && Prefabs.Length > 0 )
    {
      ArrayUtility.Clear( ref AllowedToLink );
      // parse prefab names
      foreach( var go in Prefabs )
      {
        string[] tokens = go.name.Split( new char[] {'_'} );
        if( tokens[1] == "BASE" )
        {
          // it's okay, ignore
        }
        else if( tokens.Length == 3 )
        {
          NodeLinkSetDirectionEnum type = NodeLinkSetDirectionEnum.NONE;
          if( tokens[1] == "L" ) type = NodeLinkSetDirectionEnum.L;
          if( tokens[1] == "R" ) type = NodeLinkSetDirectionEnum.R;
          if( tokens[1] == "UL" ) type = NodeLinkSetDirectionEnum.UL;
          if( tokens[1] == "UR" ) type = NodeLinkSetDirectionEnum.UR;
          if( tokens[1] == "NL" ) type = NodeLinkSetDirectionEnum.NODE_L;
          if( tokens[1] == "NR" ) type = NodeLinkSetDirectionEnum.NODE_R;
          if( type == direction ) ArrayUtility.Add( ref AllowedToLink, go );
        }
        else
          Debug.LogWarning( "parsing of node link set", this );
      }
    }
    EditorUtility.SetDirty( this );
  }

  public void FromFolder()
  {
    List<GameObject> gos = new List<GameObject>();
    string[] guids = AssetDatabase.FindAssets( "t:prefab", new string[] {Util.GetCurrentAssetDirectory()} );
    foreach( string guid in guids )
    {
      GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>( AssetDatabase.GUIDToAssetPath( guid ) );
      gos.Add( go );
    }
    Prefabs = gos.ToArray();
    EditorUtility.SetDirty( this );
  }

#endif
}