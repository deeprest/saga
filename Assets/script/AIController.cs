﻿using System.Collections.Generic;
using UnityEngine;

// TODO evaluate path to see if it's possible to follow (cannot get around an overhang)
// TODO strategic positioning; seek for melee, try to gain line of sight, evade, etc
// TODO jumping gaps
// TODO handle different pawn types? turret, airbot, wheelbot, hornet, mech

[CreateAssetMenu]
public class AIController : Controller
{
  [SerializeField] float sightRange = 6;
  [SerializeField] float sightStartRadius = 0.5f;
  [SerializeField] Transform sightOrigin;
  [SerializeField] float aimSpeedDist = 5;

  // sight, target
  int hitCount;
  bool toggleShoot;

  [SerializeField] Entity VisibleTarget;
  [SerializeField] float SightPulseInterval = 1;
  Timer SightPulseTimer = new Timer();
  
  public float idealDistance = 0;
  public float jumpMinimum = 0;
  public float jumpCheckDistanceRight = 1;
  public float jumpCheckOriginUp = 0;
  Timer jumpHoldTimer = new Timer();
  public float jumpHoldDuration = 0.2f;

  public float small = 0.1f;
  bool holdLeft;
  bool holdRight;

  public override void PreSceneTransition()
  {
    SightPulseTimer.Stop( false );
  }
  //public override void PostSceneTransition() { }

  public override void RemovePawn()
  {
    Destroy( this );
  }

  void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    SightPulseTimer.Stop( false );
  }

  public override void AssignPawn( Pawn pwn )
  {
    base.AssignPawn( pwn );
    sightOrigin = pawn.transform;

    // HACK
    // TODO Add pathing Enable/Disable functions on Entity
    if( !pawn.EnablePathing )
    {
      pawn.EnablePathing = true;
      pawn.pathAgent = new PathAgent();
      pawn.pathAgent.Client = pawn;
      pawn.pathAgent.transform = pawn.transform;
      pawn.pathAgent.AgentTypeID = Global.instance.AgentType[pawn.AgentTypeName];
    }
  }

  Vector3 targetPosition;
  Vector2 aimPos;
  List<Entity> entities = new List<Entity>();

  public override void Update()
  {
    if( pawn == null )
      return;

    if( !SightPulseTimer.IsActive )
    {
      SightPulseTimer.Start( this, SightPulseInterval );
      // reaffirm target
      VisibleTarget = null;
      entities.Clear();
      int count = Physics2D.OverlapCircleNonAlloc( pawn.transform.position, sightRange, Global.ColliderResults, Global.EnemyInterestLayers );
      for( int i = 0; i < count; i++ )
      {
        Collider2D cld = Global.ColliderResults[i];
        Entity character = cld.GetComponent<Entity>();
        if( character != null && pawn.IsEnemyTeam( character.TeamFlags ) )
          entities.Add( character );
      }

      if( entities.Count > 0 )
      {
        entities.Sort( ( a, b ) =>
        {
          float distA = Vector3.SqrMagnitude( a.transform.position - pawn.transform.position );
          float distB = Vector3.SqrMagnitude( b.transform.position - pawn.transform.position );
          if( distA < distB ) return -1;
          if( distA > distB ) return 1;
          return 0;
        } );

        targetPosition = entities[0].transform.position;
        for( int i = 0; i < entities.Count; i++ )
        {
          Vector2 delta = entities[i].transform.position - pawn.transform.position;
          hitCount = Physics2D.LinecastNonAlloc( (Vector2) sightOrigin.position + delta.normalized * sightStartRadius, entities[i].transform.position, Global.RaycastHits, Global.SightObstructionLayers );
          if( hitCount == 0 )
          {
            VisibleTarget = entities[i];
            targetPosition = entities[i].transform.position;
            break;
          }
        }

        // path to target position
        pawn.pathAgent.SetPath( targetPosition );
      }
    }

    if( VisibleTarget == null )
    {
      //
    }
    else
    {
      toggleShoot = !toggleShoot;
      if( toggleShoot )
        input.Fire = true;
      aimPos = Vector2.MoveTowards( aimPos, targetPosition, Time.deltaTime * aimSpeedDist );
    }
    input.Aim = aimPos - (Vector2) pawn.transform.position;

    // pathing
    pawn.pathAgent.UpdatePath();
    Vector2 targetDelta = targetPosition - pawn.transform.position;
    if( pawn.pathAgent.HasPath && targetDelta.magnitude > idealDistance )
    {
      /*Vector2 adjustedWaypoint = pawn.pathAgent.GetCurrentWaypoint();
      int wpcount = Physics2D.RaycastNonAlloc( (Vector2) pawn.pathAgent.GetCurrentWaypoint(), Vector2.down, Global.RaycastHits, downCastDistance, Global.CharacterCollideLayers );
      if( wpcount > 0 )
        adjustedWaypoint = Global.RaycastHits[0].point;
      Vector2 moveDelta = adjustedWaypoint - (Vector2)pawn.transform.position;*/
      Vector2 moveDelta = pawn.pathAgent.MoveDirection;

      if( moveDelta.x > small )
        input.MoveRight = true;
      if( moveDelta.x < -small )
        input.MoveLeft = true;
      if( moveDelta.y < -small )
        input.MoveDown = true;

      input.Jump = jumpHoldTimer.IsActive;
      if( input.Jump )
      {
        input.MoveRight = holdRight;
        input.MoveLeft = holdLeft;
      }
      else if( moveDelta.y > jumpMinimum )
      {
        holdRight = false;
        holdLeft = false;

        int rcount = Physics2D.RaycastNonAlloc( (Vector2) pawn.transform.position + Vector2.up * jumpCheckOriginUp, Vector2.right, Global.RaycastHits, jumpCheckDistanceRight, Global.CharacterCollideLayers );
        if( rcount > 0 )
        {
          jumpHoldTimer.Start( this, jumpHoldDuration );
          input.Jump = false;
          input.MoveRight = true;
          holdRight = true;
        }
        else
        {
          int lcount = Physics2D.RaycastNonAlloc( (Vector2) pawn.transform.position + Vector2.up * jumpCheckOriginUp, Vector2.left, Global.RaycastHits, jumpCheckDistanceRight, Global.CharacterCollideLayers );
          if( lcount > 0 )
          {
            jumpHoldTimer.Start( this, jumpHoldDuration );
            input.Jump = false;
            input.MoveLeft = true;
            holdLeft = true;
          }
        }
      }
    }

    if( pawn != null )
      pawn.ApplyInput( input );
    input = default;
  }
  
}