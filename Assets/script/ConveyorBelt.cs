using UnityEngine;
using UnityEngine.U2D;

public class ConveyorBelt : MonoBehaviour
{
  public bool CW = true;
  public float Speed = 1;

  Material ChainMaterial;
  public SpriteShapeRenderer chainRenderer;
  float offset;
  public float TextureFactor = 2;

  void Awake()
  {
#if UNITY_EDITOR
    if( !Application.isPlaying )
      ChainMaterial = chainRenderer.sharedMaterials[1];
    else
      ChainMaterial = chainRenderer.materials[1];
#else
    ChainMaterial = chainRenderer.materials[1];
#endif
  }

  void Update()
  {
    offset += Time.deltaTime * Speed * TextureFactor * (CW ? 1 : -1);
    offset = Mathf.Repeat( offset, 1 );
    ChainMaterial.mainTextureOffset = new Vector2( offset, 0 );
  }
}