﻿#define DEBUG_CHECKS
// #define OLD_CRUSH_DAMAGE

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor( typeof(Entity), true )]
public class EntityEditor : MonoBehaviourInspector
{
  public override void OnInspectorGUI()
  {
    DrawTheExposed();
    Entity obj = target as Entity;
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Manually Populate Lists in Hierarchy" ) )
    {
      Entity[] entities = obj.GetComponentsInChildren<Entity>();
      foreach( var entity in entities )
      {
        entity.AutoPopulateLists = false;
        entity.ClearLists();
        entity.PopulateLists();
      }
      EditorUtility.SetDirty( target );
    }
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Set to Auto" ) )
    {
      Entity[] entities = obj.GetComponentsInChildren<Entity>();
      foreach( var entity in entities )
      {
        entity.AutoPopulateLists = true;
        entity.ClearLists();
      }
      EditorUtility.SetDirty( target );
    }

    if( GUI.Button( EditorGUILayout.GetControlRect(), "Experimental (Assign to All)" ) )
    {
      obj.AutoPopulateLists = false;
      obj.spriteRenderers.Clear();
      obj.spriteRenderers.AddRange( obj.GetComponentsInChildren<SpriteRenderer>() );
      EditorUtility.SetDirty( obj );
      List<Entity> entities = new List<Entity>( obj.GetComponentsInChildren<Entity>() );
      entities.Remove( obj );
      foreach( var entity in entities )
      {
        entity.AutoPopulateLists = false;
        entity.spriteRenderers.Clear();
        entity.spriteRenderers.AddRange( entity.GetComponentsInChildren<SpriteRenderer>() );
        EditorUtility.SetDirty( entity );
      }
    }

    DrawDefaultInspector();
  }
}
#endif

[SelectionBase]
public class Entity : MonoBehaviour, IDamage, ILimit
{
  public bool IsUnderLimit() { return Limit.IsUnderLimit(); }
  public static Limit<Entity> Limit = new Limit<Entity>();

  public bool IgnoreCull;
  public bool culled;

  [EnumFlag]
  public TeamFlags TeamFlags;
  public Entity Instigator;

  public Rigidbody2D body;
  public BoxCollider2D box;
  public Renderer renderer;
  public Animator animator;

  public bool AutoPopulateLists = true;
  public List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();
  // composite entities
  public Entity parentEntity;

  public bool UseGravity = true;
  public bool IsStatic;

  // "local velocity"  ...without inertia or carryCharacter velocity 
  public Vector2 velocity;

  // calculated velocity
  public Vector2 Velocity
  {
    get
    {
      if( carryEntity != null )
        return velocity + carryEntity.Velocity;
      else if( carryConveyorBelt != null )
        return velocity + conveyorMove;
      else if( IsStatic && parentEntity != null )
        return parentEntity.Velocity;
      else
        return velocity + inertia;
    }
  }

  // velocity when character is in the air
  public Vector2 inertia;
  // used for walljumps
  public Vector2 overrideVelocity;
  public Timer overrideVelocityTimer = new Timer();

  public bool hanging { get; set; }
  // moving platforms / stacking characters
  public Entity carryEntity;
  public ConveyorBelt carryConveyorBelt;
  public Vector2 conveyorMove;

  public float friction = 0.05f;
  public float raylength = 0.05f;
  public float raylengthUP = 0.05f;
  public float contactSeparation = 0.01f;
  public float DownOffset = 0;
  public float HeadOffset = 0.05f;
  public float Raydown { get { return DownOffset + contactSeparation; } }
  [System.NonSerialized] public int collideMask;
  [System.NonSerialized] public int collideMask2;
  // collision flags
  public bool collideRight;
  public bool collideLeft;
  public bool collideTop;
  public bool collideBottom;
  const float corner = 0.707106769f;
  // cache raycast hits to process afterward, for things like crush detection.
  const int DirectionalRaycastHits = 4;
  public static RaycastHit2D[] bottomHits;
  public static RaycastHit2D[] topHits;
  public static RaycastHit2D[] leftHits;
  public static RaycastHit2D[] rightHits;
  protected int bottomHitCount;
  protected int topHitCount;
  protected int leftHitCount;
  protected int rightHitCount;
  // cached
  protected RaycastHit2D hit;
  protected int hitCount;
  protected Vector2 adjust;
  protected Vector2 boxOffset;

  [Header( "Pathing" )]
  public bool EnablePathing;
  public string AgentTypeName = "Small";
  public PathAgent pathAgent;

  [Header( "Damage" )]
  [ReadOnly] public bool damageGraceActive;
  public bool CanTakeDamage = true;
  [Tooltip( "ignore damage below this value" )]
  public int DamageThreshold = 1;
  public bool ReflectIfBelowThreshold;
  [deeprest.Serialize]
  public int Health = 3;
  public int MaxHealth = 5;
  public AudioClip soundHit;

  // FLASH
  protected Timer flashTimer = new Timer();
  public float flashInterval = 0.05f;
  public int flashCount = 5;
  protected bool flip;
  protected readonly float flashOn = 1f;
  [FormerlySerializedAs( "contactDamageDefinition" ), Tooltip( "deal this damage on collision" )]
  public DamageDefinition ContactDamageDefinition;
  [Tooltip( "deal this damage when crushing another entity" )]
  public DamageDefinition CrushDamageDefinition;

  [Header( "Death" )]
  public bool EnableDeathSequence;
  public float boomDuration = 3;
  public const float boomInterval = 0.15f;
  public float boomRadius = 0.3f;
  protected Timer destructionTimer;
  protected Timer deathSequenceBoomTimer;
  public Color boomFadeColor = new Color( 0.9926471f, 0.3533663f, 0.2627595f, 1 );
  public bool UseGlobalExplosion = true;

  public bool dead;
  public AudioClip soundDeath;
  public SpawnChance[] SpawnOnDeath;
  public UnityEvent EventDeath;
  public UnityEvent EventDestroyed;

  [System.Serializable]
  public class SpawnChance
  {
    public bool alwaysSpawn;
    public int weight = 1;
    public GameObject prefab;
  }

  // "collision" impedes *this* object's movement
  public System.Action UpdateCollision;
  // "hit" inflicts damage on others
  protected System.Action UpdateHit;
  // brains!!
  protected System.Action UpdateLogic;

  public virtual void PreSceneTransition() { }
  public virtual void PostSceneTransition() { }

  protected virtual void Awake()
  {
    if( !Limit.OnCreate( this ) )
      return;
    EntityAwake();
  }

  public void EntityAwake()
  {
    //
  }

  protected virtual void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    Limit.OnDestroy( this );

    flashTimer?.Stop( false );
    overrideVelocityTimer?.Stop( false );
    destructionTimer?.Stop( false );
    deathSequenceBoomTimer?.Stop( false );

    if( parentEntity )
    {
      Collider2D[] clds = GetComponentsInChildren<Collider2D>();
      SpriteRenderer[] srs = GetComponentsInChildren<SpriteRenderer>();
      parentEntity.Remove( clds, srs );
    }
    parentEntity = null;
    if( !Global.instance.LoadingScene )
      EventDestroyed?.Invoke();
  }

  public void ClearLists() { spriteRenderers.Clear(); }

  public void PopulateLists() { spriteRenderers.AddRange( GetComponentsInChildren<SpriteRenderer>() ); }

  protected virtual void Start()
  {
    if( transform.parent != null )
      parentEntity = transform.parent.GetComponentInParent<Entity>();

    // Keep this call in Start() to avoid calling GetComponents*() unnecessarily
    if( AutoPopulateLists )
      PopulateLists();

    UpdateHit = BoxHit;
    if( !IsStatic )
    {
      UpdateCollision = BoxCollision;
    }
    if( EnablePathing )
    {
      pathAgent = new PathAgent();
      pathAgent.Client = this;
      pathAgent.transform = transform;
      pathAgent.AgentTypeID = Global.instance.AgentType[AgentTypeName];
    }
    InitializeParts();

    collideMask = Global.CharacterCollideLayers;
    collideMask2 = Global.CharacterCollideLayersNoTwoWayPlatform;
    bottomHits = new RaycastHit2D[DirectionalRaycastHits];
    topHits = new RaycastHit2D[DirectionalRaycastHits];
    leftHits = new RaycastHit2D[DirectionalRaycastHits];
    rightHits = new RaycastHit2D[DirectionalRaycastHits];
  }


  public virtual void EntityUpdate()
  {
    if( !dead && UpdateLogic != null )
      UpdateLogic.Invoke();

    if( !dead && UpdateHit != null )
      UpdateHit.Invoke();

    if( !IsStatic )
      UpdatePosition();

    if( UpdateCollision != null )
      UpdateCollision.Invoke();

    DetectCrushDamage();
  }

  public virtual void EntityLateUpdate()
  {
    if( !Global.instance.Updating || dead )
      return;

    UpdateParts();
  }

  protected void BoxHit()
  {
    if( ContactDamageDefinition != null )
    {
      hitCount = Physics2D.BoxCastNonAlloc( box.transform.position, box.size, 0, velocity, Global.RaycastHits, raylength,
        Global.CharacterDamageLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];
        if( hit.transform.IsChildOf( transform ) )
          continue;
        IDamage dam = hit.transform.GetComponent<IDamage>();
        if( dam != null )
        {
          Damage dmg = new Damage
          {
            def = ContactDamageDefinition,
            instigator = this,
            damageSource = transform,
            hit = hit
          };
          dam.TakeDamage( dmg );
        }
      }
    }
  }

  public void OverrideVelocity( Vector2 pVelocity, float duration )
  {
    overrideVelocity = pVelocity;
    overrideVelocityTimer.Start( this, duration, delegate( Timer timer ) { overrideVelocity = pVelocity; }, delegate { overrideVelocity = Vector2.zero; } );
  }

  void UpdatePosition()
  {
    if( overrideVelocityTimer.IsActive )
      velocity = overrideVelocity;
    if( UseGravity )
      velocity.y += -Global.Gravity * Time.deltaTime;
    velocity.y = Mathf.Max( velocity.y, -Global.MaxVelocity );

    if( collideTop )
    {
      velocity.y = Mathf.Min( velocity.y, 0 );
    }
    if( collideBottom )
    {
      velocity.x -= (velocity.x * friction) * Time.deltaTime;
      velocity.y = Mathf.Max( velocity.y, 0 );
    }
    if( collideRight )
    {
      velocity.x = Mathf.Min( velocity.x, 0 );
    }
    if( collideLeft )
    {
      velocity.x = Mathf.Max( velocity.x, 0 );
    }

    transform.position += (Vector3) Velocity * Time.deltaTime;
  }

  protected void BoxCollision()
  {
    carryEntity = null;
    carryConveyorBelt = null;

    float dT = Time.deltaTime;
    collideRight = false;
    collideLeft = false;
    collideTop = false;
    collideBottom = false;
    bottomHitCount = 0;
    topHitCount = 0;
    rightHitCount = 0;
    leftHitCount = 0;

    boxOffset.x = boxOffset.x * Mathf.Sign( transform.localScale.x );
    adjust = (Vector2) transform.position + boxOffset;
    float down = Mathf.Max( velocity.y > 0 ? 0 : Raydown, -velocity.y * dT );

    Vector2 debugOrigin = adjust;
    Vector2 debugBox = box.size;
    Bounds bounds = new Bounds( debugOrigin, debugBox );
    Popcron.Gizmos.Square( bounds.center, bounds.size, Color.red );

    float prefer = float.MinValue;
    hitCount = Physics2D.BoxCastNonAlloc( debugOrigin, debugBox, 0, Vector2.down, Global.RaycastHits, down, collideMask );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;

      if( hit.normal.y > corner && hit.point.y > prefer )
      {
        collideBottom = true;
        prefer = hit.point.y;
        bottomHits[bottomHitCount++] = hit;
        velocity.y = Mathf.Max( velocity.y, 0 );
        inertia.x = 0;
        adjust.y = hit.point.y + debugBox.y * 0.5f + Raydown;

        // moving platforms
        Entity cha = hit.transform.GetComponent<Entity>();
        if( cha != null )
          carryEntity = cha;

        ConveyorBelt belt = hit.transform.GetComponent<ConveyorBelt>();
        if( belt != null && belt.enabled )
        {
          carryConveyorBelt = belt;
          if( carryConveyorBelt.CW )
            conveyorMove = new Vector2( hit.normal.y, -hit.normal.x ) * carryConveyorBelt.Speed;
          else
            conveyorMove = new Vector2( -hit.normal.y, hit.normal.x ) * carryConveyorBelt.Speed;
        }
      }
    }

    // Do a separate raycast to avoid hitting oneway platforms while moving upwards.
    hitCount = Physics2D.BoxCastNonAlloc( debugOrigin, debugBox, 0, Vector2.down, Global.RaycastHits, Mathf.Max( Raydown, -velocity.y * dT ), collideMask2 );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;

      if( hit.normal.y < -corner )
      {
        if( topHitCount < DirectionalRaycastHits )
        {
          collideTop = true;
          topHits[topHitCount++] = hit;
          velocity.y = Mathf.Min( velocity.y, 0 );
          adjust.y = hit.point.y - debugBox.y * 0.5f - contactSeparation;
        }
      }

      if( hit.normal.x > corner )
      {
        if( leftHitCount < DirectionalRaycastHits )
        {
          collideLeft = true;
          leftHits[leftHitCount++] = hit;
          velocity.x = Mathf.Max( velocity.x, 0 );
          inertia.x = Mathf.Max( inertia.x, 0 );
          adjust.x = hit.point.x + debugBox.x * 0.5f + contactSeparation;
          // prevent clipping through angled walls when falling fast.
          /*velocity.y -= Util.Project2D( velocity, hit.normal ).y;*/
        }
      }

      if( hit.normal.x < -corner )
      {
        if( rightHitCount < DirectionalRaycastHits )
        {
          collideRight = true;
          rightHits[rightHitCount++] = hit;
          velocity.x = Mathf.Min( velocity.x, 0 );
          inertia.x = Mathf.Min( inertia.x, 0 );
          adjust.x = hit.point.x - debugBox.x * 0.5f - contactSeparation;
          // prevent clipping through angled walls when falling fast.
          /*velocity.y -= Util.Project2D( velocity, hit.normal ).y;*/
        }
      }
    }

    transform.position = adjust - boxOffset;
    box.offset = boxOffset + velocity * Time.deltaTime;
    Popcron.Gizmos.Square( (Vector2) transform.position + box.offset, box.size, Color.cyan );
  }

  [ExposeMethod]
  public virtual void Die() { Die( default ); }

  [ExposeMethod]
  public virtual void Die( Damage damage )
  {
    if( dead )
      return;
    dead = true;
    velocity = Vector2.zero;

    if( EnableDeathSequence )
    {
      StartDeathSequence();
    }
    else
    {
      if( soundDeath != null )
        Global.instance.AudioOneShot( soundDeath, transform.position );

      if( UseGlobalExplosion )
        Instantiate( Global.instance.explosion, transform.position, Quaternion.identity );

      GameObject[] prefab = GetDeathSpawnObjects();
      for( int i = 0; i < prefab.Length; i++ )
        Instantiate( prefab[i], transform.position, Quaternion.identity );
      Destroy( gameObject );
      EventDeath?.Invoke();
    }
  }

  protected void StartDeathSequence()
  {
    deathSequenceBoomTimer = new Timer( this, (int) (boomDuration / boomInterval), boomInterval,
      delegate( Timer timer ) { Instantiate( Global.instance.explosion, (Vector2) transform.position + Random.insideUnitCircle * boomRadius, Quaternion.identity ); }, null );

    for( int i = 0; i < spriteRenderers.Count; i++ )
    {
      spriteRenderers[i].material.SetFloat( "_FlashAmount", 0 );
      spriteRenderers[i].material.SetColor( "_FlashColor", boomFadeColor );
    }
    destructionTimer = new Timer( this, boomDuration, delegate( Timer timer )
    {
      for( int i = 0; i < spriteRenderers.Count; i++ )
      {
        if( spriteRenderers[i] == null )
          continue;
        spriteRenderers[i].material.SetFloat( "_FlashAmount", timer.ProgressNormalized );
      }
    }, () =>
    {
      deathSequenceBoomTimer.Stop( true );
      GameObject[] prefab = GetDeathSpawnObjects();
      for( int i = 0; i < prefab.Length; i++ )
        Instantiate( prefab[i], transform.position, Quaternion.identity );
      Destroy( gameObject );
      EventDeath?.Invoke();
    } );
  }


  public virtual GameObject[] GetDeathSpawnObjects()
  {
    List<GameObject> gos = new List<GameObject>();
    if( SpawnOnDeath.Length > 0 )
    {
      int spawnChanceWeightTotal = 0;
      for( int i = 0; i < SpawnOnDeath.Length; i++ )
        spawnChanceWeightTotal += SpawnOnDeath[i].weight;
      int index = 0;
      int randy = Random.Range( 0, spawnChanceWeightTotal + 1 );
      int runningTotal = 0;
      for( index = 0; index < SpawnOnDeath.Length; index++ )
      {
        if( SpawnOnDeath[index].alwaysSpawn )
        {
          if( SpawnOnDeath[index].prefab != null )
            gos.Add( SpawnOnDeath[index].prefab );
          continue;
        }
        runningTotal += SpawnOnDeath[index].weight;
        if( runningTotal > randy )
        {
          if( SpawnOnDeath[index].prefab != null )
            gos.Add( SpawnOnDeath[index].prefab );
          break;
        }
      }
    }
    return gos.ToArray();
  }

  public virtual Vector2 GetShotOriginPosition() { return transform.position; }
  public virtual Vector2 GetAimVector() { return Vector2.up; }

  public virtual void AddHealth( int amount ) { Health = Mathf.Clamp( Health + amount, 0, MaxHealth ); }


  public virtual DamageResult CalculateDamageResult( Damage damage )
  {
    DamageResult dmr = new DamageResult( this );

    dmr.ignore = !CanTakeDamage && damageGraceActive;

    if( dead || (damage.instigator && IsSameTeam( damage.instigator.TeamFlags )) )
    {
      dmr.ignore = true;
      dmr.pass = true;
      return dmr;
    }

    Instigator = damage.instigator;

    // pass to sub-entity if applicable
    if( damage.hit && damage.hit.collider )
    {
      IDamage subentity = damage.hit.collider.GetComponent<IDamage>();
      if( subentity != null && !ReferenceEquals( subentity, this ) )
      {
        dmr = subentity.CalculateDamageResult( damage );
        dmr.entity = subentity;
        return dmr;
      }
    }

    if( !dmr.ignore && damage.def && damage.def.amount < DamageThreshold )
    {
      dmr.ignore = true;
      if( ReflectIfBelowThreshold )
        dmr.reflect = true;
    }

    return dmr;
  }

  [ExposeMethod]
  public virtual void TakeDamage() { TakeDamage( new Damage() {def = Global.instance.CrushDamageDefinition} ); }

  public virtual void TakeDamage( Damage damage )
  {
    if( damageGraceActive )
      return;
    AddHealth( -damage.def.amount );
    if( Health <= 0 )
    {
      flashTimer.Stop( false );
      Die( damage );
    }
    else
    {
      // hit sound
      if( soundHit != null )
        Global.instance.AudioOneShot( soundHit, transform.position );
      else if( Global.instance.SoundHit != null )
        Global.instance.AudioOneShot( Global.instance.SoundHit, transform.position );

#if DEBUG_CHECKS
      for( int i = 0; i < spriteRenderers.Count; i++ )
        if( spriteRenderers[i] == null )
          Debug.LogError( "TakeDamage(): renderer in spriteRenderers null", this );
#endif

      // color pulse
      flip = true;
      damageGraceActive = true;
      for( int i = 0; i < spriteRenderers.Count; i++ )
        spriteRenderers[i].material.SetFloat( "_FlashAmount", flashOn );
      flashTimer.Start( this, flashCount * 2, flashInterval, delegate( Timer t )
      {
        flip = !flip;
        for( int i = 0; i < spriteRenderers.Count; i++ )
          spriteRenderers[i].material.SetFloat( "_FlashAmount", flip ? flashOn : 0 );
      }, delegate
      {
        damageGraceActive = false;
        for( int i = 0; i < spriteRenderers.Count; i++ )
          spriteRenderers[i].material.SetFloat( "_FlashAmount", 0 );
      } );
    }
  }

  // Utility function for dealing damage with a single line. Does not handle reflections, passthrough, etc.
  public bool DamageOther( IDamage dam, DamageDefinition def, Transform source, RaycastHit2D hitSource )
  {
    if( dam == null )
      return false;
    Damage dmg = new Damage
    {
      def = def,
      instigator = this,
      damageSource = source,
      hit = hitSource
    };
    DamageResult dmr = dam.CalculateDamageResult( dmg );
    if( !dmr.ignore )
    {
      dmr.entity.TakeDamage( dmg );
      return true;
    }
    return false;
  }


  // This is needed for Unity Events
  public void SetCanTakeDamage( bool value ) { CanTakeDamage = value; }

  public virtual void DetectCrushDamage()
  {
#if OLD_CRUSH_DAMAGE
    // Update crush detection after UpdateCollision in this frame to guarantee that the hits are still valid, and still reference alive objects.
    const float crushMinSpeed = 0.005f;
    if( collideRight && collideLeft ) //if( hitRight.normal.y <= 0 && collideLeft && hitLeft.normal.y <= 0 )
    {
      bool crushed = false;
      Entity entity;
      for( int i = 0; i < leftHitCount; i++ )
        if( leftHits[i].transform.TryGetComponent( out entity ) && entity.Velocity.x > crushMinSpeed )
        {
          crushed = true;
          TakeCrushDamage( entity );
          break;
        }
      if( !crushed )
      {
        for( int i = 0; i < rightHitCount; i++ )
          if( rightHits[i].transform.TryGetComponent( out entity ) && entity.Velocity.x < -crushMinSpeed )
          {
            TakeCrushDamage( entity );
            break;
          }
      }
    }
    if( collideTop && collideBottom )
    {
      bool crushed = false;
      Entity entity;
      for( int i = 0; i < topHitCount; i++ )
        if( topHits[i].transform.TryGetComponent( out entity ) && entity.Velocity.y < -crushMinSpeed )
        {
          crushed = true;
          TakeCrushDamage( entity );
          break;
        }
      if( !crushed )
      {
        for( int i = 0; i < bottomHitCount; i++ )
          if( bottomHits[i].transform.TryGetComponent( out entity ) && entity.Velocity.y > crushMinSpeed )
          {
            TakeCrushDamage( entity );
            break;
          }
      }
    }
#else
    if( collideRight && collideLeft && rightHitCount > 0 && leftHitCount > 0 && rightHits[0].point.x - leftHits[0].point.x - .1f < box.size.x )
      TakeDamage( new Damage() {def = Global.instance.CrushDamageDefinition} );
    if( collideTop && collideBottom && topHitCount > 0 && bottomHitCount > 0 && topHits[0].point.y - bottomHits[0].point.y - .1f < box.size.y )
      TakeDamage( new Damage() {def = Global.instance.CrushDamageDefinition} );
#endif
  }

  public void TakeCrushDamage( Entity Crusher )
  {
    Damage crushDamage;
    if( Crusher.CrushDamageDefinition )
      crushDamage = new Damage( Crusher.CrushDamageDefinition );
    else
      crushDamage = new Damage( Global.instance.CrushDamageDefinition );
    crushDamage.damageSource = Crusher.transform;
    // crush damage should not need an instigator.
    crushDamage.instigator = null;
    TakeDamage( crushDamage );
  }

  public void Remove( Collider2D[] clds, SpriteRenderer[] srs )
  {
    foreach( var sr in srs )
      spriteRenderers.Remove( sr );
    if( parentEntity != null )
      parentEntity.Remove( clds, srs );
  }

  // for EventDestroyed unity events
  public void DestroyGameObject( GameObject go ) { Destroy( go ); }

  public virtual bool IsEnemyTeam( TeamFlags other ) { return TeamFlags != TeamFlags.None && other != TeamFlags.None && other != TeamFlags; }
  public virtual bool IsSameTeam( TeamFlags other ) { return (TeamFlags & other) > 0; }


  [Header( "Character Parts" )]
  public int CharacterLayer;

  [System.Serializable]
  public class CharacterPart
  {
    public bool enabled = true;
    public Animator animator;
    public SpriteRenderer renderer;
    public int layerAnimated;
  }

  [FormerlySerializedAs( "CharacterParts" )]
  public List<CharacterPart> AnimatedCharacterParts;

  // Call from Awake()
  public virtual void InitializeParts()
  {
    // intentionally empty
  }

  // Call from LateUpdate()
  public void UpdateParts()
  {
    for( int i = 0; i < AnimatedCharacterParts.Count; i++ )
      if( AnimatedCharacterParts[i].renderer == null )
        AnimatedCharacterParts.RemoveAt( i-- );

    foreach( var part in AnimatedCharacterParts )
      part.renderer.sortingOrder = CharacterLayer + part.layerAnimated;
  }

  public virtual void Teleport( Vector2 newPos ) { transform.position = newPos; }

  public void SetProgressFlag( string flag ) { Global.instance.ProgressFlag[flag].Value = true; }


#region AnimationEventFunctions

  public void PlaySound( AudioClip clip ) { Global.instance.AudioOneShot( clip, transform.position ); }

#endregion
}