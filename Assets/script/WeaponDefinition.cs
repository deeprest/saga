﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[CreateAssetMenu]
public class WeaponDefinition : AbilityDefinition
{
  public virtual Weapon GetNewWeapon() { return new Weapon( this ); }

  public enum WeaponCategory
  {
    Projectile,
    Laser,
    Melee
  }

  public WeaponCategory weaponCategory;
  // owner body colors
  public Color Color0;
  public Color Color1;
  public AudioClip StartSound;
  [Tooltip( "projectiles fired per round" )]
  [WeaponStat] public int ProjectileCount = 1;
  [WeaponStat] public float ShotSpread;
  [WeaponStat] public bool HoldFullAuto;
  [Tooltip( "duration between rounds" )]
  [WeaponStat] public float ShotInterval = 0.1f;

  [WeaponStat] public bool BurstEnabled;
  [Tooltip( "how many rounds are fired before cooldown" )]
  [WeaponStat] public int BurstShotCount;
  [Tooltip( "cooldown is the delay between bursts" )]
  [FormerlySerializedAs( "BurstWaitInterval" ), WeaponStat]
  public float BurstCooldown = 1;

  // IDEA add homing (auto target), before reflect
  // IDEA add guided missles (cursor is guide), before reflect
  // IDEA full auto variance
  // IDEA spawn multiple upon reflect
  // IDEA add roll along surface, after reflect

  [System.Serializable]
  public class ProjectileData
  {
    public Projectile ProjectilePrefab;
    public Color ProjectileColorIndexed;

    [Header( "Common Attributes" )]
    [WeaponStat( "Speed" )]
    public float Speed;
    public float Timeout;
    public bool UseGravity;
    public bool AlignRotationToVelocity;
    public DamageDefinition ContactDamageDefinition;
    [Tooltip( "Hits delivered before projectile is destroyed" )]
    public int Hits;

    [Header( "Guided" )]
    public bool GuideEnabled;
    public BlasterProjectile.ProjectileGuideType GuideType;
    public float maxDistance;
    public float GuidedFactor;

    [Header( "Reflect" )]
    public int ReflectCount;
    public float ReflectVelocityDecay;
    public AudioClip SoundReflect;

    [Header( "Sticky" )]
    public bool StickyEnabled;
    //public bool AlwaysStickToEntity;
    public float AttachDuration;

    [Header( "Explosion" )]
    public bool ExplodeOnFirstHit;
    public bool ExplodeOnLastHit;
    public DamageDefinition ExplosionDamage;
    public float BoomRadius;
    public GameObject explosion;
  }

  public ProjectileData data;


  [Header( "Charge Variant" )]
  public bool HasChargeVariant;
  public WeaponDefinition ChargeVariant;
  public GameObject ChargeEffect;
  public AudioClip soundCharge;
  public AudioClip soundChargeLoop;

  public string GetStats()
  {
    string retval = "";
    System.Reflection.FieldInfo[] fis = GetType().GetFields();
    foreach( var fi in fis )
      if( fi.IsDefined( typeof(WeaponStatAttribute), true ) )
      {
        object[] atts = fi.GetCustomAttributes( false );
        foreach( var att in atts )
        {
          if( att is WeaponStatAttribute )
          {
            WeaponStatAttribute wsa = ((WeaponStatAttribute)att);
            if( wsa.DisplayName != null )
              retval += wsa.DisplayName + ": " + fi.GetValue( this ) + "\n";
            else
              retval += fi.Name + ": " + fi.GetValue( this ) + "\n";
          }
        }
      }
    fis = typeof(ProjectileData).GetFields();
    foreach( var fi in fis )
      if( fi.IsDefined( typeof(WeaponStatAttribute), true ) )
      {
        object[] atts = fi.GetCustomAttributes( false );
        foreach( var att in atts )
        {
          if( att is WeaponStatAttribute )
          {
            WeaponStatAttribute wsa = ((WeaponStatAttribute)att);
            if( wsa.DisplayName != null )
              retval += wsa.DisplayName + ": " + fi.GetValue( data ) + "\n";
            else
              retval += fi.Name + ": " + fi.GetValue( data ) + "\n";
          }
        }
      }
    return retval;
  }

  /*
  // for viewing the arc of physics projectiles
  public Vector3[] GetTrajectory( Vector2 origin, Vector2 aim )
  {
    Rigidbody2D rb = weaponDefinition.data.ProjectilePrefab.GetComponent<Rigidbody2D>();
    Vector2 vel = aim.normalized * weaponDefinition.data.Speed;
    List<Vector3> points = new List<Vector3>();
    Vector2 pos = origin;
    points.Add( pos );
    for( int i = 0; i < Maxpoints; i++ )
    {
      pos += vel * Time.smoothDeltaTime;
      vel += Vector2.down * Global.Gravity * Time.smoothDeltaTime;
      points.Add( pos );
    }
    return points.ToArray();
  }
   */
}

public class WeaponStatAttribute : PropertyAttribute
{
  public string DisplayName;
  public WeaponStatAttribute() { }
  public WeaponStatAttribute( string displayName ) { this.DisplayName = displayName; }
}

public class Weapon : Ability
{
  // MODS
  public bool Modified;
  public List<WeaponMod> mods;

  public Weapon( WeaponDefinition rhsDef ) : base( rhsDef )
  {
    def = rhsDef;
    abilityDef = def;
  }

  public WeaponDefinition def;
  // transient
  public Timer shootRepeatTimer = new Timer();
  public Timer burstWaitTimer = new Timer();

  Weapon chargeVariant;


  public override void OnAcquire( Entity owner )
  {
    base.OnAcquire( owner );
    // instantiate charge variant
    if( def.ChargeVariant != null )
    {
      chargeVariant = def.ChargeVariant.GetNewWeapon();
      chargeVariant.OnAcquire( owner );
    }
  }

  public override void Activate( Vector2 origin, Vector2 aim ) { FireWeapon( owner, origin, aim ); }

  public override void UpdateAbility()
  {
    if( chargeVariant != null )
      chargeVariant.UpdateAbility();
  }

  // manages timing of each shot, which can have multiple projectiles each.
  public void FireWeapon( Entity instigator, Vector2 origin, Vector2 aim )
  {
    if( def.BurstEnabled )
    {
      if( !burstWaitTimer.IsActive )
      {
        FireWeaponInternal( instigator, origin, aim );
        if( def.BurstShotCount == 1 )
        {
          burstWaitTimer.Start( this, def.BurstCooldown );
        }
        else if( def.BurstShotCount > 1 )
        {
          burstWaitTimer.Start( this, def.BurstShotCount - 1, def.ShotInterval,
            ( timer ) =>
            {
              // burst shots need a current origin position when the instigator is moving.
              if( instigator != null )
                FireWeaponInternal( instigator, instigator.GetShotOriginPosition(), instigator.GetAimVector() );
            },
            () => { burstWaitTimer.Start( this, def.BurstCooldown ); } );
        }
      }
    }
    else if( !shootRepeatTimer.IsActive )
    {
      shootRepeatTimer.Start( this, def.ShotInterval );
      FireWeaponInternal( instigator, origin, aim );
    }
  }

  // FireWeaponInternal manages creation of multiple projectiles
  void FireWeaponInternal( Entity instigator, Vector2 origin, Vector2 aim )
  {
    if( def.ProjectileCount == 1 )
    {
      FireSingleProjectile( instigator, def.data.ProjectilePrefab, origin, aim, true );
    }
    else if( def.ProjectileCount > 1 )
    {
      // multiple projectiles, with spread
      float inc = def.ShotSpread / (def.ProjectileCount - 1);
      float val = -def.ShotSpread * 0.5f;
      bool anyFired = false;
      for( int i = 0; i < def.ProjectileCount; i++ )
      {
        if( FireSingleProjectile( instigator, def.data.ProjectilePrefab, origin, Quaternion.Euler( 0, 0, val ) * aim, false ) )
          anyFired = true;
        val += inc;
      }
      if( anyFired )
        Global.instance.AudioOneShot( def.StartSound, origin );
    }
  }

  public virtual bool FireSingleProjectile( Entity instigator, Projectile projectilePrefab, Vector2 pos, Vector2 aim, bool playSound = true )
  {
    Collider2D col = Physics2D.OverlapCircle( pos, projectilePrefab.circle.radius, Global.ProjectileNoShootLayers );
    if( col == null )
    {
      GameObject pgo = Object.Instantiate( projectilePrefab.gameObject, pos, Quaternion.identity );
      Projectile projectile = pgo.GetComponent<Projectile>();

      projectile.data = def.data;
      projectile.weaponCategory = def.weaponCategory;
      // The argument 'instigator' could be the top-level entity, or a sub-entity, such as a turret. This is done to avoid passing multiple instigators.
      projectile.instigator = instigator.parentEntity != null ? instigator.parentEntity : instigator;
      projectile.teamFlags = projectile.instigator.TeamFlags;
      projectile.shotOriginPosition = pos;

      Vector2 inheritedVelocity = Vector2.zero;

      // projected velocity
      /*Vector2 inheritedVelocity = Util.Project2D( instigator.Velocity, shoot.normalized );*/

      // special case
      /*if( instigator.carryCharacter != null )
        inheritedVelocity = instigator.carryCharacter.Velocity;
      else
        inheritedVelocity = instigator.inertia;*/

      projectile.velocity = inheritedVelocity + aim.normalized * def.data.Speed;

      IndexedColors indexedColors = projectile.GetComponent<IndexedColors>();
      if( indexedColors != null )
        indexedColors.colors[0] = def.data.ProjectileColorIndexed;

      if( playSound && def.StartSound != null )
        Global.instance.AudioOneShot( def.StartSound, pos );
      // color shifting
      /*SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
      if( sr != null )
        sr.color = Global.instance.shiftyColor;
      Light light = go.GetComponentInChildren<Light>();
      if( light != null )
        light.color = Global.instance.shiftyColor;*/
      return true;
    }
    return false;
  }

  public void FireChargeVariant( Entity instigator, Vector2 origin, Vector2 aim ) { chargeVariant.FireWeapon( instigator, origin, aim ); }

  const int Maxpoints = 1000;

  // for viewing the arc of physics projectiles
  public Vector3[] GetTrajectory( Vector2 origin, Vector2 aim )
  {
    Vector2 vel = aim.normalized * def.data.Speed;
    List<Vector3> points = new List<Vector3>();
    Vector2 pos = origin;
    points.Add( pos );
    for( int i = 0; i < Maxpoints; i++ )
    {
      pos += vel * Time.smoothDeltaTime;
      if( def.data.UseGravity )
        vel += Vector2.down * Global.Gravity * Time.smoothDeltaTime;
      points.Add( pos );
    }
    return points.ToArray();
  }
}