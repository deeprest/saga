﻿using UnityEngine;

[CreateAssetMenu]
public class AnimSequence : ScriptableObject
{
    public int fps = 24;
    public Sprite[] sprites;
}