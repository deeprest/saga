﻿using UnityEngine;

//[CreateAssetMenu]
public class AirbotAbilityDefinition : AbilityDefinition
{
  public override Ability GetNewAbility() { return new AirbotAbility( this ); }
  public float accSpeed = 10; //20
  public float maxAcc = 5; //10
  public float rotateTarget = 50;
  public float rotSpeed = 180f; //360
  public float airBrake = 4; //10
  public float inertiaBrake = 4;
}

public class AirbotAbility : Ability
{
  public AirbotAbility( AirbotAbilityDefinition rhsdef ) : base( rhsdef )
  {
    def = rhsdef;
    abilityDef = rhsdef;
  }

  AirbotAbilityDefinition def;
  Animator animator;
  Vector2 vel = Vector2.zero;

  public override void Equip( Transform parentTransform )
  {
    base.Equip( parentTransform );
    biped = owner as PlayerBiped;
    animator = go.GetComponent<Animator>();
    animator.Play( "idle" );
  }

  public override void Unequip()
  {
    base.Unequip();
    biped.flying = false;
    biped.rotating = false;
  }

  public override void UpdateAbility()
  {
    if( !biped.onGround && biped.dashStart )
    {
      IsActive = true;
      biped.flying = true;
      vel = owner.velocity;
    }

    if( !biped.input.Dash || biped.onGround )
      Deactivate();

    if( IsActive )
    {
      animator.Play( "spin" );

      if( !owner.collideTop && biped.input.MoveUp )
        vel += Vector2.up * def.accSpeed * Time.deltaTime;
      if( !owner.collideRight && biped.input.MoveRight )
        vel += Vector2.right * def.accSpeed * Time.deltaTime;
      if( !owner.collideLeft && biped.input.MoveLeft )
        vel += Vector2.left * def.accSpeed * Time.deltaTime;
      if( !owner.collideBottom && biped.input.MoveDown )
        vel += Vector2.down * def.accSpeed * Time.deltaTime;
      if( !(biped.input.MoveUp || biped.input.MoveDown) )
        vel.y = Mathf.MoveTowards( vel.y, 0, def.airBrake * Time.deltaTime );
      if( !(biped.input.MoveRight || biped.input.MoveLeft) )
        vel.x = Mathf.MoveTowards( vel.x, 0, def.airBrake * Time.deltaTime );
      vel.x = Mathf.Clamp( vel.x, -def.maxAcc, def.maxAcc );
      vel.y = Mathf.Clamp( vel.y, -def.maxAcc, def.maxAcc );
      owner.velocity = vel;
      owner.inertia = Vector2.MoveTowards( owner.inertia, Vector2.zero, def.inertiaBrake * Time.deltaTime );

      biped.rotating = true;
      biped.rotateSpeed = def.rotSpeed;
      biped.rotateTarget = Mathf.Clamp( (vel.x / def.maxAcc) * -def.rotateTarget, -def.rotateTarget, def.rotateTarget );
    }
    else
    {
      animator.Play( "idle" );
      biped.rotating = false;
      vel = Vector2.zero;
    }
  }

  public override void Deactivate()
  {
    IsActive = false;
    biped.flying = false;
  }
}