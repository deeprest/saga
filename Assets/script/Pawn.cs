﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
/*[CustomEditor( typeof(Pawn), true )]
public class PawnEditor : Editor
{
  public override void OnInspectorGUI()
  {
    Pawn obj = target as Pawn;
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Select Controller" ) )
    {
      Selection.activeObject = obj.controller;
    }
    DrawDefaultInspector(); 
  }
}*/
#endif


public class Pawn : Entity
{
  public Controller controller;
  public InputState input;
  public InputState pinput;
  // input aim is an offset from the pawn position
  public Vector2 inputAim;
  public GameObject InteractIndicator;

  protected override void Awake()
  {
    // do not call base.Awake() to avoid being added to Limit
    EntityAwake();
  }

  protected override void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    base.OnDestroy();
    Object.Destroy( InteractIndicator );
  }

  public void ApplyInput( InputState state )
  {
    input = state;
  }
 
  protected void ResetInput()
  {
    pinput = input;
    input = default;
  }

  public virtual void OnControllerAssigned(){}
  public virtual void OnControllerUnassigned(){}
  public virtual void UnselectWorldSelection(){}
  
}
