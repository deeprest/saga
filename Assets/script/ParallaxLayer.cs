﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(ParallaxLayer) )]
public class ParallaxLayerEditor : Editor
{
  public override void OnInspectorGUI()
  {
    // DIRTY HACK
    EditorUtility.SetDirty( target );
    //(target as ParallaxLayer).LateUpdate();
    DrawDefaultInspector();
  }
}
#endif

[ExecuteAlways]
public class ParallaxLayer : MonoBehaviour
{
  public bool ScaleGameObject;
  public Vector2 Scale;
  public Vector2 offset;
  private Transform cam;

  void Start()
  {
    if( Application.isPlaying )
      if( Global.instance.GLR_Enabled )
        cam = Global.instance.glrCamBackground.transform;
      else
        cam = Global.instance.CameraController.transform;
  }

  public void LateUpdate()
  {
#if UNITY_EDITOR
    if( ScaleGameObject )
      transform.localScale = Vector2.one - Scale;
    if( !Application.isPlaying )
    {
      // avoid changing the transform unnecessarily, which makes the scene dirty.
      if( cam == null && SceneView.GetAllSceneCameras() != null && SceneView.GetAllSceneCameras()[0] != null )
        cam = SceneView.GetAllSceneCameras()[0].transform;
    }
#endif
    if( cam != null )
    {
      if( transform.parent )
        transform.position = (Vector3) offset + transform.parent.position + Vector3.Scale( cam.position - transform.parent.position - (Vector3) offset, Scale );
      else
        transform.position = (Vector3) offset + Vector3.Scale( cam.position - (Vector3) offset, Scale );
    }
  }
}