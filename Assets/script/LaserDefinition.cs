﻿using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Serialization;
/*
TODO 
+ laser light
laser aim track
mech ReflectIfBelowThreshold on armor is not reflecting
support reflections for laser
support multiple concurrent laser beams
*/
public class LaserDefinition : WeaponDefinition
{
  public override Weapon GetNewWeapon() { return new Laser( this ); }
  [Header( "Laser" )]
  public DamageDefinition LaserDamageDefinition;
  public GameObject LaserBeamPrefab;
  public float BeamDistance = 10;
  public float BeamRadius = 0.2f;
  [FormerlySerializedAs( "hitPrefab" )]
  public GameObject HitPrefab;
  public AudioClip SoundActive;

  public float BeamLightSize = 1;
  public Color BeamColorIndexed = Color.red;
  public int BeamHits = 1;

  public bool EnableSlowOnHit;
  public float SlowOnHitDuration = 0.25f;
}

// NOTE you must call UpdateAbility each frame for this weapon to work.
public class Laser : Weapon
{
  public Laser( LaserDefinition rhsdef ) : base( rhsdef ) { laserDef = rhsdef; }
  public LaserDefinition laserDef;

  // Transient
  public Timer timeoutTimer = new Timer();
  Vector2 shotOrigin;
  GameObject soundObject;
  AudioSource source;
  // ParticleSystem sparks;
  Vector3[] positions = new Vector3[2];
  // light mesh
  Vector3[] vertices;
  Vector2[] uvs;
  Color[] colors;
  int[] indices;
  Mesh mesh;
  FieldInfo fi_mesh;
  FieldInfo fi_bounds;

  public class LaserBeam
  {
    public GameObject go;
    public float angleOffset;
    public LineRenderer lineRenderer;
    public Light2D lineLight;
    public GameObject hitObject;
    public int hits = 0;
  }

  // supports multiple concurrent beams
  List<LaserBeam> beams = new List<LaserBeam>();
  int beamIndex;

  public override void OnAcquire( Entity owner )
  {
    base.OnAcquire( owner );
    mesh = new Mesh();
    vertices = new Vector3[8];
    uvs = new[]
    {
      new Vector2( 0, 0 ), new Vector2( 0, 0 ), new Vector2( 0, 0 ), new Vector2( 0, 0 ),
      new Vector2( 0, 0 ), new Vector2( 0, 0 ), new Vector2( 0, 0 ), new Vector2( 0, 0 )
    };
    colors = new Color[8];
    indices = new int[18];
    indices[0] = 0;
    indices[1] = 1;
    indices[2] = 2;
    indices[3] = 0;
    indices[4] = 2;
    indices[5] = 3;
    // right
    indices[6] = 1;
    indices[7] = 4;
    indices[8] = 5;
    indices[9] = 1;
    indices[10] = 5;
    indices[11] = 2;
    // left
    indices[12] = 6;
    indices[13] = 0;
    indices[14] = 3;
    indices[15] = 6;
    indices[16] = 3;
    indices[17] = 7;

    // use reflection to access private fields in code we cannot modify.
    fi_mesh = typeof(Light2D).GetField( "m_Mesh", BindingFlags.Instance | BindingFlags.NonPublic );
    fi_bounds = typeof(Light2D).GetField( "m_LocalBounds", BindingFlags.Instance | BindingFlags.NonPublic );
  }

  public override bool FireSingleProjectile( Entity instigator, Projectile projectilePrefab, Vector2 pos, Vector2 aim, bool playSound = true )
  {
    if( beams.Count < def.ProjectileCount )
    {
      LaserBeam newBeam = new LaserBeam();
      newBeam.go = Object.Instantiate( laserDef.LaserBeamPrefab, pos, Quaternion.identity );
      newBeam.lineRenderer = newBeam.go.GetComponentInChildren<LineRenderer>();
      newBeam.lineLight = newBeam.go.GetComponentInChildren<Light2D>();
      beams.Add( newBeam );
      beamIndex = beams.Count - 1;
    }
    Collider2D col = Physics2D.OverlapCircle( pos, laserDef.BeamRadius, Global.ProjectileNoShootLayers );
    if( col == null )
    {
      timeoutTimer.Start( this, def.ShotInterval, null, () => { StopFiring(); } );
      LaserBeam beam = beams[beamIndex];
      beam.go.SetActive( true );
      beam.hits = laserDef.BeamHits;
      beam.angleOffset = Vector2.SignedAngle( instigator.GetAimVector(), aim );
      IsActive = true;
      beamIndex = (beamIndex + 1) % def.ProjectileCount;
      return true;
    }
    beams[beamIndex].go.SetActive( false );
    beamIndex = (beamIndex + 1) % def.ProjectileCount;
    return false;
  }

  public override void Deactivate()
  {
    if( IsActive && !def.BurstEnabled )
      StopFiring();
  }

  void StopFiring()
  {
    Object.Destroy( soundObject );
    foreach( var beam in beams )
    {
      Object.Destroy( beam.go );
      Object.Destroy( beam.hitObject );
    }
    beams.Clear();
    IsActive = false;
    timeoutTimer.Stop( false );
  }

  public override void UpdateAbility()
  {
    base.UpdateAbility();
    if( IsActive )
    {
      shotOrigin = owner.GetShotOriginPosition();
      foreach( var beam in beams )
        UpdateSingleBeam( beam, Quaternion.Euler( 0, 0, beam.angleOffset ) * owner.GetAimVector() );

      if( beams.Count > 0 && soundObject == null )
      {
        soundObject = new GameObject( "laserSound", new[] { typeof(AudioSource) } );
        AudioSource audioSource = soundObject.GetComponent<AudioSource>();
        audioSource.clip = laserDef.SoundActive;
        audioSource.loop = true;
        audioSource.Play();
      }
    }
  }

  Timer slow = new Timer();


  void UpdateSingleBeam( LaserBeam beam, Vector2 aim )
  {
    /*offset += Time.deltaTime * speed;
    offset = Mathf.Repeat( offset, 1 );
    LaserMaterial.mainTextureOffset = new Vector2( offset, 0 );*/

    beam.hits = laserDef.BeamHits;
    positions[0] = shotOrigin;
    Vector2 endPoint = (Vector2)positions[0] + aim.normalized * laserDef.BeamDistance;
    int hitCount = Physics2D.CircleCastNonAlloc( shotOrigin, laserDef.BeamRadius, aim, Global.RaycastHits, laserDef.BeamDistance, Global.DefaultProjectileCollideLayers );
    for( int i = 0; i < hitCount; i++ )
    {
      RaycastHit2D hit = Global.RaycastHits[i];
      if( hit.transform != null && !hit.transform.IsChildOf(owner.transform) )
      {
        endPoint = hit.point + hit.normal * laserDef.BeamRadius;
        Damage dmg = new Damage() { def = laserDef.LaserDamageDefinition, instigator = owner, damageSource = beam.go.transform, hit = hit };
        IDamage dam = hit.transform.GetComponent<IDamage>();
        if( dam != null )
        {
          DamageResult dmr = dam.CalculateDamageResult( dmg );
          if( dmr.pass )
            continue;

          if( laserDef.EnableSlowOnHit )
          {
            Entity entity = dmr.entity as Entity;
            bool wasAlive = !(entity is null) && !entity.dead;
            dmr.entity.TakeDamage( dmg );
            if( wasAlive && entity.dead )
            {
              if( !slow.IsActive )
              {
                Global.instance.Slow();
                slow.Start( this, laserDef.SlowOnHitDuration, null, () => { Global.instance.NoSlow(); } );
              }
            }
          }
          else
          {
            dmr.entity.TakeDamage( dmg );
          }

          Hit( endPoint, beam );
          break;
        }
        else
        {
          Hit( endPoint, beam );
          // hit something solid
          break;
        }
      }
    }
    if( hitCount == 0 && beam.hitObject != null )
      Object.Destroy( beam.hitObject );

    positions[1] = endPoint;
    beam.lineRenderer.SetPositions( positions );
    beam.go.GetComponent<IndexedColors>().colors[0] = laserDef.BeamColorIndexed;
    beam.go.GetComponent<IndexedColors>().ExplicitUpdate();

    Vector2 line = endPoint - shotOrigin;
    Vector2 cross = new Vector2( -line.y, line.x ).normalized * laserDef.BeamRadius * 0.5f;
    mesh.Clear();
    float fo = beam.lineLight.shapeLightFalloffSize;
    vertices[0] = new Vector2( -fo, -fo ) - cross;
    vertices[1] = new Vector2( -fo, -fo ) + cross;
    vertices[2] = new Vector2( -fo, -fo ) + cross + line;
    vertices[3] = new Vector2( -fo, -fo ) - cross + line;
    // right side
    vertices[4] = new Vector2( -fo, -fo ) + cross * (1 + laserDef.BeamLightSize);
    vertices[5] = new Vector2( -fo, -fo ) + cross * (1 + laserDef.BeamLightSize) + line;
    // left side
    vertices[6] = new Vector2( -fo, -fo ) - cross * (1 + laserDef.BeamLightSize);
    vertices[7] = new Vector2( -fo, -fo ) - cross * (1 + laserDef.BeamLightSize) + line;
    mesh.vertices = vertices;
    mesh.uv = uvs;
    colors[0] = beam.lineLight.color;
    colors[1] = beam.lineLight.color;
    colors[2] = beam.lineLight.color;
    colors[3] = beam.lineLight.color;
    colors[4] = new Color( 1, 1, 1, 0 );
    colors[5] = new Color( 1, 1, 1, 0 );
    colors[6] = new Color( 1, 1, 1, 0 );
    colors[7] = new Color( 1, 1, 1, 0 );
    mesh.colors = colors;
    mesh.triangles = indices;

    fi_mesh.SetValue( beam.lineLight, mesh );
    fi_bounds.SetValue( beam.lineLight, CalculateBoundingSphere( ref vertices, ref colors, beam.lineLight.shapeLightFalloffSize ) );
    beam.go.transform.position = shotOrigin;
    /*beam.go.transform.rotation = Quaternion.LookRotation( Vector3.forward, (endPoint - shotOrigin) );*/
  }

  void Hit( Vector2 position, LaserBeam beam )
  {
    if( beam.hitObject == null && laserDef.HitPrefab != null )
    {
      beam.hitObject = Object.Instantiate( laserDef.HitPrefab, position, Quaternion.LookRotation( Vector3.forward, position - shotOrigin ) );
      IndexedColors indexedColors = beam.hitObject.GetComponent<IndexedColors>();
      indexedColors.colors[0] = laserDef.BeamColorIndexed;
      indexedColors.ExplicitUpdate();
    }
    else
      beam.hitObject.transform.position = position;
  }

  // COPIED FROM UnityEngine.Experimental.Rendering.Universal.LightUtility
  public static Bounds CalculateBoundingSphere( ref Vector3[] vertices, ref Color[] colors, float falloffDistance )
  {
    Bounds localBounds = new Bounds();

    Vector3 minimum = new Vector3( float.MaxValue, float.MaxValue );
    Vector3 maximum = new Vector3( float.MinValue, float.MinValue );
    for( int i = 0; i < vertices.Length; i++ )
    {
      Vector3 vertex = vertices[i];
      vertex.x += falloffDistance * colors[i].r;
      vertex.y += falloffDistance * colors[i].g;

      minimum.x = vertex.x < minimum.x ? vertex.x : minimum.x;
      minimum.y = vertex.y < minimum.y ? vertex.y : minimum.y;
      maximum.x = vertex.x > maximum.x ? vertex.x : maximum.x;
      maximum.y = vertex.y > maximum.y ? vertex.y : maximum.y;
    }

    localBounds.max = maximum;
    localBounds.min = minimum;

    return localBounds;
  }
}