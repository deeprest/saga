﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class Shield : MonoBehaviour, IDamage
{
  Entity character;

  public AudioSource source;
  public AudioClip soundHit;
  public DamageDefinition damageDefinition;
  public float lightIntensity;
  public Light2D light;
  public SpriteRenderer sr;
  public float pulseDuration = 1;
  Timer pulseTimer = new Timer();
  public float hitPushSpeed = 1;
  public float hitPushDuration = 1;
  public float reflectOffset = 0.1f;

  void OnDestroy() { pulseTimer.Stop( false ); }

  void Awake()
  {
    sr.material.SetFloat( "_FlashAmount", 0 );
    light.intensity = 0;
    character = GetComponentInParent<Entity>();
  }

  void Update()
  {
    if( character is PlayerBiped )
      sr.flipX = ((PlayerBiped) character).facingRight != (((PlayerBiped) character).GetAimVector().x > 0);
  }

  public DamageResult CalculateDamageResult( Damage damage )
  {
    DamageResult dmr = new DamageResult( this );
    if( character != null && damage.instigator != null && character.IsSameTeam( damage.instigator.TeamFlags ) )
      dmr.ignore = true;

    // when the shield reflects here, then TakeDamage might not be called... so no light pulse, etc.
    /*if( damage.def.amount <= ReflectBelowDamage )
      dmr.reflect = true;*/
    return dmr;
  }

  public void TakeDamage( Damage damage )
  {
    // pulse as if absorbing damage

    if( soundHit != null )
    {
      source.clip = soundHit;
      source.Play();
    }

    sr.material.SetFloat( "_FlashAmount", 1 );

    light.intensity = lightIntensity;
    pulseTimer.Start( this, pulseDuration, delegate( Timer tmr )
    {
      sr.material.SetFloat( "_FlashAmount", 1.0f - tmr.ProgressNormalized );
      light.intensity = (1.0f - tmr.ProgressNormalized) * lightIntensity;
    }, delegate
    {
      sr.material.SetFloat( "_FlashAmount", 0 );
      light.intensity = 0;
    } );

    // bounce entity backwards
    Entity chr = damage.instigator;
    if( chr != null )
    {
      if( damageDefinition != null )
      {
        Damage dmg = new Damage();
        dmg.def = this.damageDefinition;
        chr.TakeDamage( dmg );
      }
    }
    if( character )
    {
      character.OverrideVelocity( damage.def.amount * ((Vector2) (transform.position - damage.damageSource.transform.position)).normalized, hitPushDuration );
      /*switch( damage.def.typeEnum )
      {
       case Damage.DamageTypeEnum.Fire:
         character.OverrideVelocity( hitPushSpeed * ((Vector2) (transform.position - damage.damageSource.transform.position)).normalized, hitPushDuration );
         break;
      }*/
    }
  }
}