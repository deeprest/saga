﻿using UnityEngine;

// todo support grapping to moving objects

public class GraphookDefinition : AbilityDefinition
{
  public override Ability GetNewAbility() { return new GraphookAbility( this ); }

  [Header( "Graphook" )]
  // properties
  public float grapDistance = 10;
  public float grapSpeed = 5;
  public float grapTimeout = 5;
  public float grapPullSpeed = 10;
  public float grapStopDistance = 0.1f;
  public float grapReverseThreshold = 0.5f;
  public float inertiaCarryOver = 0.5f;

  // asset references
  public AudioClip grapShotSound;
  public AudioClip grapHitSound;
  public GameObject graphookTipPrefab;
  public GameObject grapCablePrefab;
}

public class GraphookAbility : Ability
{
  public GraphookAbility( GraphookDefinition gad ) : base( gad )
  {
    gdef = gad;
    abilityDef = gad;
  }

  GraphookDefinition gdef;

  // transient, state
  [SerializeField] GameObject graphookTip;
  public LineRenderer grapCableRenderer;
  Timer grapTimer = new Timer();
  Vector2 grapSize;
  Vector2 graphitpos;
  Transform parent;
  bool grapShooting;
  bool grapPulling;
  float grapDeltaMagnitude;
  float grapDeltaMagnitudePrevious;
  Vector2 lastGoodDelta;
  Vector3[] positions = new Vector3[2];

  public override void Equip( Transform parentTransform )
  {
    parent = parentTransform;
    graphookTip = Object.Instantiate( gdef.graphookTipPrefab, parentTransform.position, Quaternion.LookRotation( Vector3.forward, parentTransform.up ), parentTransform );
    GameObject cableGO = Object.Instantiate( gdef.grapCablePrefab, parentTransform.position, Quaternion.identity, parentTransform );
    grapCableRenderer = cableGO.GetComponent<LineRenderer>();
    graphookTip.SetActive( true );
  }

  public override void Unequip()
  {
    StopGrap( true );
    Object.Destroy( graphookTip );
    if( grapCableRenderer != null )
      Object.Destroy( grapCableRenderer.gameObject );
  }

  private void OnDestroy() { Unequip(); }

  public override void Activate( Vector2 origin, Vector2 aim )
  {
    if( IsActive )
      StopGrap( true );
    else
      ShootGraphook( origin, aim );
  }

  public override void Deactivate()
  {
    if( biped.takingDamage )
      StopGrap( false );
  }

  public override void UpdateAbility()
  {
    if( grapPulling )
    {
      positions[0] = owner.GetShotOriginPosition();
      positions[1] = graphookTip.transform.position;
      grapCableRenderer.SetPositions( positions );
      // parent is guarded by conditional above. If the parent object is destroyed be sure to Deactivate()
      Vector2 grapDelta = graphitpos - (Vector2) parent.position;
      grapDeltaMagnitudePrevious = grapDeltaMagnitude;
      grapDeltaMagnitude = grapDelta.magnitude;
      // if no forward progess, deactivate
      if( grapDeltaMagnitude - grapDeltaMagnitudePrevious > gdef.grapReverseThreshold * Time.smoothDeltaTime )
        StopGrap( false );
      // the owner will stop short of the destination, so keep the inertia.
      if( grapDeltaMagnitude < gdef.grapStopDistance )
      {
        StopGrap( true );
      }
      else
      {
        owner.velocity = grapDelta.normalized * gdef.grapPullSpeed;
        lastGoodDelta = grapDelta;
      }
    }
  }

  public override void PreSceneTransition() { StopGrap( false ); }

  public override void PostSceneTransition() { }

  void ShootGraphook( Vector2 origin, Vector2 direction )
  {
    IsActive = true;
    grapCableRenderer.enabled = true;
    Vector3 pos = origin;
    if( !Physics2D.Linecast( origin, pos, Global.ProjectileNoShootLayers ) )
    {
      RaycastHit2D hit = Physics2D.Raycast( pos, direction, gdef.grapDistance, Global.DefaultProjectileCollideLayers );
      if( hit )
      {
        grapShooting = true;
        graphitpos = hit.point;
        Vector2 grapDelta = graphitpos - (Vector2) parent.position;
        grapDeltaMagnitude = grapDelta.magnitude;
        grapDeltaMagnitudePrevious = grapDeltaMagnitude;

        graphookTip.transform.parent = null;
        graphookTip.transform.localScale = Vector3.one;
        graphookTip.transform.position = pos;
        graphookTip.transform.rotation = Quaternion.LookRotation( Vector3.forward, graphitpos - origin );

        grapTimer.Start( this, gdef.grapTimeout, delegate
        {
          pos = origin;
          graphookTip.transform.position = Vector3.MoveTowards( graphookTip.transform.position, graphitpos, gdef.grapSpeed * Time.deltaTime );
          //grap cable
          grapCableRenderer.gameObject.SetActive( true );
          grapCableRenderer.transform.parent = null;
          grapCableRenderer.transform.localScale = Vector3.one;
          grapCableRenderer.transform.position = pos;
          grapCableRenderer.transform.rotation = Quaternion.LookRotation( Vector3.forward, graphookTip.transform.position - pos );

          positions[0] = owner.GetShotOriginPosition();
          positions[1] = graphookTip.transform.position;
          grapCableRenderer.SetPositions( positions );

          if( Vector3.Distance( graphookTip.transform.position, graphitpos ) < 0.01f )
          {
            grapShooting = false;
            grapPulling = true;
            grapTimer.Stop( false );
            grapTimer.Start( this, gdef.grapTimeout, null, delegate { StopGrap( true ); } );
            Global.instance.AudioOneShot( gdef.grapHitSound, origin );
          }
        }, delegate { StopGrap( false ); } );
        Global.instance.AudioOneShot( gdef.grapShotSound, origin );
      }
    }
  }

  void StopGrap( bool useInertia )
  {
    grapShooting = false;
    grapPulling = false;
    if( parent != null )
    {
      graphookTip.transform.parent = parent;
      grapCableRenderer.transform.parent = parent;
    }
    graphookTip.transform.localPosition = Vector3.zero;
    graphookTip.transform.localRotation = Quaternion.identity;
    grapCableRenderer.gameObject.SetActive( false );
    grapCableRenderer.enabled = false;
    grapTimer.Stop( false );
    if( useInertia )
    {
      owner.inertia = lastGoodDelta.normalized * gdef.grapPullSpeed * gdef.inertiaCarryOver;
    }
    IsActive = false;
  }
}