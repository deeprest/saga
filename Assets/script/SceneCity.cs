﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using UnityEngine.Profiling;
using UnityEngine.UI;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(SceneCity) )]
public class SceneCityEditor : MonoBehaviourInspector
{
  public override void OnInspectorGUI()
  {
    SceneCity level = target as SceneCity;
    EditorGUILayout.BeginHorizontal();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Generate" ) )
      level.Generate();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "+1" ) )
    {
      level.seed++;
      level.Generate();
    }
    EditorGUILayout.EndHorizontal();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Destroy" ) )
      level.DestroyAll();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "Write png" ) )
      level.WriteStructurePNG();
    DrawTheExposed();
    DrawDefaultInspector();
  }
}
#endif

public class Column
{
  public int xindex;
  public List<LevelNode> nodes = new List<LevelNode>();
}

public static class PixelBit
{
  public const byte None = 0;
  public const byte BuildingBackground = 64;
  public const byte Building = 128;
}

[ExecuteInEditMode]
public class SceneCity : SceneScript
{
  [Header( "ChopDrop" )]
  [SerializeField] bool DoChopDrop = true;
  [SerializeField] Chopper chopper;
  [SerializeField] float runRightDuration = 3;


  [Header( "Level Generation" )]
  public bool GenerateLevelOnStart = true;
#if UNITY_EDITOR
  public bool RandomSeedOnStart = false;
  public int seed;
#endif
  public Vector2Int dimension = new Vector2Int( 20, 8 );
  private Vector2Int cellsize = new Vector2Int( 10, 10 );
  public int GroundY = 2;
  public bool UseDensity;
  [Range( 0, 100 )] [SerializeField] int density = 60;
  public bool SineCurve;
  [SerializeField] int spaceMax = 2;
  [SerializeField] int buildingWidthMax = 3;
  [SerializeField] float highwayY = 10;
  public int MaxLinkPasses = 30;

  public GameObject spawnPointPrefab;
  public InputField inputSeed;
  [SerializeField] BoxCollider2D safeZone;

  // marching square
  string StructurePath { get { return Application.persistentDataPath + "/structure.png"; } }

  [SerializeField] Texture2D StructureTexture;
  Dictionary<int, List<GameObject>> built = new Dictionary<int, List<GameObject>>();
  Color32[] structureData;
  public MarchingSquare.MarchingSquareData building;
  public MarchingSquare.MarchingSquareData buildingBG;
  public MarchingSquare.MarchingSquareData underground;
  List<MarchingSquare.MarchingSquareData> msd;

  public GameObject[] HighwayLinks;
  public GameObject[] HighwayLinksBG;
  // nodelinks
  List<GameObject> gens = new List<GameObject>();
  List<LineSegment> debugSegments = new List<LineSegment>();


#region Restaurant

  [Header( "Restaurant" )]
  public float scrollSpeed = 600;
  public float scrollSpeed2 = 300;
  public float scrollDuration = 5;
  public float fadeDur = 1;
  public float waitToFade = 3;
  public float verticalOffset = 1;
  public float offsetRightA = 100;
  public float offsetRightB = 100;
  public float alpha = 0.1f;

  public Text textCanvas;
  public Text textBigScroller;
  public Text textBigScroller2;

  public void TitleDisplay( Vector3 targetPos, RestaurantNamegen.RestaurantTitle rti )
  {
    // fade in; drop down
    Color fade0 = rti.color;
    fade0.a = 0;

    textCanvas.gameObject.SetActive( true );
    textCanvas.text = rti.title;
    textCanvas.color = fade0;
    Vector3 startCanvas = targetPos + Vector3.up * verticalOffset;
    textCanvas.transform.position = startCanvas;

    Timer fadeTimer0 = new Timer();
    fadeTimer0.Start( this, fadeDur, delegate( Timer timer )
    {
      fade0.a = timer.ProgressNormalized;
      textCanvas.color = fade0;
      textCanvas.transform.position = startCanvas + (targetPos - startCanvas) * timer.ProgressNormalized;
    }, () =>
    {
      fade0.a = 1;
      textCanvas.color = fade0;
      // wait; then fade out and move away
      fadeTimer0.Start( this, waitToFade, null, () =>
      {
        fadeTimer0.Start( this, fadeDur, delegate( Timer timer )
        {
          fade0.a = 1f - timer.ProgressNormalized;
          textCanvas.color = fade0;
          textCanvas.transform.position = targetPos + (startCanvas - targetPos) * timer.ProgressNormalized;
        }, () => { } );
      } );
    } );

    // scroll right to left
    Color mc = rti.color;
    mc.a = 0;
    textBigScroller.gameObject.SetActive( true );
    textBigScroller.text = rti.title;
    textBigScroller.font = rti.font;
    textBigScroller.color = mc;
    textBigScroller2.gameObject.SetActive( true );
    textBigScroller2.text = rti.title;
    textBigScroller2.font = rti.font;
    textBigScroller2.color = mc;
    float startTime = Time.time;
    Vector3 startMesh = targetPos + Vector3.right * offsetRightA;
    Vector3 startMesh2 = targetPos + Vector3.right * offsetRightB;
    textBigScroller.transform.position = startMesh;
    textBigScroller2.transform.position = startMesh2;
    float randomOffsetY = Random.value * 2 - 1;
    Color fade1 = textCanvas.color;
    fade1.a = 0;
    Timer fadeTimer1 = new Timer();
    fadeTimer1.Start( this, scrollDuration * 0.1f, delegate( Timer timer )
    {
      fade1.a = timer.ProgressNormalized * alpha;
      textBigScroller.color = fade1;
      textBigScroller2.color = fade1;
    }, () =>
    {
      fadeTimer1.Start( this, scrollDuration * 0.8f, null, () =>
      {
        fadeTimer1.Start( this, scrollDuration * 0.1f, delegate( Timer timer )
        {
          fade1.a = (1f - timer.ProgressNormalized) * alpha;
          textBigScroller.color = fade1;
          textBigScroller2.color = fade1;
        }, () => { } );
      } );
    } );
    Timer asdf = new Timer( this, scrollDuration, delegate( Timer timer )
    {
      textBigScroller.transform.position = startMesh + (Vector3.up * randomOffsetY) + Vector3.left * (Time.time - startTime) * scrollSpeed;
      textBigScroller2.transform.position = startMesh2 + Vector3.left * (Time.time - startTime) * scrollSpeed2;
    }, () =>
    {
      textBigScroller.gameObject.SetActive( false );
      textBigScroller2.gameObject.SetActive( false );
    } );
  }

#endregion


  public override void StartScene()
  {
    base.StartScene();
    bounds = new Bounds();

    if( GenerateLevelOnStart )
      Generate();

    if( DoChopDrop )
    {
      chopper.StartDrop( Global.instance.CurrentPlayer );
      Global.instance.PlayerController.FireOnlyInputMode();
      new Timer( this, runRightDuration, delegate
      {
        // get the input state to *modify* instead of overwrite/assign
        ref InputState inputStateRef = ref Global.instance.PlayerController.GetInput();
        inputStateRef.MoveRight = true;
      }, delegate { Global.instance.PlayerController.NormalInputMode(); } );
    }

    //Global.instance.CameraController.orthoTarget = 3;

    if( inputSeed != null )
      inputSeed.onEndEdit.AddListener( ( x ) => { AssignSeed( x ); } );
  }

  public override void UpdateScene()
  {
    base.UpdateScene();
    foreach( var segment in debugSegments )
      Popcron.Gizmos.Line( segment.a, segment.b, Color.red );
  }

  void AddObject( GameObject go )
  {
    Collider2D cld = go.GetComponent<Collider2D>();
    if( cld != null )
      bounds.Encapsulate( cld.bounds );
    gens.Add( go );
  }

  public void GenerateNext()
  {
    Global.instance.Seed += 1;
    Global.instance.FloatSetting["Seed"].Value = Global.instance.Seed;
    if( inputSeed != null )
      inputSeed.text = Global.instance.Seed.ToString();
    Generate();
  }

  public void AssignSeed( string input )
  {
    int value;
    if( int.TryParse( input, out value ) )
      Global.instance.FloatSetting["Seed"].Value = value;
  }

  public void Generate()
  {
    Profiler.BeginSample( "Level Generation" );
    DestroyAll();

    if( Application.isPlaying )
      Random.InitState( Global.instance.Seed );
#if UNITY_EDITOR
    else
      Random.InitState( seed );
#endif

    bounds.Encapsulate( Vector3.right * dimension.x * cellsize.x );
    bounds.Encapsulate( Vector3.up * dimension.y * cellsize.y );

    InitializeStructure();
    int buildingWidth = 1;
    int spaceWidth = 0;
    for( int x = 0; x <= dimension.x; x++ )
    {
      SetStructureValue( x, 0, PixelBit.None );
      if( x == 0 || x == dimension.x )
      {
        for( int y = 0; y < dimension.y; y++ )
          SetStructureValue( x, y, PixelBit.None );
      }
      else if( spaceWidth > 0 )
      {
        spaceWidth--;
        if( spaceWidth == 0 )
          buildingWidth = Random.Range( 1, buildingWidthMax + 1 );
      }
      else if( buildingWidth > 0 )
      {
        for( int y = 1; y < dimension.y; y++ )
        {
          int height = dimension.y;
          if( SineCurve )
            height = Mathf.RoundToInt( dimension.y * Mathf.Sin( ((float)x / (float)dimension.x) * Mathf.PI ) + 0.5f );
          if( y < height )
          {
            if( !UseDensity || Random.Range( 0, 100 ) < density )
              SetStructureBitOn( x, y, PixelBit.Building | PixelBit.BuildingBackground );
            else
              SetStructureBitOn( x, y, PixelBit.BuildingBackground );
          }
        }
        buildingWidth--;
        if( buildingWidth == 0 )
          spaceWidth = Random.Range( 1, spaceMax + 1 );
      }
    }
    UpdateStructure( 0, 0, dimension.x, dimension.y );

    //AddObject( Instantiate( spawnPointPrefab, new Vector3( 2, (GroundY+1)*cellsize.y, 0 ), Quaternion.identity ) );
    GenerateChain( HighwayLinksBG, new Vector2( 0, highwayY ), false );
    GenerateChain( HighwayLinks, new Vector2( 0, highwayY ), true );

    GenerateAllLayerNavMesh();

    if( Application.isPlaying )
    {
      Global.instance.MinimapRender( bounds );
      if( rain != null )
        rain.Initialize( bounds );
    }
    Profiler.EndSample();
  }

  void GenerateChain( GameObject[] gos, Vector2 pos, bool overlapCheck )
  {
    //List<GameObject> gos = new List<GameObject>( Resources.LoadAll<GameObject>( "LevelFreeNode/" + folder ) );
    List<NodeLink> links = new List<NodeLink>();
    // first link
    CreateChainLink( gos[0], pos, links );
    for( int i = 0; i < MaxLinkPasses; i++ )
    {
      List<NodeLink> newlinks = new List<NodeLink>();
      List<NodeLink> removelinks = new List<NodeLink>();
      foreach( var link in links )
      {
        if( link.transform.position.x > cellsize.x * dimension.x )
          continue;
        GameObject prefab = link.linkSet.AllowedToLink[Random.Range( 0, link.linkSet.AllowedToLink.Length )];

        if( overlapCheck )
        {
          Bounds bounds = prefab.GetComponent<PrefabAABB>().bounds;
          LineSegment seg;
          seg.a = link.transform.position + bounds.min;
          seg.b = link.transform.position + bounds.max;
          debugSegments.Add( seg );

          Collider2D[] overlap = Physics2D.OverlapAreaAll( seg.a, seg.b );
          Collider2D[] ignore = link.transform.root.GetComponentsInChildren<Collider2D>();
          List<Collider2D> conflict = new List<Collider2D>( overlap );
          for( int c = 0; c < overlap.Length; c++ )
          {
            for( int g = 0; g < ignore.Length; g++ )
            {
              if( overlap[c] == ignore[g] )
              {
                conflict.Remove( ignore[g] );
                break;
              }
            }
          }

          if( conflict.Count == 0 )
          {
            CreateChainLink( prefab, link.transform.position, newlinks );
          }
          else
          {
            string conflictnames = "";
            foreach( var cn in conflict )
              conflictnames += cn.name + " ";
            //Debug.Log( "prefab " + prefab.name + " will not spawn due to overlap with " + conflictnames, conflict[0] );
          }
        }
        else
        {
          CreateChainLink( prefab, link.transform.position, newlinks );
        }
        removelinks.Add( link );
      }
      links.AddRange( newlinks );
      foreach( var ugh in removelinks )
      {
        ugh.GetComponent<SpriteRenderer>().enabled = false;
        links.Remove( ugh );
      }
    }
  }

  void CreateChainLink( GameObject prefab, Vector2 pos, List<NodeLink> newLinks )
  {
    GameObject go = null;
    if( Application.isPlaying )
    {
      go = Instantiate( prefab, pos, Quaternion.identity );
    }
    else
    {
#if UNITY_EDITOR
      go = (GameObject)PrefabUtility.InstantiatePrefab( prefab );
      go.transform.position = pos;
#endif
    }
    go.name = prefab.name;
    AddObject( go );
    newLinks.AddRange( go.GetComponentsInChildren<NodeLink>() );
  }

  void DestroyObject( Object obj )
  {
    if( Application.isEditor && !Application.isPlaying )
      DestroyImmediate( obj );
    else
      Destroy( obj );
  }

  public void DestroyAll()
  {
    // node links
    debugSegments.Clear();
    for( int i = 0; i < gens.Count; i++ )
      DestroyObject( gens[i] );
    gens.Clear();

    // marching square built
    foreach( var pair in built )
      if( built[pair.Key] != null && built[pair.Key].Count > 0 )
        for( int i = 0; i < built[pair.Key].Count; i++ )
        {
          DestroyObject( built[pair.Key][i] );
        }
    built.Clear();
  }

  public void DestroyEverythingOutsideSafeZone()
  {
    if( safeZone == null )
      return;
    GameObject[] gos = gameObject.scene.GetRootGameObjects();
    for( int i = 0; i < gos.Length; i++ )
      if( gos[i].transform.root != transform.root && !safeZone.bounds.Contains( gos[i].transform.position ) )
        DestroyObject( gos[i] );
  }

  public void InitializeStructure()
  {
    msd = new List<MarchingSquare.MarchingSquareData>();
    msd.Add( building );
    msd.Add( buildingBG );
    //msd.Add( wall );
    foreach( var ms in msd )
      ms.indexBuffer = new int[dimension.x, dimension.y];
    StructureTexture = new Texture2D( dimension.x + 1, dimension.y + 1 );
    structureData = new Color32[(dimension.x + 1) * (dimension.y + 1)];
  }

  public void DeserializeStructure()
  {
    if( File.Exists( StructurePath ) )
    {
      byte[] bytes = File.ReadAllBytes( StructurePath );
      if( bytes.Length > 0 )
      {
        StructureTexture.filterMode = FilterMode.Point;
        StructureTexture.LoadImage( bytes );
        structureData = StructureTexture.GetPixels32();
      }
    }
    else
    {
      Color32 black = new Color32( 0, 0, 0, 255 );
      for( int i = 0; i < dimension.x * dimension.y; i++ )
        structureData[i] = black;
    }
    UpdateStructure( 0, 0, dimension.x, dimension.y );
  }

  public void WriteStructurePNG()
  {
    for( int x = 0; x < StructureTexture.width; x++ )
    {
      for( int y = 0; y < StructureTexture.height; y++ )
      {
        Color32 color32 = structureData[x + y * dimension.x];
        StructureTexture.SetPixel( x, y, new Color( (float)color32.r / 255, 0, 0, 1 ) );
      }
    }
    StructureTexture.Apply();
    File.WriteAllBytes( StructurePath, StructureTexture.EncodeToPNG() );
  }

  public void SetStructureValue( int x, int y, byte value )
  {
    x = Mathf.Clamp( x, 0, dimension.x );
    y = Mathf.Clamp( y, 0, dimension.y );
    structureData[x + y * dimension.x].r = value;
  }

  public void SetStructureBitOn( int x, int y, byte value )
  {
    x = Mathf.Clamp( x, 0, dimension.x );
    y = Mathf.Clamp( y, 0, dimension.y );
    structureData[x + y * dimension.x].r |= value;
  }


  public void UpdateStructure( int ox, int oy, int w, int h )
  {
    ox = Mathf.Clamp( ox, 0, dimension.x );
    oy = Mathf.Clamp( oy, 0, dimension.y );
    w = Mathf.Clamp( w, 1, dimension.x - ox );
    h = Mathf.Clamp( h, 1, dimension.y - oy );
    // first pass
    for( int y = oy; y < oy + h && y < dimension.y; y++ )
    {
      for( int x = ox; x < ox + w && x < dimension.x; x++ )
      {
        int key = x + y * dimension.x;
        if( built.ContainsKey( key ) && built[key] != null )
        {
          for( int i = 0; i < built[key].Count; i++ )
            Destroy( built[key][i] );
          built[key].Clear();
        }
        foreach( var ms in msd )
        {
          // getpixels returns:
          // 23
          // 01
          // marching squares bit values:
          // 24
          // 18
          int index = 0;
          if( (structureData[key].r & ms.bit) > 0 )
            index += 1;
          if( (structureData[(x + 1) + y * dimension.x].r & ms.bit) > 0 )
            index += 8;
          if( (structureData[x + (y + 1) * dimension.x].r & ms.bit) > 0 )
            index += 2;
          if( (structureData[(x + 1) + (y + 1) * dimension.x].r & ms.bit) > 0 )
            index += 4;
          ms.indexBuffer[x, y] = index;
        }
      }
    }

    // second pass: neighbors
    /* foreach( var ms in msd )
    {
      for( int y = 1; y < BlockSize - 1; y++ )
      {
        for( int x = 1; x < BlockSize - 1; x++ )
        {
          //int index = x + y * BlockSize;
          int[] n = new int[9];
          n[ 0 ] = ms.indexBuffer[ x - 1, y - 1 ];
          n[ 1 ] = ms.indexBuffer[ x , y - 1 ];
          n[ 2 ] = ms.indexBuffer[ x + 1, y - 1 ];
          n[ 3 ] = ms.indexBuffer[ x - 1, y ];
          n[ 4 ] = ms.indexBuffer[ x, y ];
          n[ 5 ] = ms.indexBuffer[ x + 1, y ];
          n[ 6 ] = ms.indexBuffer[ x - 1, y + 1 ];
          n[ 7 ] = ms.indexBuffer[ x, y + 1 ];
          n[ 8 ] = ms.indexBuffer[ x + 1, y + 1 ];

        }
      }
    }*/

    for( int y = oy; y < oy + h && y < dimension.y; y++ )
    {
      for( int x = ox; x < ox + w && x < dimension.x; x++ )
      {
        int key = x + y * dimension.x;

        int buildingIndex = building.indexBuffer[x, y];
        if( IsUnderground( x, y ) )
        {
          GameObject[] prefabs = underground.ms.Cells[buildingIndex].prefab;
          if( prefabs.Length > 0 )
            SingleCell( key, x, y, prefabs[Random.Range( 0, prefabs.Length )] );
        }
        else
        {
          GameObject[] prefabs = building.ms.Cells[buildingIndex].prefab;
          if( prefabs.Length > 0 )
            SingleCell( key, x, y, prefabs[Random.Range( 0, prefabs.Length )] );
        }

        {
          int buildingBackgroundIndex = buildingBG.indexBuffer[x, y];
          GameObject[] prefabs = buildingBG.ms.Cells[buildingBackgroundIndex].prefab;
          if( prefabs.Length > 0 )
            SingleCell( key, x, y, prefabs[Random.Range( 0, prefabs.Length )] );
        }

        /*
        foreach( var ms in msd )
        {
          int buildingIndex = ms.indexBuffer[x, y];
          SingleCell( key, x, y, ms.ms.Cells[buildingIndex].bottom, null );
        }

        /*MarchingSquareCell cell = building.ms.Cells[building.indexBuffer[x, y]];
        if( cell != null )
        {
          int doorIndex = 0;
          if( (structureData[key].r & PixelBit.Door) > 0 )
            doorIndex += 1;
          if( (structureData[(x + 1) + y * BlockSize].r & PixelBit.Door) > 0 )
            doorIndex += 8;
          if( (structureData[x + (y + 1) * BlockSize].r & PixelBit.Door) > 0 )
            doorIndex += 2;
          if( (structureData[(x + 1) + (y + 1) * BlockSize].r & PixelBit.Door) > 0 )
            doorIndex += 4;

          if( doorIndex > 0 && cell.door != null )
            SingleCell( key, x, y, cell.door, cell.top );
          else
            SingleCell( key, x, y, cell.bottom, cell.top );
        }

        cell = wall.ms.Cells[wall.indexBuffer[x, y]];
        if( cell != null )
          SingleCell( key, x, y, cell.bottom, cell.top );
          */
      }
    }
  }

  void SingleCell( int key, int x, int y, GameObject prefab )
  {
    if( prefab != null )
    {
      GameObject go = null;
      Vector3 pos = new Vector3( x * cellsize.x, y * cellsize.y, 0 );
      if( Application.isPlaying )
      {
        go = Global.instance.Spawn( prefab, pos, Quaternion.identity, null, false, true );
      }
      else
      {
#if UNITY_EDITOR
        go = (GameObject)PrefabUtility.InstantiatePrefab( prefab );
        go.transform.position = pos;
#endif
      }

      if( !built.ContainsKey( key ) )
        built[key] = new List<GameObject>();
      built[key].Add( go );

      // do not spawn variants when generating in editor
      if( Application.isPlaying )
      {
        // NOTE it's important to destroy generated objects when destroying/regenerating the city,
        // but during regular play this may destroy enemies that have wandered.
        GenerationVariant[] vars = go.GetComponentsInChildren<GenerationVariant>();
        foreach( var cmp in vars )
        {
          GameObject asdf = cmp.Generate();
          if( asdf != null )
            built[key].Add( asdf );
        }
      }
    }
  }

  bool IsUnderground( int x, int y ) { return y <= GroundY; }
}