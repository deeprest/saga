﻿using System.Collections.Generic;
using UnityEngine;

public class WeaponMod : ScriptableObject
{
  public string Name;
  public string Description;
  public GameObject Icon;
  
  // WeaponType[] AllowedTypes
  // bool CanApplyMultipleTimes
  
  public virtual bool CanApplyMod( Weapon weapon, ref string message )
  {
    if( weapon.mods != null && weapon.mods.Contains( this ) )
    {
      message = "Mod is already applied";
      return false;
    }
    return true;
  }

  public virtual void ApplyMod( Weapon weapon )
  {
    if( weapon.mods == null || weapon.mods.Count == 0 )
      weapon.mods = new List<WeaponMod>();
    
    if( !weapon.Modified )
    {
      // create a copy, to avoid modifying the original def
      WeaponDefinition defcopy = Instantiate( weapon.def );
      weapon.def = defcopy;
    }

    weapon.Modified = true;
    weapon.mods.Add( this );
    /*if( weapon.name.Contains( "(Clone)" ) )
      weapon.name = weapon.name.Substring( 0, weapon.name.Length - 7 );
    if( !weapon.name.EndsWith( "[MOD]" ) )
      weapon.name = weapon.name + "[MOD]";*/
  }
  
  public string GetStats()
  {
    string retval = "";
    System.Reflection.FieldInfo[] fis = GetType().GetFields();
    foreach( var fi in fis )
      if( fi.IsDefined( typeof(WeaponStatAttribute), true ) )
        retval += fi.Name + ": " + fi.GetValue( this ) + "\n";
    return retval;
  }
}
