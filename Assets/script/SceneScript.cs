﻿using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Tilemaps;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

public class SceneScript : MonoBehaviour
{
  [Header( "On Start" )]
  public bool SpawnPlayer = true;
  public bool RenderMinimap;
  public bool GenerateVariants;

  [Header( "Settings" )]
  public Color backgroundColor;
  public AudioLoop music;
  public CameraZone ForceCameraZone;
  public Light2D ambientLight;
  public Bounds bounds;
  public Rain rain;
  public Tilemap tilemap;
  public RestaurantNamegen RestaurantNamegen;
  [Header( "DynamicSpawn" )]
  public bool DynamicSpawnEnabled;
  public float DSDistance;
  Vector2 DSpoint;
  public GameObject DynamicSpawnObject;
  UnityEvent OnSceneStart;
  
  public virtual void StartScene()
  {
    if( ForceCameraZone != null )
      Global.instance.OverrideCameraZone( ForceCameraZone );

    // set the LookTarget before calling this
    Global.instance.CameraController.Teleport();

    Global.instance.CameraController.cam.backgroundColor = backgroundColor;
    Global.instance.glrCamBackground.backgroundColor = backgroundColor;

    if( music != null )
      Global.instance.MusicCrossFadeTo( music, Global.instance.MusicTransitionDuration );

    if( ambientLight != null )
    {
      float targetIntensity = ambientLight.intensity;
      new Timer( this, 3, delegate( Timer timer ) { ambientLight.intensity = timer.ProgressNormalized * targetIntensity; }, null );
    }

    // Optional
    if( rain != null )
      rain.Initialize( bounds );

    WalkerSystem[] WalkerSystems = FindObjectsOfType<WalkerSystem>();
    if( WalkerSystems != null )
      for( int i = 0; i < WalkerSystems.Length; i++ )
        WalkerSystems[i].Generate();

    RestaurantTitleTrigger[] triggers = FindObjectsOfType<RestaurantTitleTrigger>();
    // temp: generate a new business name for each one registered
    foreach( var rtiTrigger in triggers )
      rtiTrigger.AssignInfo( RestaurantNamegen.GenerateRestaurantTitle() );

    if( RenderMinimap )
      Global.instance.MinimapRender( bounds );
    if( SpawnPlayer )
    {
      if( Global.instance.CurrentPlayer == null )
        Global.instance.SpawnPlayer();
      else
      {
        Global.instance.CurrentPlayer.transform.position = Global.instance.FindRandomSpawnPosition();
        Global.instance.CurrentPlayer.velocity = Vector2.zero;
      }
    }
    if( GenerateVariants )
    {
      foreach( var gv in generationVariants )
        gv.Generate();
    }

    OnSceneStart?.Invoke();
  }

  public virtual void UpdateScene()
  {
    if( DynamicSpawnEnabled && Global.instance.CurrentPlayer != null )
    {
      Vector2 ppos = (Vector2) Global.instance.CurrentPlayer.transform.position;
      if( Vector2.SqrMagnitude( ppos - DSpoint ) > DSDistance * DSDistance )
      {
        DSpoint = ppos;

        Vector2 spawnVector = Global.instance.CurrentPlayer.velocity.normalized * (Global.instance.CameraController.orthoTarget * 2 + 2);
        spawnVector += new Vector2( -spawnVector.y, spawnVector.x ).normalized * (Random.value * 2 - 1);
        if( spawnVector.sqrMagnitude < 1 )
          spawnVector = Random.insideUnitCircle * Global.instance.UpdateCullDistance;

        Vector3 SpawnPos = ppos + spawnVector;
        NavMeshHit navhit;
        if( NavMesh.SamplePosition( SpawnPos, out navhit, 1.0f, NavMesh.AllAreas ) )
          SpawnPos = navhit.position;
        SpawnPos.z = 0;
        Global.instance.Spawn( DynamicSpawnObject, SpawnPos, Quaternion.identity, null, true );
      }
    }
  }

  public virtual Vector3 FindSpawnPosition() { return Global.instance.FindRandomSpawnPosition(); }

  public void PlayerInputOff() { Global.instance.Controls.BipedActions.Disable(); }

  public void CameraZoom( float value ) { Global.instance.CameraController.orthoTarget = value; }

  public void AssignCameraZone( CameraZone zone ) { Global.instance.OverrideCameraZone( zone ); }

#if UNITY_EDITOR
  [ExposeMethod]
  public void EncapsulateBounds_Editor()
  {
    EncapsulateBounds();
    EditorUtility.SetDirty( this );
  }
#endif

  public void EncapsulateBounds()
  {
    // todo establish a sensible bounds default without crawling through every object in the scene
    bounds = new Bounds();
    bounds.size = new Vector3( 1, 1, 1 );
    // encapsulate existing objects in scene
    GameObject[] gos = Object.FindObjectsOfType<GameObject>();
    foreach( var go in gos )
    {
      if( go == gameObject )
        continue;
      Collider2D[] cld = go.GetComponentsInChildren<Collider2D>();
      foreach( var c in cld )
        bounds.Encapsulate( c.bounds );
    }
  }

  const int MaxLayerCount = 2;
  BoxCollider[] layerNavmeshBox = new BoxCollider[MaxLayerCount];
  const string NavMeshBoxName = "NavMeshBox_";

  public void GenerateAllLayerNavMesh()
  {
    for( int i = 0; i < transform.childCount; i++ )
    {
      Transform tns = transform.GetChild( i );
      if( tns.name.StartsWith( NavMeshBoxName ) )
        Util.Destroy( tns.gameObject );
    }
    Bounds[] layerBounds = new Bounds[MaxLayerCount];
    // encapsulate existing objects in scene
    GameObject[] gos = Object.FindObjectsOfType<GameObject>();
    for( int i = 0; i < MaxLayerCount; i++ )
    {
      GameObject nmb;
      if( layerNavmeshBox.Length <= i || layerNavmeshBox[i] == null )
      {
        nmb = new GameObject( NavMeshBoxName + i );
        layerNavmeshBox[i] = nmb.AddComponent<BoxCollider>();
      }
      else
        nmb = layerNavmeshBox[i].gameObject;
      nmb.transform.position = Vector3.down * Global.GameplayLayerOffset * i;
      nmb.transform.rotation = Quaternion.Euler( -90, 0, 0 );
      nmb.transform.parent = transform;

      bool layerHasObjects = false;
      layerBounds[i] = new Bounds( Vector3.down * Global.GameplayLayerOffset * i, new Vector3( 1, 1, 1 ) );
      foreach( var go in gos )
      {
        // only consider objects within layer boundaries
        if( go == gameObject ||
          go.transform.position.y > Global.GameplayLayerOffset * -i + Global.GameplayLayerOffset * 0.5f ||
          go.transform.position.y < Global.GameplayLayerOffset * -i - Global.GameplayLayerOffset * 0.5f )
          continue;
        Collider2D[] clds = go.GetComponentsInChildren<Collider2D>();
        foreach( var cld in clds )
        {
          if( cld.transform.position.y < Global.GameplayLayerOffset * -i + Global.GameplayLayerOffset * 0.5f &&
            cld.transform.position.y > Global.GameplayLayerOffset * -i - Global.GameplayLayerOffset * 0.5f )
            layerBounds[i].Encapsulate( cld.bounds );
        }
        if( clds.Length > 0 )
          layerHasObjects = true;
      }
      if( layerHasObjects )
      {
        layerNavmeshBox[i].size = new Vector3( layerBounds[i].size.x, 0, layerBounds[i].size.y );
        Vector3 boxpos = layerBounds[i].center;
        boxpos.z = 0;
        layerNavmeshBox[i].transform.position = boxpos;
      }
      else
      {
        Util.Destroy( layerNavmeshBox[i].gameObject );
      }
    }
  }

  public void GenerateAllLayerNavMesh( Vector2 size )
  {
    for( int i = 0; i < transform.childCount; i++ )
    {
      Transform tns = transform.GetChild( i );
      if( tns.name.StartsWith( NavMeshBoxName ) )
        Util.Destroy( tns.gameObject );
    }
    for( int i = 0; i < MaxLayerCount; i++ )
    {
      GameObject nmb;
      if( layerNavmeshBox.Length <= i || layerNavmeshBox[i] == null )
      {
        nmb = new GameObject( NavMeshBoxName + i );
        layerNavmeshBox[i] = nmb.AddComponent<BoxCollider>();
      }
      else
        nmb = layerNavmeshBox[i].gameObject;
      nmb.transform.position = Vector3.down * Global.GameplayLayerOffset * i;
      nmb.transform.rotation = Quaternion.Euler( -90, 0, 0 );
      nmb.transform.parent = transform;
      layerNavmeshBox[i].size = new Vector3( size.x, 0, size.y );
      layerNavmeshBox[i].transform.position = Vector3.down * Global.GameplayLayerOffset * i + (Vector3.right * size.x * 0.5f);
    }
  }

#if UNITY_EDITOR
  [ExposeMethod]
  public void GenerateNavmesh()
  {
    GenerateAllLayerNavMesh();
    EditorUtility.SetDirty( this );
  }

  [ExposeMethod]
  public void GenerateNavmesh_Size( Vector2 size )
  {
    GenerateAllLayerNavMesh( size );
    EditorUtility.SetDirty( this );
  }
#endif

  [SerializeField] GenerationVariant[] generationVariants;
  [ExposeMethod] void CollectVariants() { generationVariants = Object.FindObjectsOfType<GenerationVariant>(); }

  [Header( "Boss" )]
  public BossBattleConfig[] BossConfig;
  Timer bossintroTimer = new Timer();

  [System.Serializable]
  public struct BossBattleConfig
  {
    public BossPrototype Boss;
    public AudioLoop BossIntroMusic;
    public AudioLoop BattleMusic;
    public float bosswait;
    public bool FadeIn;
    public bool DialogueBeforeBattleEnabled;
    public string DialogueBeforeBattle;
    public UnityEvent OnBattleStart;
  }

  public void StartBossIntro( int index )
  {
    BossBattleConfig bc = BossConfig[index];
    // PreConversation() is called from StartConversation() but we want it to happen now.
    ((PlayerBiped) Global.instance.CurrentPlayer).PreConversation();
    Global.instance.MusicCrossFadeTo( bc.BossIntroMusic, 1 );
    SpriteRenderer[] srs = bc.Boss.GetComponentsInChildren<SpriteRenderer>();
    if( bc.FadeIn )
    {
      // fade in all renderers
      foreach( var sr in srs )
        sr.color = new Color( 1, 1, 1, 0 );
    }
    bossintroTimer.Start( this, bc.bosswait, ( timer ) =>
    {
      if( bc.FadeIn )
      {
        foreach( var sr in srs )
          sr.color = new Color( 1, 1, 1, timer.ProgressNormalized );
      }
    }, () =>
    {
      Global.instance.MusicCrossFadeTo( null, 2 );
      if( bc.DialogueBeforeBattleEnabled )
      {
        if( Global.instance.StartConversation(
          new[] {bc.Boss.GetComponent<ITalker>(), Global.instance.CurrentPlayer.GetComponent<ITalker>()},
          bc.DialogueBeforeBattle,
          false,
          () =>
          {
            // SpeakCancel() enables the biped actions, so disable again here 
            Global.instance.Controls.BipedActions.Disable();
            StartBossBattle( bc );
          } ) )
          return;
      }
      else
        StartBossBattle( bc );
    } );
  }

  void StartBossBattle( BossBattleConfig bc )
  {
    Global.instance.Controls.BipedActions.Aim.Enable();
    Global.instance.Controls.BipedActions.NextAbility.Enable();
    Global.instance.Controls.BipedActions.NextWeapon.Enable();
    bossintroTimer.Start( this, 2, null, () =>
    {
      Global.instance.Controls.BipedActions.Enable();
      bc.Boss.enabled = true;
      Global.instance.PlayMusic( bc.BattleMusic );
      bc.OnBattleStart.Invoke();
    } );
  }

  public void FadeOutMusic( float duration ) { Global.instance.MusicCrossFadeTo( null, duration ); }
  
  public void TurnOffDynamicSpawn()
  {
    // this function is for scene trigger convenience
    DynamicSpawnEnabled = false;
  }
}