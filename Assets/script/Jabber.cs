﻿using UnityEngine;
using System.Collections.Generic;

// gibberish similar to these games: Okami, Undertale, Animal Crossing

// per-clip offset
[System.Serializable]
public class PhonemeClip
{
  public bool UseClipLength = false;
  public bool UseOffset = true;
  public float Offset;
  public AudioClip clip;
}

[System.Serializable]
public class Phoneme
{
  public string PatternLetters;
  public bool isDefault;
  public List<PhonemeClip> phoclips;
}
  
[CreateAssetMenu()]
public class Jabber : ScriptableObject
{
  public int LettersPerPhoneme = 3;
  public float PhonemeLength = 0.1f;
  public float SilenceLength = 0.1f;

  public bool EnablePitchVariation = false;
  [Range(-1,1)]
  public float PitchOffset = 0;
  public bool PitchSine = false;
  [Range(0,0.5f)]
  public float PitchSineRange = 0.1f;
  [Range(0.01f,5)]
  public float PitchSinSpeed = 1;
  public bool PitchRandom = true;
  [Range(0,0.5f)]
  public float PitchRandomRange = 0.1f;

  public bool RandomPhoneme = false;
  public List<Phoneme> Phonemes;
}
