﻿using System.Collections.Generic;
using Malee.List;
using UnityEngine;
using UnityEngine.AI;

// NOTE Only supports horizontally-chained zone chunks. Chunks must be a uniform size.
// DONE move dynamic objects, too
// DONE support layers for navmesh in citywrap
// todo Minimap
// todo particle systems' transform setting should be local to origin root transform
// DONE: Walkers segments
// DONE: navmesh needs to regenerate after each chunk change
// DONE: Nodes need to update based on focus position, not if it hits the borders.

[System.Serializable]
public class ZoneChunk
{
  public string name;
  public GameObject Parent;
  public GameObject[] OtherLayers;
}

[System.Serializable]
public class ZoneChunkList : ReorderableArray<ZoneChunk> { }

public class CityWrap : SceneScript
{
  public Transform focus;
  [Reorderable] public ZoneChunkList chunks;
  public Vector2 chunkSize = new Vector2( 30, 10 );

  Bounds bo;
  [System.NonSerialized]
  public GameObject navmeshObject;
  
  const int MaxColliderCountPerQuery = 1000;
  Collider2D[] colliders = new Collider2D[MaxColliderCountPerQuery];
  List<Bounds> debugBounds = new List<Bounds>();

  public override void StartScene()
  {
    base.StartScene();
    GenerateAllLayerNavMesh( new Vector2( chunkSize.x * 3, chunkSize.y ) );
    if( focus == null )
      focus = Global.instance.CameraController.transform;
    // Get the navmesh surface object to move around as the city wraps
    navmeshObject = Global.instance.GetNavMeshObject();
    NavMeshSurface[] surfaces = navmeshObject.GetComponents<NavMeshSurface>();
    foreach( var surface in surfaces )
      surface.BuildNavMesh();
  }

  int ModIndex( int index )
  {
    int ugh = index;
    while( ugh < 0 )
      ugh += chunks.Length;
    return ugh % chunks.Length;
  }

  public override void UpdateScene()
  {
    float sizex = Global.instance.UpdateCullDistance * 2;
    bo = new Bounds( new Vector3( focus.position.x, focus.position.y, 0 ), new Vector3( sizex, sizex, 0 ) );
    Popcron.Gizmos.Square( bo.center, bo.size, Color.yellow );

    // relocate a range of chunks
    bool relocate = false;
    int minx = Mathf.FloorToInt( bo.min.x / chunkSize.x );
    int maxx = Mathf.FloorToInt( bo.max.x / chunkSize.x );
    for( int i = minx; i <= maxx; i++ )
    {
      int modIndex = ModIndex( i );
      Vector3 oldPos = chunks[modIndex].Parent.transform.position;
      Vector3 newPos = new Vector3( chunkSize.x * i, 0, 0 );
      if( Vector3.SqrMagnitude( oldPos - newPos ) > 1 )
      {
        chunks[modIndex].Parent.transform.position = newPos;
        chunks[modIndex].Parent.SetActive( true );
        relocate = true;
        // keep the other gameplay layer parents aligned with the zone parent.
        for( int j = 0; j < chunks[modIndex].OtherLayers.Length; j++ )
        {
          Vector2 pos = chunks[modIndex].OtherLayers[j].transform.position;
          pos.x = newPos.x;
          chunks[modIndex].OtherLayers[j].transform.position = pos;
        }
        // move all dynamic/root objects with a rectangle to the new location
        MoveObjectsToNewZone( oldPos, newPos );
      }
      if( relocate )
        navmeshObject.transform.position = new Vector3( chunkSize.x * (Mathf.Floor( focus.position.x / chunkSize.x ) - 1), 0, 0 );
    }
#if UNITY_EDITOR    
    foreach( var bound in debugBounds )
      Popcron.Gizmos.Square( bound.center, bound.size, Color.blue );
#endif    
  }
  
  void MoveObjectsToNewZone( Vector2 oldPos, Vector2 newPos )
  {
    Bounds moveBounds = new Bounds();
    moveBounds.SetMinMax( new Vector3( oldPos.x, -Global.GameplayLayerOffset * 0.5f, 0 ), new Vector3( oldPos.x + chunkSize.x, Global.GameplayLayerOffset * 0.5f, 0 ) );
#if UNITY_EDITOR
    debugBounds.Add( moveBounds );
    new Timer( this, 2, null, () => { debugBounds.Remove( moveBounds ); } );
#endif
    Vector3 delta = newPos - oldPos;
    int count = Physics2D.OverlapAreaNonAlloc( oldPos, oldPos + Vector2.right * chunkSize.x + Vector2.up * chunkSize.y, colliders );
    for( int i = 0; i < count; i++ )
    {
      if( colliders[i].transform == colliders[i].transform.root )
        colliders[i].transform.position = colliders[i].transform.position + delta;
    }
    if( count == MaxColliderCountPerQuery )
      MoveObjectsToNewZone( oldPos, newPos );
  }

  public override Vector3 FindSpawnPosition()
  {
    // wrap spawn position to avoid "walking" the city around
    Vector3 pos = base.FindSpawnPosition();
    int xIndex = Mathf.FloorToInt( pos.x / chunkSize.x );
    Vector3 offset = pos - new Vector3( chunkSize.x * xIndex, 0, 0 );
    return new Vector3( chunkSize.x * ModIndex( xIndex ), 0, 0 ) + offset;
  }
}