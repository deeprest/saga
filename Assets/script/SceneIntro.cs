﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Random = UnityEngine.Random;


public class SceneIntro : SceneScript
{
  [Header( "Scene Intro" )]
  public AudioLoop[] IntroMusic;
  public bool EnableKeyPressToLoadScene = true;
  bool introFlag = false;
  [SerializeField] Animator animator;
  [SerializeField] SceneReference nextScene;
  public Text titleText;
  public Text subtitleText;
  public SpriteRenderer[] titleGraphics;
  public Text[] titleTexts;
  [SerializeField] bool RandomizeSeed = true;
  [SerializeField] Font[] fonts;

  int effectIndex;
  Timer changeEffectTimer = new Timer();
  Timer fadeTimer = new Timer();
  Timer rotationTimer = new Timer();
  Timer musicChangeTimer = new Timer();
  public ParticleSystem beans;
  public Camera effectCam;
  public float EffectChangeInterval = 7;
  public float RotationLerpDuration = 5;
  public float FadeDuration = 3;
  public float zoom = 0.03f;
  public float rotationAmount = 0.4f;

  string[] sponsoredby =
  {
    "Sponsored by",
    "Made in partnership with",
    "Brought to you by",
    "Produced in cooperation with",
    "A promotional game created by"
  };
  string[] partOf =
  {
    "a division of {0} {1}®.",
    "a wholly-owned subsidiary of {0} {1}®.",
    "a {0}® brand.",
    "of the {0}® family of brands.",
    "and in association with {0}®"
  };
  string[] worldwide =
  {
    "Worldwide",
    "Solutions",
    "Finance",
    "Bank and Trust",
    "Dilly Dilly",
    "Introspections"
  };
  string[] allaboutthebeans = {"An Endless Supply of", "Seriously, Enough About", "What's With", "Mystery of", "The Secret of", "Legend of", "Tell Me About", "Primarily Concerning", "All About", "With Regard To", "Can We Focus On", "Have You Forgotten About", "What About"};

  public Color[] beanColors = new Color[]
  {
    new Color( 1, 1, 1, 1 ),
    new Color( 0, 0, 0, 1 ),
    new Color( 1, 0.2914167f, 0.227f, 1 )
  };
  Color clear = new Color( 1, 1, 1, 0 );

  public override void StartScene()
  {
    Global.instance.Controls.GlobalActions.StartGame.performed += StopIntro;
    if( RandomizeSeed )
      Random.InitState( DateTime.Now.Second );
    animator.Play( "intro" );
    int[] musicIndex = Util.ShuffledIndexList( IntroMusic.Length );
    int musicShuffleIndex = 0;
    Global.instance.PlayMusic( IntroMusic[musicIndex[musicShuffleIndex]] );
    musicChangeTimer = new Timer( this, 60, 6, ( timer ) =>
    {
      musicShuffleIndex = (musicShuffleIndex + 1) % IntroMusic.Length;
      Global.instance.MusicCrossFadeTo( IntroMusic[musicIndex[musicShuffleIndex]], 3 );
    }, null );
    Global.instance.HideHUD();
    // Ignore the camera zoom setting
    Camera.main.orthographic = true;
    Camera.main.orthographicSize = 4f - zoom;
    Global.instance.CameraController.orthoTarget = Camera.main.orthographicSize;

    titleText.font = fonts[Random.Range( 0, fonts.Length )];
    titleText.material = titleText.font.material;
    RestaurantNamegen.Initialize( DateTime.Now.Second );
    string template = partOf[Random.Range( 0, partOf.Length - 1 )];
    subtitleText.text = sponsoredby[Random.Range( 0, sponsoredby.Length - 1 )] + " " + RestaurantNamegen.GenName() + "™, " + string.Format( template, RestaurantNamegen.GenName(), worldwide[Random.Range( 0, worldwide.Length )] );
    subtitleText.material = subtitleText.font.material;

    foreach( var tg in titleTexts )
      tg.color = clear;
    foreach( var tg in titleGraphics )
      tg.color = clear;
    titleText.gameObject.SetActive( true );
    fadeTimer.Start( this, FadeDuration,
      delegate( Timer timer )
      {
        foreach( var tg in titleTexts )
          tg.color = Color.Lerp( clear, Color.white, timer.ProgressNormalized );
        foreach( var tg in titleGraphics )
          tg.color = Color.Lerp( clear, Color.white, timer.ProgressNormalized );
      }, () =>
      {
        foreach( var tg in titleTexts )
          tg.color = Color.white;
        foreach( var tg in titleGraphics )
          tg.color = Color.white;
      } );
    changeEffectTimer.Start( this, 6, null, ChangeEffect );
    effectCam.orthographicSize = 3.97f;
  }

  void ChangeFont()
  {
    titleText.font = fonts[Random.Range( 0, fonts.Length )];
    titleText.material = titleText.font.material;
  }

  void ChangeEffect()
  {
    if( !titleText.gameObject.activeSelf )
    {
      titleText.text = allaboutthebeans[Random.Range( 0, allaboutthebeans.Length )] + " The Beans";
      foreach( var tg in titleTexts )
      {
        tg.gameObject.SetActive( true );
        tg.color = clear;
      }
      foreach( var tg in titleGraphics )
      {
        tg.gameObject.SetActive( true );
        tg.color = clear;
      }
      fadeTimer.Start( this, FadeDuration,
        delegate( Timer timer )
        {
          foreach( var tg in titleTexts )
            tg.color = Color.Lerp( clear, Color.white, timer.ProgressNormalized );
          foreach( var tg in titleGraphics )
            tg.color = Color.Lerp( clear, Color.white, timer.ProgressNormalized );
        }, () =>
        {
          foreach( var tg in titleTexts )
            tg.color = Color.white;
          foreach( var tg in titleGraphics )
            tg.color = Color.white;
        } );
      ChangeFont();
    }
    else
    {
      ParticleSystem.MainModule main = beans.main;
      ParticleSystem.MinMaxGradient mainColor = main.startColor;
      mainColor.color = beanColors[Random.Range( 0, beanColors.Length )];
      main.startColor = mainColor;

      effectCam.transform.localPosition = new Vector3( (Random.value * 2 - 1) * 0.03f, (Random.value * 2 - 1) * 0.02f, -10 );

      int random = Random.Range( 0, 3 );
      if( random == 0 )
        effectCam.orthographicSize = 4f + zoom;
      else
        effectCam.orthographicSize = 4f - zoom;

      Quaternion startRot = effectCam.transform.rotation;
      Quaternion targetRot = default;
      if( random == 0 )
        targetRot = Quaternion.Euler( 0, 0, Random.Range( 0, 2 ) > 0 ? rotationAmount : -rotationAmount );
      else
        targetRot = Quaternion.Euler( 0, 0, 0 );
      rotationTimer.Start( this, RotationLerpDuration, timer => { effectCam.transform.rotation = Quaternion.Lerp( startRot, targetRot, timer.ProgressNormalized ); }, null );
      /*if( random == 0 )
        effectCam.transform.rotation = Quaternion.Euler( 0, 0, Random.Range( 0, 2 ) > 0 ? 0.4f : -0.4f );
      else
        effectCam.transform.rotation = Quaternion.Euler( 0, 0, 0 );*/
      foreach( var tg in titleTexts )
      {
        tg.transform.rotation = Quaternion.Euler( 0, 0, targetRot.eulerAngles.z * 2f );
        tg.gameObject.SetActive( false );
      }
      foreach( var tg in titleGraphics )
      {
        tg.transform.rotation = Quaternion.Euler( 0, 0, targetRot.eulerAngles.z * 2f );
        tg.gameObject.SetActive( false );
      }
    }
    changeEffectTimer.Start( this, EffectChangeInterval, null, ChangeEffect );
  }

  private void OnDestroy()
  {
    Global.instance.Controls.GlobalActions.Any.performed -= StopIntro;
    Global.instance.CameraController.orthoTarget = Global.instance.FloatSetting["Zoom"].Value;
    changeEffectTimer.Stop( false );
    rotationTimer.Stop( false );
    fadeTimer.Stop( false );
  }

  void StopIntro( InputAction.CallbackContext ctx )
  {
    if( !Application.isEditor || EnableKeyPressToLoadScene )
      StopIntro();
  }

  public void StopIntro()
  {
    if( introFlag )
      return;
    introFlag = true;
    
    // stop the music from changing, but do not stop the music, let the scene change make it fade out.
    musicChangeTimer.Stop( false );
    // do not let the timer die during scene change
    fadeTimer.Start( Global.instance, new TimerParams() {duration = 1, unscaledTime = true, UpdateDelegate = delegate( Timer timer ) { subtitleText.color = Color.Lerp( Color.white, Color.clear, timer.ProgressNormalized ); }, CompleteDelegate = () => { subtitleText.gameObject.SetActive( false ); }} );
    Global.instance.LoadScene( nextScene, false, true, true );
  }
}