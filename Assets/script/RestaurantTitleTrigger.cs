using UnityEngine;
using UnityEngine.UI;

public class RestaurantTitleTrigger : MonoBehaviour, ITrigger
{
  public bool once;
  bool triggered;
  public RestaurantNamegen.RestaurantTitle info;
  public float repeatDelay = 10;
  Timer delayTimer = new Timer();
  [SerializeField] Text inWorldTitle;

  public void Trigger( Collider2D trigger, Transform instigator )
  {
    if( instigator.root != Global.instance.PlayerController.pawn.transform.root )
      return;
    if( !once || (once && !triggered) )
    {
      if( !delayTimer.IsActive )
      {
        triggered = true;
        delayTimer.Start( this, repeatDelay );
        if( Global.instance.sceneScript != null && Global.instance.sceneScript is SceneCity )
          ((SceneCity)Global.instance.sceneScript).TitleDisplay( transform.position, info );
        else
          Debug.LogWarning( "Title Trigger cannot find a scene script." );
      }
    }
  }

  public void AssignInfo( RestaurantNamegen.RestaurantTitle nfo )
  {
    info = nfo;
    inWorldTitle.font = nfo.font;
    inWorldTitle.text = nfo.title;
    inWorldTitle.color = nfo.color;
  }
}