﻿using UnityEngine;

//[CreateAssetMenu]
public class AnimatedController : Controller
{
  public override void AssignPawn( Pawn pwn )
  {
    base.AssignPawn( pwn );
    (pawn as PlayerBiped).ResetInputOnLateUpdate = true;
    Global.instance.CameraController.LookTarget = this;
  }

  public override void LateUpdate()
  {
    pawn.ApplyInput( input );
    input = default;
  }
}