using UnityEngine;

[CreateAssetMenu(fileName ="WeaponMod_1", menuName="WeaponMod/1")]
public class WeaponMod_1 : WeaponMod
{
  [WeaponStat] public int ProjectileCount;
  [WeaponStat] public int ShotSpread;

  /*public override bool CanApplyMod( WeaponInstance weapon, ref string message )
  {
    bool retval = base.CanApplyMod( weapon, ref message );
    if( !retval )
      return retval;
    return true;
  }*/
  
  public override void ApplyMod( Weapon weapon )
  {
    base.ApplyMod( weapon );
    weapon.def.ProjectileCount = ProjectileCount;
    weapon.def.ShotSpread = ShotSpread;
  }
}