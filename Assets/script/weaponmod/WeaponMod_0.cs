using UnityEngine;

[CreateAssetMenu( fileName = "WeaponMod_0", menuName = "WeaponMod/0" )]
public class WeaponMod_0 : WeaponMod
{
  [WeaponStat( "Added Projectiles" )] public int ProjectileCount;
  [WeaponStat] public int ShotSpread;

  public override bool CanApplyMod( Weapon weapon, ref string message )
  {
    if( weapon.def.weaponCategory == WeaponDefinition.WeaponCategory.Melee || weapon.def.weaponCategory == WeaponDefinition.WeaponCategory.Laser )
    {
      message = "Incompatible weapon.";
      return false;
    }
    if( weapon.mods != null && weapon.mods.Contains( this ) )
    {
      message = "Mod is already applied.";
      return false;
    }
    return true;
  }

  public override void ApplyMod( Weapon weapon )
  {
    base.ApplyMod( weapon );
    weapon.def.ProjectileCount = ProjectileCount;
    weapon.def.ShotSpread = ShotSpread;
  }
}