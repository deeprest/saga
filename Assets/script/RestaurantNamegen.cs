using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(RestaurantNamegen) )]
public class RestaurantNamegenEditor : Editor
{
  public override void OnInspectorGUI()
  {
    var obj = target as RestaurantNamegen;

    if( GUILayout.Button( "Initialize" ) )
      obj.Initialize( obj.seed );
    if( GUILayout.Button( "Gen Name" ) )
      obj.genname = obj.GenName();
    if( GUILayout.Button( "Gen Object" ) )
      Selection.activeGameObject = obj.GenerateGameObject( Random.Range( 0, obj.fonts.Length ) );
    if( GUILayout.Button( "Gen All" ) )
    {
      obj.DeleteAll();
      obj.Initialize( obj.seed );
      obj.GenerateList( obj.fonts.Length, Vector3.zero );
    }
    if( GUILayout.Button( "Gen +" ) )
    {
      obj.seed++;
      obj.DeleteAll();
      obj.Initialize( obj.seed );
      obj.GenerateList( obj.fonts.Length, Vector3.zero );
    }
    if( GUILayout.Button( "Delete" ) )
      obj.DeleteAll();
    DrawDefaultInspector();
  }
}
#endif


[CreateAssetMenu( fileName = "namegen", menuName = "generator", order = 0 )]
public class RestaurantNamegen : ScriptableObject
{
  [ReadOnly]
  public string genname;
  public int seed;
  public Font[] fonts;
  public GameObject prefab;
  public Color[] colors = new[] {Color.white, Color.red, Color.yellow};
  public List<GameObject> gens = new List<GameObject>();

  // possesive or not
  [System.NonSerialized] string[] OwnerAdjective =
  {
    "Trustworthy", "Slippery", "Big", "Delicious", "Shiny", "Sassy", "Bubbly", "Mysterious", "Greasy", "Obnoxious",
    "Polite", "Baby", "Papa", "Ms."
  };
  [System.NonSerialized] string[] Owner =
    {"Jerry", "Martha", "Reginald", "Susie", "Stan", "Willy", "Gino", "Sally", "Mike", "Bubba"};

  // adjectives optional. Might include "of"
  [System.NonSerialized] string[] PlaceAdjective =
  {
    "Cozy", "Dank", "Legitimate", "Golden", "Brown", "Shiny", "Dreamy", "Red", "Formidable", "Strong", "Mysterious",
    "Imperial", "Filthy", "Affordable", "Fantastic", "Nearby", "Sticky"
  };
  [System.NonSerialized] string[] Place =
  {
    "Restaurant", "Beanery", "Bistro", "Cafe", "Eatery", "Canteen", "Tavern", "Bungalow", "Saloon", "Hideout", "Place",
    "House", "Shack", "Hut", "Pit", "Emporium", "Hole", "Barrel", "Domain", "Edifice", "Castle", "Temple", "Factory",
    "Palace", "Diner", "Buffet", "Grill", "Garden"
  };

  // could have multiple "Steak and Shake"
  [System.NonSerialized] string[] FoodAdjective =
  {
    "Slippery", "Golden", "Tasty", "Shiny", "Spicy", "Crunchy", "Potent", "Wiggly", "Bouncy", "Gooey", "Mind-Blowing"
  };
  [System.NonSerialized] string[] Food =
  {
    "Bean", "Noodle", "Cuisine", "Taco", "Burger", "Sandwich", "Pancake", "Pizza", "Lemonade", "Waffle", "Cracker",
    "Nugget", "Food", "Edible", "Stuff"
  };

  public enum FormatType
  {
    // Carl's
    Owners,
    // Pizza Pit
    FoodPlace,
    // Greasy Nugget Palace
    AdjectiveFoodPlace,
    // Golden Corral
    AdjectivePlace,
    // Susie's Shack
    OwnersPlace,
    // Filthy Carl's Beanery
    AdjectiveOwnersPlace,
    // Jerry's Emporium of Waffles
    OwnersPlaceOfFoods,
    // Greasy Willy's Barrel of Tacos
    AdjectiveOwnersPlaceOfFoods,
    // Greasy Willy's Taco Barrel
    AdjectiveOwnersFoodPlace,
    // Big Ben's Slippery Bean Bistro
    AdjectiveOwnersAdjectiveFoodPlace
  }

  [System.NonSerialized] FormatType format;

  int iOwner;
  int[] ownerIndex;
  int iPlace;
  int[] placeIndex;
  int iFood;
  int[] foodIndex;
  int iOwnerAdjective;
  int[] ownerAdjectiveIndex;
  int iFoodAdjective;
  int[] foodAdjectiveIndex;
  int iPlaceAdjective;
  int[] placeAdjectiveIndex;

  public void OnEnable() { Initialize( seed ); }

  public void Initialize( int Seed )
  {
    if( formatWeights == null || formatWeights.Count != System.Enum.GetValues( typeof(FormatType)).Length )
    {
      formatWeights = new List<FormatWeight>();
      FormatType[] types = (FormatType[]) System.Enum.GetValues( typeof(FormatType) );
      string[] names = System.Enum.GetNames( typeof(FormatType) );
      for( int i = 0; i < names.Length; i++ )
        formatWeights.Add( new FormatWeight() {type = types[i], weight = 1} );
    }
    CalculateFormatWeights();
    
    Random.InitState( Seed );

    iOwner = 0;
    ownerIndex = Util.ShuffledIndexList( Owner.Length );
    iPlace = 0;
    placeIndex = Util.ShuffledIndexList( Place.Length );
    iFood = 0;
    foodIndex = Util.ShuffledIndexList( Food.Length );
    iOwnerAdjective = 0;
    ownerAdjectiveIndex = Util.ShuffledIndexList( OwnerAdjective.Length );
    iFoodAdjective = 0;
    foodAdjectiveIndex = Util.ShuffledIndexList( FoodAdjective.Length );
    iPlaceAdjective = 0;
    placeAdjectiveIndex = Util.ShuffledIndexList( PlaceAdjective.Length );
  }

  public string GenName()
  {
    //format = (FormatType) Random.Range( 0, System.Enum.GetNames( typeof(FormatType) ).Length );
    format = GetRandomFormatWeighted();
    string owner = Owner[ownerIndex[iOwner % ownerIndex.Length]];
    string ownerAdjective = OwnerAdjective[ownerAdjectiveIndex[iOwnerAdjective % ownerAdjectiveIndex.Length]];
    string placeAdjective = PlaceAdjective[placeAdjectiveIndex[iPlaceAdjective % placeAdjectiveIndex.Length]];
    string foodAdjective = FoodAdjective[foodAdjectiveIndex[iFoodAdjective % foodAdjectiveIndex.Length]];
    string place = Place[placeIndex[iPlace % placeIndex.Length]];
    string food = Food[foodIndex[iFood % foodIndex.Length]];
    string foodPlural = food + "s";
    if( food.EndsWith( "ch" ) | food.EndsWith( "sh" ) | food.EndsWith( "s" ) | food.EndsWith( "x" ) |
      food.EndsWith( "z" ) )
      foodPlural = food + "es";

    if( format == FormatType.Owners )
    {
      genname = owner + "'s";
      iOwner++;
    }
    if( format == FormatType.OwnersPlace )
    {
      genname = string.Join( " ", owner + "'s", place );
      iOwner++;
      iPlace++;
    }
    if( format == FormatType.OwnersPlaceOfFoods )
    {
      genname = string.Join( " ", owner + "'s", place + " of", foodPlural );
      iOwner++;
      iPlace++;
      iFood++;
    }
    if( format == FormatType.AdjectiveOwnersPlaceOfFoods )
    {
      genname = string.Join( " ", ownerAdjective, owner + "'s", place + " of", foodPlural );
      iOwnerAdjective++;
      iOwner++;
      iPlace++;
      iFood++;
    }
    if( format == FormatType.FoodPlace )
    {
      genname = string.Join( " ", food, place );
      iPlace++;
      iFood++;
    }
    if( format == FormatType.AdjectiveFoodPlace )
    {
      genname = string.Join( " ", foodAdjective, food, place );
      iPlace++;
      iFood++;
      iFoodAdjective++;
    }
    if( format == FormatType.AdjectiveOwnersFoodPlace )
    {
      genname = string.Join( " ", ownerAdjective, owner + "'s", food, place );
      iOwnerAdjective++;
      iOwner++;
      iFood++;
      iPlace++;
    }
    if( format == FormatType.AdjectivePlace )
    {
      genname = string.Join( " ", placeAdjective, place );
      iPlaceAdjective++;
      iPlace++;
    }
    if( format == FormatType.AdjectiveOwnersPlace )
    {
      genname = string.Join( " ", ownerAdjective, owner + "'s", place );
      iOwnerAdjective++;
      iOwner++;
      iPlace++;
    }
    if( format == FormatType.AdjectiveOwnersAdjectiveFoodPlace )
    {
      genname = string.Join( " ", ownerAdjective, owner + "'s", foodAdjective, food, place );
      iOwnerAdjective++;
      iOwner++;
      iFoodAdjective++;
      iFood++;
      iPlace++;
    }

    return genname;
  }

  public void GenerateList( int count, Vector3 origin )
  {
    for( int i = 0; i < count; i++ )
    {
      GameObject go = GenerateGameObject( i );
      go.transform.position = origin + new Vector3( 0, -0.5f * i, 0 );
    }
  }

  [System.Serializable]
  public struct RestaurantTitle
  {
    public string title;
    public Color color;
    public Font font;
  }

  public RestaurantTitle GenerateRestaurantTitle()
  {
    RestaurantTitle rti = new RestaurantTitle();
    rti.title = GenName();
    rti.font = fonts[Random.Range( 0, fonts.Length )];
    rti.color = colors[Random.Range( 0, colors.Length )];
    return rti;
  }

  public GameObject GenerateGameObject( RestaurantTitle rti )
  {
    GameObject go = Instantiate( prefab );
    TextMesh txt = go.GetComponentInChildren<TextMesh>();
    txt.text = rti.title;
    txt.font = rti.font;
    txt.color = rti.color;
    go.GetComponent<MeshRenderer>().sharedMaterial = txt.font.material;
    gens.Add( go );
    return go;
  }


  public GameObject GenerateGameObject( int fontIndex )
  {
    string genName = GenName();
    GameObject go = Instantiate( prefab );
    TextMesh txt = go.GetComponentInChildren<TextMesh>();
    txt.text = genName;
    txt.font = fonts[fontIndex];
    txt.color = colors[Random.Range( 0, colors.Length )];
    go.GetComponent<MeshRenderer>().sharedMaterial = txt.font.material;
    gens.Add( go );
    return go;
  }

  public void DeleteAll()
  {
    foreach( var gameObject in gens )
      Util.Destroy( gameObject );
    gens.Clear();
  }

  [System.Serializable]
  public struct FormatWeight
  {
    public FormatType type;
    public float weight;
  }

  public List<FormatWeight> formatWeights; // = new List<FormatWeight>();
  float totalWeight;

  void CalculateFormatWeights()
  {
    totalWeight = 0;
    for( int i = 0; i < formatWeights.Count; i++ )
      totalWeight += formatWeights[i].weight;
  }

  FormatType GetRandomFormatWeighted()
  {
    float randy = Random.Range( 0, totalWeight );
    float runningTotal = 0;
    for( int i = 0; i < formatWeights.Count; i++ )
    {
      runningTotal += formatWeights[i].weight;
      if( runningTotal > randy )
        return formatWeights[i].type;
    }
    return formatWeights[0].type;
  }
}