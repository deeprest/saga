using UnityEngine;

public enum PriorityEnum
{
  Mandatory = 0,
  Conversation,
  Remark
}


[System.Serializable]
public class ConvoDocument
{
  public string[] random;
  public Conversation[] conversations;
}

[System.Serializable]
public class Conversation
{
  public string id;
  public int participants;
  public int lettersPerSecond = 15;
  public DialogueLine[] dialogue;
}

[System.Serializable]
public class DialogueLine
{
  public int id;
  [TextArea(1,4)]
  public string say;
  public float wait;
}

/*public class StringListAttribute : PropertyAttribute
{
  public string listName;
  public StringListAttribute(string listName)
  {
    this.listName = listName;
  }
}*/
