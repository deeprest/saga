﻿using System.Collections.Generic;
using UnityEngine;

// DONE rotation experiment stuff
// DONE minimap render must go back to ortho

public class CameraController : MonoBehaviour
{
  public Camera cam;
  public Controller LookTarget;

  public bool UseVerticalRange = true;
  public bool CursorInfluence;
  public float cursorAlpha = 0.5f;
  public float lerpAlpha = 50;
  public float zOffset;
  public float yHalfWidth = 2;
  public float orthoTarget = 1;
  public float orthoSpeed = 1;
  [SerializeField] CameraZone ActiveCameraZone;
  public bool CameraZoneOverride;
  public bool SMOOTH;

  // lerp target position
  public Vector2 pos;
  Vector2 aimOffset;
  float ortho;
  bool lerping;
  Timer lerpTimer = new Timer();

  public float RotationAngleLimit = 15;
  bool rotationExperiment;

  public bool RotationExperiment
  {
    get { return rotationExperiment; }
    set
    {
      rotationExperiment = value;
      cam.orthographic = !value;
      cam.transform.rotation = Quaternion.identity;
    }
  }

  public void PreSceneTransition() { }
  public void PostSceneTransition() { }

  public void AssignOverrideCameraZone( CameraZone zone )
  {
    ActiveCameraZone = zone;
    CameraZoneOverride = zone != null;
  }

  public CameraZone GetActiveZone() { return ActiveCameraZone; }

  public void Lerp( Vector3 targetPosition, float orthoTarget, float duration, bool constrain = true,
    System.Action whenDone = null )
  {
    float startOrtho = cam.orthographicSize;
    Vector2 startPos = transform.position;
    ortho = orthoTarget;
    pos = targetPosition;
    // set the target values and advance the position and ortho values explicitly
    if( constrain )
      GetCameraTargetValues( ref pos, ref ortho, pos, ortho );
    TimerParams tp = new TimerParams()
    {
      unscaledTime = true,
      duration = duration,
      CompleteDelegate = () =>
      {
        lerping = false;
        whenDone?.Invoke();
      },
      UpdateDelegate = ( Timer t ) =>
      {
        cam.orthographicSize = Mathf.Lerp( startOrtho, ortho, t.ProgressNormalized );
        Vector3 lerp = Vector3.Lerp( startPos, pos, t.ProgressNormalized );
        lerp.z = zOffset;
        transform.position = lerp;
      }
    };
    lerping = true;
    lerpTimer.Start( this, tp );
  }

  public void Teleport()
  {
    if( LookTarget != null && LookTarget.pawn != null )
      pos = LookTarget.pawn.transform.position;
    else
      pos = Vector3.zero;
    transform.position = new Vector3( pos.x, pos.y, zOffset );
    GetCameraTargetValues( ref pos, ref ortho, false );
    cam.orthographicSize = ortho;
    transform.position = new Vector3( pos.x, pos.y, zOffset );
  }

  public void CameraLateUpdate()
  {
    if( !enabled )
      return;
    if( !lerping )
    {
      ortho = orthoTarget;
      float DT = Mathf.Min( 0.015f, Time.unscaledDeltaTime );

      if( LookTarget == Global.instance.PlayerController && RotationExperiment )
      {
        // TODO limit rotation or distance for perspective cameras
        // take desired rotation and project four points onto gameplay plane
        // clip points to bounds
        // IF new bounds is larger than desired then: derive focal point and rotation from clipped bounds
        // ELSE set rotation to zero and calculate Z distance

        GetCameraTargetValues( ref pos, ref ortho, true );
        Vector3 lookTarget = LookTarget.pawn.transform.position;
        lookTarget.z = zOffset;
        transform.position = lookTarget;

        Vector3 angles = Quaternion.LookRotation( (LookTarget.pawn.transform.position + (Vector3)aimOffset - transform.position).normalized, Vector3.up ).eulerAngles;
        angles.x = Mathf.Clamp( Util.NormalizeAngle( angles.x ), -RotationAngleLimit, RotationAngleLimit );
        angles.y = Mathf.Clamp( Util.NormalizeAngle( angles.y ), -RotationAngleLimit, RotationAngleLimit );
        cam.transform.rotation = Quaternion.Euler( angles );
      }
      else
      {
        // NOTE: Smoothing is undersirable when riding a carryObject/lift
        Vector2 smooth = pos;
        GetCameraTargetValues( ref smooth, ref ortho, CursorInfluence );
        cam.orthographicSize = Mathf.Lerp( cam.orthographicSize, ortho, orthoSpeed * DT );
        if( SMOOTH && Vector2.Distance( pos, smooth ) > SmoothThreshold )
          pos = Vector2.Lerp( pos, smooth, SmoothSpeed * Time.deltaTime );
        else
          pos = smooth;

        Vector3 convert = pos;
        convert.z = zOffset;
        transform.position = convert;
      }
    }
  }

  // NOTE: Smoothing is undersirable when riding a carryObject/lift
  public float SmoothThreshold = 1;
  public float SmoothSpeed = 10;

  void GetCameraTargetValues( ref Vector2 pos, ref float ortho, bool cursorInfluence )
  {
    if( LookTarget != null && LookTarget.pawn != null )
    {
      Vector2 lookTarget = LookTarget.pawn.transform.position;
      if( cursorInfluence )
      {
        aimOffset = Vector2.Lerp( aimOffset, LookTarget.pawn.inputAim * cursorAlpha, lerpAlpha * Time.unscaledDeltaTime );
        lookTarget = (Vector2)LookTarget.pawn.transform.position + aimOffset;
      }

      // TODO?  THIs ONLY WORKS WITH CAMERA COLLIDERS, NOT CAMERA ZONES. here: do a boxcast to desired position with the camera box before assigning pos to lookTarget?
      //Physics2D.BoxCast( (Vector2)LookTarget.pawn.transform.position, new Vector2( cam.orthographicSize, cam.orthographicSize * cam.aspect), 0, aimOffset, aimOffset.magnitude, Global. )
      // WE MUST PROJECT INTO EVERY CAMERA UNION POLYGON LINE BEFORE CLIPPING TO POLY
      pos.x = lookTarget.x;

      if( UseVerticalRange )
      {
        if( lookTarget.y > pos.y + yHalfWidth )
          pos.y = lookTarget.y - yHalfWidth;
        if( lookTarget.y < pos.y - yHalfWidth )
          pos.y = lookTarget.y + yHalfWidth;
      }
      else
      {
        pos.y = lookTarget.y;
      }
      GetCameraTargetValues( ref pos, ref ortho, LookTarget.pawn.transform.position, cam.orthographicSize );
    }
  }

  void GetCameraTargetValues( ref Vector2 pos, ref float ortho, Vector2 origin, float camOrthoSize )
  {
    // auto-switch to zone the player enters (respects the ignore flag on the zone)
    if( !CameraZoneOverride )
    {
      CameraZone zone = null;
      CameraZone.DoesOverlapAnyZone( origin, ref zone );
      ActiveCameraZone = zone;
    }

    if( ActiveCameraZone != null )
    {
      if( ActiveCameraZone.SetOrtho )
        ortho = ActiveCameraZone.orthoTarget;

      float hh, hw, xangle, yangle;

      if( cam.orthographic )
      {
        hh = camOrthoSize;
        hw = camOrthoSize * cam.aspect;
      }
      else
      {
        // draw gray lines showing camera rectangle
        yangle = Mathf.Deg2Rad * cam.fieldOfView * 0.5f;
        hh = Mathf.Tan( yangle ) * -transform.position.z;
        xangle = yangle * ((float)cam.pixelWidth / (float)cam.pixelHeight);
        hw = Mathf.Tan( xangle ) * -transform.position.z;
      }

      Vector2 debug = pos;

      if( ActiveCameraZone.EncompassBounds )
      {
        Bounds bounds = new Bounds();
        Vector2 center = Vector2.zero;
        for( int i = 0; i < ActiveCameraZone.colliders.Length; i++ )
          center += (Vector2)ActiveCameraZone.colliders[i].transform.position + ActiveCameraZone.colliders[i].offset;
        center /= ActiveCameraZone.colliders.Length;
        bounds.center = center;
        for( int i = 0; i < ActiveCameraZone.colliders.Length; i++ )
          bounds.Encapsulate( ActiveCameraZone.colliders[i].bounds );

        pos.x = bounds.center.x;
        pos.y = bounds.center.y;

        if( cam.orthographic )
        {
          if( bounds.extents.y > bounds.extents.x )
            ortho = bounds.extents.y;
          else
            ortho = bounds.extents.x / cam.aspect;
        }
        else
        {
          if( bounds.extents.y > bounds.extents.x )
            ortho = bounds.extents.y;
          else
            ortho = bounds.extents.x / cam.aspect;
          //pos.z = -Mathf.Max( bounds.extents.x / Mathf.Tan( 0.5f * cam.fieldOfView ), bounds.extents.y / Mathf.Tan( 0.5f * cam.fieldOfView / cam.aspect ) );
        }
        Popcron.Gizmos.Line( new Vector3( bounds.min.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.max.y ), Color.yellow );
        Popcron.Gizmos.Line( new Vector3( bounds.min.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.max.y ), Color.yellow );
        Popcron.Gizmos.Line( new Vector3( bounds.max.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.min.y ), Color.yellow );
        Popcron.Gizmos.Line( new Vector3( bounds.max.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.min.y ), Color.yellow );
      }
      else
      {
        if( CameraZoneOverride )
        {
          // if the origin(lookTarget) is outside of all the colliders, then clip to inside.
          Collider2D overlap = null;
          List<Vector2> points = new List<Vector2>();
          foreach( var cld in ActiveCameraZone.colliders )
          {
            if( cld.OverlapPoint( origin ) )
            {
              overlap = cld;
              break;
            }
            else
            {
              points.Add( cld.ClosestPoint( origin ) );
            }
          }
          if( overlap == null )
          {
            Vector2 neworigin = Util.FindClosest( origin, points.ToArray() );
            origin = neworigin + (neworigin - origin).normalized;
          }
        }

        if( ActiveCameraZone.FollowEdges )
        {
          List<Vector2> pots = new List<Vector2>();
          foreach( var cld in ActiveCameraZone.colliders )
          {
            if( cld is EdgeCollider2D )
              pots.Add( (cld as EdgeCollider2D).ClosestPoint( pos ) );
          }
          pos = Util.FindClosest( pos, pots.ToArray() );
        }
        else if( ActiveCameraZone.UsePolygonUnion )
        {
#region POLYGON_UNION

          // prevent multiple colliders within same zone from overriding clip points of one another by using a union poly.

          Vector2[] poly = ActiveCameraZone.union;
          Vector2 UL = (Vector2)pos + Vector2.left * hw + Vector2.up * hh;
          if( ClipToInsidePolygon( poly, ref UL, origin ) )
          {
            if( pos.y > UL.y - hh )
              pos.y = UL.y - hh;
            if( pos.x < UL.x + hw )
              pos.x = UL.x + hw;
          }

          Vector2 UR = (Vector2)pos + Vector2.right * hw + Vector2.up * hh;
          if( ClipToInsidePolygon( poly, ref UR, origin ) )
          {
            if( pos.y > UR.y - hh )
              pos.y = UR.y - hh;
            if( pos.x > UR.x - hw )
              pos.x = UR.x - hw;
          }

          Vector2 LL = (Vector2)pos + Vector2.left * hw + Vector2.down * hh;
          if( ClipToInsidePolygon( poly, ref LL, origin ) )
          {
            if( pos.y < LL.y + hh )
              pos.y = LL.y + hh;
            if( pos.x < LL.x + hw )
              pos.x = LL.x + hw;
          }

          Vector2 LR = (Vector2)pos + Vector2.right * hw + Vector2.down * hh;
          if( ClipToInsidePolygon( poly, ref LR, origin ) )
          {
            if( pos.y < LR.y + hh )
              pos.y = LR.y + hh;
            if( pos.x > LR.x - hw )
              pos.x = LR.x - hw;
          }

#endregion
        }
        else
        {
          foreach( var cld in ActiveCameraZone.colliders )
          {
            if( !CameraZoneOverride && !cld.OverlapPoint( origin ) )
              continue;
            Vector2 UL = (Vector2)pos + Vector2.left * hw + Vector2.up * hh;
            if( ClipToInsideCollider2D( cld, ref UL, origin ) )
            {
              if( pos.y > UL.y - hh )
                pos.y = UL.y - hh;
              if( pos.x < UL.x + hw )
                pos.x = UL.x + hw;
            }

            Vector2 UR = (Vector2)pos + Vector2.right * hw + Vector2.up * hh;
            if( ClipToInsideCollider2D( cld, ref UR, origin ) )
            {
              if( pos.y > UR.y - hh )
                pos.y = UR.y - hh;
              if( pos.x > UR.x - hw )
                pos.x = UR.x - hw;
            }

            Vector2 LL = (Vector2)pos + Vector2.left * hw + Vector2.down * hh;
            if( ClipToInsideCollider2D( cld, ref LL, origin ) )
            {
              if( pos.y < LL.y + hh )
                pos.y = LL.y + hh;
              if( pos.x < LL.x + hw )
                pos.x = LL.x + hw;
            }

            Vector2 LR = (Vector2)pos + Vector2.right * hw + Vector2.down * hh;
            if( ClipToInsideCollider2D( cld, ref LR, origin ) )
            {
              if( pos.y < LR.y + hh )
                pos.y = LR.y + hh;
              if( pos.x > LR.x - hw )
                pos.x = LR.x - hw;
            }
          }
        }

#if UNITY_EDITOR
        Vector3 cp3 = transform.position;
        cp3 = pos;
        Popcron.Gizmos.Line( new Vector3( -hw, -hh, 0 ) + cp3, new Vector3( -hw, hh, 0 ) + cp3, Color.green );
        Popcron.Gizmos.Line( new Vector3( -hw, hh, 0 ) + cp3, new Vector3( hw, hh, 0 ) + cp3, Color.green );
        Popcron.Gizmos.Line( new Vector3( hw, hh, 0 ) + cp3, new Vector3( hw, -hh, 0 ) + cp3, Color.green );
        Popcron.Gizmos.Line( new Vector3( hw, -hh, 0 ) + cp3, new Vector3( -hw, -hh, 0 ) + cp3, Color.green );
        cp3 = debug;
        Popcron.Gizmos.Line( new Vector3( -hw, -hh, 0 ) + cp3, new Vector3( -hw, hh, 0 ) + cp3, Color.blue );
        Popcron.Gizmos.Line( new Vector3( -hw, hh, 0 ) + cp3, new Vector3( hw, hh, 0 ) + cp3, Color.blue );
        Popcron.Gizmos.Line( new Vector3( hw, hh, 0 ) + cp3, new Vector3( hw, -hh, 0 ) + cp3, Color.blue );
        Popcron.Gizmos.Line( new Vector3( hw, -hh, 0 ) + cp3, new Vector3( -hw, -hh, 0 ) + cp3, Color.blue );

        if( cam.orthographic )
        {
          Bounds bounds = new Bounds( transform.position,
            new Vector3( cam.orthographicSize, cam.orthographicSize * cam.aspect, 0 ) );
          Popcron.Gizmos.Line( new Vector3( bounds.min.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.max.y ),
            Color.green );
          Popcron.Gizmos.Line( new Vector3( bounds.min.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.max.y ),
            Color.green );
          Popcron.Gizmos.Line( new Vector3( bounds.max.x, bounds.max.y ), new Vector3( bounds.max.x, bounds.min.y ),
            Color.green );
          Popcron.Gizmos.Line( new Vector3( bounds.max.x, bounds.min.y ), new Vector3( bounds.min.x, bounds.min.y ),
            Color.green );
        }

#endif
      }
    }
  }

  bool ClipToInsideCollider2D( Collider2D cld, ref Vector2 cp, Vector2 origin )
  {
    if( !cld.OverlapPoint( cp ) )
    {
      Vector2[] points = null;
      if( cld is PolygonCollider2D )
        points = (cld as PolygonCollider2D).points;
      else if( cld is BoxCollider2D )
      {
        BoxCollider2D box = cld as BoxCollider2D;
        points = new Vector2[4];
        points[0] = box.offset + (Vector2.left * box.size.x * 0.5f) + (Vector2.down * box.size.y * 0.5f);
        points[1] = box.offset + (Vector2.right * box.size.x * 0.5f) + (Vector2.down * box.size.y * 0.5f);
        points[2] = box.offset + (Vector2.right * box.size.x * 0.5f) + (Vector2.up * box.size.y * 0.5f);
        points[3] = box.offset + (Vector2.left * box.size.x * 0.5f) + (Vector2.up * box.size.y * 0.5f);
      }
      List<Vector2> pots = new List<Vector2>();
      // add transform position for world space
      for( int i = 0; i < points.Length; i++ )
        points[i] += (Vector2)cld.transform.position;
      for( int i = 0; i < points.Length; i++ )
      {
        int next = (i + 1) % points.Length;
        /*Vector2 intersection = cp;
        if( Util.LineSegmentsIntersectionWithPrecisonControl( points[i], points[next], origin, cp, ref intersection ) )
        {
          pots.Add( intersection );
          Popcron.Gizmos.Line( origin, intersection, Color.red );
        }*/
        Vector2 segment = points[next] - points[i];
        if( !Util.DoLinesIntersect( points[i].x, points[i].y, points[next].x, points[next].y, origin.x, origin.y, cp.x, cp.y ) )
          continue;
        Vector2 perp = new Vector2( -segment.y, segment.x );
        Popcron.Gizmos.Line( points[i], points[i] + perp, Color.blue );
        Vector2 projectionPerp = Util.Project2D( (cp - points[i]), perp.normalized );
        if( Vector2.Dot( perp.normalized, projectionPerp.normalized ) < 0 )
        {
          Popcron.Gizmos.Line( points[i], points[i] + projectionPerp, Color.red );
          Vector2 adjust = cp - projectionPerp;
          if( Vector2.Dot( segment.normalized, (adjust - points[i]).normalized ) > 0 )
          {
            if( (adjust - points[i]).magnitude > segment.magnitude )
              adjust = points[next];
          }
          else
            adjust = points[i];
          pots.Add( adjust );
        }
        else
          Popcron.Gizmos.Line( points[i], points[i] + projectionPerp, Color.magenta );
      }

      float distance = Mathf.Infinity;
      Vector2 closest = cp;
      foreach( var p in pots )
      {
        float dist = Vector2.Distance( p, origin );
        if( dist < distance )
        {
          closest = p;
          distance = dist;
        }
      }
      cp = closest;

      return true;
    }
    return false;
  }

  bool ClipToInsidePolygon( Vector2[] poly, ref Vector2 cp, Vector2 origin )
  {
    if( !Util.IsPointInPolygon( poly, cp ) )
    {
      Vector2[] points = poly;
      List<Vector2> pots = new List<Vector2>();
      for( int i = 0; i < points.Length; i++ )
      {
        int next = (i + 1) % points.Length;
        Vector2 segment = points[next] - points[i];
        if( !Util.DoLinesIntersect( points[i].x, points[i].y, points[next].x, points[next].y, origin.x, origin.y, cp.x, cp.y ) )
          continue;
        Vector2 perp = new Vector2( -segment.y, segment.x );
        Popcron.Gizmos.Line( points[i], points[i] + perp, Color.blue );
        Vector2 projectionPerp = Util.Project2D( (cp - points[i]), perp.normalized );
        if( Vector2.Dot( perp.normalized, projectionPerp.normalized ) < 0 )
        {
          Popcron.Gizmos.Line( points[i], points[i] + projectionPerp, Color.red );
          Vector2 adjust = cp - projectionPerp;
          if( Vector2.Dot( segment.normalized, (adjust - points[i]).normalized ) > 0 )
          {
            if( (adjust - points[i]).magnitude > segment.magnitude )
              adjust = points[next];
          }
          else
            adjust = points[i];
          pots.Add( adjust );
        }
        else
          Popcron.Gizmos.Line( points[i], points[i] + projectionPerp, Color.magenta );
      }

      float distance = Mathf.Infinity;
      Vector2 closest = cp;
      foreach( var p in pots )
      {
        float dist = Vector2.Distance( p, origin );
        if( dist < distance )
        {
          closest = p;
          distance = dist;
        }
      }
      cp = closest;

      return true;
    }
    return false;
  }
}