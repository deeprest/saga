﻿using UnityEngine;

/*
Sensors, Weapons, Mobility, Core
Armour, Shields

*/
public class BossProtoTwo : BossPrototype
{

  int MyLayer;
  int LayerProjectiles;

  protected override void Start()
  {
    base.Start();
    UpdateLogic = Logic;
    MyLayer = LayerMask.GetMask( new string[] {"entity"} );
    LayerProjectiles = LayerMask.GetMask( new string[] {"projectile", "projectileCollide"} );
  }

  void Logic()
  {
    if( Health <= 0 )
      return;

    if( !SightPulseTimer.IsActive )
    {
      SightPulseTimer.Start( this, 1 );
      SightPulse();
    }

    if( NearbyTarget == null )
    {
      //animator.Play( "idle" );
      if( Instigator != null )
        NearbyTarget = Instigator;
    }

    if( NearbyTarget != null && !hitPauseTimer.IsActive )
    {
      Vector2 targetpos = NearbyTarget.transform.position;
      Vector2 delta = targetpos - (Vector2) transform.position;
      hitCount = Physics2D.LinecastNonAlloc( transform.position, targetpos, Global.RaycastHits, Global.SightObstructionLayers );
      if( hitCount == 0 )
      {
        if( Mathf.Abs( delta.x ) > 1 && delta.y < 2 )
        {
          velocity.x = (delta.x > 0 ? 1 : -1) * MoveSpeed;
          // walk towards enemy
          //animator.Play( "walk" );
        }
      }


      if( ipe == null )
        ProjectileSightPulse();

      if( ipe != null )
      {
        // if projectile is going to hit my legs then jump
        int count = Physics2D.RaycastNonAlloc( ipe.transform.position, ipe.velocity, Global.RaycastHits, ipe.velocity.magnitude, MyLayer );
        for( int i = 0; i < count; i++ )
        {
          if( Global.RaycastHits[i].transform.GetInstanceID() == transform.GetInstanceID() && 
            Global.RaycastHits[i].point.y < transform.position.y )
          {
            Vector2 projectileDelta = transform.position - ipe.transform.position;
            float time = projectileDelta.magnitude / ipe.velocity.magnitude;
            if( time < avoidHitMaxTime )
            {
              if( collideBottom )
                velocity.y = jumpVel;
              break;
            }
          }
        }
        if( count == 0 )
          ipe = null;
      }

      
    }
  }

  public float avoidHitMaxTime = 0.35f;
  public float jumpVel = 5;
  // incoming projectile from enemy
  Projectile ipe;
  void ProjectileSightPulse()
  {
    int count = Physics2D.OverlapCircleNonAlloc( SightOrigin.position, sightRange, Global.ColliderResults, LayerProjectiles );
    for( int i = 0; i < count; i++ )
    {
      Projectile projectile = Global.ColliderResults[i].GetComponent<Projectile>();
      if( projectile != null && IsEnemyTeam( projectile.teamFlags ))
      {
        ipe = projectile;
        break;
      }
    }
  }
  
}