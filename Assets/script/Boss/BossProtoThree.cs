﻿using UnityEngine;
using System.Collections.Generic;

/*
Core: vulnerable to: all, heat, cold
Sensors: sight, trigger, motion
Mobility: roll, biped, flight
Attacks: smash, projectiles, forces          
Defenses: Dodge, Armour, Shields

*/
public class BossProtoThree : BossPrototype, ITrigger
{
  public float avoidHitMaxTime = 0.35f;
  public float AirFriction = .1f;
  // incoming projectile from enemy
  Projectile ipe;
  public float DodgeSpeed = 2;
  public float DodgeRadius = 1;
  bool dodgeFlip;
  int MyLayer;
  int LayerProjectiles;

  // tentacle
  public Tentacle tentacle0;
  public Tentacle[] tentacles;
  string[] keys;


  public override void EntityLateUpdate()
  {
    if( !Global.instance.Updating || dead )
      return;

    UpdateParts();
    for( int i = 0; i < tentacles.Length; i++ )
      tentacles[i].TentacleUpdate();
  }


  protected override void Start()
  {
    EnablePathing = true;
    base.Start();
    UpdateLogic = Logic;
    MyLayer = LayerMask.GetMask( new string[] { "entity" } );
    LayerProjectiles = LayerMask.GetMask( new string[] { "projectile", "projectileCollide" } );

    Collider2D[] clds = transform.GetComponentsInChildren<Collider2D>();
    foreach( var cld in clds )
    {
      if( cld.isTrigger && cld.name.StartsWith( "trigger" ) )
        flags.Add( cld.name, false );
    }
    keys = new string[flags.Keys.Count];
    flags.Keys.CopyTo( keys, 0 );
  }

  void Logic()
  {
    if( Health <= 0 )
      return;
    //velocity -= velocity.sqrMagnitude > 0.0001f ? velocity.normalized * AirFriction : Vector2.zero;

    if( !SightPulseTimer.IsActive )
    {
      SightPulseTimer.Start( this, SightPulseInterval );
      SightPulse();
    }

    if( NearbyTarget == null )
    {
      //animator.Play( "idle" );
      if( Instigator != null )
        NearbyTarget = Instigator;
    }

    if( NearbyTarget != null && !hitPauseTimer.IsActive )
    {
      Vector2 targetpos = NearbyTarget.transform.position;
      Vector2 delta = targetpos - (Vector2)transform.position;
      hitCount = Physics2D.LinecastNonAlloc( transform.position, targetpos, Global.RaycastHits, Global.SightObstructionLayers );
      if( hitCount == 0 )
      {
        if( Mathf.Abs( delta.x ) > 1 && delta.y < 2 )
        {
          //velocity.x = (delta.x > 0 ? 1 : -1) * MoveSpeed;
          //animator.Play( "walk" );
        }
      }

      if( ipe == null )
        ProjectileSightPulse();

      if( ipe != null )
      {
        Vector2 projectileDelta = ipe.transform.position - transform.position;
        Vector2 tangent = new Vector2( -projectileDelta.y, projectileDelta.x );
        Vector2 intersect = Vector2.zero;
        if( Util.LineSegmentsIntersectionWithPrecisonControl( (Vector2)ipe.transform.position, (Vector2)ipe.transform.position + ipe.velocity, (Vector2)transform.position - tangent.normalized * DodgeRadius, (Vector2)transform.position + tangent.normalized * DodgeRadius, ref intersect ) )
        {
          float time = projectileDelta.magnitude / ipe.velocity.magnitude;
          if( time < avoidHitMaxTime )
          {
            Vector2 dodge = intersect - (Vector2)transform.position;
            if( collideBottom )
              pathAgent.SetPath( (Vector2)transform.position + Vector2.up );
            else if( collideTop )
              pathAgent.SetPath( (Vector2)transform.position + Vector2.down );
            else if( collideRight )
              pathAgent.SetPath( (Vector2)transform.position + Vector2.left );
            else if( collideLeft )
              pathAgent.SetPath( (Vector2)transform.position + Vector2.right );
            else
              pathAgent.SetPath( (Vector2)transform.position - (dodge.normalized * DodgeSpeed) );
          }
        }
      }
    }
    pathAgent.UpdatePath();
    if( pathAgent.HasPath )
      velocity = pathAgent.MoveDirection.normalized * MoveSpeed;
    else
      velocity += -velocity * 0.5f * MoveSpeed * Time.deltaTime;

    foreach( var pair in flags )
      if( pair.Value )
        animator.SetBool( pair.Key, true );
    for( int i = 0; i < keys.Length; i++ )
      flags[keys[i]] = false;
  }

  void ProjectileSightPulse()
  {
    int count = Physics2D.OverlapCircleNonAlloc( SightOrigin.position, sightRange, Global.ColliderResults, LayerProjectiles );
    for( int i = 0; i < count; i++ )
    {
      Projectile projectile = Global.ColliderResults[i].GetComponent<Projectile>();
      if( projectile != null && IsEnemyTeam( projectile.teamFlags ) )
      {
        ipe = projectile;
        break;
      }
    }
  }

  Dictionary<string, bool> flags = new Dictionary<string, bool>();
  public void SetFlagFalse( string name ) { animator.SetBool( name, false ); }

  public void Trigger( Collider2D trigger, Transform instigator )
  {
    if( instigator.root != Global.instance.PlayerController.pawn.transform.root )
      return;
    if( flags.ContainsKey( trigger.name ) )
      flags[trigger.name] = true;
    else
      Debug.LogWarning( "trigger / flag not found: " + trigger.name );
  }
}