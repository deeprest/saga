﻿using UnityEngine;

public class BossProtoOne : BossPrototype
{
  [SerializeField] Door[] doors;

  bool movementDisabled;
  [SerializeField] Entity[] Jets;
  int destroyedJetCount;

  protected override void Start()
  {
    base.Start();
    EventDestroyed.AddListener( OpenBossDoors );
    foreach( Entity jet in Jets )
    {
      jet.EventDestroyed.AddListener( () =>
      {
        if( ++destroyedJetCount == Jets.Length )
        {
          movementDisabled = true;
          UseGravity = true;
          MoveSpeed = 0;
        }
      } );
    }
  }

  public void OpenBossDoors()
  {
    foreach( var door in doors )
    {
      door.LockDoor( false );
      door.Open();
    }
  }
}