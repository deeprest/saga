﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDevScene : SceneScript
{
  public Entity[] boss;
  public int index;

  public override void UpdateScene()
  {
    if( index < boss.Length - 1 && (boss[index].dead || boss[index]==null) )
    {
      index++;
      new Timer( this, 5, null, () => { boss[index].gameObject.SetActive( true ); } );
    }
  }
}