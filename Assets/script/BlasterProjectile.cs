﻿//#define PUFF_IN_CODE

using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class BlasterProjectile : Projectile, IDamage
{
  public Light2D light;
  [SerializeField] GameObject hitPrefab;
  [SerializeField] IndexedColors indexedColors;

  protected Timer timeoutTimer = new Timer();
  bool flagStick;
  bool flagBoom;
  int hits;
  public int reflectHits;

  // guided, unguided, homing missiles
  Vector2 worldSeekPoint;
  [SerializeField] ParticleSystem MissileSmokePuff;

  public enum ProjectileGuideType
  {
    WORLD_POINT,
    LASER_GUIDED,
    AIM,
    // SELECTED_TARGET,
    // AUTO_TARGET
  }

  void OnDestroy() { timeoutTimer.Stop( false ); }

  void Start()
  {
    timeoutTimer.Start( this, data.Timeout, null, delegate()
    {
      if( gameObject != null )
      {
        if( data.ExplodeOnLastHit )
          Boom( default );
        else
          Die( transform.position );
      }
    } );
    if( data.AlignRotationToVelocity )
      transform.rotation = Quaternion.Euler( new Vector3( 0, 0, Mathf.Rad2Deg * Mathf.Atan2( -velocity.normalized.x, velocity.normalized.y ) ) );

    // todo hits
    hits = data.Hits;
    reflectHits = data.ReflectCount;

    if( instigator != null )
    {
      if( data.GuideEnabled )
      {
        switch( data.GuideType )
        {
          case ProjectileGuideType.WORLD_POINT:
#if PUFF_IN_CODE
        smokePuffTimer = new Timer();
        SmokePuff();
#endif
            worldSeekPoint = instigator.GetShotOriginPosition() + instigator.GetAimVector().normalized * data.maxDistance;
            int hitCount = Physics2D.RaycastNonAlloc( instigator.GetShotOriginPosition(), instigator.GetAimVector(), Global.RaycastHits, data.maxDistance, Global.DefaultProjectileCollideLayers );
            for( int i = 0; i < hitCount; i++ )
            {
              RaycastHit2D hit = Global.RaycastHits[i];
              if( hit.transform != null && hit.transform != circle.transform && (instigator == null || !hit.transform.IsChildOf( instigator.transform )) )
              {
                worldSeekPoint = hit.point;
                break;
              }
            }
            break;
        }
      }
    }
  }


  public override void UpdateProjectile()
  {
    if( flagStick )
      return;
    if( data.AlignRotationToVelocity && velocity.sqrMagnitude > 0.0001f )
      transform.rotation = Quaternion.Euler( new Vector3( 0, 0, Mathf.Rad2Deg * Mathf.Atan2( -velocity.normalized.x, velocity.normalized.y ) ) );

    if( instigator )
    {
      int hitCount = Physics2D.CircleCastNonAlloc( transform.position /*+ (transform.rotation*circle.offset)*/, circle.radius * circle.transform.localScale.x, velocity, Global.RaycastHits, velocity.magnitude * Time.deltaTime, Global.DefaultProjectileCollideLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        RaycastHit2D hit = Global.RaycastHits[i];
        if( hit.transform != null && hit.transform != circle.transform && (instigator == null || !hit.transform.IsChildOf( instigator.transform )) )
        {
          // If the collider has a rigibody, this will return an IDamage from the object with the rigidbody.
          IDamage dam = hit.transform.GetComponent<IDamage>();
          if( dam != null )
          {
            Damage dmg = new Damage() {def = data.ContactDamageDefinition, damageSource = transform, instigator = instigator, hit = hit};
            if( data.ExplodeOnFirstHit || data.ExplodeOnLastHit )
              dmg.def = data.ExplosionDamage;
            DamageResult dmr = dam.CalculateDamageResult( dmg );
            if( dmr.pass )
            {
              if( !dmr.ignore && dmg.def != null )
                dmr.entity.TakeDamage( dmg );
              /*Hit( hit.point );*/
              continue;
            }
            else if( !dmr.ignore && data.ExplodeOnFirstHit )
              Boom( hit );
            else if( (dmr.reflect || reflectHits-- > 0) ) //&& !data.AlwaysStickToEntity )
            {
              if( !dmr.ignore && dmg.def != null )
                dmr.entity.TakeDamage( dmg );
              Reflect( hit );
            }
            else if( data.StickyEnabled )
              Stick( hit );
            else
            {
              if( !dmr.ignore )
              {
                if( data.ExplodeOnLastHit )
                  Boom( hit );
                else if( dmg.def != null )
                  dmr.entity.TakeDamage( dmg );
              }
              Die( hit.point );
            }
          }
          else if( reflectHits-- > 0 )
            Reflect( hit );
          else if( data.StickyEnabled )
            Stick( hit );
          else if( data.ExplodeOnLastHit )
            Boom( hit );
          else
            Die( hit.point );
          return;
        }
      }
    }
    
    if( data.GuideEnabled )
    {
      switch( data.GuideType )
      {
        // unguided = seek the cursor world point at the time of firing.
        case ProjectileGuideType.WORLD_POINT:
          // if( Util.Cross( transform.up, (worldSeekPoint - (Vector2) transform.position).normalized ) > 0 )
          velocity += Util.Project2D( (worldSeekPoint - (Vector2) transform.position).normalized, new Vector2( velocity.y, -velocity.x ) ) * data.GuidedFactor * Time.deltaTime;
          break;

        // guided = seek the cursor each frame.
        case ProjectileGuideType.AIM:
          if( instigator )
            velocity += Util.Project2D( (instigator.GetShotOriginPosition() + instigator.GetAimVector() - (Vector2) transform.position).normalized, new Vector2( velocity.y, -velocity.x ) ) * data.GuidedFactor * Time.deltaTime;
          break;

        case ProjectileGuideType.LASER_GUIDED:
          if( instigator )
          {
            int hitCount = Physics2D.RaycastNonAlloc( instigator.GetShotOriginPosition(), instigator.GetAimVector(), Global.RaycastHits, data.maxDistance, Global.DefaultProjectileCollideLayers );
            for( int i = 0; i < hitCount; i++ )
            {
              RaycastHit2D hit = Global.RaycastHits[i];
              if( hit.transform != null && hit.transform != circle.transform && (instigator == null || !hit.transform.IsChildOf( instigator.transform )) )
              {
                worldSeekPoint = hit.point;
                break;
              }
            }
            if( hitCount == 0 )
              worldSeekPoint = instigator.GetShotOriginPosition() + instigator.GetAimVector().normalized * data.maxDistance;
            velocity += Util.Project2D( (worldSeekPoint - (Vector2) transform.position).normalized, new Vector2( velocity.y, -velocity.x ) ) * data.GuidedFactor * Time.deltaTime;
          }
          break;

        // todo homing = seek a selected target.
        //case ProjectileMoveType.SELECTED_TARGET: break;
        // todo homing = seek an automatically acquired target.
        // case ProjectileMoveType.AUTO_TARGET: break;
      }


      velocity = velocity.normalized * data.Speed;
    }

    if( data.UseGravity )
      velocity += Vector2.down * Global.Gravity * Time.deltaTime;

    transform.position += (Vector3) velocity * Time.deltaTime;

    /*SpriteRenderer sr = GetComponent<SpriteRenderer>();
    sr.color = Global.instance.shiftyColor;
    light.color = Global.instance.shiftyColor;*/
  }


#if PUFF_IN_CODE
  // heavy-handed *potential* optimization
  Timer smokePuffTimer;
  void SmokePuff()
  {
    ParticleSystem.EmitParams emit = new ParticleSystem.EmitParams
    {
      position = transform.position
    };
    Global.instance.MissileSmoke.Emit( emit, 1 );
    if( !smokePuffTimer.IsActive )
      smokePuffTimer.Start( this, 1f / 60f, null, SmokePuff );
  }
#endif

  public DamageResult CalculateDamageResult( Damage damage )
  {
    DamageResult dmr = new DamageResult( this );
    if( damage.instigator == instigator )
    {
      dmr.ignore = true;
      dmr.pass = true;
    }
    return dmr;
  }

  public void TakeDamage( Damage damage )
  {
    // moved to CalculateDamageResult()
    /*if( damage.instigator == instigator )
      return;*/
    if( data.ExplodeOnLastHit )
      Boom( damage.hit );
    else
      Die( damage.hit.point );
  }

  void Die( Vector2 point )
  {
    Hit( point );
    transform.position = point;
    enabled = false;
    velocity = Vector2.zero;
    light.enabled = false;
    Destroy( gameObject );
  }

  void Hit( Vector2 point )
  {
    // simply indicate that a collision has happened
    if( hitPrefab != null )
    {
      GameObject go = Instantiate( hitPrefab, point, transform.rotation );
      if( indexedColors != null )
      {
        IndexedColors ic = go.GetComponent<IndexedColors>();
        if( ic != null )
        {
          indexedColors.colors.CopyTo( ic.colors, 0 );
          ic.ExplicitUpdate();
        }
      }
    }
  }

  void Boom( RaycastHit2D hit )
  {
    // Explode and damage all within a radius.
    if( flagBoom )
      return;
    flagBoom = true;
    // disable collider before explosion to avoid unnecessary OnCollisionEnter2D() calls
    circle.enabled = false;
    timeoutTimer.Stop( false );

    if( MissileSmokePuff )
    {
      // detach particle system that it does not suddenly pop out of existence when the gameobject is destroyed. It will destroy itself.
      MissileSmokePuff.transform.SetParent( null, true );
      MissileSmokePuff.Stop();
    }

    // call Destroy() before TakeDamage() in case it throws a nullref exception
    Destroy( gameObject );
    // explode at current position instead of hit position, in case the transform has moved.
    Instantiate( data.explosion, transform.position, Quaternion.identity );
    
    int count = Physics2D.OverlapCircleNonAlloc( transform.position, data.BoomRadius, Global.ColliderResults, Global.StickyBombCollideLayers );
    for( int i = 0; i < count; i++ )
    {
      //if( Global.ColliderResults[i] )
      {
        IDamage dam = Global.ColliderResults[i].GetComponent<IDamage>();
        if( dam == null )
          dam = Global.ColliderResults[i].transform.GetComponentInParent<IDamage>();
        if( dam != null )
        {
          Damage dmg = new Damage
          {
            def = data.ExplosionDamage,
            instigator = instigator,
            damageSource = transform,
            hit = hit
          };
          DamageResult dmr = dam.CalculateDamageResult( dmg );
          if( !dmr.ignore )
            dmr.entity.TakeDamage( dmg );
        }
      }
    }
  }

  void Reflect( RaycastHit2D hit )
  {
    transform.position = hit.point + hit.normal * circle.radius * circle.transform.localScale.x;

    Vector2 hitVelocity = Util.Project2D( velocity, hit.normal );
    velocity = Vector3.Reflect( velocity, hit.normal );
    velocity -= velocity * data.ReflectVelocityDecay;

    // add entity velocity to projectile
    Entity reflectingEntity = hit.collider.GetComponent<Entity>();
    if( reflectingEntity != null )
    {
      velocity += reflectingEntity.Velocity;
      // add a single frame of velocity to avoid clipping through fast-moving objects
      transform.position += (Vector3) reflectingEntity.Velocity * Time.deltaTime;
    }

    // align rotation to reflected velocity
    if( data.AlignRotationToVelocity )
      transform.rotation = Quaternion.Euler( new Vector3( 0, 0, Mathf.Rad2Deg * Mathf.Atan2( -velocity.normalized.x, velocity.normalized.y ) ) );

    if( hitVelocity.sqrMagnitude > 10 )
    {
      if( data.SoundReflect != null )
        Global.instance.AudioOneShot( data.SoundReflect, hit.point );
      Hit( hit.point );
    }

    // Reflect at somewhat of a tangent
    /*if( damage.instigator == null )
    projectile.velocity = Vector3.Reflect( projectile.velocity.normalized, (damage.point - (Vector2) transform.position).normalized );
    */

    // Reflect directly back at the instigator!
    /*projectile.velocity = Vector3.Reflect( projectile.velocity, (damage.instigator.transform.position - transform.position).normalized );
    */

    // Allow the projectile to damage the instigator!
    /*foreach( var cldr in projectile.instigator.IgnoreCollideObjects )
    {
      if( cldr == null )
        Debug.Log( "ignored collide object is null" );
      else
        projectile.ignoreColliders.Add( cldr );
    }
    projectile.IgnoreColliders( false );
    */

    // a projectile with a new instigator can damage the original instigator. 
    /*projectile.instigator = instigator;*/

    // "ignoreColliders" are checked in TakeDamage()
    /*ignoreColliders.Add( hit.collider );*/

    // this only works for rigidbodies
    /*Physics2D.IgnoreCollision( circle, surface );*/
  }

  void Stick( RaycastHit2D hit )
  {
    // turn the other object layer INDEX into a bitfield, and verify it's in StickyBombCollideLayers
    /*if( (Global.StickyBombCollideLayers & (1 << hit.collider.gameObject.layer)) > 0 )*/
    {
      Projectile projectile = hit.transform.GetComponent<Projectile>();
      if( projectile != null )
      {
        if( instigator == null || projectile.instigator == null || projectile.instigator != instigator )
          Boom( hit );
        else
          // ignore collision with projectiles from same instigator
          return;
      }
      flagStick = true;
      circle.enabled = false;
      transform.parent = hit.collider.transform;
      transform.position = hit.point;
      // +Y = forward
      transform.rotation = Quaternion.Euler( new Vector3( 0, 0, Mathf.Rad2Deg * Mathf.Atan2( hit.normal.x, -hit.normal.y ) ) );
      velocity = Vector2.zero;
      // NOTE be sure the "hit" animator state uses a flashing animation.
      animator.Play( "hit" );
      timeoutTimer.Start( this, data.AttachDuration, null, delegate
      {
        if( data.ExplosionDamage != null )
          Boom( hit );
        else
        {
          Die( hit.point );
          // todo damage other
        }
      } );
    }
  }
}