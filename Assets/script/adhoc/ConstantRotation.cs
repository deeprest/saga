﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteAlways]
public class ConstantRotation : MonoBehaviour
{
    [SerializeField] bool unscaledTime;
    [SerializeField] Transform target;
    public float speed = 1;
    float rot;

    // Update is called once per frame
    void Update()
    {
        if( target != null )
        {
            rot += speed * (unscaledTime ? Time.unscaledDeltaTime : Time.deltaTime);
            target.rotation = Quaternion.Euler( 0, 0, rot );
        }
    }
    
}
