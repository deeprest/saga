﻿using System.Collections.Generic;
using UnityEngine;

public class BossPrototype : Entity
{
  [Header( "Boss" )]
  public bool EnableVictorySequence = true;
  [SerializeField] GameObject explosion;
  public float MoveSpeed = 2;
  // sight, target
  public Entity NearbyTarget;
  public Transform SightOrigin;
  public float sightRange = 6;
  public float SightPulseInterval = 1;
  public Timer SightPulseTimer = new Timer();
  public Timer hitPauseTimer = new Timer();
  [SerializeField] float hitPause = 3;
  // enemies to clean up upon death
  public List<Entity> bossEnemies;
  public List<Entity> whenAllDeadIDie;

  ConstantRotation[] constRots;

  protected override void Start()
  {
    base.Start();
    UpdateLogic = Logic;
    UpdateHit = Hit;
    UpdateCollision = BoxCollision;

    foreach( var enemy in bossEnemies )
      enemy.EventDestroyed.AddListener( () => { bossEnemies.Remove( enemy ); } );

    foreach( var entity in whenAllDeadIDie )
    {
      entity.EventDestroyed.AddListener( () =>
      {
        whenAllDeadIDie.Remove( entity );
        if( whenAllDeadIDie.Count == 0 && !Global.instance.LoadingScene )
          Die( default );
      } );
    }

    constRots = GetComponentsInChildren<ConstantRotation>();
  }

  protected void SightPulse()
  {
    // reaffirm target
    NearbyTarget = null;
    int count = Physics2D.OverlapCircleNonAlloc( SightOrigin.position, sightRange, Global.ColliderResults, Global.EnemyInterestLayers );
    for( int i = 0; i < count; i++ )
    {
      Entity potentialTarget = Global.ColliderResults[i].GetComponent<Entity>();
      if( potentialTarget != null && IsEnemyTeam( potentialTarget.TeamFlags ) )
      {
        NearbyTarget = potentialTarget;
        break;
      }
    }
  }

  protected override void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    base.OnDestroy();
    hitPauseTimer.Stop( false );
    SightPulseTimer.Stop( false );
  }

  public float rot = 1;

  void Logic()
  {
    foreach( var constantRotation in constRots )
    {
      constantRotation.speed = -velocity.x * rot;
    }
    if( Health <= 0 )
      return;

    if( !SightPulseTimer.IsActive )
    {
      SightPulseTimer.Start( this, SightPulseInterval );
      SightPulse();
    }

    if( NearbyTarget == null )
    {
      //animator.Play( "idle" );
      if( Instigator != null )
        NearbyTarget = Instigator;
    }

    if( NearbyTarget != null && !hitPauseTimer.IsActive )
    {
      Vector2 targetpos = NearbyTarget.transform.position;
      Vector2 delta = targetpos - (Vector2)transform.position;
      hitCount = Physics2D.LinecastNonAlloc( transform.position, targetpos, Global.RaycastHits, Global.SightObstructionLayers );
      if( hitCount == 0 )
      {
        if( delta.y < 2 )
        {
          velocity.x = (delta.x > 0 ? 1 : -1) * MoveSpeed;
        }
      }
    }
  }

  void Hit()
  {
    if( ContactDamageDefinition != null )
    {
      hitCount = Physics2D.BoxCastNonAlloc( transform.position, box.size, 0, velocity, Global.RaycastHits, raylength, Global.CharacterDamageLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        hit = Global.RaycastHits[i];
        IDamage dam = hit.transform.GetComponent<IDamage>();
        if( dam != null )
        {
          Damage dmg = new Damage();
          dmg.def = ContactDamageDefinition;
          dmg.instigator = this;
          dmg.damageSource = transform;
          dmg.hit = hit;
          DamageResult dmr = dam.CalculateDamageResult( dmg );
          if( !dmr.ignore )
          {
            dam.TakeDamage( dmg );
            HitPause();
          }
        }
      }
    }
  }

  void HitPause()
  {
    velocity = Vector2.zero;
    //animator.Play( "laugh" );
    hitPauseTimer.Start( this, hitPause, null, delegate
    {
      //animator.Play( "idle" );
    } );
  }

  public override void Die( Damage damage )
  {
    if( dead )
      return;

    if( EnableVictorySequence )
    {
      dead = true;
      velocity = Vector2.zero;
      StartBossDeathSequence();
    }
    else
      base.Die( damage );
  }

  void StartBossDeathSequence()
  {
    // allow only one boss at a time to potentially start the destruction/victory sequence
    if( Global.instance.BossDestructionFlag )
      return;
    Global.instance.BossDestructionFlag = true;

    destructionTimer = new Timer();

    // destroy all child entities
    List<Entity> entities = new List<Entity>( GetComponentsInChildren<Entity>() );
    entities.Remove( this );
    entities.AddRange( bossEnemies );
    foreach( var entity in entities )
      entity.Die( default );

    // destroy all projectiles and sources of danger
    Projectile[] projectiles = FindObjectsOfType<Projectile>();
    foreach( var projectile in projectiles )
      Destroy( projectile.gameObject );

    Global.instance.MusicCrossFadeTo( null, 2 );
    Global.instance.PlayerController.DisableBipedControls();
    // todo it's possible that the player has been destroyed by now
    if( Global.instance.CurrentPlayer is PlayerBiped )
      ((PlayerBiped)Global.instance.CurrentPlayer).VictorySequenceBegin();

    deathSequenceBoomTimer = new Timer( this, (int)(boomDuration / boomInterval), boomInterval,
      delegate( Timer timer )
      {
        if( explosion != null )
          Instantiate( explosion, (Vector2)transform.position + Random.insideUnitCircle * boomRadius, Quaternion.identity );
        else
          Instantiate( Global.instance.explosion, (Vector2)transform.position + Random.insideUnitCircle * boomRadius, Quaternion.identity );
      }, null );
    for( int i = 0; i < spriteRenderers.Count; i++ )
    {
      spriteRenderers[i].material.SetInt( "_Minimap", 1 );
      spriteRenderers[i].material.SetColor( "_MinimapColor", Color.black );
    }

    Color redish = new Color( 1, 0.5f, 0.5f, 0.3f );
    Global.instance.bossfade.color = redish;
    Global.instance.bossfade.gameObject.SetActive( true );

    Global.instance.Pause();
    EventDeath?.Invoke();
    destructionTimer.unscaledTime = true;
    destructionTimer.Start( this, 1, null, () =>
    {
      Global.instance.Unpause();
      destructionTimer.unscaledTime = false;

      bool startFlashing = false;
      destructionTimer.Start( this, boomDuration * 0.6f, delegate( Timer timer )
      {
        if( timer.ProgressNormalized < 0.5f )
        {
          if( !startFlashing && timer.ProgressNormalized > 0.4f )
          {
            startFlashing = true;
            ScreenFlashFlip( redish );
          }
        }
        else
        {
          screenFlashTimer.Stop( false );
          Global.instance.bossfade.color = Color.Lerp( redish, Color.white, 2 * (timer.ProgressNormalized - 0.5f) );
          for( int i = 0; i < spriteRenderers.Count; i++ )
            spriteRenderers[i].material.SetFloat( "_MinimapBlend", timer.ProgressNormalized );
        }
      }, delegate
      {
        //EventDeath?.Invoke();

        destructionTimer.Start( this, boomDuration * 0.4f, delegate( Timer timer )
        {
          for( int i = 0; i < spriteRenderers.Count; i++ )
            spriteRenderers[i].material.SetColor( "_MinimapColor", Color.Lerp( Color.black, Color.clear, timer.ProgressNormalized * 1.5f ) );
        }, () =>
        {
          destructionTimer.Start( this, 3, delegate( Timer timer ) { Global.instance.bossfade.color = Color.Lerp( Color.white, Color.clear, timer.ProgressNormalized ); }, () =>
          {
            Global.instance.bossfade.gameObject.SetActive( false );

            GameObject[] prefab = GetDeathSpawnObjects();
            for( int i = 0; i < prefab.Length; i++ )
              Instantiate( prefab[i], transform.position, Quaternion.identity );

            Destroy( gameObject );
            EventDestroyed?.Invoke();

            // CRITICAL: this call cleans up latent state
            // ((PlayerBiped)CurrentPlayer).VictorySequenceEnd();
            Global.instance.StartVictorySequence();
          } );
        } );
      } );
    } );
  }

  Timer screenFlashTimer = new Timer();
  float screenFlashInterval = 0.05f;
  bool screenFlashOn;

  void ScreenFlashFlip( Color redish )
  {
    screenFlashTimer.Start( this, screenFlashInterval, null, delegate
    {
      screenFlashOn = !screenFlashOn;
      if( screenFlashOn )
        Global.instance.bossfade.color = Color.white;
      else
        Global.instance.bossfade.color = redish;
      ScreenFlashFlip( redish );
    } );
  }

  /*public override void AwareOfEnemy( Entity enemy )
  {
    NearbyTarget = enemy;
  }*/

  public void WakeUp()
  {
    //animator.Play( "wakeup" );
    new Timer( this, 3, null, () => enabled = true );
  }
}