﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( (typeof(IndexedColors)) )]
public class ICSEditor : Editor
{
  public override void OnInspectorGUI()
  {
    if( !Application.isPlaying || !PrefabUtility.IsPartOfPrefabAsset( target ) )
    {
      var ics = target as IndexedColors;
      ics.ExplicitUpdate();
    }
    DrawDefaultInspector();
  }
}
#endif

public class IndexedColors : MonoBehaviour
{
  public Renderer[] srs;
  public UnityEngine.UI.Text[] txts;
  public Light2D light;
  public Color[] colors = new Color[1];

#if UNITY_EDITOR
  void OnValidate()
  {
    if( !Application.isPlaying )
    {
      if( srs != null )
      {
        for( int i = 0; i < srs.Length; i++ )
        {
          if( srs[i] == null )
            ArrayUtility.RemoveAt( ref srs, i-- );
          else if( srs[i].sharedMaterial == null )
            Debug.LogWarning( "Missing material", gameObject );
          /*else if( !srs[i].sharedMaterial.HasProperty( "_IndexColors" ) )
            Debug.LogWarning( "IndexedColors renderer material does not have corrent property in shader.", gameObject );*/
        }
      }
    }
  }
#endif

  void Start() { ExplicitUpdate(); }

  public void ExplicitUpdate()
  {
#if UNITY_EDITOR
    if( Application.isPlaying )
    {
      if( txts != null && txts.Length > 0 )
        for( int i = 0; i < txts.Length; i++ )
          if( txts[i] != null )
          {
            txts[i].material.SetColor( "_Color0", colors[0] );
            txts[i].material.SetColor( "_Color1", colors[1] );
          }

      if( srs != null && srs.Length > 0 )
        for( int i = 0; i < srs.Length; i++ )
          if( srs[i] != null )
            srs[i].material.SetColorArray( "_IndexColors", colors );
    }
    else
    {
      if( txts != null && txts.Length > 0 )
        for( int i = 0; i < txts.Length; i++ )
          if( txts[i] != null )
          {
            if( txts[i].material.HasProperty( "_Color0" ) )
              txts[i].material.SetColor( "_Color0", colors[0] );
            if( txts[i].material.HasProperty( "_Color1" ) )
              txts[i].material.SetColor( "_Color1", colors[1] );
          }

      if( srs != null && srs.Length > 0 )
        for( int i = 0; i < srs.Length; i++ )
          if( srs[i] != null )
            srs[i].sharedMaterial.SetColorArray( "_IndexColors", colors );
    }
#else
        if( txts != null && txts.Length > 0 )
        for( int i = 0; i < txts.Length; i++ )
          if( txts[i] != null )
          {
            txts[i].material.SetColor( "_Color0", colors[0] );
            txts[i].material.SetColor( "_Color1", colors[1] );
          }

      if( srs != null && srs.Length > 0 )
        for( int i = 0; i < srs.Length; i++ )
          if( srs[i] != null )
            srs[i].material.SetColorArray( "_IndexColors", colors );
#endif

    if( light != null )
      light.color = colors[0];
  }
}