using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemSettings : MonoBehaviour
{
    void Awake()
    {
        ParticleSystemRenderer particleSystemRenderer = GetComponent<ParticleSystemRenderer>();
        particleSystemRenderer.sortingFudge = Random.Range( 0, int.MaxValue );
    }
}
