﻿using UnityEngine;

[CreateAssetMenu]
public class CharacterIdentity : ScriptableObject
{
  public string CharacterName;
  public Sprite Icon;
  // only on right side of screen for now
  public Sprite Face;
  public Color SpeechTextColor = Color.white;
  public AnimatorOverrideController animationController;

  public TextAsset TextAsset;
  //public Dialogue DialogueAsset;
}