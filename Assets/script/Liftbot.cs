﻿using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(Liftbot) )]
[CanEditMultipleObjects]
public class LiftbotEdtitor : MonoBehaviourInspector
{
  public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();

    Liftbot bot = target as Liftbot;
    Vector2 origin = bot.transform.position;
    if( Application.isPlaying )
      origin = bot.origin;

    float length = 0;

    if( bot.path != null && bot.path.Length > 0 )
    {
      Vector2[] points = bot.path;
      for( int i = 0; i < points.Length; i++ )
      {
        if( bot.pingpong && i + 1 == points.Length )
        {
          length *= 2;
          break;
        }
        int next = (i + 1) % points.Length;
        Vector2 segment = points[next] - points[i];
        length += segment.magnitude;

        Popcron.Gizmos.Line( origin + points[i], origin + points[next], Color.white );
      }

      EditorGUILayout.LabelField( "path length", length.ToString() );
      float duration = (length / bot.flySpeed) + (bot.pingpong ? bot.path.Length * 2 - 2 : bot.path.Length) * bot.WaypointWaitDuration;
      EditorGUILayout.LabelField( "return time", duration.ToString() );
    }

    if( bot.switches != null )
      for( int i = 0; i < bot.switches.Length; i++ )
      {
        ArrayUtility.Clear( ref bot.switches[i].syncSwitches );
        for( int j = 0; j < bot.switches.Length; j++ )
          if( bot.switches[j] != bot.switches[i] )
            ArrayUtility.Add( ref bot.switches[i].syncSwitches, bot.switches[j] );
      }
  }

  void OnSceneGUI()
  {
    Handles.color = Color.yellow;
    Liftbot bot = target as Liftbot;
    if( bot.path != null && bot.path.Length > 0 )
    {
      Vector2[] points = bot.path;
      for( int i = 0; i < points.Length; i++ )
      {
        if( bot.pingpong && i + 1 == points.Length )
          break;
        int next = (i + 1) % points.Length;

        Vector2 origin = bot.transform.position;
        if( Application.isPlaying )
          origin = bot.origin;
        //Popcron.Gizmos.Line( origin + points[i], origin + points[next], Color.yellow );
        Handles.DrawLine( origin + points[i], origin + points[next] );
      }
    }
  }
}
#endif

public class Liftbot : Entity, IWorldSelectable
{
  [Header( "Liftbot" )]
  [FormerlySerializedAs( "IsTriggeredByPlayer" )]
  public bool TriggeredByPlayer;
  public bool WaitForTriggerAtEachWaypoint;
  public float flySpeed = 2;
  const float slowSpeed = 200;
  [FormerlySerializedAs( "waitDuration" )]
  public float WaypointWaitDuration = 1;
  public const float closeEnough = 0.25f;

  public Switch[] switches;

  int pathIndex;
  public Vector2 origin { get { return (Vector2) originTransform.position + originOffset; } }
  public Transform originTransform;
  public Vector2 originOffset;
  public Vector2[] path;
  Timer timeout = new Timer();
  public bool pingpong;
  int indexIncrement = 1;

  /*[System.Serializable]
    public struct LiftPathPoint
    {
      public Vector2 Point;
      public bool Stop;
      public float WaitDuration;
      public bool SmoothArrival;
      public float SmoothArrivalDistance;
      public float Speed;
    
      public bool EnableWaitToSync;
      public Liftbot SyncWithBot; 
      public LiftPathPoint SyncWithPoint;
      public System.Action UponArrival;
    
      public bool ToggleColliderEnabled;
      public bool SetCharacterSpriteLayer;
      public int CharacterSpriteLayer;
    }
    public bool UseAdvancedPath;
    public LiftPathPoint[] AdvancedPath;*/

  protected override void Start()
  {
    base.Start();
    UpdateLogic = UpdateLiftbot;
    UpdateHit = null;
    UpdateCollision = null;
    // this should be the "city chunk" or whatever transform origin is used.
    if( transform.root == transform )
      originTransform = Global.instance.transform;
    else
      originTransform = transform.root;
    originOffset = transform.position - originTransform.position;
    if( !TriggeredByPlayer )
      timeout.Start( this, WaypointWaitDuration, null, NextWaypoint );
  }

  protected override void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    base.OnDestroy();
    timeout.Stop( false );
  }

  [ExposeMethod]
  public void NextWaypoint()
  {
    int next = pathIndex + indexIncrement;
    if( pingpong && (next >= path.Length || next < 0) )
      indexIncrement = -indexIncrement;
    pathIndex = (pathIndex + indexIncrement) % path.Length;
    // Might need a timeout if the liftbot collides with anything.
    //  timeout.Start( DistanceToWaypoint() / flySpeed, null, NextWaypoint );
  }

  void UpdateLiftbot()
  {
    /*Vector2 line = path[pathIndex] - path[(pathIndex-1+path.Length) % path.Length];
    Popcron.Gizmos.Line( origin+path[pathIndex], origin +path[(pathIndex-1+path.Length) % path.Length], Color.green );*/

    if( path.Length > 0 )
    {
      Vector2 delta = origin + path[pathIndex] - (Vector2) transform.position;
      if( delta.sqrMagnitude < closeEnough * closeEnough )
      {
        // WARNING
        // This does not update player position, which creates an immediate an noticeable offset
        // between the player's feet and the liftbot when reaching a waypoint. 
        /*transform.position = origin + path[pathIndex];*/

        // Smooth arrival
        velocity = delta * Time.smoothDeltaTime * slowSpeed;
        if( TriggeredByPlayer && (WaitForTriggerAtEachWaypoint || (pathIndex == 0 || pathIndex == path.Length - 1)) )
        {
          // do nothing
        }
        else if( WaypointWaitDuration > 0 )
        {
          if( !timeout.IsActive )
            timeout.Start( this, WaypointWaitDuration, null, NextWaypoint );
        }
        else
          NextWaypoint();
      }
      else
      {
        velocity = delta.normalized * flySpeed;
      }
    }
  }

  /*public override void TakeDamage( Damage damage )
  {
    // absorb hits, but do not take damage
  }*/

  public void Highlight()
  {
    if( Global.instance.CurrentPlayer != null )
    {
      Global.instance.CurrentPlayer.InteractIndicator.SetActive( true );
      Global.instance.CurrentPlayer.InteractIndicator.transform.position = transform.position;
    }
  }

  public void Unhighlight()
  {
    if( Global.instance.CurrentPlayer != null )
      Global.instance.CurrentPlayer.InteractIndicator.SetActive( false );
  }

  public void Select() { Select( null ); }

  public void Select( Entity pawn )
  {
    if( TriggeredByPlayer && (origin + path[pathIndex] - (Vector2) transform.position).sqrMagnitude < closeEnough * closeEnough )
      NextWaypoint();
  }

  public void Unselect() { }

  public Vector2 GetPosition() { return transform.position; }
}