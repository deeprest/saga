﻿using UnityEngine;
  
public class TinyLevel : CityWrap
{
  [Header( "ChopDrop" )]
  [SerializeField] bool DoChopDrop = true;
  [SerializeField] Chopper chopper;
  [SerializeField] float runRightDuration = 3;

  public override void StartScene()
  {
    base.StartScene();

    if( DoChopDrop )
    {
      chopper.StartDrop( Global.instance.CurrentPlayer );
      Global.instance.PlayerController.FireOnlyInputMode();
      new Timer( this, runRightDuration, delegate
      {
        // get the input state to *modify* instead of overwrite/assign
        ref InputState inputStateRef = ref Global.instance.PlayerController.GetInput();
        inputStateRef.MoveRight = true;
      }, delegate { Global.instance.PlayerController.NormalInputMode(); } );
    }
    
  }
}