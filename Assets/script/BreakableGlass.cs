﻿using UnityEngine;

public class BreakableGlass : MonoBehaviour, IDamage
{
  public bool dead;
  public int Health;
  public int DamageThreshold;
  public bool ReflectIfBelowThreshold;
  public AudioClip SoundHit;
  public ParticleSystem ps;
  public BoxCollider2D box;
  public SpriteRenderer spriteRenderer;
  public float VelocityAmount = 1;

  public DamageResult CalculateDamageResult( Damage damage )
  {
    DamageResult dmr = new DamageResult( this );
    if( dead )
      dmr.ignore = true;
    if( damage.def != null && damage.def.amount < DamageThreshold )
    {
      // damage is below threshold, so reflect/ignore
      if( ReflectIfBelowThreshold )
        dmr.reflect = true;
    }
    return dmr;
  }

  public void TakeDamage( Damage damage )
  {
    Health -= damage.def.amount;
    if( Health <= 0 )
    {
      dead = true;
      box.enabled = false;
      spriteRenderer.enabled = false;
      Global.instance.AudioOneShot( SoundHit, transform.position );

      Vector2 velocity = Vector2.zero;

      Projectile projectile = damage.damageSource.GetComponent<Projectile>();
      if( projectile != null )
      {
        velocity = projectile.velocity;
      }
      else
      {
        Entity entity = damage.damageSource.GetComponent<Entity>();
        if( entity != null )
          velocity = entity.velocity;
      }

      ParticleSystem.VelocityOverLifetimeModule volm = ps.velocityOverLifetime;
      volm.x = velocity.x * VelocityAmount;
      volm.y = velocity.y * VelocityAmount;
      /*ParticleSystem.EmitParams emitParams = new ParticleSystem.EmitParams();
      emitParams.velocity = projectile.velocity;
      ps.Emit( emitParams, 100 );*/
      ps.Emit( 100 );
    }
  }

}