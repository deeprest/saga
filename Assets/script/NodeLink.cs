﻿using UnityEngine;

public class NodeLink : MonoBehaviour
{
  // A prefab cannot have a reference to itself! It gets serialized internally as a local reference.
  // So I keep a list of prefabs in a ScriptedObject (NodeLinkSet).
  // https://forum.unity.com/threads/prefab-with-reference-to-itself.412240/
  public NodeLinkSet linkSet;

  public GameObject endCap;
}



