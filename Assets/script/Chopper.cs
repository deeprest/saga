﻿using UnityEngine;

public class Chopper : MonoBehaviour
{
  public Transform hangPoint;
  public Transform chopperStartPoint;
  public float speed = 5;
  Timer timer = new Timer();
  public float timeUntilDrop = 1;
  bool started;

  public void StartDrop( Entity ent )
  {
    started = true;
    transform.position = chopperStartPoint.position;
    ent.hanging = true;
    ent.velocity = Vector3.zero;
    ent.transform.parent = hangPoint;
    ent.transform.localPosition = Vector3.zero;

    timer.Start( this, timeUntilDrop, null, delegate
    {
      // entity may have be destroyed by now.
      if( ent )
      {
        ent.transform.parent = null;
        ent.hanging = false;
        ent.inertia = Vector2.right * speed;
      }
    } );
  }

  void Update()
  {
    if( started )
      transform.position += Vector3.right * speed * Time.deltaTime;
  }
}