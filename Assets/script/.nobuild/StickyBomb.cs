﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class StickyBomb : Projectile, IDamage
{
  public GameObject explosion;
  Timer timeoutTimer = new Timer();

  [Header( "FlashingLight" )]
  Timer pulseTimer = new Timer();
  [SerializeField] float pulseInterval = 0.2f;
  [SerializeField] Light2D light;

  void Start()
  {
    timeoutTimer.Start( this, timeout, null, delegate { TakeDamage( new Damage( def.contactDamageDefinition ) ); } );
    pulseTimer.Start( this, int.MaxValue, pulseInterval, delegate( Timer obj ) { light.enabled = !light.enabled; },
      null );
  }

  void OnDestroy()
  {
    timeoutTimer.Stop( false );
    pulseTimer.Stop( false );
  }


  void Update()
  {
    // +X = forward
    if( AlignRotationToVelocity )
      transform.rotation = Quaternion.Euler( new Vector3( 0, 0,
        Mathf.Rad2Deg * Mathf.Atan2( velocity.normalized.y, velocity.normalized.x ) ) );
  }

  void Boom()
  {
    if( flagBoom )
      return;
    flagBoom = true;
    // disable collider before explosion to avoid unnecessary OnCollisionEnter2D() calls
    circle.enabled = false;
    timeoutTimer.Stop( false );
    int count = Physics2D.OverlapCircleNonAlloc( transform.position, def.BoomRadius, clds,
      Global.StickyBombCollideLayers );
    for( int i = 0; i < count; i++ )
    {
      if( clds[i] != null )
      {
        IDamage dam = clds[i].GetComponent<IDamage>();
        if( dam == null )
          dam = clds[i].transform.GetComponentInParent<IDamage>();
        if( dam != null )
        {
          Damage dmg = new Damage();
          dmg.def = contactDamageDefinition;
          dmg.instigator = instigator;
          dmg.damageSource = transform;
          dmg.hit = hit;
          dam.TakeDamage( dmg );
        }
      }
    }
    Destroy( gameObject );
    Instantiate( explosion, transform.position, Quaternion.identity );
  }

  public bool TakeDamage( Damage damage )
  {
    Boom();
    return true;
  }

  void OnCollisionEnter2D( Collision2D collision )
  {
    if( flagStick )
      return;
    if( (Global.StickyBombCollideLayers & (1 << collision.gameObject.layer)) > 0 && collision.transform != null &&
      (instigator == null || !collision.transform.IsChildOf( instigator.transform )) &&
      !ignoreHits.Contains( collision.transform ) )
    {
      flagStick = true;

      Projectile projectile = collision.transform.GetComponent<Projectile>();
      if( projectile != null )
      {
        if( instigator == null || projectile.instigator == null || projectile.instigator != instigator )
          Boom();
        else
          // ignore collision with projectiles from same instigator
          return;
      }
      def.AlignRotationToVelocity = false;
      transform.parent = collision.transform;
      // +X = forward
      transform.rotation = Quaternion.Euler( new Vector3( 0, 0,
        Mathf.Rad2Deg * Mathf.Atan2( -collision.contacts[0].normal.y, -collision.contacts[0].normal.x ) ) );

      animator.Play( "flash" );
      timeoutTimer.Start( this, def.AttachDuration, null, delegate { Boom(); } );
    }
  }
}