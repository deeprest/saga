﻿using UnityEngine;

/*public struct ActionData
{
  public Entity instigator;
  public List<string> actions;
  public Transform indicator;
}

public interface IAction
{
  void OnAction( Entity instigator, string action = "default" );
  void GetActionContext( ref ActionData actionData );
}*/


public interface IDamage
{
  DamageResult CalculateDamageResult( Damage damage );
  void TakeDamage( Damage damage );
}

/*public interface IOwnable
{
  bool IsOwned();
  void ClearOwner();
  void AssignOwner( Entity owner );
}*/

public interface ITrigger
{
  void Trigger( Collider2D trigger, Transform instigator );
}

public interface IWorldSelectable
{
  void Highlight();
  void Unhighlight();
  void Select();
  void Select( Entity instigator );
  void Unselect();
  Vector2 GetPosition();
}


public struct TalkerCameraInfo
{
  public Vector2 focus;
  public float orthoSize;
}

public interface ITalker
{
  CharacterIdentity GetIdentity();
  void OnSay( string say );
  void OnSpeakEnd();
  TalkerCameraInfo GetCameraInfo();
}

