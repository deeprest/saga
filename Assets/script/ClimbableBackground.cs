﻿using UnityEngine;
using UnityEngine.Serialization;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(ClimbableBackground) )]
[CanEditMultipleObjects]
public class ClimbableBackgroundEdtitor : MonoBehaviourInspector
{public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();
    ClimbableBackground obj = target as ClimbableBackground;
    Vector2 origin = obj.transform.position;
    if( Application.isPlaying )
      origin = obj.origin;
    if( obj.localPath != null && obj.localPath.Length > 0 )
    {
      Vector2[] points = obj.localPath;
      for( int i = 0; i < points.Length; i++ )
      {
        int next = (i + 1) % points.Length;
        Popcron.Gizmos.Line( origin + points[i], origin + points[next], Color.white );
        Popcron.Gizmos.Circle( origin + points[i], 0.025f, Quaternion.identity, Color.yellow );
      }
    }
  }
  void OnSceneGUI()
  {
    Handles.color = Color.yellow;
    ClimbableBackground obj = target as ClimbableBackground;
    Vector2 origin = obj.transform.position;
    if( Application.isPlaying )
      origin = obj.origin;
    if( obj.localPath != null && obj.localPath.Length > 0 )
    {
      Vector2[] points = obj.localPath;
      for( int i = 0; i < points.Length; i++ )
      {
        Handles.DrawLine( origin + points[i], origin + points[(i + 1) % points.Length] );
        Handles.DrawSolidDisc( origin + points[i], -Vector3.forward, 0.025f );
      }
      /*Handles.DrawSolidDisc( origin + Util.FindClosestPointOnPath( obj.localTest, obj.path ), -Vector3.forward, 0.05f );*/
    }
  }
}
#endif

public class ClimbableBackground : MonoBehaviour
{
  [FormerlySerializedAs( "SnapToVertical" )]
  public bool SnapToPath;
  public Vector2 origin;
  public Vector2[] localPath;
}