﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Gizmos = Popcron.Gizmos;

public class Airbot : Entity
{
  Timer hitPauseTimer = new Timer();
  const float hitPauseOffset = 0.5f;
  private Vector2 direction;

  // Attack
  [SerializeField] float AttackSpeed = 3;
  private float speed;
  public float targetOffset = 0.5f;
  Vector3 targetPosition;
  public Vector2 lastKnownTargetDirection;
  public Vector2 lastKnownTargetPosition;

  // sight, target
  [SerializeField] Transform sightOrigin;
  [SerializeField] Entity visibleTarget;
  public float sightRange = 6;
  Timer SightPulseTimer = new Timer();
  SortedList<float, EntityColliderPair> nearby = new SortedList<float, EntityColliderPair>();

  protected override void Start()
  {
    base.Start();
    collideMask = Global.CharacterCollideLayers & ~LayerMask.GetMask( new string[] { "twowayPlatform" } );
    UpdateLogic = AirbotLogic;
    UpdateHit = AirbotHit;
    UpdateCollision = BoxCollision;
  }

  void SightPulse()
  {
    nearby.Clear();
    int count = Physics2D.OverlapCircleNonAlloc( transform.position, sightRange, Global.ColliderResults, Global.EnemyInterestLayers );
    for( int i = 0; i < count; i++ )
    {
      Collider2D cld = Global.ColliderResults[i];
      Entity potentialTarget = Global.ColliderResults[i].GetComponent<Entity>();
      if( potentialTarget != null && IsEnemyTeam( potentialTarget.TeamFlags ) )
      {
        float dist = Vector3.Distance( transform.position, potentialTarget.transform.position );
        if( !nearby.ContainsKey( dist ) )
          nearby.Add( dist, new EntityColliderPair { entity = potentialTarget, collider = cld } );
      }
    }
  }

  protected override void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    base.OnDestroy();
    hitPauseTimer.Stop( false );
    SightPulseTimer.Stop( false );
  }

  void AssignTarget( Entity target )
  {
    visibleTarget = target;
    lastKnownTargetDirection = visibleTarget.velocity;
    lastKnownTargetPosition = target.transform.position;
    // todo Animator.StringToHash
    animator.Play( "alert" );
    speed = AttackSpeed;
    pathAgent.SetPath( (Vector2)target.transform.position + Vector2.up * targetOffset );
    searchCountdownTimer.Start( this, searchCountdownDuration );
  }

  void AirbotLogic()
  {
    if( !SightPulseTimer.IsActive )
    {
      SightPulse();
      SightPulseTimer.Start( this, 1 );
    }

    if( visibleTarget == null && Instigator != null )
    {
      AssignTarget( Instigator );
    }
    else if( nearby.Count == 0 )
    {
      // todo Animator.StringToHash
      animator.Play( "idle" );
      Wander();
    }
    else
    {
      if( !hitPauseTimer.IsActive )
      {
        visibleTarget = null;
        Vector2 targetpos;
        foreach( var pair in nearby )
        {
          if( pair.Value.collider == null )
            continue;
          targetpos = pair.Value.collider.bounds.center;
          // check line of sight to potential target
          hitCount = Physics2D.LinecastNonAlloc( sightOrigin.position, targetpos, Global.RaycastHits, Global.SightObstructionLayers );
          if( hitCount == 0 )
          {
            AssignTarget( pair.Value.entity );
            break;
          }
        }
        if( visibleTarget == null )
        {
          if( searchCountdownTimer.IsActive )
          {
            // todo Animator.StringToHash
            animator.Play( "idle" );
            Search();
          }
          else
          {
            // todo Animator.StringToHash
            animator.Play( "idle" );
            Wander();
          }
        }
      }
    }

    pathAgent.UpdatePath();
    Vector2 moveDirection = pathAgent.MoveDirection;
    if( !pathAgent.HasPath )
    {
      // no path found, so sample the nave mesh
      NavMeshHit navhit;
      if( NavMesh.SamplePosition( transform.position, out navhit, 1.0f, NavMesh.AllAreas ) )
      {
        moveDirection = navhit.position - transform.position;
      }
      else
      {
        // there is probably no nav mesh to sample, so try to move towards target position
        if( visibleTarget != null )
          moveDirection = visibleTarget.transform.position - transform.position;
        else
        {
          // stuck!
          moveDirection = Vector2.zero;
          speed = 0;
        }
      }
    }
    velocity = moveDirection.normalized * speed;
  }


  [Header( "Wander" )]
  [SerializeField] float WanderSpeed = 1;
  Timer wanderTimer = new Timer();
  public float WanderRadius = 0.5f;
  public float WanderInterval = 3;
  public float WanderAngleChange = 15;

  void Wander()
  {
    speed = WanderSpeed;
    if( !wanderTimer.IsActive )
    {
      if( (bottomHitCount + topHitCount + leftHitCount + rightHitCount) > 0 )
      {
        Vector2 newDirection = Vector2.down;
        if( bottomHitCount > 0 )
          newDirection = Vector2.up;
        if( topHitCount > 0 )
          newDirection = Vector2.down;
        if( leftHitCount > 0 )
          newDirection = Vector2.right;
        if( rightHitCount > 0 )
          newDirection = Vector2.left;
        direction = Quaternion.AngleAxis( 30 * (Random.value * 2 - 1), Vector3.forward ) * newDirection * WanderRadius;
      }
      else if( !pathAgent.HasPath )
      {
        if( direction == default )
          direction = Random.insideUnitCircle * WanderRadius;
        direction = Quaternion.AngleAxis( WanderAngleChange * (Random.value * 2 - 1), Vector3.forward ) * direction.normalized * WanderRadius;
      }
      pathAgent.SetPath( transform.position + (Vector3)direction );
      wanderTimer.Start( this, WanderInterval );
    }
  }

  [Header( "Search" )]
  public float SearchSpeed = 2.5f;
  public float SearchRadius = 2;
  Timer fanTimer = new Timer();
  public float anglerange;
  Timer searchCountdownTimer = new Timer();
  public float searchCountdownDuration = 5;

  void Search()
  {
    if( !fanTimer.IsActive )
      fanTimer.Start( this, 10, null, null );
#if UNITY_EDITOR
    if( fanTimer.IsActive )
    {
      Vector3 vecMax, vecMin;
      anglerange = 180f * fanTimer.ProgressNormalized;
      vecMax = Quaternion.AngleAxis( anglerange, Vector3.forward ) * lastKnownTargetDirection.normalized * SearchRadius;
      vecMin = Quaternion.AngleAxis( -anglerange, Vector3.forward ) * lastKnownTargetDirection.normalized * SearchRadius;
      Gizmos.Line( transform.position, transform.position + vecMax, Color.white );
      Gizmos.Line( transform.position, transform.position + vecMin, Color.white );
      Gizmos.Line( transform.position, transform.position + (Vector3)direction, Color.green );
    }
#endif
    //SightPulse();
    speed = SearchSpeed;
    if( !pathAgent.HasPath )
    {
      if( fanTimer.IsActive )
        direction = Quaternion.AngleAxis( 180f * fanTimer.ProgressNormalized * (Random.value * 2 - 1), Vector3.forward ) * lastKnownTargetDirection.normalized * SearchRadius;
      pathAgent.SetPath( transform.position + (Vector3)direction );
    }
  }

  void AirbotHit()
  {
    //hitCount = Physics2D.CircleCastNonAlloc( transform.position, circle.radius, velocity, Global.RaycastHits, raylength, Global.CharacterDamageLayers );
    hitCount = Physics2D.BoxCastNonAlloc( transform.position, box.size, 0, velocity, Global.RaycastHits, raylength, Global.CharacterDamageLayers );
    for( int i = 0; i < hitCount; i++ )
    {
      hit = Global.RaycastHits[i];
      if( hit.transform.IsChildOf( transform ) )
        continue;
      if( ReferenceEquals( transform, hit.transform ) || hit.transform.IsChildOf( transform ) )
        continue;
      if( DamageOther( hit.transform.GetComponent<IDamage>(), ContactDamageDefinition, transform, hit ) )
      {
        pathAgent.SetPath( new Vector3( hit.point.x, hit.point.y, 0 ) + Vector3.up * hitPauseOffset );
        HitPause();
      }
    }
  }

  void HitPause()
  {
    // todo Animator.StringToHash
    animator.Play( "laugh" );
    hitPauseTimer.Start( this, 1, null, delegate { animator.Play( "idle" ); } );
  }

  public override void Die( Damage damage )
  {
    if( dead )
      return;

    // Alert nearby allies that I'm being attacked, if they have line of sight to me.
    if( damage.instigator != null )
    {
      int count = Physics2D.OverlapCircleNonAlloc( transform.position, 3, Global.ColliderResults, LayerMask.GetMask( new string[] { "entity" } ) );
      if( count > 0 )
      {
        for( int i = 0; i < count; i++ )
        {
          Collider2D cld = Global.ColliderResults[i];
          Entity entity = cld.GetComponent<Entity>();
          if( entity != null && !ReferenceEquals( this, entity ) && entity.TeamFlags == TeamFlags )
          {
            int rcount = Physics2D.LinecastNonAlloc( transform.position, cld.transform.position, Global.RaycastHits, Global.SightObstructionLayers );
            if( rcount == 0 )
              entity.Instigator = damage.instigator;
          }
        }
      }
    }

    base.Die( damage );
  }
}