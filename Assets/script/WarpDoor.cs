using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(WarpDoor) )]
[CanEditMultipleObjects]
public class WarpDoorEditor : Editor
{
  public override void OnInspectorGUI()
  {
    WarpDoor door = target as WarpDoor;
    if( door.Connected != null )
    {
      if( door.EnforceOffsetInEditor )
      {
        door.Connected.transform.position = (Vector2)door.transform.position + ((door.OffsetDownward ? Vector2.down : Vector2.up) * Global.GameplayLayerOffset);
        // disable other door offset to avoid recursive shennanigans
        ((WarpDoor)door.Connected).EnforceOffsetInEditor = false;
      }
      if( GUI.Button( EditorGUILayout.GetControlRect(), "Select Connected Door" ) )
      {
        if( door.Connected != null )
        {
          Selection.activeGameObject = door.Connected.gameObject;
          SceneView.lastActiveSceneView.FrameSelected();
        }
      }
    }
    else
    {
      EditorGUILayout.LabelField( "This uses the Offset" );
      if( GUI.Button( EditorGUILayout.GetControlRect(), "Create Connected Door" ) )
      {
        GameObject go = (GameObject)PrefabUtility.InstantiatePrefab( PrefabUtility.GetCorrespondingObjectFromSource( door.gameObject ) );
        go.transform.position = (door.OffsetDownward ? Vector2.down : Vector2.up) * Global.GameplayLayerOffset;
        WarpDoor other = go.GetComponent<WarpDoor>();
        door.Connected = other;
        door.EnforceOffsetInEditor = true;
        other.Connected = door;
        other.GameplayLayer = door.GameplayLayer + (door.OffsetDownward ? 1 : -1);
        EditorUtility.SetDirty( door );
        EditorUtility.SetDirty( other );
      }
    }
    door.EnforceOffsetInEditor = EditorGUILayout.Toggle( "Enforce Offset", door.EnforceOffsetInEditor );
    door.OffsetDownward = EditorGUILayout.Toggle( "Offset is Downward", door.OffsetDownward );
    DrawDefaultInspector();
  }
}
#endif

public class WarpDoor : Door
{
  public int GameplayLayer;
  public Vector2Int textureDimension;
#if UNITY_EDITOR
  [HideInInspector] public bool EnforceOffsetInEditor;
  [HideInInspector] public bool OffsetDownward = true;
#endif

  [SerializeField] MeshRenderer portalMeshRenderer;
  Global.ViewPortal portal;
  public void PreSceneTransition() { Global.instance.DoneWithViewPortal( portal ); }

  public override void Start()
  {
    base.Start();
    OnOpen = delegate { isOpen = true; };
    OnClose = null;
  }

  public void EnterDoor( Entity[] entities )
  {
    if( isOpen )
    {
      Global.instance.DoorTransitionRender( entities, Connected.transform.position );
      Global.instance.GLR_MoveToLayer( ((WarpDoor)Connected).GameplayLayer );
    }
  }

  void OnWillRenderObject()
  {
    if( Connected != null && (portal == null || portal.owner != this) &&
       Global.instance.RequestViewPortal( ref portal ) && !Global.instance.LoadingScene )
    {
      if( portal.texture.width != textureDimension.x || portal.texture.height != textureDimension.y )
      {
        portal.texture.DiscardContents();
        portal.texture.Release();
        portal.texture = new RenderTexture( textureDimension.x, textureDimension.y, 24, RenderTextureFormat.Default, 0 );
        portal.texture.filterMode = FilterMode.Point;
        portal.camera.GetComponent<Camera>().targetTexture = portal.texture;
        portal.camera.GetComponent<Camera>().orthographicSize = 0.5f * textureDimension.y / Global.PixelDensity;
      }
      portal.owner = this;
      portalMeshRenderer.material.mainTexture = portal.texture;
      portal.camera.transform.position = Connected.transform.position;
      // Used in CityWrap, to avoid "dangling" portals
      portal.camera.transform.parent = Connected.transform;
    }
  }
}