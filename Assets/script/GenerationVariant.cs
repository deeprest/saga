﻿using UnityEngine;

public class GenerationVariant : MonoBehaviour
{
  public GameObject[] random;

  public GameObject Generate()
  {
    if( random.Length > 0 )
    {
      GameObject prefab = random[Random.Range( 0, random.Length )];
      if( prefab != null )
      {
        if( Application.isPlaying )
          return Global.instance.Spawn( prefab, transform.position, Quaternion.identity, transform.parent );
        else
          return Instantiate( prefab, transform.position, Quaternion.identity, transform.parent );
      }
    }
    return null;
  }
}