﻿using UnityEngine;
using UnityEngine.UI;

public class WeaponModUI : DiegeticUI
{
  [Header( "WeaponModUI" )]
  public WeaponMod WeaponMod;

  public GameObject StatTemplate;
  public GameObject ModStatTemplate;

  public Text FadeCoverModName;
  public Text FadeCoverText;
  public string FadeCoverMessage;
  public GameObject FadeCover;
  public Text Message;

  public Transform WeaponIconParent;
  GameObject weaponIconInstance;

  public Transform ModIconParent;
  GameObject modIconInstance;
  public Text ModName;
  public Text ModDescription;

  public AudioLoop MenuMusic;
  public AudioSource Source;

  public override void Start()
  {
    base.Start();
    FadeCoverModName.text = WeaponMod.Name;
  }

  public override void Highlight()
  {
    base.Highlight();
    FadeCoverText.text = Global.instance.ReplaceWithControlNames( FadeCoverMessage );
  }

  public override void Select( Entity instigator )
  {
    base.Select( instigator );
    Global.instance.MusicCrossFadeTo( MenuMusic, 1 );
    FadeCover.SetActive( false );
    Source.PlayOneShot( Global.instance.SoundSelect );

    Weapon wpn0 = ((PlayerBiped) Global.instance.CurrentPlayer).weapon;
    Weapon wpn1 = new Weapon( wpn0.def );
    WeaponMod.ApplyMod( wpn1 );
    ShowStatsForWeapon( wpn1.def );
    
    if( wpn0.abilityDef.AnimatedIcon != null )
      weaponIconInstance = Instantiate( wpn0.abilityDef.AnimatedIcon, WeaponIconParent );
    else if( wpn0.abilityDef.Icon != null )
      WeaponIconParent.GetComponent<Image>().sprite = wpn0.abilityDef.Icon;

    ShowModStats( WeaponMod );
    modIconInstance = Instantiate( WeaponMod.Icon, ModIconParent );
    ModName.text = WeaponMod.Name;
    ModDescription.text = WeaponMod.Description;
  }

  public override void Unselect()
  {
    base.Unselect();
    if( Global.instance.sceneScript.music != null )
      Global.instance.MusicCrossFadeTo( Global.instance.sceneScript.music, 2 );
    for( int i = 0; i < StatTemplate.transform.parent.childCount; i++ )
    {
      Transform child = StatTemplate.transform.parent.GetChild( i );
      if( child.gameObject == StatTemplate )
        continue;
      Destroy( child.gameObject );
    }
    for( int i = 0; i < ModStatTemplate.transform.parent.childCount; i++ )
    {
      Transform child = ModStatTemplate.transform.parent.GetChild( i );
      if( child.gameObject == ModStatTemplate )
        continue;
      Destroy( child.gameObject );
    }
    FadeCover.SetActive( true );
    Destroy( weaponIconInstance );
    Destroy( modIconInstance );
  }

  void ShowStatsForWeapon( WeaponDefinition weaponDefinition )
  {
    StatTemplate.SetActive( false );
    // create a copy to show stat changes without modifying original
    GameObject textObject = Instantiate( StatTemplate, StatTemplate.transform.parent );
    textObject.SetActive( true );
    textObject.GetComponent<Text>().text = weaponDefinition.GetStats();
  }

  void ShowModStats( WeaponMod mod )
  {
    ModStatTemplate.SetActive( false );
    GameObject textObject = Instantiate( ModStatTemplate, ModStatTemplate.transform.parent );
    textObject.SetActive( true );
    textObject.GetComponent<Text>().text = mod.GetStats();
  }

  public void ConfirmButtonPressed()
  {
    string message = "Success";
    bool success = ((PlayerBiped) Global.instance.CurrentPlayer).ModifyWeapon( ((PlayerBiped) Global.instance.CurrentPlayer).weapon, WeaponMod, ref message );
    Message.text = message;
    if( success )
      Source.PlayOneShot( Global.instance.SoundConfirm );
    else
      Source.PlayOneShot( Global.instance.SoundDenied );
  }
}