﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.U2D;

public class ChainsawDefinition : WeaponDefinition
{
  public override Weapon GetNewWeapon() { return new Chainsaw( this ); }
  
  [Header( "Chainsaw" )]
  public AudioClip soundActive;
  public AudioClip soundGrind;
  public DamageDefinition contactDamageDefinition;
}

public class Chainsaw : Weapon
{
  public Chainsaw( ChainsawDefinition rhsdef ) : base( rhsdef ) { chainsawDef = rhsdef; }
  public ChainsawDefinition chainsawDef;
  Material ChainMaterial;
  float offset;
  public float Speed = 1;
  float speed;
  public float Radius = 0.12f;
  public float Distance = 0.41f;
  AudioSource[] source;
  
  public SpriteShapeRenderer chainRenderer;
  public ParticleSystem sparks;
  public Light2D sparksLight;

  public override void Equip( Transform parentTransform )
  {
    base.Equip( parentTransform );

    chainRenderer = go.transform.GetChild( 0 ).GetComponent<SpriteShapeRenderer>();
#if UNITY_EDITOR
    if( !Application.isPlaying )
      ChainMaterial = chainRenderer.sharedMaterials[1];
    else
      ChainMaterial = chainRenderer.materials[1];
#else
    ChainMaterial = chainRenderer.materials[1];
#endif
    sparks = go.GetComponentInChildren<ParticleSystem>();
    sparksLight = go.GetComponentInChildren<Light2D>();
    source = go.GetComponents<AudioSource>();
  }

  public override void Activate( Vector2 origin, Vector2 aim )
  {
    IsActive = true;
    speed = Speed;
    source[0].clip = chainsawDef.soundActive;
    source[0].loop = true;
    source[0].Play();
  }

  public override void Deactivate()
  {
    IsActive = false;
    speed = 0;
    sparks.Stop();
    sparksLight.enabled = false;
    source[0].Stop();
    source[1].Stop();
  }

  public override void UpdateAbility()
  {
    if( IsActive )
    {
      offset += Time.deltaTime * speed;
      offset = Mathf.Repeat( offset, 1 );
      ChainMaterial.mainTextureOffset = new Vector2( offset, 0 );

      bool AtLeastOneHit = false;
      Vector2 origin = go.transform.position;
      if( biped != null )
        origin = biped.GetShotOriginPosition();
      int hitCount = Physics2D.CircleCastNonAlloc( origin, Radius, biped.GetAimVector(), Global.RaycastHits, Distance, Global.DefaultProjectileCollideLayers );
      for( int i = 0; i < hitCount; i++ )
      {
        RaycastHit2D hit = Global.RaycastHits[i];
        if( hit.transform.root == go.transform.root )
          continue;
        AtLeastOneHit = true;
        IDamage dam = hit.transform.GetComponent<IDamage>();
        if( dam != null )
        {
          Damage dmg = new Damage
          {
            def = chainsawDef.contactDamageDefinition,
            instigator = owner,
            damageSource = go.transform,
            hit = hit
          };
          DamageResult dmr = dam.CalculateDamageResult( dmg );
          if( !dmr.ignore )
            dmr.entity.TakeDamage( dmg );
        }

        sparks.Play();
        sparksLight.enabled = true;

        source[1].clip = chainsawDef.soundGrind;
        source[1].loop = true;
        source[1].Play();
        /*ParticleSystem.EmissionModule emit = sparks.emission;
        ParticleSystem.MinMaxCurve rate = emit.rateOverTime;
        rate.constant = 300;
        emit.rateOverTime = rate;
        ParticleSystem.VelocityOverLifetimeModule volm = sparks.velocityOverLifetime;
        volm.speedModifierMultiplier = 1;*/

        /*Global.instance.CameraController.orthoTarget = zoom;*/

        // We don't want the saw to cut through walls/doors and hit objects on the other side.
        break;
      }
      if( !AtLeastOneHit )
      {
        sparks.Stop();
        sparksLight.enabled = false;
        source[1].Stop();
      }
    }
    else
    {
      Global.instance.CameraController.orthoTarget = Global.instance.FloatSetting["Zoom"].Value;
    }
  }
}