// #pragma warning disable 414
// define LIGHTING_TOGGLE

using System.Collections;
using System.Collections.Generic;
using System.IO;
using Dungbeetle;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine.Tilemaps;
using LitJson;
using Ionic.Zip;
using UnityEngine.Serialization;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;
using Gizmos = Popcron.Gizmos;
using JsonUtil = deeprest.JsonUtil;
using SerializedObject = deeprest.SerializedObject;
#if LIGHTING_TOGGLE
using UnityEngine.Experimental.Rendering.Universal;
#endif
#if UNITY_EDITOR
using UnityEditor;

#endif

public class Global : MonoBehaviour
{
#region Header

  public static Global instance;
  public static bool IsQuiting;

  public static RaycastHit2D[] RaycastHits = new RaycastHit2D[32];
  public static Collider2D[] ColliderResults = new Collider2D[64];

  // Deserialization
  [SerializeField] Dictionary<string, GameObject> ResourceLookup = new Dictionary<string, GameObject>();

  [System.Serializable]
  public struct SerializedDictionaryPair
  {
    public string key;
    public string value;
  }

  public SerializedDictionaryPair[] ResourceReplacementKeys;
  Dictionary<string, string> replacements = new Dictionary<string, string>();


  [Header( "Global Settings" )]
  public SceneReference[] sceneRefs;
  [Tooltip( "Pretend this is a build we're running" )]
  public bool SimulatePlayer;
  public int Seed;

  public float UpdateCullDistance = 10;
  public int MaxUpdateCount = 100;

  public const float PixelDensity = 64;
  public static float Gravity = 16;
  public const float MaxVelocity = 60;
  // A target may be "within range" because of collider overlap, but fail a distance test between transforms. This helps prevent a descrepency.
  public const float MaxEntityRadius = 1;
  [SerializeField] SceneReference InitialScene;
  [SerializeField] SceneReference ReturnToThisSceneAfterBoss;

  // timescale when in slo-mo
  [SerializeField] float slowtime = 0.2f;
  Timer fadeTimer = new Timer();
  public float RepathInterval = 1;
  // sidestep
  /*public bool GlobalSidestepping = true;
  public float SidestepDistance = 1f;
  public float SidestepRaycastDistance = 1f;
  public float SidestepInterval = 1f;
  public float SidestepIgnoreWithinDistanceToGoal = 0.5f;*/

  public static string[] persistentFilenames = new string[]
  {
    "settings.json"
  };

  [Header( "Settings" )]
  public GameObject ToggleTemplate;
  public GameObject SliderTemplate;
  public GameObject StringTemplate;
  public GameObject DividerTemplate;

  string settingsPath { get { return Application.persistentDataPath + "/" + "settings.json"; } }

  public Dictionary<string, FloatValue> FloatSetting = new Dictionary<string, FloatValue>( System.StringComparer.OrdinalIgnoreCase );
  public Dictionary<string, BoolValue> BoolSetting = new Dictionary<string, BoolValue>( System.StringComparer.OrdinalIgnoreCase );
  public Dictionary<string, StringValue> StringSetting = new Dictionary<string, StringValue>( System.StringComparer.OrdinalIgnoreCase );
  // screen settings
  public GameObject ScreenSettingsPrompt;
  public Text ScreenSettingsCountdown;
  Timer ScreenSettingsCountdownTimer = new Timer();

  [Header( "References" )]
  public CameraController CameraController;
  [SerializeField] AudioClip VolumeChangeNoise;
  public ParticleSystem MissileSmoke;

  [Header( "Prefabs" )]
  public GameObject audioOneShotPrefab;
  public GameObject AvatarPrefab;

  [Header( "Transient (Assigned at runtime)" )]
  public bool Updating;
  [ReadOnly] public SceneScript sceneScript;
  [ReadOnly] public Pawn CurrentPlayer;
  [ReadOnly] public PlayerController PlayerController;

  public Dictionary<string, int> AgentType = new Dictionary<string, int>();
  NavMeshSurface[] meshSurfaces;

  [Header( "UI" )]
  public GameObject UI;
  [SerializeField] private DevConsole.DevConsoleUI devConsole;
  [SerializeField] Text BuildVersion;
  [SerializeField] Text BuildTimestamp;
  [SerializeField] Text CommitHash;
  CanvasScaler CanvasScaler;
  public GameObject PauseMenu;
  [SerializeField] UIScreen SceneList;
  public GameObject SceneListElementTemplate;
  [SerializeField] UIScreen MusicList;
  [SerializeField] BugReportUI BugReport;
  public GameObject MusicListElementTemplate;
  [SerializeField] GameObject HUD;
  DiegeticUI ActiveDiegetic;

  bool MenuShowing { get { return PauseMenu.activeInHierarchy; } }

  public GameObject LoadingScreen;
  [SerializeField] Image fader;
  public SpriteRenderer bossfade;
  public GameObject ready;
  [SerializeField] GameObject OnboardingControls;
  [SerializeField] Image RecordingIndicator;
  // cursor
  public float CursorOuter = 1;
  public float CursorSensitivity = 1;
  public bool AimSnap;
  public bool AutoAim;
  public bool ShowAimPath;
  public bool ShowInputDisplay;
  // status
  public Image weaponIcon;
  public Image abilityIcon;
  // settings
  public GameObject SettingsParent;
  [SerializeField] Selectable previousNavSelectable;
  public UIScreen ConfirmDialog;
  public bool HealthAudioPitch;
  public bool HealthTimescale;

  [Header( "Input" )]
  public Controls Controls;
  public bool UsingGamepad;
  public System.Action OnGameControllerChanged;
  public Color ControlNameColor = Color.red;
  Dictionary<string, string> ReplaceControlNames = new Dictionary<string, string>();
  // input display
  [SerializeField] InputResponse InputDisplayOnCamera;

  [Header( "Debug" )]
  [SerializeField] Text debugFPS;
  [SerializeField] Text debugText1;
  [SerializeField] Text debugText2;
  [SerializeField] Text debugText3;
  [SerializeField] Text debugText4;
  [SerializeField] Text debugText5;

  // loading screen
  public bool LoadingScene;
  float prog = 0;
  [SerializeField] Image progress;
  [SerializeField] float progressSpeed = 0.5f;

  Timer fpsTimer = new Timer();
  int frames;
  float zoomDelta = 0;

  [Header( "Misc" )]
  // color shift
  public Color shiftyColor = Color.red;
  [SerializeField] float colorShiftSpeed = 1;
  [SerializeField] Image shifty;
  // spinner on loading screen
  float progTarget = 0;
  [SerializeField] GameObject spinner;
  [SerializeField] float spinnerMoveSpeed = 1;
  int spawnCycleIndex = 0;
  [SerializeField] Team[] Teams;
  public DamageDefinition CrushDamageDefinition;

  [Header( "Audio" )]
  public AudioLoop[] Music;
  public UnityEngine.Audio.AudioMixer mixer;
  public UnityEngine.Audio.AudioMixerSnapshot snapSlowmo;
  public UnityEngine.Audio.AudioMixerSnapshot snapNormal;
  public UnityEngine.Audio.AudioMixerSnapshot snapMusicSilence;
  public float MusicTransitionDuration = 1f;
  // For audio mixer snapshots
  public float AudioFadeDuration = 0.1f;
  // Reserved for intro clips (Will replace with a decent plugin eventually "IntroClip" on asset store)
  [SerializeField] AudioSource musicSourceIntro;
  // These are used to crossfade between audio clips.
  [SerializeField] AudioSource musicSource0;
  [SerializeField] AudioSource musicSource1;
  AudioSource activeMusicSource;
  Timer musicCrossFadeTimer = new Timer();

  [Header( "Common / Global Refs" )]
  public AudioLoop Victory;
  public AudioClip AwesomeSting;
  [FormerlySerializedAs( "soundReflect" )]
  public AudioClip SoundReflect;
  [FormerlySerializedAs( "soundHit" )]
  public AudioClip SoundHit;
  public AudioClip SoundConfirm;
  public AudioClip SoundSelect;
  public AudioClip SoundDenied;
  public AudioClip SoundWeaponFail;
  public GameObject explosion;

  [Header( "Minimap" )]
  [SerializeField] Camera MinimapCamera;
  [SerializeField] GameObject Minimap;
  [SerializeField] float MinimapScrollerScale = 4;
  RenderTexture rt;
  RenderTexture rt2;
  [SerializeField] int pixelsPerUnit = 1;
  [FormerlySerializedAs( "bigsheet" ), SerializeField]
  Material bigsheetMaterial;
  [SerializeField] Material backgroundMaterial;
  [SerializeField] GameObject MinimapScroller;
  [SerializeField] float mmScrollSpeed = 800;
  [Header( "Minimap Render" )]
  [SerializeField] Shader mmShader;
  [SerializeField] Color[] mmColor;
  [SerializeField] RectTransform mmPlayer;
  [SerializeField] GameObject mmCharacterLayer;

#if LIGHTING_TOGGLE
  [Header( "Lighting Toggle" )]
  public static bool LightingEnabled;
  // to do mmx material; bosseye mat; buster shot use indexed shader; disable Global
  public Material[] LitMaterials;
  List<ShaderVariant> ShaderVariants = new List<ShaderVariant>();
  public Shader unlitShader;
  [System.Serializable]
  public struct ShaderVariant
  {
    public Material material;
    public Shader litShader;
  }
  public LightData[] lightComponents;
  [System.Serializable]
  public struct LightData
  {
    public Light2D light;
    public bool EnabledInPrefab;
    // blendstyle?
  }
#endif

#endregion

  // This will make sure there is always a GLOBAL object when playing a scene in the editor
  [RuntimeInitializeOnLoadMethod]
  static void OnLoadMethod()
  {
    Application.wantsToQuit += WantsToQuit;
#if UNITY_EDITOR
    EditorApplication.playModeStateChanged += OnPlayModeChange;
#endif
  }
#if UNITY_EDITOR
  static void OnPlayModeChange( PlayModeStateChange pmcs )
  {
    /*if( pmcs == PlayModeStateChange.ExitingPlayMode )
    {
      Global.instance.SetSkin( 0 );
    }*/
  }
#endif

  static bool WantsToQuit()
  {
    IsQuiting = true;
    // do pre-quit stuff here
    if( Global.instance != null )
      Global.instance.WriteSettings();
#if LIGHTING_TOGGLE
    // return project shaders to normal
    if( Application.isEditor )
      Global.instance.EnableLighting();
#endif

#if UNITY_EDITOR
    EditorApplication.playModeStateChanged -= OnPlayModeChange;
#endif
    return true;
  }

  public static void CustomLogger( string condition, string stackTrace, LogType type )
  {
    if( Global.instance == null )
      return;
    switch( type )
    {
      case LogType.Error:
        Global.instance.devConsole.LogError( condition + stackTrace );
        break;

      case LogType.Exception:
        Global.instance.devConsole.LogError( condition + stackTrace );
        break;

      case LogType.Assert:
        Global.instance.devConsole.LogError( condition + stackTrace );
        break;

      case LogType.Warning:
        Global.instance.devConsole.LogWarning( condition + stackTrace );
        break;

      case LogType.Log:
        Global.instance.devConsole.Log( condition );
        break;
    }
  }

  // moved these to be closer to their definitions
  public static int CharacterCollideLayers;
  public static int CharacterCollideLayersNoTwoWayPlatform;
  public static int CharacterDamageLayers;
  public static int CrushDamageLayers;
  public static int TriggerLayers;
  public static int WorldSelectableLayers;
  public static int ProjectileNoShootLayers;
  public static int DefaultProjectileCollideLayers;
  public static int FlameProjectileCollideLayers;
  public static int DamageCollideLayers;
  public static int StickyBombCollideLayers;
  public static int EnemyInterestLayers;
  public static int SightObstructionLayers;

  void Awake()
  {
    if( instance != null )
    {
      Destroy( gameObject );
      return;
    }
    instance = this;
    DontDestroyOnLoad( gameObject );

    // note: allowing characters to collide with each other introduces the risk of being forced into a corner.
    CharacterCollideLayers = LayerMask.GetMask( "Default", "destructible", "triggerAndCollision", "twowayPlatform", "entityCollisionOnly" );
    CharacterCollideLayersNoTwoWayPlatform = CharacterCollideLayers & ~LayerMask.GetMask( "twowayPlatform" );
    CharacterDamageLayers = LayerMask.GetMask( "entity", "destructible" );
    CrushDamageLayers = CharacterCollideLayersNoTwoWayPlatform | LayerMask.GetMask( "entity" );
    TriggerLayers = LayerMask.GetMask( "trigger", "triggerAndCollision", "rigidbody" );
    WorldSelectableLayers = LayerMask.GetMask( "worldselect", "entity" );
    ProjectileNoShootLayers = LayerMask.GetMask( "Default" );
    DefaultProjectileCollideLayers = LayerMask.GetMask( "Default", "entity", "junk", "projectileCollide", "triggerAndCollision", "destructible" );
    FlameProjectileCollideLayers = LayerMask.GetMask( "Default", "entity", "triggerAndCollision", "destructible" );
    DamageCollideLayers = LayerMask.GetMask( "entity", "triggerAndCollision", "projectile", "destructible" );
    StickyBombCollideLayers = LayerMask.GetMask( "Default", "entity", "junk", "triggerAndCollision", "projectileCollide", "destructible" );
    EnemyInterestLayers = LayerMask.GetMask( "entity" );
    SightObstructionLayers = LayerMask.GetMask( "Default", "triggerAndCollision" ); /*, "destructible"} );*/

    CanvasScaler = UI.GetComponent<CanvasScaler>();
    BuildVersion.text = GeneratedBuildInfo.BuildVersion;
    BuildTimestamp.text = GeneratedBuildInfo.BuildTimestamp;
    CommitHash.text = GeneratedBuildInfo.CommitHash;

    // musicSource1 is used as the loop source
    activeMusicSource = musicSource1;

    // intialize input before settings so that Controls exists for the Input Display objects
    InitializeInput();
    InitializeSettings();
    VerifyPersistentData();
    ReadSettings();
    ApplyScreenSettings();
    IntializeViewPortals();
    InitializeProgressFlags();

    if( BoolSetting["RandomSeed"].Value )
      Seed = System.DateTime.Now.Second;
    else
      Seed = Mathf.RoundToInt( FloatSetting["Seed"].Value ) % 20;
    UnityEngine.Random.InitState( Seed );
    PlayerController = ScriptableObject.CreateInstance<PlayerController>();
    // WarpDoor transition
    renderTexture = new RenderTexture( ResolutionWidth, ResolutionHeight, 24, RenderTextureFormat.Default, 0 );

    // SCRIPT EXECUTION ORDER Global.cs is first priority so that Awake called from scene load in editor respects the code below.
    //Entity.Limit.UpperLimit = 1000;
    Entity.Limit.EnforceUpper = false;

    GUI.enabled = false;
#if UNITY_STANDALONE
    Application.logMessageReceived += CustomLogger;
    // show unhandled excpetions on the dev console 
    System.AppDomain.CurrentDomain.UnhandledException += ( sender, args ) => devConsole.LogError( sender.ToString() + args.ToString() );
#endif
    SceneManager.sceneLoaded += delegate( Scene arg0, LoadSceneMode arg1 ) { Debug.Log( "scene loaded: " + arg0.name ); };
    SceneManager.activeSceneChanged += delegate( Scene arg0, Scene arg1 ) { 
      
      Debug.Log( "active scene changed from " + arg0.name + " to " + arg1.name );
      //Timer.OnSceneChange();
    };

    GameObject[] res = Resources.LoadAll<GameObject>( "" );
    foreach( GameObject go in res )
      ResourceLookup.Add( go.name, go );
    foreach( var pair in ResourceReplacementKeys )
      replacements.Add( pair.key, pair.value );

    meshSurfaces = FindObjectsOfType<NavMeshSurface>();
    foreach( var mesh in meshSurfaces )
      AgentType[NavMesh.GetSettingsNameFromID( mesh.agentTypeID )] = mesh.agentTypeID;

    TimerParams fpsTimerParams = new TimerParams();
    fpsTimerParams.unscaledTime = true;
    fpsTimerParams.repeat = true;
    fpsTimerParams.loops = int.MaxValue;
    fpsTimerParams.interval = 1;
    fpsTimerParams.IntervalDelegate = delegate( Timer tmr )
    {
      debugFPS.text = frames.ToString();
      frames = 0;
    };
    fpsTimer.Start( this, fpsTimerParams );

    RecordingIndicator.gameObject.SetActive( false );

    HideHUD();
    HideMinimap();
    HidePauseMenu();
    HideLoadingScreen();
    SpeechBubble.SetActive( false );
    SpeechTextWorld.SetActive( false );
    BugReport.gameObject.SetActive( false );
    Gizmos.Enabled = false;

#if LIGHTING_TOGGLE
    unlitShader = Shader.Find( "Sprites/Default" );
    foreach( var material in LitMaterials )
      ShaderVariants.Add( new ShaderVariant(){material = material,litShader = material.shader});
#endif
  }

  void Start()
  {
#if UNITY_EDITOR
    // workaround for Unity Editor_bug where AudioMixer.SetFloat() does not work in Awake()
    mixer.SetFloat( "MasterVolume", Util.DbFromNormalizedVolume( FloatSetting["MasterVolume"].Value ) );
    mixer.SetFloat( "MusicVolume", Util.DbFromNormalizedVolume( FloatSetting["MusicVolume"].Value ) );
    mixer.SetFloat( "SFXVolume", Util.DbFromNormalizedVolume( FloatSetting["SFXVolume"].Value ) );
#endif

    if( Application.isEditor && !SimulatePlayer )
    {
      LoadingScreen.SetActive( false );
      fader.color = Color.clear;
      Updating = true;
      sceneScript = FindObjectOfType<SceneScript>();
      if( sceneScript != null )
        sceneScript.StartScene();
      else if( CurrentPlayer == null )
        SpawnPlayer();
      GenerateNavMesh();
    }
    else
    {
      LoadScene( InitialScene, true, false, true, true, null );
    }
  }

  public string ReplaceWithControlNames( string source, bool colorize = true )
  {
    // This is inefficient. DO NOT call this every frame.
    // todo make this less awful, and support composites
    //action.GetBindingDisplayString( InputBinding.DisplayStringOptions.DontUseShortDisplayNames );

    string outstr = "";
    string[] tokens = source.Split( new char[] {'['} );
    foreach( var tok in tokens )
    {
      if( tok.Contains( "]" ) )
      {
        string[] ugh = tok.Split( new char[] {']'} );
        if( ugh.Length > 2 )
          return "BAD FORMAT";
        int inputTypeIndex = UsingGamepad ? 1 : 0;
        // warning!! controls must be in correct order per-action: keyboard+mouse first, then gamepad
        InputAction ia = Controls.BipedActions.Get().FindAction( ugh[0] );
        if( ia == null )
          ia = Controls.MenuActions.Get().FindAction( ugh[0] );
        if( ia == null )
          ia = Controls.GlobalActions.Get().FindAction( ugh[0] );
        if( ia == null )
          ia = Controls.SpeechActions.Get().FindAction( ugh[0] );
        if( ia == null )
          return "ACTION NOT FOUND: " + ugh[0];
        if( ia.controls.Count <= inputTypeIndex )
          return "(no binding)";
        InputControl ic = ia.controls[inputTypeIndex];
        string controlName = "BAD NAME";
        if( ic.shortDisplayName != null )
          controlName = ic.shortDisplayName;
        else
          controlName = ic.name.ToUpper();

        if( ReplaceControlNames.ContainsKey( controlName.ToUpper() ) )
          controlName = ReplaceControlNames[controlName.ToUpper()];

        if( colorize )
          outstr += "<color=#" + ColorUtility.ToHtmlStringRGB( ControlNameColor ) + ">" + controlName + "</color>";
        else
          outstr += controlName;
        outstr += ugh[1];
      }
      else
        outstr += tok;
    }
    return outstr;
  }

  void InitializeInput()
  {
    InputSystem.onDeviceChange += ( device, change ) =>
    {
      switch( change )
      {
        case InputDeviceChange.Added:
          Debug.Log( "Device added: " + device );
          break;

        case InputDeviceChange.Removed:
          Debug.Log( "Device removed: " + device );
          break;

        case InputDeviceChange.ConfigurationChanged:
          Debug.Log( "Device configuration changed: " + device );
          break;
      }
    };

    ReplaceControlNames.Add( "DELTA", "Mouse" );
    ReplaceControlNames.Add( "LMB", "Left Mouse Button" );
    ReplaceControlNames.Add( "RMB", "Right Mouse Button" );
    ReplaceControlNames.Add( "LB", "Left Bumper" );
    ReplaceControlNames.Add( "RB", "Right Bumper" );
    ReplaceControlNames.Add( "LT", "Left Trigger" );
    ReplaceControlNames.Add( "RT", "Right Trigger" );

    Controls = new Controls();
    Controls.Enable();
    Controls.MenuActions.Disable();
    Controls.SpeechActions.Disable();

    /*Controls.GlobalActions.Any.performed += ( obj ) =>
    {
      bool newvalue = obj.control.device.name.Contains( "Gamepad" );
      if( newvalue != UsingGamepad )
      {
        UsingGamepad = newvalue;
        OnGameControllerChanged.Invoke();
        OnboardingControls.GetComponent<OnboardingControls>().UpdateText();
      }
    };*/

    Controls.GlobalActions.Quit.performed += ( obj ) => Application.Quit();
    Controls.GlobalActions.Menu.performed += ( obj ) => TogglePauseMenu();
    Controls.DevActions.DEVPause.performed += ( obj ) =>
    {
      if( Paused )
        Unpause();
      else
        Pause();
    };

    Controls.GlobalActions.ConsoleToggle.performed += context =>
    {
      if( !devConsole.gameObject.activeSelf )
      {
        devConsole.Activate();
        Controls.BipedActions.Disable();
        Controls.MenuActions.Disable();
        Controls.DevActions.Disable();
      }
      else
      {
        devConsole.Deactivate();
        Controls.BipedActions.Enable();
        Controls.MenuActions.Enable();
        Controls.DevActions.Enable();
      }
    };
    Controls.GlobalActions.Screenshot.performed += ( obj ) => Util.Screenshot();

    Controls.DevActions.DEVRecordToggle.performed += ( obj ) =>
    {
      PlayerController.RecordToggle();
      RecordingIndicator.gameObject.SetActive( PlayerController.IsRecording() );
    };

    Controls.DevActions.DEVRecordPlayback.performed += ( obj ) =>
    {
      PlayerController.PlaybackToggle();
      RecordingIndicator.gameObject.SetActive( PlayerController.IsRecording() );
    };

    Controls.MenuActions.Back.performed += ( obj ) =>
    {
      if( MenuShowing )
      {
        /*PauseMenu.SetActive( false );*/
      }
      else
      {
        // ...or if there is none, close diagetic UI
        if( ActiveDiegetic != null && CurrentPlayer != null )
          CurrentPlayer.UnselectWorldSelection();
      }
    };

    // DEVELOPMENT
    Controls.DevActions.DEVGizmos.performed += ( obj ) => { Gizmos.Enabled = !Gizmos.Enabled; };
    Controls.DevActions.DEVRespawn.performed += ( obj ) => { Respawn(); };
    Controls.DevActions.DEVSlow.performed += ( obj ) =>
    {
      if( Slowed )
        NoSlow();
      else
        Slow();
    };

    Controls.DevActions.DEVSave.performed += ( obj ) => { SaveGame( "SAVETEST" ); };
    Controls.DevActions.DEVLoad.performed += ( obj ) => { LoadGame( "SAVETEST" ); };

    if( Application.isEditor )
    {
      InputAction DevCursorUnlock = new InputAction( "CursorUnlock", InputActionType.Button, "<Keyboard>/escape" );
      DevCursorUnlock.Enable();
      DevCursorUnlock.performed += context =>
      {
        if( Cursor.lockState != CursorLockMode.None )
        {
          Cursor.lockState = CursorLockMode.None;
          Cursor.visible = true;
        }
        else
        {
          Cursor.lockState = CursorLockMode.Locked;
          Cursor.visible = false;
        }
      };
      Controls.GlobalActions.Menu.AddBinding( "<Keyboard>/tab" );
    }
    else
    {
      Controls.GlobalActions.Menu.AddBinding( "<Keyboard>/escape" );
    }

    Controls.GlobalActions.BugReport.performed += context => { ShowBugReport(); };

    Controls.SpeechActions.Continue.performed += context =>
    {
      if( SpeechContinueTimer.IsActive )
        SpeakContinue();
    };
    Controls.SpeechActions.End.performed += context => { SpeakCancel(); };
  }

  public void LoadScene( SceneReference scene, bool waitForFadeIn = true, bool spawnPlayer = true, bool fadeOut = true, bool showLoadingScreen = true, System.Action onFail = null ) { StartCoroutine( LoadSceneRoutine( scene.GetSceneName(), waitForFadeIn, spawnPlayer, fadeOut, showLoadingScreen, onFail ) ); }

  public void LoadScene( string scene, bool waitForFadeIn = true, bool spawnPlayer = true, bool fadeOut = true, bool showLoadingScreen = true, System.Action onFail = null ) { StartCoroutine( LoadSceneRoutine( scene, waitForFadeIn, spawnPlayer, fadeOut, showLoadingScreen, onFail ) ); }

  IEnumerator LoadSceneRoutine( string scene, bool waitForFadeIn = true, bool spawnPlayer = true, bool fadeOut = true, bool showLoadingScreen = true, System.Action onFail = null )
  {
    Updating = false;
    if( fadeOut )
    {
      FadeBlack();
      while( fadeTimer.IsActive )
        yield return null;
    }
    else
    {
      fader.color = Color.black;
      fader.gameObject.SetActive( true );
      yield return null;
    }
    
    Pause();
    HUD.SetActive( false );
    if( showLoadingScreen )
      yield return ShowLoadingScreenRoutine( "Loading... " + scene );

    // pre scene transition
    if( CurrentPlayer != null )
    {
      CurrentPlayer.PreSceneTransition();
      SceneManager.MoveGameObjectToScene( CurrentPlayer.gameObject, gameObject.scene );
      // prevent portal cameras from being destroyed along with WarpDoors.
      foreach( var portal in portals )
        DoneWithViewPortal( portal );
    }
    // reaffirm current camera settings in case they were left in an undesired state
    CameraController.RotationExperiment = BoolSetting["RotationExperiment"].Value;
    CameraController.orthoTarget = FloatSetting["Zoom"].Value;
    CameraController.cam.orthographicSize = CameraController.orthoTarget;

    CameraController.PreSceneTransition();
    for( int i = 0; i < Controller.All.Count; i++ )
      Controller.All[i].PreSceneTransition();

    SpeechTimer.Stop( true );
    SpeechContinueTimer.Stop( false );

    LoadingScene = true;
    progress.fillAmount = 0;
    AsyncOperation asyncOperation = SceneManager.LoadSceneAsync( scene, LoadSceneMode.Single );
    if( asyncOperation != null )
    {
      while( !asyncOperation.isDone )
      {
        progress.fillAmount = asyncOperation.progress;
        yield return null;
      }
      
      //Timer.OnSceneChange();

      for( int i = 0; i < Controller.All.Count; i++ )
        Controller.All[i].PostSceneTransition();

      if( CurrentPlayer != null )
      {
        CurrentPlayer.transform.position = FindBestSpawnPosition();
        CurrentPlayer.PostSceneTransition();
      }
      else if( spawnPlayer )
      {
        SpawnPlayer();
      }

      // Scene Script should start it's music.
      sceneScript = FindObjectOfType<SceneScript>();
      if( sceneScript )
        sceneScript.StartScene();

      GenerateNavMesh();
      /*Global.instance.MinimapRender( meshSurfaces[0].center );*/
    }
    else
    {
      Debug.LogError( "Scene failed to load: " + scene );
      onFail?.Invoke();
    }

    LoadingScene = false;
    // if scene load fails, allow normal play to continue
    HUD.SetActive( true );
    progress.fillAmount = 1;
    yield return null;
    Unpause();
    if( showLoadingScreen )
      HideLoadingScreen();
    FadeClear();
    if( waitForFadeIn )
      while( fadeTimer.IsActive )
        yield return null;
    Updating = true;
  }

  /*[ExposeMethod]
  public void GenerateNavMesh()
  {
    if( Application.isEditor && !Application.isPlaying )
    {
      meshSurfaces = FindObjectsOfType<NavMeshSurface>();
      foreach( var mesh in meshSurfaces )
        AgentType[NavMesh.GetSettingsNameFromID( mesh.agentTypeID )] = mesh.agentTypeID;
    }
    GameObject generatedMeshCollider = Util.GenerateNavMeshForEdgeColliders();
    foreach( var mesh in meshSurfaces )
      mesh.BuildNavMesh();
    Util.Destroy( generatedMeshCollider );
  }*/

  [ExposeMethod]
  public void GenerateNavMesh()
  {
    if( Application.isEditor && !Application.isPlaying )
    {
      meshSurfaces = FindObjectsOfType<NavMeshSurface>();
      foreach( var mesh in meshSurfaces )
        AgentType[NavMesh.GetSettingsNameFromID( mesh.agentTypeID )] = mesh.agentTypeID;
    }
    List<NavMeshBuildSource> sources = new List<NavMeshBuildSource>();
    if( sceneScript == null )
      sceneScript = FindObjectOfType<SceneScript>();
    if( sceneScript != null )
    {
      Tilemap tilemap = sceneScript.tilemap;
      if( tilemap != null )
      {
        NavMeshBuildSource navMeshBuildSource = new NavMeshBuildSource();
        navMeshBuildSource.size = Vector3.one;
        tilemap.CompressBounds();
        Vector3 pos;
        for( int x = tilemap.cellBounds.xMin; x < tilemap.cellBounds.xMax; x++ )
        {
          for( int y = tilemap.cellBounds.yMin; y < tilemap.cellBounds.yMax; y++ )
          {
            pos = new Vector3( x, y, 0 );
            Vector3Int posint = new Vector3Int( Mathf.FloorToInt( pos.x ), Mathf.FloorToInt( pos.y ), 0 );
            bool valid = !tilemap.HasTile( posint );
            if( !valid )
            {
              TileBase tilebase = tilemap.GetTile( posint );
              TileData tiledata = new TileData();
              tilebase.GetTileData( posint, null, ref tiledata );
              valid = tiledata.colliderType == Tile.ColliderType.None;
            }
            if( valid )
            {
              navMeshBuildSource.component = tilemap;
              navMeshBuildSource.shape = NavMeshBuildSourceShape.Box;
              navMeshBuildSource.size = Vector3.one * (tilemap.cellSize.x + 0.05f);
              navMeshBuildSource.transform = GetCellTransformMatrix( tilemap, Vector3.one, posint );
              sources.Add( navMeshBuildSource );
            }
          }
        }
      }
    }
    GameObject generatedMeshCollider = Util.GenerateNavMeshForEdgeColliders();
    foreach( var mesh in meshSurfaces )
      mesh.BuildNavMesh( sources );
    Util.Destroy( generatedMeshCollider );
  }

  public static Matrix4x4 GetCellTransformMatrix( Tilemap tilemap, Vector3 scale, Vector3Int vec3int ) { return Matrix4x4.TRS( Vector3.Scale( tilemap.GetCellCenterWorld( vec3int ), scale ) - tilemap.layoutGrid.cellGap, tilemap.transform.rotation, tilemap.transform.lossyScale ) * tilemap.orientationMatrix * tilemap.GetTransformMatrix( vec3int ); }

  void EXPUpdate()
  {
    frames++;
    Timer.UpdateTimers();
    /*debugText1.text = Camera.main.orthographicSize.ToString( "##.#" );
    debugText2.text = "Active Timers: " + Timer.ActiveTimers.Count;
    debugText3.text = "Remove Timers: " + Timer.RemoveTimers.Count;
    debugText4.text = "New Timers: " + Timer.NewTimers.Count;*/

    if( !Updating )
      return;

    if( GLR_Enabled )
    {
      // track camera position and autoselect layer
      int gamelayer = Mathf.FloorToInt( Camera.main.transform.position.y / GameplayLayerOffset * 0.5f );
      if( GLR_CurrentLayer != gamelayer )
        GLR_MoveToLayer( gamelayer );
    }

    if( !Paused )
    {
      if( sceneScript != null )
        sceneScript.UpdateScene();

      // update all entities within the cull radius
      Vector3 ppos = Camera.main.transform.position;
      Entity reference;
      int count = 0;
      for( int i = 0; i < Entity.Limit.All.Count; i++ )
      {
        reference = Entity.Limit.All[i];
        reference.culled = Vector3.SqrMagnitude( reference.transform.position - ppos ) > UpdateCullDistance * UpdateCullDistance;
        if( (reference.IgnoreCull || !reference.culled) && reference.gameObject.activeInHierarchy && reference.enabled )
        {
          reference.EntityUpdate();
          count++;
        }
      }
      // update entites outside the cull radius, up to the max update count
      for( int i = 0; i < Entity.Limit.All.Count && count < MaxUpdateCount; i++ )
      {
        reference = Entity.Limit.All[i];
        if( reference.culled && reference.gameObject.activeInHierarchy && reference.enabled )
        {
          reference.EntityUpdate();
          count++;
        }
      }
      debugText5.text = count.ToString();

      for( int i = 0; i < Controller.All.Count; i++ )
        Controller.All[i].Update();

      // pawns are not added to Limit
      for( int i = 0; i < Controller.All.Count; i++ )
        if( Controller.All[i].pawn != null )
          Controller.All[i].pawn.EntityUpdate();
    }

    if( LoadingScene )
    {
      //prog = Mathf.MoveTowards( prog, progTarget, Time.unscaledDeltaTime * progressSpeed );
      //progress.fillAmount = prog;
    }

    float H = 0;
    float S = 1;
    float V = 1;
    Color.RGBToHSV( shiftyColor, out H, out S, out V );
    H += colorShiftSpeed * Time.unscaledDeltaTime;
    shiftyColor = Color.HSVToRGB( H, 1, 1 );
    shifty.color = shiftyColor;

    if( Minimap.activeInHierarchy )
    {
      MinimapScroller.transform.position += (Vector3) (-Controls.MenuActions.Move.ReadValue<Vector2>() * mmScrollSpeed * Time.unscaledDeltaTime);
      mmPlayer.anchoredPosition = pixelsPerUnit * MinimapCamera.worldToCameraMatrix.MultiplyPoint( PlayerController.pawn.transform.position );
    }
  }

  void OnApplicationFocus( bool hasFocus )
  {
    if( hasFocus )
      Cursor.lockState = ActiveDiegetic || Paused ? CursorLockMode.None : CursorLockMode.Locked;
    else
      Cursor.lockState = CursorLockMode.None;
    Cursor.visible = (Cursor.lockState == CursorLockMode.None);
  }

  void LateUpdate()
  {
    EXPUpdate();

    if( Updating && !Paused )
    {
      for( int i = 0; i < Entity.Limit.All.Count; i++ )
        if( Entity.Limit.All[i].enabled )
          Entity.Limit.All[i].EntityLateUpdate();

      for( int i = 0; i < Controller.All.Count; i++ )
        Controller.All[i].LateUpdate();

      // pawns are not added to Limit
      for( int i = 0; i < Controller.All.Count; i++ )
        if( Controller.All[i].pawn != null && Controller.All[i].pawn.enabled )
          Controller.All[i].pawn.EntityLateUpdate();

      CameraController.CameraLateUpdate();

      if( GLR_Enabled )
      {
        if( CameraController.cam.orthographic )
        {
          glrCam2.orthographicSize = CameraController.cam.orthographicSize;
          glrCamBackground.orthographicSize = CameraController.cam.orthographicSize;
        }
        else
        {
          glrCam2.fieldOfView = CameraController.cam.fieldOfView;
          glrCam2.transform.rotation = CameraController.transform.rotation;
          glrCamBackground.fieldOfView = CameraController.cam.fieldOfView;
          glrCamBackground.transform.rotation = CameraController.transform.rotation;
        }
      }
    }
  }

  public void SpawnPlayer()
  {
    GameObject go = Spawn( AvatarPrefab, FindBestSpawnPosition(), Quaternion.identity, null, false );
    Pawn pawn = go.GetComponent<Pawn>();
    CurrentPlayer = pawn;
    HUD.SetActive( true );
    PlayerController.AssignPawn( pawn );
  }

  [ExposeMethod]
  public void Respawn()
  {
    /*Chopper chopper = FindObjectOfType<Chopper>();
      if( chopper != null )
        chopper.StartDrop( CurrentPlayer );
      else*/
    if( PlayerController.pawn != null )
    {
      if( sceneScript != null )
        PlayerController.pawn.transform.position = sceneScript.FindSpawnPosition();
      else
        PlayerController.pawn.transform.position = FindRandomSpawnPosition();
      PlayerController.pawn.velocity = Vector2.zero;
    }
    else
    {
      SpawnPlayer();
    }
  }

  public Vector3 FindBestSpawnPosition()
  {
    GameObject go = null;
    go = GameObject.FindGameObjectWithTag( "FirstSpawn" );
    if( go == null )
      go = GameObject.FindGameObjectWithTag( "Respawn" );
    if( go != null )
      return go.transform.position;
    return Vector3.zero;
  }

  public Vector3 FindRandomSpawnPosition()
  {
    // cycle through random spawn points
    GameObject go = null;
    List<GameObject> gos = new List<GameObject>();
    gos.AddRange( GameObject.FindGameObjectsWithTag( "Respawn" ) );
    gos.AddRange( GameObject.FindGameObjectsWithTag( "FirstSpawn" ) );
    GameObject[] spawns = gos.ToArray();
    if( spawns.Length > 0 )
    {
      spawnCycleIndex %= spawns.Length;
      go = spawns[spawnCycleIndex++];
    }
    if( go != null )
    {
      Vector3 pos = go.transform.position;
      pos.z = 0;
      return pos;
    }
    return Vector3.zero;
  }

#region Time

  [Header( "Time" )]
  public static bool Paused;
  public static bool Slowed;
  public float CurrentTimescale = 1;

  public void Slow()
  {
    Slowed = true;
    Time.timeScale = slowtime;
    Time.fixedDeltaTime = 0.02f * Time.timeScale;
    mixer.TransitionToSnapshots( new UnityEngine.Audio.AudioMixerSnapshot[] {snapNormal, snapSlowmo}, new float[] {0, 1}, AudioFadeDuration );
  }

  public void NoSlow()
  {
    Slowed = false;
    Time.timeScale = CurrentTimescale;
    Time.fixedDeltaTime = 0.02f * Time.timeScale;
    mixer.TransitionToSnapshots( new UnityEngine.Audio.AudioMixerSnapshot[] {snapNormal, snapSlowmo}, new float[] {1, 0}, AudioFadeDuration );
  }

  public void Pause()
  {
    Paused = true;
    Time.timeScale = 0;
  }

  public void Unpause()
  {
    Paused = false;
    Time.timeScale = CurrentTimescale;
  }

#endregion

  public void ShowHUD() { HUD.SetActive( true ); }

  public void HideHUD() { HUD.SetActive( false ); }

#region BugReport

  public void ShowBugReport()
  {
    Pause();
    Controls.DevActions.Disable();
    Controls.MenuActions.Disable();
    Controls.BipedActions.Disable();
    UnityEngine.Cursor.lockState = CursorLockMode.None;
    UnityEngine.Cursor.visible = true;
    EnableRaycaster( true );
    StartCoroutine( BeginBugReport() );
  }

  IEnumerator BeginBugReport()
  {
    var attachments = new Dungbeetle.AttachmentCollection();
    attachments.AddString( "Example attachment (could be a saved game, settings etc.)." );
    attachments.AddString( "Yet another example attachment..." );

    ScreenshotSource screenshotSource = new ScreenshotSource();
    yield return screenshotSource.Wait( 5f ); // Wait for screenshot to be written to file, or time out.
    if( screenshotSource.Ready )
      yield return attachments.Wait(); // Wait for log to be read from file (don't time out, the length is capped anyway).

    if( !screenshotSource.Ready )
    {
      Debug.LogError( "The screenshot couldn't be read from file." );
      yield break;
    }
    else if( !attachments.Ready )
    {
      Debug.LogError( "The attachments couldn't be read from file." );
      yield break;
    }
    BugReport.Launch( SceneManager.GetActiveScene().name, Dungbeetle.BuildNameArk.BuildName, attachments, screenshotSource );
  }

  public void HideBugReport()
  {
    BugReport.Unselect();
    Unpause();
    // NOTE copied from HidePauseMenu
    EnableRaycaster( false );
    if( ActiveDiegetic != null )
    {
      ActiveDiegetic.InteractableOn();
      UnityEngine.Cursor.lockState = CursorLockMode.None;
      UnityEngine.Cursor.visible = true;
    }
    else
    {
      PlayerController.OnUnpause();
      Controls.MenuActions.Disable();
      UnityEngine.Cursor.lockState = CursorLockMode.Locked;
      UnityEngine.Cursor.visible = false;
    }
    if( CurrentPlayer != null )
      Controls.BipedActions.Enable();
    Controls.DevActions.Enable();
  }

#endregion

  void ShowPauseMenu()
  {
    if( ScreenSettingsCountdownTimer.IsActive )
      return;

    // Anything not in the pause menu, deactivate here 
    HideBugReport();
    HideMinimap();

    Pause();
    mixer.SetFloat( "MusicVolume", Util.DbFromNormalizedVolume( FloatSetting["MusicVolume"].Value * 0.8f ) );
    mixer.SetFloat( "SFXVolume", Util.DbFromNormalizedVolume( FloatSetting["SFXVolume"].Value * 0.8f ) );
    if( ActiveDiegetic != null )
    {
      ActiveDiegetic.InteractableOff();
    }
    else
    {
      PlayerController.OnPauseMenu();
      Controls.MenuActions.Enable();
    }
    PauseMenu.SetActive( true );
    HUD.SetActive( false );
    UnityEngine.Cursor.lockState = CursorLockMode.None;
    UnityEngine.Cursor.visible = true;
    EnableRaycaster( true );
  }

  void HidePauseMenu()
  {
    if( ScreenSettingsCountdownTimer.IsActive )
      return;
    Unpause();
    mixer.SetFloat( "MusicVolume", Util.DbFromNormalizedVolume( FloatSetting["MusicVolume"].Value ) );
    mixer.SetFloat( "SFXVolume", Util.DbFromNormalizedVolume( FloatSetting["SFXVolume"].Value ) );
    PauseMenu.SetActive( false );
    HUD.SetActive( true );
    EnableRaycaster( false );
    if( ActiveDiegetic != null )
    {
      ActiveDiegetic.InteractableOn();
      UnityEngine.Cursor.lockState = CursorLockMode.None;
      UnityEngine.Cursor.visible = true;
    }
    else
    {
      PlayerController.OnUnpause();
      Controls.MenuActions.Disable();
      UnityEngine.Cursor.lockState = CursorLockMode.Locked;
      UnityEngine.Cursor.visible = false;
    }
#if UNITY_WEBGL && !UNITY_EDITOR
      // cannot write settings on exit in webgl builds, so write them here
      WriteSettings();
#endif
  }

  void TogglePauseMenu()
  {
    if( MenuShowing )
      HidePauseMenu();
    else
      ShowPauseMenu();
  }

  public void DiegeticMenuOn( DiegeticUI dui )
  {
    ActiveDiegetic = dui;
    Controls.BipedActions.Disable();
    Controls.MenuActions.Enable();
    OverrideCameraZone( dui.CameraZone );
    Cursor.lockState = CursorLockMode.None;
    Cursor.visible = true;
  }

  public void DiegeticMenuOff()
  {
    ActiveDiegetic = null;
    Controls.MenuActions.Disable();
    Controls.BipedActions.Enable();
    OverrideCameraZone( null );
    Cursor.lockState = CursorLockMode.Locked;
    Cursor.visible = false;
  }

#region Minimap

  public void ToggleMinimap()
  {
    if( Minimap.activeInHierarchy )
      HideMinimap();
    else
      ShowMinimap();
  }

  public void ShowMinimap()
  {
    // TEMP
    MinimapRender( sceneScript.bounds );
    //

    Minimap.SetActive( true );
    Controls.BipedActions.Disable();
    // exclusion to turn minimap off
    Controls.BipedActions.Minimap.Enable();
    Controls.MenuActions.Enable();

    mmPlayer.anchoredPosition = MinimapCamera.worldToCameraMatrix.MultiplyPoint( PlayerController.pawn.transform.position );
    MinimapScroller.transform.localPosition = -mmPlayer.anchoredPosition * MinimapScroller.transform.localScale.x;
  }

  public void HideMinimap()
  {
    Minimap.SetActive( false );
    Controls.BipedActions.Enable();
    Controls.MenuActions.Disable();
  }

  public void MinimapRender( Bounds bounds )
  {
    if( bounds.size.magnitude < 1 )
    {
      Debug.LogWarning( "Scene bounds has not been set." );
      return;
    }
    /*MinimapCamera.targetTexture.DiscardContents( true, true );
    rt.DiscardContents();
     PixelPerfectCamera ppc = MinimapCamera.GetComponent<PixelPerfectCamera>();*/
    Vector2 size = bounds.size * pixelsPerUnit; //* ppc.assetsPPU;
    rt = new RenderTexture( Mathf.FloorToInt( size.x ), Mathf.FloorToInt( size.y ), 32, UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_UNorm );
    rt.filterMode = FilterMode.Point;
    rt.wrapMode = TextureWrapMode.Repeat;

    rt2 = new RenderTexture( Mathf.FloorToInt( size.x ), Mathf.FloorToInt( size.y ), 32, UnityEngine.Experimental.Rendering.GraphicsFormat.R8G8B8A8_UNorm );
    rt2.filterMode = FilterMode.Point;
    rt2.wrapMode = TextureWrapMode.Repeat;

    Shader cached = bigsheetMaterial.shader;
    Shader cached2 = backgroundMaterial.shader;
    bigsheetMaterial.shader = mmShader;
    backgroundMaterial.shader = mmShader;

    int cachedCullingMask = MinimapCamera.cullingMask;

    bigsheetMaterial.color = mmColor[0];
    backgroundMaterial.color = mmColor[1];
    //MinimapCamera.cullingMask = LayerMask.GetMask( new string[] { "Default", "entity", "triggerAndCollision", "" } );
    MinimapCamera.transform.position = bounds.center;
    MinimapCamera.targetTexture = rt; //mmrt0;
    MinimapCamera.clearFlags = CameraClearFlags.SolidColor;
    MinimapCamera.backgroundColor = Color.clear;
    MinimapCamera.orthographicSize = size.y * 0.5f;
    MinimapCamera.Render();

    MinimapScroller.GetComponent<RectTransform>().sizeDelta = size;
    MinimapScroller.GetComponent<RectTransform>().localScale = new Vector3( MinimapScrollerScale, MinimapScrollerScale, 1 );
    MinimapScroller.GetComponent<RawImage>().texture = rt;

    bigsheetMaterial.color = mmColor[2];
    MinimapCamera.cullingMask = LayerMask.GetMask( new string[] {"entity"} );
    MinimapCamera.targetTexture = rt2;
    MinimapCamera.clearFlags = CameraClearFlags.Color;
    MinimapCamera.backgroundColor = Color.clear;
    MinimapCamera.Render();

    mmCharacterLayer.GetComponent<RectTransform>().sizeDelta = size;
    mmCharacterLayer.GetComponent<RawImage>().texture = rt2;

    MinimapCamera.cullingMask = cachedCullingMask;
    bigsheetMaterial.shader = cached;
    backgroundMaterial.shader = cached2;
  }

#endregion

#region GameplayLayerRender_GLR

  [Header( "Gameplay Layer Render (GLR)" )]
  public bool GLR_Enabled;
  int GLR_CurrentLayer;
  // The primary gameplay layer is always on top, so that it has infinite overhead clearance.
  // The other layers are offset downward.
  // DO NOT CHANGE THIS CONSTANT. Existing level content relies on this value;
  public const float GameplayLayerOffset = 2000;
  public Camera glrCam2;
  public Camera glrCamBackground;

  [ExposeMethod]
  void EnableGameplayLayerRender()
  {
    GLR_Enabled = true;
    CameraController.cam.depth = 2;
    CameraController.cam.clearFlags = CameraClearFlags.Nothing;
    // background layer is bit 8
    CameraController.cam.cullingMask &= ~(1 << 8);
    glrCam2.enabled = true;
    glrCam2.depth = 1;
    glrCam2.clearFlags = CameraClearFlags.Nothing;

    glrCamBackground.enabled = true;
    glrCamBackground.depth = 0;

    GLR_MoveToLayer( 0 );
  }

  [ExposeMethod]
  void DisableGameplayLayerRender()
  {
    GLR_Enabled = false;
    CameraController.cam.clearFlags = CameraClearFlags.SolidColor;
    // background layer is bit 8
    CameraController.cam.cullingMask |= (1 << 8);
    glrCam2.enabled = false;
    glrCamBackground.enabled = false;
  }

  [ExposeMethod]
  public void GLR_MoveToLayer( int layer )
  {
    GLR_CurrentLayer = layer;
    if( layer == 0 )
    {
      Vector2 offset = new Vector2( 0, -GameplayLayerOffset );
      glrCam2.transform.localPosition = offset;
      glrCamBackground.transform.localPosition = Vector3.zero;
    }
    else if( layer == 1 )
    {
      Vector2 offset = new Vector2( 0, GameplayLayerOffset );
      glrCam2.transform.localPosition = offset;
      glrCamBackground.transform.localPosition = offset;
    }
  }

  [Header( "WarpDoor" )]
  [SerializeField] RawImage transitionImage;
  [FormerlySerializedAs( "transitionDuration" )]
  public float WarpDoorTransitionDuration = 1;
  RenderTexture renderTexture;

  // WarpDoor
  public void DoorTransitionRender( Entity[] entities, Vector2 newPos )
  {
    transitionImage.texture = renderTexture;
    Camera.main.targetTexture = renderTexture;
    if( GLR_Enabled )
    {
      glrCamBackground.targetTexture = renderTexture;
      glrCamBackground.Render();
      glrCamBackground.targetTexture = null;

      glrCam2.targetTexture = renderTexture;
      glrCam2.Render();
      glrCam2.targetTexture = null;
    }
    Camera.main.Render();
    Camera.main.targetTexture = null;

    transitionImage.enabled = true;
    Color blend = Color.white;

    //CurrentPlayer.input.Jump = false;

    bool firstUpdate = true;
    new Timer( this, WarpDoorTransitionDuration, delegate( Timer timer )
    {
      if( firstUpdate )
      {
        firstUpdate = false;
        foreach( var ent in entities )
        {
          ent.Teleport( newPos );
          if( ent == CurrentPlayer )
            CameraController.Teleport();
        }
      }
      blend.a = 1f - timer.ProgressNormalized;
      transitionImage.color = blend;

      //CurrentPlayer.input.Jump = false;
    }, delegate { transitionImage.enabled = false; } );
  }

#endregion


  public void EnableRaycaster( bool enable = true ) { UI.GetComponent<UnityEngine.UI.GraphicRaycaster>().enabled = enable; }

  void ShowLoadingScreen( string message = "Loading.." )
  {
    LoadingScreen.SetActive( true );
    Text txt = LoadingScreen.GetComponentInChildren<Text>();
    txt.text = message;
  }

  void HideLoadingScreen() { LoadingScreen.SetActive( false ); }

  IEnumerator ShowLoadingScreenRoutine( string message = "Loading.." )
  {
    // This is a coroutine simply to wait a single frame after activating the loading screen.
    // Otherwise the screen will not show!
    ShowLoadingScreen( message );
    yield return null;
  }

  public GameObject Spawn( string resourceName, Vector3 position, Quaternion rotation, Transform parent = null, bool limit = true, bool initialize = true )
  {
    // allow the lookup to check the name replacement table
    GameObject prefab = null;
    if( ResourceLookup.ContainsKey( resourceName ) )
    {
      prefab = ResourceLookup[resourceName];
    }
    else if( replacements.ContainsKey( resourceName ) )
    {
      if( ResourceLookup.ContainsKey( replacements[resourceName] ) )
        prefab = ResourceLookup[replacements[resourceName]];
    }
    if( prefab != null )
      return Spawn( prefab, position, rotation, parent, limit, initialize );
    return null;
  }

  public GameObject Spawn( GameObject prefab, Vector3 position, Quaternion rotation, Transform parent = null, bool limit = true, bool initialize = true )
  {
    if( limit )
    {
      ILimit[] limits = prefab.GetComponentsInChildren<ILimit>();
      foreach( var cmp in limits )
      {
        if( !cmp.IsUnderLimit() )
          return null;
      }
    }

    GameObject go = Instantiate( prefab, position, rotation, parent );
    go.name = prefab.name;

    if( initialize )
    {
      deeprest.SerializedComponent[] scs = go.GetComponentsInChildren<deeprest.SerializedComponent>();
      foreach( var sc in scs )
        sc.AfterDeserialize();
    }
    return go;
  }

  public void AudioOneShot( AudioClip clip, Vector3 position )
  {
    // independent, temporary positional sound object
    GameObject go = Instantiate( audioOneShotPrefab, position, Quaternion.identity );
    AudioSource source = go.GetComponent<AudioSource>();
    source.PlayOneShot( clip );
    new Timer( this, clip.length, null, delegate { Destroy( go ); } );
  }

  public void FadeBlack()
  {
    fader.color = new Color( fader.color.r, fader.color.g, fader.color.b, 0 );
    fadeTimer.Stop( true );
    // set gameObject active after Stop() because FadeClear() CompleteDelegate
    // makes the gameObject inactive
    fader.gameObject.SetActive( true );
    TimerParams tp = new TimerParams
    {
      unscaledTime = true,
      repeat = false,
      duration = 1,
      UpdateDelegate = delegate( Timer t )
      {
        Color fc = fader.color;
        fc.a = t.ProgressNormalized;
        fader.color = fc;
      },
      CompleteDelegate = delegate { fader.color = Color.black; }
    };
    fadeTimer.Start( this, tp );
  }

  public void FadeClear()
  {
    //fader.color = new Color( fader.color.r, fader.color.g, fader.color.b, 1 );
    //fader.gameObject.SetActive( true );
    fadeTimer.Stop( true );
    TimerParams tp = new TimerParams
    {
      unscaledTime = true,
      repeat = false,
      duration = 1,
      UpdateDelegate = delegate( Timer t )
      {
        Color fc = fader.color;
        fc.a = 1 - t.ProgressNormalized;
        fader.color = fc;
      },
      CompleteDelegate = delegate
      {
        fader.color = Color.clear;
        fader.gameObject.SetActive( false );
      }
    };
    fadeTimer.Start( this, tp );
  }

#region Speech

  [Header( "Speech" )]
  public bool UseSpeechBubble = true;
  public bool UseSpeechFace = true;
  public bool UseSpeechCamera = true;
  public GameObject SpeechBubble;
  public GameObject SpeechIcon;
  public Image SpeechFaceRight;
  public Image SpeechFaceLeft;
  public Text SpeechCharacterName;
  public Text SpeechText;
  public GameObject SpeechTextWorld;
  [SerializeField] Camera SpeechIconCamera;
  CharacterIdentity SpeechCharacter;
  PriorityEnum SpeechPriority = 0;
  public float SpeechRange = 8;
  public Timer SpeechTimer = new Timer();

  [Header( "Conversation" )]
  public Timer SpeechContinueTimer = new Timer();
  public float minInterval = 1;
  ITalker speakTalkerCurrent;
  public Animator CameraSpaceAnimator;
  public float speechCamLerpDuration = 3;

  public class PreConversationState
  {
    public bool cursorInfluence;
    public float orthoTarget;
    public bool vertical;
  }

  PreConversationState preconvostate;
  System.Action OnConversationComplete;

  void EndConversation()
  {
    if( preconvostate == null )
      return;
    // CameraSpaceAnimator.Play( "speechBarsDisable" );
    CameraController.CursorInfluence = preconvostate.cursorInfluence;
    CameraController.orthoTarget = preconvostate.orthoTarget;
    CameraController.UseVerticalRange = preconvostate.vertical;
    preconvostate = null;

    if( CurrentPlayer != null )
      ((PlayerBiped) CurrentPlayer).PostConversation();
  }

  void StartConversation()
  {
    preconvostate = new PreConversationState();
    preconvostate.cursorInfluence = CameraController.CursorInfluence;
    preconvostate.orthoTarget = CameraController.orthoTarget;
    preconvostate.vertical = CameraController.UseVerticalRange;
    if( CurrentPlayer != null )
    {
      ((PlayerBiped) CurrentPlayer).PreConversation();
      CameraController.Lerp( CurrentPlayer.transform.position, 2, speechCamLerpDuration, true, () => { CameraController.orthoTarget = 2; } );
    }
    // CameraSpaceAnimator.Play( "speechBarsEnable" );
    CameraController.CursorInfluence = false;
    SpeechHelperNext.gameObject.SetActive( false );
    SpeechHelperEnd.gameObject.SetActive( false );
  }

  public bool StartConversation( ITalker[] talkers, string convoID, bool showBars, System.Action OnComplete = null )
  {
    if( !SpeechContinueTimer.IsActive )
    {
      OnConversationComplete = OnComplete;
      var input = new StringReader( talkers[0].GetIdentity().TextAsset.text );
      var deserializer = new DeserializerBuilder().WithNamingConvention( new CamelCaseNamingConvention() ).Build();
      var doc = deserializer.Deserialize<ConvoDocument>( input );
      for( int i = 0; i < doc.conversations.Length; i++ )
        if( doc.conversations[i].id == convoID )
        {
          StartConversation();
          return SayLine( talkers, doc.conversations[i], 0, PriorityEnum.Conversation );
        }
    }
    Debug.LogError( "Conversation id not found: " + convoID );
    OnConversationComplete = null;
    return false;
  }

  bool SayLine( ITalker[] talkers, Conversation conversation, int i, PriorityEnum priority ) { return SpeakWait( conversation.dialogue[i].say, talkers, conversation, i, priority ); }

  [SerializeField] Animation SpeechNextArrow;
  [SerializeField] GameObject SpeechHelperNext;
  [SerializeField] GameObject SpeechHelperEnd;

  bool SpeakWait( string say, ITalker[] talkers, Conversation conversation, int i, PriorityEnum priority )
  {
    ITalker talker = talkers[conversation.dialogue[i].id];
    float interval = Mathf.Max( minInterval, (float) say.Length / conversation.lettersPerSecond );
    if( !Mathf.Approximately( conversation.dialogue[i].wait, 0 ) )
      interval = conversation.dialogue[i].wait;
    if( Speak( talker, say, interval, priority ) )
    {
      if( UseSpeechBubble )
      {
        // characterCountVisible is not correct unless we call cachedTextGenerator.Populate first.
        Vector2 extents = SpeechText.GetComponent<RectTransform>().sizeDelta;
        TextGenerationSettings settings = SpeechText.GetGenerationSettings( extents );
        if( SpeechText.cachedTextGenerator.PopulateWithErrors( say, settings, SpeechText.gameObject ) )
        {
          // if text overflows, wait for input and display next 'page'.
          int count = SpeechText.cachedTextGenerator.characterCountVisible;
          if( count > 0 && count < say.Length )
          {
            say = SpeechText.text.Substring( count, say.Length - count );
            SpeechNextArrow.gameObject.SetActive( true );
            SpeechNextArrow.Play();
          }
          else
          {
            say = null;
          }
        }
        else
        {
          Debug.LogError( "speech text generator error", SpeechText.gameObject );
          return false;
        }
      }
      else
      {
        say = null;
      }
      if( i == conversation.dialogue.Length - 1 )
      {
        SpeechHelperEnd.gameObject.SetActive( true );
        SpeechHelperNext.gameObject.SetActive( false );
      }
      else
      {
        SpeechHelperNext.gameObject.SetActive( true );
      }
      // wait for SpeakContinue to be called
      SpeechContinueTimer.Start( this, float.MaxValue, null, () =>
      {
        // TODO check if talker still exists
        talker.OnSpeakEnd();
        SpeechTimer.Stop( false );
        // if the line is too long for the text box, show another page.
        if( !string.IsNullOrEmpty( say ) )
        {
          SpeakWait( say, talkers, conversation, i, priority );
          return;
        }
        // continue to next dialogue line in the conversation.
        if( ++i < conversation.dialogue.Length )
          SayLine( talkers, conversation, i, priority );
      } );
      return true;
    }
    return false;
  }

  void SpeakContinue()
  {
    SpeechNextArrow.Stop();
    SpeechNextArrow.gameObject.SetActive( false );
    SpeechTimer.Stop( false );
    SpeechContinueTimer.Stop( true );
  }

  void SpeakCancel()
  {
    EndConversation();
    // TODO check if talker still exists
    speakTalkerCurrent.OnSpeakEnd();
    speakTalkerCurrent = null;
    SpeechTimer.Stop( false );
    SpeechContinueTimer.Stop( false );
    if( UseSpeechBubble )
    {
      SpeechBubble.SetActive( false );
      SpeechCharacter = null;
      SpeechIconCamera.enabled = false;
    }
    else
    {
      SpeechTextWorld.SetActive( false );
    }
    Controls.SpeechActions.Disable();
    Controls.BipedActions.Enable();
    CurrentPlayer.InteractIndicator.SetActive( true );

    OnConversationComplete?.Invoke();
    OnConversationComplete = null;
  }

  public bool Speak( ITalker talker, string text, float timeout, PriorityEnum priority )
  {
    // Equal priority is allowed to override if it's a one-off remark.
    // Conversations should not stop for one another, though. 
    if( !SpeechTimer.IsActive || ((priority < SpeechPriority) || (SpeechPriority == PriorityEnum.Remark)) )
    {
      if( BoolSetting["SpeechDisableControls"].Value )
        Controls.BipedActions.Disable();
      else
        Controls.BipedActions.Interact.Disable();
      Controls.SpeechActions.Enable();
      CurrentPlayer.InteractIndicator.SetActive( false );

      /*if( speakTalkerCurrent!= null )
        speakTalkerCurrent.SpeakEnd();*/
      speakTalkerCurrent = talker;
      speakTalkerCurrent.OnSay( text );

      SpeechTimer.Stop( true );
      SpeechCharacter = talker.GetIdentity();
      SpeechPriority = priority;

      SpeechBubble.SetActive( UseSpeechBubble );
      SpeechTextWorld.SetActive( !UseSpeechBubble );
      if( UseSpeechBubble )
      {
        SpeechCharacterName.text = SpeechCharacter.CharacterName;
        SpeechCharacterName.color = SpeechCharacter.SpeechTextColor;
        SpeechFaceRight.gameObject.SetActive( UseSpeechFace );
        SpeechFaceLeft.gameObject.SetActive( UseSpeechFace );
        if( talker != CurrentPlayer.GetComponent<ITalker>() )
          SpeechFaceRight.sprite = SpeechCharacter.Face;
        /*SpeechText.color = Color.white;*/
        SpeechText.horizontalOverflow = HorizontalWrapMode.Wrap;
        SpeechText.text = text;
        SpeechIconCamera.enabled = UseSpeechCamera;
        SpeechIcon.SetActive( UseSpeechCamera );
        SpeechTimer.Start( this, float.MaxValue, timer =>
        {
          TalkerCameraInfo info = speakTalkerCurrent.GetCameraInfo();
          SpeechIconCamera.transform.position = info.focus;
          SpeechIconCamera.orthographicSize = info.orthoSize;
        }, null );
        if( priority == PriorityEnum.Remark )
        {
          SpeechHelperEnd.gameObject.SetActive( true );
          SpeechHelperNext.gameObject.SetActive( false );
        }
      }
      else
      {
        Text speechText = SpeechTextWorld.GetComponent<Text>();
        speechText.color = talker.GetIdentity().SpeechTextColor;
        speechText.text = text;
        Vector2 pos = talker.GetCameraInfo().focus + Vector2.up * 0.75f;

        Vector2 sizeDelta = SpeechTextWorld.GetComponent<RectTransform>().sizeDelta;
        Bounds bounds = new Bounds();
        Vector3 boundsCenter = CameraController.transform.position;
        boundsCenter.z = 0;
        bounds.center = boundsCenter;
        float widthRatio = ((float) Screen.width / (float) Screen.height);
        bounds.size = new Vector3( (2 * Camera.main.orthographicSize * widthRatio - sizeDelta.x), 2 * CameraController.orthoTarget - sizeDelta.y, 0 );
        SpeechTextWorld.transform.position = bounds.ClosestPoint( pos );
      }

      return true;
      // If you want the speech to time out, use this.
      /* SpeechTimer.Start( this, timeout, delegate( Timer timer ) { SpeechIconCamera.transform.position = talker.GetFocusPosition(); }, delegate(){ SpeakEnd();} );*/
    }
    return false;
  }

#endregion

  public void OverrideCameraZone( CameraZone zone ) { CameraController.AssignOverrideCameraZone( zone ); }

  void VerifyPersistentData()
  {
    // Ensure that all persistent data files exist. If not, unpack them from the archive within the build.
    bool unpack = false;
    foreach( var filename in persistentFilenames )
      if( !File.Exists( Application.persistentDataPath + "/" + filename ) )
        unpack = true;
    if( unpack )
    {
      string zipPath = Application.temporaryCachePath + "/persistent.zip";
      TextAsset zipfile = Resources.Load( "persistent" ) as TextAsset;
      if( zipfile != null )
      {
        File.WriteAllBytes( zipPath, zipfile.bytes );
        Debug.Log( "Unzipping persistent: " + zipPath );
        using( ZipFile zip = ZipFile.Read( zipPath ) )
        {
          zip.ExtractAll( Application.persistentDataPath, ExtractExistingFileAction.OverwriteSilently );
        }
      }
      else
      {
        Debug.LogWarning( "no level directory or zip file in build: " + name );
      }
    }
  }

#region Settings

  string[] resolutions = {"512x512", "640x360", "640x400", "1024x1024", "1280x720", "1280x800", "1920x1080", "1920x1200", "2048x2048"};
  int ResolutionWidth;
  int ResolutionHeight;

  void InitializeSettings()
  {
    SettingUI[] settings = PauseMenu.GetComponentsInChildren<SettingUI>( true );
    // use existing UI objects, if they exist
    foreach( var s in settings )
    {
      if( s.isString )
      {
        s.stringValue.Init();
        StringSetting.Add( s.stringValue.name, s.stringValue );
      }
      if( s.isInteger )
      {
        s.intValue.Init();
        FloatSetting.Add( s.intValue.name, s.intValue );
      }
      if( s.isBool )
      {
        s.boolValue.Init();
        BoolSetting.Add( s.boolValue.name, s.boolValue );
      }
    }

    // screen settings are applied explicitly when user pushes button
    CreateBoolSetting( "Fullscreen", false, null );
    CreateStringSetting( "Resolution", "1280x800", null );
    CreateFloatSetting( "ResolutionSlider", 4, 0, resolutions.Length - 1, resolutions.Length - 1, delegate( float value )
    {
      string Resolution = resolutions[Mathf.FloorToInt( Mathf.Clamp( value, 0, resolutions.Length - 1 ) )];
      string[] tokens = Resolution.Split( new char[] {'x'} );
      ResolutionWidth = int.Parse( tokens[0].Trim() );
      ResolutionHeight = int.Parse( tokens[1].Trim() );
      StringSetting["Resolution"].Value = ResolutionWidth.ToString() + "x" + ResolutionHeight.ToString();
    } );
    CreateFloatSetting( "UIScale", 1, 0.1f, 4, 20, null );

    CreateFloatSetting( "MasterVolume", 0.8f, 0, 1, 20, delegate( float value ) { mixer.SetFloat( "MasterVolume", Util.DbFromNormalizedVolume( value ) ); } );
    CreateFloatSetting( "MusicVolume", 0.9f, 0, 1, 20, delegate( float value ) { mixer.SetFloat( "MusicVolume", Util.DbFromNormalizedVolume( value ) ); } );
    CreateFloatSetting( "SFXVolume", 1, 0, 1, 20, delegate( float value )
    {
      mixer.SetFloat( "SFXVolume", Util.DbFromNormalizedVolume( value ) );
      if( Updating )
        AudioOneShot( VolumeChangeNoise, Camera.main.transform.position );
    } );
    /*CreateFloatSetting( "MusicTrack", 0, 0, MusicLoops.Length - 1, 1.0f / (MusicLoops.Length - 1), delegate ( float value ){ PlayMusicLoop( MusicLoops[Mathf.FloorToInt( Mathf.Clamp( value, 0, MusicLoops.Length - 1 ) )] ); } );*/

    CreateBoolSetting( "VSync", true, delegate( bool value ) { QualitySettings.vSyncCount = value ? 1 : 0; } );
#if LIGHTING_TOGGLE
    CreateBoolSetting( "Lighting", true, delegate( bool value )
    {
      if( value )
        EnableLighting();
      else
        DisableLighting();
    } );
#endif
    CreateBoolSetting( "ShowOnboardingControls", true, OnboardingControls.SetActive );
    CreateFloatSetting( "CursorSensitivity", 0.5f, 0.1f, 2, 19, delegate( float value ) { CursorSensitivity = value; } );
    CreateFloatSetting( "Zoom", 3, 1, 5, 20, delegate( float value ) { CameraController.orthoTarget = value; } );

    CreateDivider( "Developer Settings" );

    CreateBoolSetting( "UseCameraVertical", true, delegate( bool value ) { CameraController.UseVerticalRange = value; } );
    CreateBoolSetting( "CursorInfluence", false, delegate( bool value )
    {
      if( CameraController != null )
        CameraController.CursorInfluence = value;
    } );
    /*CreateBoolSetting( "AimSnap", false, delegate( bool value ) { AimSnap = value; } );
    CreateBoolSetting( "AutoAim", false, delegate( bool value ) { AutoAim = value; } );
    CreateBoolSetting( "ShowAimPath", false, delegate( bool value ) { ShowAimPath = value; } );*/
    CreateBoolSetting( "AutoWallslide", true, null );

    CreateBoolSetting( "UseSpeechBubble", true, delegate( bool value ) { UseSpeechBubble = value; } );
    CreateBoolSetting( "UseSpeechFace", true, delegate( bool value ) { UseSpeechFace = value; } );
    CreateBoolSetting( "UseSpeechCamera", false, delegate( bool value ) { UseSpeechCamera = value; } );
    CreateBoolSetting( "SpeechDisableControls", true, null );
    /*CreateBoolSetting( "ShowInputDisplay", false, delegate( bool value )
    {
      ShowInputDisplay = value;
      InputDisplayOnCamera.gameObject.SetActive( ShowInputDisplay );
      if( ShowInputDisplay )
        InputDisplayOnCamera.Enable();
      else
        InputDisplayOnCamera.Disable();
    } );*/
    CreateBoolSetting( "ShowDevInfo", false, delegate( bool value )
    {
      debugFPS.gameObject.SetActive( value );
      /*debugText1.gameObject.SetActive( value );
      debugText2.gameObject.SetActive( value );
      debugText3.gameObject.SetActive( value );
      debugText4.gameObject.SetActive( value );
      debugText5.gameObject.SetActive( value );*/
      //debugTextReportBug.gameObject.SetActive( value );
    } );
    CreateBoolSetting( "HealthAudioPitch", true, delegate( bool value )
    {
      HealthAudioPitch = value;
      if( value )
      {
        if( CurrentPlayer != null )
          ((PlayerBiped) CurrentPlayer).UpdateHealthAudio();
      }
      else
      {
        MusicPitch( 1 );
      }
    } );
    CreateBoolSetting( "HealthTimescale", true, delegate( bool value )
    {
      HealthTimescale = value;
      if( value )
      {
        if( CurrentPlayer != null )
          ((PlayerBiped) CurrentPlayer).UpdateHealthTimescale();
      }
      if( !value )
      {
        CurrentTimescale = 1;
        Time.timeScale = 1;
      }
    } );

    CreateBoolSetting( "RotationExperiment", false, delegate( bool value ) { CameraController.RotationExperiment = value; } );
    CreateBoolSetting( "GamplayLayerRender", false, delegate( bool value )
    {
      GLR_Enabled = value;
      if( GLR_Enabled )
        EnableGameplayLayerRender();
      else
        DisableGameplayLayerRender();
    } );
    CreateBoolSetting( "CrushMeansInstantDeath", false, null );

    CreateFloatSetting( "CursorOuter", 1, 0, 1, 20, delegate( float value ) { CursorOuter = value; } );

    CreateFloatSetting( "CameraLerpAlpha", 10, 0, 10, 100, delegate( float value ) { CameraController.lerpAlpha = value; } );
    //CreateFloatSetting( "ThumbstickDeadzone", .3f, 0, .5f, 10, delegate ( float value ) { deadZone = value; } );
    CreateFloatSetting( "PlayerSpeedFactor", 0.3f, 0, 1, 10, delegate( float value )
    {
      if( PlayerController != null )
        PlayerController.HACKSetSpeed( value );
    } );
    CreateBoolSetting( "RandomSeed", false, null );
    CreateFloatSetting( "Seed", 0, 0, 20, 20, null );
    CreateFloatSetting( "UpdateCullDistance", 10, 10, 100, 9, delegate( float value ) { UpdateCullDistance = value; } );
    CreateFloatSetting( "MaxUpdateCount", 100, 0, 1000, 100, delegate( float value ) { MaxUpdateCount = (int) value; } );
    CreateFloatSetting( "EntityLimit", 100, 100, 10000, 100, delegate( float value ) { Entity.Limit.UpperLimit = (int) value; } );
    CreateFloatSetting( "PickupLimit", 100, 50, 1000, 950, delegate( float value ) { Pickup.Limit.UpperLimit = (int) value; } );
    foreach( var scene in sceneRefs )
    {
      if( scene.GetSceneName() == "GLOBAL" )
        continue;
      GameObject go = Instantiate( SceneListElementTemplate, SceneListElementTemplate.transform.parent );
      go.GetComponentInChildren<Text>().text = scene.GetSceneName();
      go.GetComponentInChildren<Button>().onClick.AddListener( () =>
      {
        HidePauseMenu();
        LoadScene( scene.GetSceneName() );
      } );
      if( SceneList.InitiallySelected == null )
        SceneList.InitiallySelected = go;
    }
    Destroy( SceneListElementTemplate );

    foreach( var audioLoop in Music )
    {
      GameObject go = Instantiate( MusicListElementTemplate, MusicListElementTemplate.transform.parent );
      go.GetComponentInChildren<Text>().text = audioLoop.name;
      go.GetComponentInChildren<Button>().onClick.AddListener( () =>
      {
        PlayMusic( audioLoop );
        /*if( audioLoop.intro!=null )
          PlayMusic( audioLoop );
        else
          CrossFadeTo( audioLoop );*/
      } );
      if( MusicList.InitiallySelected == null )
        MusicList.InitiallySelected = go;
    }
    Destroy( MusicListElementTemplate );
  }

  // explicit UI navigation
  public void NextNav( Selectable selectable )
  {
    Navigation previousNav = previousNavSelectable.navigation;
    previousNav.selectOnDown = selectable;
    previousNavSelectable.navigation = previousNav;
    Navigation thisNav = selectable.navigation;
    thisNav.selectOnUp = previousNavSelectable;
    selectable.navigation = thisNav;
    previousNavSelectable = selectable;
  }

  void CreateBoolSetting( string key, bool value, System.Action<bool> onChange )
  {
    BoolValue bv;
    if( !BoolSetting.TryGetValue( key, out bv ) )
    {
      GameObject go = Instantiate( ToggleTemplate, SettingsParent.transform );
      SettingUI sss = go.GetComponent<SettingUI>();
      sss.isBool = true;
      bv = sss.boolValue;
      bv.name = key;
      bv.Init();
      BoolSetting.Add( key, bv );
    }
    bv.onValueChanged = onChange;
    bv.Value = value;
    NextNav( bv.toggle );
  }

  void CreateFloatSetting( string key, float value, float min, float max, int steps, System.Action<float> onChange )
  {
    FloatValue bv;
    if( !FloatSetting.TryGetValue( key, out bv ) )
    {
      GameObject go = Instantiate( SliderTemplate, SettingsParent.transform );
      SettingUI sss = go.GetComponent<SettingUI>();
      sss.isInteger = true;
      bv = sss.intValue;
      bv.name = key;
      bv.minValue = min;
      bv.maxValue = max;
      bv.steps = steps;
      bv.Init();
      FloatSetting.Add( key, bv );
    }
    bv.onValueChanged = onChange;
    bv.Value = value;
    NextNav( bv.slider );
  }

  void CreateStringSetting( string key, string value, System.Action<string> onChange )
  {
    StringValue val;
    if( !StringSetting.TryGetValue( key, out val ) )
    {
      GameObject go = Instantiate( StringTemplate, SettingsParent.transform );
      SettingUI sss = go.GetComponent<SettingUI>();
      sss.isString = true;
      val = sss.stringValue;
      val.name = key;
      val.Init();
      StringSetting.Add( key, val );
    }
    val.onValueChanged = onChange;
    val.Value = value;
    if( val.inputField != null )
      NextNav( val.inputField );
  }

  void CreateDivider( string title )
  {
    GameObject go = Instantiate( DividerTemplate, SettingsParent.transform );
    Text text = go.GetComponentInChildren<Text>();
    text.text = title;
  }

  void ReadSettings()
  {
    JsonData json = new JsonData();
    if( File.Exists( settingsPath ) )
    {
      string gameJson = File.ReadAllText( settingsPath );
      if( gameJson.Length > 0 )
      {
        JsonReader reader = new JsonReader( gameJson );
        json = JsonMapper.ToObject( reader );
      }
      foreach( var pair in BoolSetting )
        pair.Value.Value = JsonUtil.Read( pair.Value.Value, json, "settings", pair.Key );
      foreach( var pair in FloatSetting )
        pair.Value.Value = JsonUtil.Read( pair.Value.Value, json, "settings", pair.Key );
      foreach( var pair in StringSetting )
        pair.Value.Value = JsonUtil.Read( pair.Value.Value, json, "settings", pair.Key );
    }
  }

  void WriteSettings()
  {
    JsonWriter writer = new JsonWriter();
    writer.PrettyPrint = true;
    writer.WriteObjectStart(); // root

    writer.WritePropertyName( "settings" );
    writer.WriteObjectStart();
    foreach( var pair in StringSetting )
    {
      writer.WritePropertyName( pair.Key );
      writer.Write( pair.Value.Value );
    }
    foreach( var pair in FloatSetting )
    {
      writer.WritePropertyName( pair.Key );
      writer.Write( pair.Value.Value, "0.##" );
    }
    foreach( var pair in BoolSetting )
    {
      writer.WritePropertyName( pair.Key );
      writer.Write( pair.Value.Value );
    }
    writer.WriteObjectEnd();

    writer.WriteObjectEnd(); // root end
    //print( settingsPath );
    File.WriteAllText( settingsPath, writer.ToString() );

#if UNITY_WEBGL && !UNITY_EDITOR
    FileSync();
#endif
  }

#if UNITY_WEBGL && !UNITY_EDITOR
  [DllImport( "__Internal" )]
  private static extern void FileSync();
#endif

  public void ApplyScreenSettings()
  {
    string[] tokens = StringSetting["Resolution"].Value.Split( new char[] {'x'} );
    ResolutionWidth = int.Parse( tokens[0].Trim() );
    ResolutionHeight = int.Parse( tokens[1].Trim() );
#if UNITY_WEBGL
    // let the web page determine the windowed size
    if( BoolSetting["Fullscreen"].Value )
      Screen.SetResolution( ResolutionWidth, ResolutionHeight, true );
    else
      Screen.fullScreen = false;
#else
    Screen.SetResolution( ResolutionWidth, ResolutionHeight, BoolSetting["Fullscreen"].Value );
#endif
    CanvasScaler.scaleFactor = FloatSetting["UIScale"].Value;
    ScreenSettingsCountdownTimer.Stop( false );
    ConfirmDialog.Unselect();
    // WarpDoor rendertexture for screen fade
    if( renderTexture != null )
      Destroy( renderTexture );
    renderTexture = new RenderTexture( ResolutionWidth, ResolutionHeight, 24, RenderTextureFormat.Default, 0 ); //GraphicsFormat.R8G8B8A8_UNorm, 0 );
  }

  struct ScreenSettings
  {
    public bool Fullscreen;
    public string Resolution;
    public float UIScale;
  }

  ScreenSettings CachedScreenSettings;

  public void ScreenChangePrompt()
  {
    CachedScreenSettings.Fullscreen = Screen.fullScreen;
    CachedScreenSettings.Resolution = Screen.width.ToString() + "x" + Screen.height.ToString();
    CachedScreenSettings.UIScale = CanvasScaler.scaleFactor;

    ConfirmDialog.Select();
    Screen.SetResolution( ResolutionWidth, ResolutionHeight, BoolSetting["Fullscreen"].Value );
    CanvasScaler.scaleFactor = FloatSetting["UIScale"].Value;

    TimerParams tp = new TimerParams
    {
      unscaledTime = true,
      repeat = false,
      duration = 10,
      UpdateDelegate = delegate( Timer obj ) { ScreenSettingsCountdown.text = "Accept changes or revert in <color=orange>" + (10 - obj.ProgressSeconds).ToString( "0" ) + "</color> seconds"; },
      CompleteDelegate = RevertScreenSettings
    };
    ScreenSettingsCountdownTimer.Start( this, tp );
  }

  public void RevertScreenSettings()
  {
    BoolSetting["Fullscreen"].Value = CachedScreenSettings.Fullscreen;
    StringSetting["Resolution"].Value = CachedScreenSettings.Resolution;
    FloatSetting["UIScale"].Value = CachedScreenSettings.UIScale;
    ApplyScreenSettings();
  }

#endregion

#region Music
  
  public void PlayMusic( AudioLoop audioLoop )
  {
    audioLoop.Play( musicSourceIntro, musicSource0 );
    activeMusicSource = musicSource0;
    musicSourceIntro.volume = 1;
    activeMusicSource.volume = 1;
    musicSource1.Stop();
  }
  
  public void StopMusic()
  {
    musicSourceIntro.Stop();
    musicSource0.Stop();
    musicSource1.Stop();
  }

  public void MusicCrossFadeTo( AudioLoop audioLoop, float duration )
  {
    // NOTE will not gracefully crossfade while an AudioLoop is playing it's intro clip.
    AudioSource previousMusicSource;
    if( activeMusicSource == musicSource0 )
    {
      activeMusicSource = musicSource1;
      previousMusicSource = musicSource0;
    }
    else
    {
      activeMusicSource = musicSource0;
      previousMusicSource = musicSource1;
    }
    
    if( audioLoop )
      audioLoop.Play( musicSourceIntro, activeMusicSource );
    else
      activeMusicSource.clip = null;
    
    musicCrossFadeTimer.unscaledTime = true;
    musicCrossFadeTimer.Start( this, duration, delegate( Timer obj )
    {
      musicSourceIntro.volume = obj.ProgressNormalized;
      activeMusicSource.volume = obj.ProgressNormalized;
      previousMusicSource.volume = 1 - obj.ProgressNormalized;
    }, null );
  }

  /*public void CrossFadeToClip( AudioClip clip, float transitionDuration )
  {
    AudioSource next = (activeMusicSource == musicSource0) ? musicSource1 : musicSource0;
    next.clip = clip;
    Timer t = new Timer( this, transitionDuration, delegate( Timer obj )
    {
      activeMusicSource.volume = obj.ProgressNormalized;
      next.volume = 1 - obj.ProgressNormalized;
    }, null );
  }*/


  public void MusicPitch( float value ) { activeMusicSource.pitch = value; }

#endregion

#region Boss_VictorySequence

  // allow only one boss at a time to potentially start the destruction/victory sequence
  public bool BossDestructionFlag;

  [DevConsole.Command( "StartVictorySequence" )]
  public static void StartVictorySequence_Command() { Global.instance.StartVictorySequenceDebug(); }

  [ExposeMethod]
  public void StartVictorySequenceDebug()
  {
    ((PlayerBiped) Global.instance.CurrentPlayer).VictorySequenceBegin();
    Global.instance.StartVictorySequence();
  }

  public void StartVictorySequence()
  {
    musicSource0.Stop();
    musicSource1.Stop();
    musicSource0.PlayOneShot( Victory.intro );
    musicSource0.volume = 1;
    musicSource0.pitch = 1;
    musicSource1.volume = 0;
    musicSource1.pitch = 1;
    CameraZone ActiveZone = CameraController.GetActiveZone();
    if( ActiveZone != null )
      ActiveZone.EncompassBounds = false;
    ((PlayerBiped) CurrentPlayer).VictoryMusicStart();
    CameraController.Lerp( CurrentPlayer.transform.position + Vector3.up * 0.25f, 0.8f, 5, true, () => { CameraController.enabled = false; } );

    Timer victoryTimer = new Timer();
    TimerParams tp = new TimerParams();
    tp.duration = Victory.ConsideredDoneDuration;
    tp.unscaledTime = true;
    tp.CompleteDelegate = delegate
    {
      // victory sting
      AudioOneShot( AwesomeSting, CameraController.pos );
      ((PlayerBiped) CurrentPlayer).VictoryStingBegin();
      new Timer( this, 1, null, () =>
      {
        musicSource0.volume = 0;
        LoadScene( ReturnToThisSceneAfterBoss );
        new Timer( this, 1, null, () =>
        {
          CameraController.enabled = true;
          PlayerController.NormalInputMode();
          BossDestructionFlag = false;
          ((PlayerBiped) CurrentPlayer).VictorySequenceEnd();
        } );
      } );
    };
    victoryTimer.Start( this, tp );
  }

#endregion

  public GameObject GetNavMeshObject() { return meshSurfaces[0].gameObject; }

#region ViewPortal

  // DoorCamera, ViewPortal
  [FormerlySerializedAs( "DoorCameraPrefab" ), SerializeField]
  GameObject PortalCameraPrefab;
  const int MaxViewPortals = 4;

  public class ViewPortal
  {
    public bool inUse;
    public GameObject camera;
    public RenderTexture texture;
    public MonoBehaviour owner;
  }

  ViewPortal[] portals = new ViewPortal[MaxViewPortals];

  void IntializeViewPortals()
  {
    for( int i = 0; i < MaxViewPortals; i++ )
    {
      portals[i] = new ViewPortal();
      portals[i].inUse = false;
      portals[i].texture = new RenderTexture( 48, 48, 24, RenderTextureFormat.Default, 0 );
      portals[i].texture.filterMode = FilterMode.Point;
      portals[i].camera = Instantiate( PortalCameraPrefab, transform );
      portals[i].camera.GetComponent<Camera>().targetTexture = portals[i].texture;
    }
  }

  int portalIndex;

  public bool RequestViewPortal( ref ViewPortal portal )
  {
    portal = null;
    // find an inactive portal
    for( int i = 0; i < MaxViewPortals; i++ )
    {
      if( portals[i].inUse == false )
      {
        portal = portals[i];
        portal.inUse = true;
        portalIndex = i;
        break;
      }
    }
    if( portal == null )
    {
      // if no inactive portal found, take one using a rolling index
      portalIndex = (portalIndex + 1 + MaxViewPortals) % MaxViewPortals;
      portal = portals[portalIndex];
      portal.inUse = true;
    }
    portal.camera.SetActive( true );
    return true;
  }

  public void DoneWithViewPortal( ViewPortal portal )
  {
    portal.inUse = false;
    portal.camera.SetActive( false );
    portal.camera.transform.parent = transform;
  }

#endregion


#region ProgressFlag

  public class ProgressBool
  {
    bool _value;
    bool _valueInitial = false;
    public System.Action<bool> onValueChanged;

    public bool Value
    {
      get { return _value; }
      set
      {
        if( value != _value || _valueInitial )
        {
          _value = value;
          _valueInitial = false;
          if( onValueChanged != null )
            onValueChanged.Invoke( _value );
        }
      }
    }
  }

  public Dictionary<string, ProgressBool> ProgressFlag = new Dictionary<string, ProgressBool>();
  // WARNING Enum values cannot be set in UnityEvents in the editor Inspector!!
  // public Dictionary<ProgressBit, ProgressBool> ProgressBools = new Dictionary<ProgressBit, ProgressBool>();

  void InitializeProgressFlags()
  {
    // foreach( ProgressBit value in System.Enum.GetValues( typeof(ProgressBit) ) )
    //   ProgressBools.Add( value, new ProgressBool() );
  }

  public void RegisterProgressFlag( string flag, System.Action<bool> OnChange )
  {
    if( !ProgressFlag.ContainsKey( flag ) )
      ProgressFlag.Add( flag, new ProgressBool() );
    ProgressFlag[flag].onValueChanged += OnChange;
  }

  public bool SetProgressFlag( string flag )
  {
    // returns true if the value changed
    if( !ProgressFlag.ContainsKey( flag ) )
    {
      Debug.LogWarning( "Progress flag <" + flag + "> has not be registered. " );
      return false;
    }
    bool value = ProgressFlag[flag].Value;
    ProgressFlag[flag].Value = true;
    return ProgressFlag[flag].Value != value;
  }
  /*public void RegisterProgressFlag( ProgressBit flag, System.Action<bool> OnChange )
  {
    ProgressBools[flag].onValueChanged += OnChange;
  }*/

#endregion

#region DevConsole

  [DevConsole.Command( "help" )]
  public static void Command_Help()
  {
    DevConsole.DevConsoleUI.Instance.Log( "Command List:" );
    foreach( var pair in DevConsole.DevConsole.methodInfoCache )
    {
      System.Reflection.ParameterInfo[] pis = pair.Value.GetParameters();
      string parms = "";
      foreach( var parm in pis )
      {
        parms += string.Join( " ", "<" + parm.ParameterType.Name + ">", parm.Name );
      }
      DevConsole.DevConsoleUI.Instance.Log( pair.Key + " " + parms );
    }
  }

  /*[DevConsole.Command( "skin" )]
  public static void Command_Skin( int index ) { Global.instance.SetSkin( index ); }*/

  [DevConsole.Command( "nullref" )]
  public static void Command_Nullref()
  {
    // simulate a null ref to be sure the dev console is logging them properly
    DevConsole.DevConsoleUI.Instance.Log( "nullref thrown" );
    throw new System.NullReferenceException();
  }

  public GameObject DebugLoot;

  [DevConsole.Command( "loot" )]
  public static void Command_Loot()
  {
    Vector2 pos = Global.instance.CameraController.pos;
    Instantiate( Global.instance.DebugLoot, pos + Vector2.up, Quaternion.identity );
  }

#if UNITY_EDITOR
  [ExposeMethod]
  public void GatherAllPrefabs()
  {
    ArrayUtility.Clear( ref SpawnablePrefabs );
    string[] guids = AssetDatabase.FindAssets( "t:prefab", new[] {"Assets/prefab"} );
    foreach( string guid in guids )
    {
      GameObject go = AssetDatabase.LoadAssetAtPath<GameObject>( AssetDatabase.GUIDToAssetPath( guid ) );
      if( go != null && go.GetComponentsInChildren<Entity>().Length > 0 )
        ArrayUtility.Add( ref SpawnablePrefabs, go );
    }
    EditorUtility.SetDirty( this );
  }
#endif
  // assigned in editor
  public GameObject[] SpawnablePrefabs;

  [DevConsole.Command( "spawn" ), DevConsole.Params]
  public static void Command_Spawn( params string[] args )
  {
    if( args.Length == 0 )
      return;
    if( args[0].Equals( "list", System.StringComparison.OrdinalIgnoreCase ) )
    {
      string output = "";
      foreach( var item in Global.instance.SpawnablePrefabs )
        output += item.name + "; ";
      for( int i = 0; i < output.Length; i += 80 )
        output.Insert( i, "\n" );
      DevConsole.DevConsoleUI.Instance.Log( output );
    }
    else
    {
      Vector2 pos = Global.instance.CameraController.pos + Vector2.right * 2;
      for( int i = 0; i < args.Length; i++ )
      for( int j = 0; j < Global.instance.SpawnablePrefabs.Length; j++ )
        if( Global.instance.SpawnablePrefabs[j].name.Replace( ' ', '_' ).Equals( args[i].Replace( ' ', '_' ), System.StringComparison.OrdinalIgnoreCase ) )
        {
          Object.Instantiate( Global.instance.SpawnablePrefabs[j], pos, Quaternion.identity );
          break;
        }
    }
  }

  [DevConsole.Command( "show" ), DevConsole.Params]
  public static void Command_Show2( params string[] args )
  {
    if( args[0].Equals( "setting", System.StringComparison.OrdinalIgnoreCase ) || args[0].Equals( "settings", System.StringComparison.OrdinalIgnoreCase ) )
    {
      if( args.Length == 1 )
      {
        string output = "";
        output += "[Bool]\n";
        foreach( var pair in instance.BoolSetting )
          output += pair.Key + " " + pair.Value.Value + "\n";
        output += "[Float]\n";
        foreach( var pair in instance.FloatSetting )
          output += pair.Key + " " + pair.Value.Value + "\n";
        output += "[String]\n";
        foreach( var pair in instance.StringSetting )
          output += pair.Key + " " + pair.Value.Value + "\n";
        DevConsole.DevConsoleUI.Instance.Log( output );
      }
      else
      {
        if( instance.BoolSetting.ContainsKey( args[1] ) )
          DevConsole.DevConsoleUI.Instance.Log( instance.BoolSetting[args[1]].Value.ToString() );
        else if( instance.FloatSetting.ContainsKey( args[1] ) )
          DevConsole.DevConsoleUI.Instance.Log( instance.FloatSetting[args[1]].Value.ToString() );
        else if( instance.StringSetting.ContainsKey( args[1] ) )
          DevConsole.DevConsoleUI.Instance.Log( instance.StringSetting[args[1]].Value.ToString() );
        else
          DevConsole.DevConsoleUI.Instance.LogError( "Setting not found." );
      }
    }
    else
      DevConsole.DevConsoleUI.Instance.LogError( "Key Not found" );
  }


  [DevConsole.Command( "set" )]
  public static void Command_Set( string key, string value )
  {
    if( instance.BoolSetting.ContainsKey( key ) )
    {
      bool result;
      if( bool.TryParse( value, out result ) )
        instance.BoolSetting[key].Value = result;
      else
        DevConsole.DevConsoleUI.Instance.LogError( "Cannot parse bool value" );
    }
    else if( instance.FloatSetting.ContainsKey( key ) )
    {
      float result;
      if( float.TryParse( value, out result ) )
        instance.FloatSetting[key].Value = result;
      else
        DevConsole.DevConsoleUI.Instance.LogError( "Cannot parse float value" );
    }
    else if( instance.StringSetting.ContainsKey( key ) )
    {
      instance.StringSetting[key].Value = key;
    }
    else
      DevConsole.DevConsoleUI.Instance.LogError( "Setting \"" + key + "\" not found." );
  }

#endregion

  public Team GetTeam( TeamFlags flags )
  {
    for( int i = 0; i < Teams.Length; i++ )
      if( Teams[i].flags == flags )
        return Teams[i];
    return null;
  }


  public void SaveGame( string saveStateName ) { SerializedObject.WriteScene( "SAVETEST" ); }

  public void LoadGame( string saveStateName ) { }

#if LIGHTING_TOGGLE
  void EnableLighting()
  {
    LightingEnabled = true;
    if( sceneScript != null && sceneScript.ambientLight != null )
      sceneScript.ambientLight.enabled = true;
    foreach( var variant in ShaderVariants )
    {
      variant.material.shader = variant.litShader;
    }
    /*
    if( !Application.isEditor )
    {
      // WARNING these are components on prefabs and in scenes
      foreach( var data in lightComponents )
      {
        if( data.EnabledInPrefab )
          data.light.enabled = true;
      }
    }*/
  }

  void DisableLighting()
  {
    LightingEnabled = false;
    if( sceneScript != null && sceneScript.ambientLight != null )
      sceneScript.ambientLight.enabled = false;
    foreach( var variant in ShaderVariants )
    {
      variant.material.shader = unlitShader;
      variant.material.color = Color.white;
    }
    /*
    if( !Application.isEditor )
    {
      // WARNING these are components on prefabs and in scenes
      foreach( var data in lightComponents )
      {
        data.light.enabled = false;
      }
    }*/
  }
#endif

  /*
  #region Skins
  
    [System.Serializable]
    public struct SkinUnion
    {
      public Material sharedMaterial;
      public Texture2D[] diffuse;
      public Texture2D[] emissive;
    }
  
    public SkinUnion[] skins;
    int skinIndex;
    public int skinCount;
  
    public void NextSkin()
    {
      skinIndex = (skinIndex + 1) % skinCount;
      SetSkin( skinIndex );
    }
  
    public void SetSkin( int index )
    {
      for( int i = 0; i < skins.Length; i++ )
      {
        // simply setting the texture does not work for some reason.
        //skins[i].sharedMaterial.SetTexture( "_MainTex", skins[i].skin[index] );
        Texture2D tex = ((Texture2D)skins[i].sharedMaterial.GetTexture( "_MainTex" ));
        tex.SetPixels32( skins[i].diffuse[index].GetPixels32() );
        tex.Apply();
        tex = ((Texture2D)skins[i].sharedMaterial.GetTexture( "_EmissiveTex" ));
        tex.SetPixels32( skins[i].emissive[index].GetPixels32() );
        tex.Apply();
        
      }
    }
  
  #endregion
  */
}