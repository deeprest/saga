﻿#if UNITY_EDITOR
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using UnityEditor.Experimental.SceneManagement;
using UnityEngine.AI;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Experimental.U2D.Animation;
using UnityEngine.Tilemaps;
using Object = UnityEngine.Object;
using Debug = UnityEngine.Debug;

public static class StaticUtility
{
  [MenuItem( "Assets/ShowGUID", true )]
  static bool CanExecute() { return Selection.assetGUIDs.Length > 0; }

  [MenuItem( "Assets/ShowGUID" )]
  static void Execute()
  {
    for( int i = 0; i < Selection.objects.Length; i++ )
      Debug.Log( Selection.objects[i].GetType().ToString() + " " + Selection.objects[i].name + " " + Selection.assetGUIDs[i] );
  }

  /* Util.cs
  public static string GetCurrentAssetDirectory()
  {
    foreach( var obj in UnityEditor.Selection.GetFiltered<Object>( UnityEditor.SelectionMode.Assets ) )
    {
      var path = UnityEditor.AssetDatabase.GetAssetPath( obj );
      if( string.IsNullOrEmpty( path ) )
        continue;

      if( System.IO.Directory.Exists( path ) )
        return path;
      else if( System.IO.File.Exists( path ) )
        return System.IO.Path.GetDirectoryName( path );
    }

    return "Assets";
  }

  public static Object[] GetAssetsFromSelectedFolder( string search = "t:prefab" )
  {
    List<Object> objects = new List<Object>();
    string[] guids = AssetDatabase.FindAssets( search, new string[] { GetCurrentAssetDirectory() } );
    foreach( string guid in guids )
      objects.Add( AssetDatabase.LoadAssetAtPath<Object>( AssetDatabase.GUIDToAssetPath( guid ) ) );
    return objects.ToArray();
  }
  */
}


public class CustomUtility : EditorWindow
{
  // This will make sure there is always a GLOBAL object when playing a scene in the editor
  [RuntimeInitializeOnLoadMethod]
  static void OnLoadMethod()
  {
    if( EditorPrefs.GetBool( "CreateGlobalOnStart" ) )
    {
      for( int i = 0; i < SceneManager.sceneCount; i++ )
      {
        Scene scene = SceneManager.GetSceneAt( i );
        if( scene.name == "GLOBAL" )
          return;
      }
      GameObject.Instantiate( Resources.Load<GameObject>( "GLOBAL" ) );
    }
  }

  /*static string todo;*/

  [MenuItem( "Window/Custom Utility", false, 2 )]
  static void Init()
  {
    // Get existing open window or if none, make a new one:
    CustomUtility window = (CustomUtility)EditorWindow.GetWindow( typeof(CustomUtility) );
    window.Show();
  }

  private void OnEnable()
  {
    /*todo = File.ReadAllText( Application.dataPath + "/../todo" );*/
  }

  static bool buildMacOS = true;
  static bool buildLinux = false;
  static bool buildWebGL = false;
  static bool buildWindows = false;
  static bool buildAutoSceneRefs = false;

  string progressMessage = "";
  float progress = 0;
  bool processing = false;
  System.Action ProgressUpdate;
  System.Action ProgressDone;
  int index = 0;
  int length = 0;

  List<AudioSource> auds;
  // List<string> scenes;
  List<GameObject> gos;
  List<Object> objects;
  List<Light2D> lights;
  List<string> assetPaths;
  int layer;

  GameObject replacementPrefab;
  bool ProcessSubObjects;
  bool ReplacePrefab;
  bool ReplaceSprite;
  bool ReplaceMaterial;
  bool AutoFixSprite;

  enum OperationContext
  {
    SELECTED = 0,
    SCENE,
    PROJECT
  }

  OperationContext operationContext;

  string FilterObjectName = "";
  string FilterSubObjectName = "";
  Sprite replacementSprite;
  Material replacementMaterial;
  string[] guids;
  /*float fudgeFactor;*/

  // font output
  Texture2D fontImage;

  // SpriteLibrary
  string category;
  SpriteLibraryAsset sla;
  Sprite[] sprites;

  Vector2 scrollPosition = Vector2.zero;
  bool foldTodo = true;
  bool foldSettings;
  bool foldBuild;
  bool foldGeneric;
  bool foldFont;
  bool foldMultiTool;
  bool foldSprite;
  int fontAdvance = 16;

  // Tilemap
  bool foldTilemap;
  Texture2D tilemap;
  Texture2D palette;
  Tilemap tilemapInScene;
  Texture2D tileset;
  GameObject paletteObject;
  Dictionary<string, int> paletteIndex;
  Vector2Int tileSize = new Vector2Int( 16, 16 );
  int removeTileIndex = 1;

  void StartJob( string message, int count, System.Action update, System.Action done )
  {
    progressMessage = message;
    processing = true;
    index = 0;
    progress = 0;
    length = count;
    ProgressUpdate = update;
    ProgressDone = done;
  }

  void OnGUI()
  {
    if( !Application.isPlaying && EditorPrefs.GetBool( "ConstantEditorRepaint" ) )
    {
      EditorApplication.QueuePlayerLoopUpdate();
      SceneView.RepaintAll();
      Repaint();
    }

    if( processing )
    {
      if( index == length )
      {
        processing = false;
        progress = 1;
        ProgressUpdate = null;
        if( ProgressDone != null )
          ProgressDone();
        progressMessage = "Done";
      }
      else
      {
        ProgressUpdate();
        progress = (float)index++ / (float)length;
      }
      // this call forces OnGUI to be called repeatedly instead of on-demand
      Repaint();
    }

    /*
    if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "NEW ACTION" ) )
    {
      int count = 0;
      if( Selection.objects.Length > 0 )
      {
        objects = new List<Object>( Selection.objects );
        count = objects.Count;
      }
      if( count > 0 )
      {
        StartJob( "Searching...", count, delegate ()
        {
          
            //objects[index]
          
        }, null );
      }
    }
    */

    EditorGUILayout.Space();
    EditorGUI.ProgressBar( EditorGUILayout.GetControlRect( false, 30 ), progress, progressMessage );

    scrollPosition = EditorGUILayout.BeginScrollView( scrollPosition, false, true );
    foldSettings = EditorGUILayout.Foldout( foldSettings, "Settings" );
    if( foldSettings )
    {
      EditorPrefs.SetBool( "ConstantEditorRepaint", EditorGUILayout.Toggle( "ConstantEditorRepaint", EditorPrefs.GetBool( "ConstantEditorRepaint", true ) ) );
      EditorPrefs.SetBool( "CreateGlobalOnStart", EditorGUILayout.Toggle( "CreatGlobalOnStart", EditorPrefs.GetBool( "CreateGlobalOnStart", true ) ) );
      Popcron.Gizmos.Enabled = EditorGUILayout.Toggle( "Gizmos.Enabled", Popcron.Gizmos.Enabled );
    }
    EditorGUILayout.Space();
    foldBuild = EditorGUILayout.Foldout( foldBuild, "Build" );
    if( foldBuild )
    {
      /*if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Run WebGL Build" ) )
      {
        string path = EditorUtility.OpenFolderPanel( "Select WebGL build folder", "", "" );
        if( path.Length != 0 )
        {
          Debug.Log( path );
          Util.Execute( new Util.Command { cmd = "exec /usr/local/bin/static", args = "", dir = path }, true );
        }
      }*/
      EditorGUILayout.BeginHorizontal();
      buildMacOS = EditorGUILayout.ToggleLeft( "MacOS", buildMacOS, GUILayout.MaxWidth( 60 ) );
      buildLinux = EditorGUILayout.ToggleLeft( "Linux", buildLinux, GUILayout.MaxWidth( 60 ) );
      buildWindows = EditorGUILayout.ToggleLeft( "Windows", buildWindows, GUILayout.MaxWidth( 60 ) );
      buildWebGL = EditorGUILayout.ToggleLeft( "WebGL", buildWebGL, GUILayout.MaxWidth( 60 ) );
      EditorGUILayout.EndHorizontal();
      buildAutoSceneRefs = EditorGUILayout.Toggle( "Auto Add Scene Refs", buildAutoSceneRefs );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Build" ) )
      {
        if( BuildPipeline.isBuildingPlayer )
          return;

        Build();
      }
    }

    GUILayout.Space( 10 );
    foldGeneric = EditorGUILayout.Foldout( foldGeneric, "Custom Utils" );
    if( foldGeneric )
    {
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Auto Parent to Zones" ) )
      {
        // gather all root objects with Area tag
        GameObject[] zones = GameObject.FindGameObjectsWithTag( "Zone" );
        // check all root objects 
        GameObject[] roots = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach( var zone in zones )
        {
          for( int i = 0; i < zone.transform.childCount; i++ )
          {
            ArrayUtility.Add( ref roots, zone.transform.GetChild( i ).gameObject );
          }
        }
        //Undo.RecordObjects( roots, "Parent to Root Areas" );
        foreach( var go in roots )
        {
          if( !go.CompareTag( "Zone" ) && !go.CompareTag( "Scene" ) )
          {
            foreach( var zone in zones )
            {
              if( zone.GetComponent<Collider2D>().OverlapPoint( go.transform.position ) )
              {
                go.transform.parent = zone.transform;
                break;
              }
            }
          }
        }
        EditorSceneManager.MarkSceneDirty( SceneManager.GetActiveScene() );
      }

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Auto Parent to Rooms" ) )
      {
        GameObject[] rooms = GameObject.FindGameObjectsWithTag( "Room" );
        if( rooms.Length > 0 )
        {
          Debug.Log( "Room count: " + rooms.Length );
          gos = new List<GameObject>( Object.FindObjectsOfType<GameObject>() );
          List<GameObject> gathered = new List<GameObject>();
          StartJob( "Gathering...", gos.Count, delegate()
          {
            GameObject go = gos[index];
            if( !go.CompareTag( "Zone" ) && !go.CompareTag( "Scene" ) && !go.CompareTag( "Room" ) && !go.CompareTag( "Parallax" ))
            {
              Transform top = go.transform;
              while( top.parent != null && !top.parent.CompareTag( "Zone" ) && !top.parent.CompareTag( "Scene" ) && !top.parent.CompareTag( "Room" ) )
                top = top.parent;
              gathered.Add( top.gameObject );
            }
          }, delegate()
          {
            Debug.Log( "Gathered Count: " + gathered.Count );
            StartJob( "Reparenting...", rooms.Length, delegate()
            {
              GameObject room = rooms[index];
              BoxCollider2D box = room.GetComponent<BoxCollider2D>();
              if( box != null )
                for( int i = 0; i < gathered.Count; i++ )
                  if( box.OverlapPoint( gathered[i].transform.position ) )
                    gathered[i].transform.parent = room.transform;
            }, delegate() { EditorSceneManager.MarkSceneDirty( SceneManager.GetActiveScene() ); } );
          } );
        }
        else
        {
          Debug.Log( "Must have an object tagged 'Room' with a box collider" );
        }
      }

      /*GUILayout.Label( "Character", EditorStyles.boldLabel );*/
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Select Player Character" ) )
        if( Application.isPlaying )
          Selection.activeGameObject = Global.instance.CurrentPlayer.gameObject;

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Move Selection Up (Offset)" ) )
      {
        GameObject[] gos = Selection.GetFiltered<GameObject>( SelectionMode.TopLevel );
        Undo.SetCurrentGroupName( "TEST UP" );
        Undo.RecordObjects( gos, "Move Up (Offset)" );
        foreach( var obj in gos )
          obj.transform.position = obj.transform.position + Vector3.up * Global.GameplayLayerOffset;
      }

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Move Selection Down (Offset)" ) )
      {
        GameObject[] gos = Selection.GetFiltered<GameObject>( SelectionMode.TopLevel );
        Undo.RecordObjects( gos, "Move Down (Offset)" );
        Undo.SetCurrentGroupName( "TEST Down" );
        foreach( var obj in gos )
          obj.transform.position = obj.transform.position + Vector3.down * Global.GameplayLayerOffset;
      }

      EditorGUILayout.BeginHorizontal();
      layer = (LayerMask)EditorGUILayout.IntField( layer, GUILayout.MaxWidth( 40 ) );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Find GameObjects by Layer" ) )
      {
        gos = new List<GameObject>( Resources.FindObjectsOfTypeAll<GameObject>() );
        List<GameObject> found = new List<GameObject>();
        StartJob( "Searching...", gos.Count, delegate()
        {
          GameObject go = gos[index];
          if( go.layer == layer )
            found.Add( go );
        }, delegate()
        {
          foreach( var go in found )
          {
            Debug.Log( "Found: " + go.name, go );
          }
        } );
      }
      EditorGUILayout.EndHorizontal();
#if LIGHTING_TOGGLE
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Gather Light Components" ) )
      {
        lights = new List<Light2D>( Resources.FindObjectsOfTypeAll<Light2D>() );
        List<Light2D> found = new List<Light2D>();
        StartJob( "Searching...", lights.Count, delegate() { found.Add( lights[index] ); }, delegate()
        {
          List<Global.LightData> assetLights = new List<Global.LightData>();
          foreach( var light in found )
          {
            string assetpath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot( light );
            assetLights.Add( new Global.LightData() {EnabledInPrefab = light.enabled, light = light} );
            if( light.enabled )
              Debug.Log( "Path: " + assetpath + " Object: " + light.name + " in scene " + light.gameObject.scene.name, light );
            else
              Debug.LogWarning( "[DISABLED] Path: " + assetpath + " Object: " + light.name + " in scene " + light.gameObject.scene.name, light );
          }
          const string globalObject = "Assets/Resources/Global.prefab";
          GameObject go = PrefabUtility.LoadPrefabContents( globalObject );
          Global gbl = go.GetComponent<Global>();
          if( gbl.lightComponents == null )
            gbl.lightComponents = new Global.LightData[0];
          else
            ArrayUtility.Clear( ref gbl.lightComponents );
          ArrayUtility.AddRange( ref gbl.lightComponents, assetLights.ToArray() );
          PrefabUtility.SaveAsPrefabAsset( go, globalObject );
          AssetDatabase.SaveAssets();
        } );
      }
#endif
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Show GUID of selected assets" ) )
      {
        for( int i = 0; i < Selection.assetGUIDs.Length; i++ )
          Debug.Log( Selection.objects[i].GetType().ToString() + " " + Selection.objects[i].name + " " + Selection.assetGUIDs[i] );
      }

      /*fudgeFactor = EditorGUILayout.FloatField( "fudge", fudgeFactor );*/

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Generate Edge Collider Nav Obstacles" ) )
      {
        GameObject go = new GameObject( "Nav Obstacle" );
        go.transform.position = Vector3.back;
        go.layer = LayerMask.NameToLayer( "Ignore Raycast" );
        //go.AddComponent<MeshRenderer>();
        //MeshFilter mf = go.AddComponent<MeshFilter>();

        List<CombineInstance> combine = new List<CombineInstance>();
        EdgeCollider2D[] edges = FindObjectsOfType<EdgeCollider2D>();
        for( int e = 0; e < edges.Length; e++ )
        {
          EdgeCollider2D edge = edges[e];
          /*
          Vector3[] v = new Vector3[(edge.points.Length - 1) * 4];
          int[] indices = new int[(edge.points.Length) * 4];
          Vector3[] n = new Vector3[v.Length];
          for( int i = 0; i < edge.points.Length - 1; i++ )
          {
            Vector2 a = edge.points[i];
            Vector2 b = edge.points[i + 1];
            Vector2 segment = b - a;
            Vector2 fudge = segment.normalized * fudgeFactor;
            Vector2 cross = (new Vector2( -segment.y, segment.x )).normalized * (edge.edgeRadius + fudgeFactor);
            v[i * 4] = a + cross - fudge;
            v[i * 4 + 1] = a + cross + segment + fudge;
            v[i * 4 + 2] = a - cross + segment + fudge;
            v[i * 4 + 3] = a - cross - fudge;
            n[i * 4] = Vector3.back;
            n[i * 4 + 1] = Vector3.back;
            n[i * 4 + 2] = Vector3.back;
            n[i * 4 + 3] = Vector3.back;
            indices[i * 4] = i * 4;
            indices[i * 4 + 1] = i * 4 + 1;
            indices[i * 4 + 2] = i * 4 + 2;
            indices[i * 4 + 3] = i * 4 + 3;
          }
          */
          CombineInstance ci = new CombineInstance();
          ci.transform = edge.transform.localToWorldMatrix;
          //ci.mesh = new Mesh();
          ci.mesh = edge.CreateMesh( false, false );
          //ci.mesh.vertices = v;
          //ci.mesh.normals = n;
          //ci.mesh.SetIndices( indices, MeshTopology.Quads, 0 );
          combine.Add( ci );
        }

        Mesh mesh = new Mesh();
        if( combine.Count == 1 )
        {
          mesh = combine[0].mesh;
          //go.transform.position = (Vector3)combine[0].transform.GetColumn( 3 );
        }
        else
          mesh.CombineMeshes( combine.ToArray(), false, false );

        MeshCollider mc = go.AddComponent<MeshCollider>();
        mc.sharedMesh = mesh;
        //mf.sharedMesh = mesh;
      }

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 20 ), "Zip Persistent Files" ) )
      {
        List<string> persistentFilenames = new List<string>();
        foreach( string pfn in Global.persistentFilenames )
          persistentFilenames.Add( Application.persistentDataPath + "/" + pfn );
        ZipUtil.Zip( Application.dataPath + "/Resources/persistent.bytes", persistentFilenames.ToArray() );
      }
      /*
          if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Zip Data" ) )
          {
            //string[] scenes = new string[]{ "home" };
            scenes = new List<string>();
      
            string[] dirs = Directory.GetDirectories( Application.persistentDataPath );
            foreach( var dir in dirs )
            {
              string basename = Path.GetFileName( dir );
              if( basename == "Unity" )
                continue;
              scenes.Add( basename );
            }
      
            StartJob( "Zipping...", scenes.Count, delegate()
            {
              string basename = scenes[ index ];
              Debug.Log( "Zipping level: " + basename );
              string[] files = Directory.GetFiles( Application.persistentDataPath + "/" + basename );
              ZipUtil.Zip( Application.dataPath + "/Resources/zone/" + basename + ".bytes", files );
            },
              delegate()
              {
                List<string> persistentFilenames = new List<string>();
                foreach( string pfn in Global.persistentFilenames )
                  persistentFilenames.Add( Application.persistentDataPath + "/" + pfn );
                ZipUtil.Zip( Application.dataPath + "/Resources/persistent.bytes", persistentFilenames.ToArray() );
      
                AssetDatabase.Refresh();
                // highlight folder in project view
                Object obj = AssetDatabase.LoadAssetAtPath( "Assets/Resources/level/" + scenes[ 0 ] + ".bytes", typeof(Object) );
                //    UnityEngine.Object obj = AssetDatabase.LoadAssetAtPath("Assets/Resources/level/home.bytes", typeof(UnityEngine.Object));
                Selection.activeObject = obj;
                EditorGUIUtility.PingObject( obj );
              } );
          }
          */

      /*GUILayout.Space( 10 );
      GUILayout.Label( "Audio Sources", EditorStyles.boldLabel );
      audioDistanceMin = EditorGUILayout.IntField( "audioDistanceMin", audioDistanceMin );
      audioDistanceMax = EditorGUILayout.IntField( "audioDistanceMax", audioDistanceMax );
      audioRolloff = (AudioRolloffMode)EditorGUILayout.EnumPopup( "rolloff", audioRolloff );
      audioDoppler = EditorGUILayout.IntField( "doppler", audioDoppler );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Apply to All Prefabs!" ) )
      {
        auds = new List<AudioSource>();
        string[] guids = AssetDatabase.FindAssets( "t:prefab" );
        foreach( string guid in guids )
        {
          GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>( AssetDatabase.GUIDToAssetPath( guid ) );
          AudioSource[] source = prefab.GetComponentsInChildren<AudioSource>();
          foreach( var ass in source )
            auds.Add( ass );
        }
        StartJob( "Applying...", auds.Count, delegate ()
        {
          AudioSource ass = auds[index];
          Debug.Log( "audio source modified in prefab: " + ass.gameObject.name, ass.gameObject );
          ass.minDistance = audioDistanceMin;
          ass.maxDistance = audioDistanceMax;
          ass.rolloffMode = audioRolloff;
          ass.dopplerLevel = audioDoppler;
        }, null );
      }*/

      GUILayout.Label( "Quad Mesh", EditorStyles.boldLabel );
      md = EditorGUILayout.Vector2Field( "Quad Dimensions", md );
      if( GUI.Button( EditorGUILayout.GetControlRect(), "Generate Quad Mesh" ) )
      {
        var mesh = new Mesh();
        var vertices = new Vector3[4];
        vertices[0] = new Vector3( -md.x * 0.5f, +md.y * 0.5f, 0 );
        vertices[1] = new Vector3( +md.x * 0.5f, +md.y * 0.5f, 0 );
        vertices[2] = new Vector3( -md.x * 0.5f, -md.y * 0.5f, 0 );
        vertices[3] = new Vector3( +md.x * 0.5f, -md.y * 0.5f, 0 );
        mesh.vertices = vertices;
        var uvs = new Vector2[4];
        uvs[0] = new Vector2( 0, 1 );
        uvs[1] = new Vector2( 1, 1 );
        uvs[2] = new Vector2( 0, 0 );
        uvs[3] = new Vector2( 1, 0 );
        mesh.uv = uvs;
        var indices = new[] { 0, 1, 2, 3, 2, 1 };
        mesh.SetIndices( indices, MeshTopology.Triangles, 0 );
        mesh.RecalculateNormals();
        AssetDatabase.CreateAsset( mesh, Path.Combine( Util.GetCurrentAssetDirectory(), "quad " + md.x + "x" + md.y + ".asset" ) );
        EditorUtility.SetDirty( mesh );
        AssetDatabase.Refresh();
      }

      // if( GUI.Button( EditorGUILayout.GetControlRect(), "DO IT" ) )
      // {
      // string path = Path.Combine( Path.GetDirectoryName( Application.dataPath ), "ProjectSettings", "ProjectSettings.asset" );
      // string PlayerSettingsFile = File.ReadAllText( path );
      // string output = Regex.Replace( PlayerSettingsFile, @"(bundleVersion\:).*", "$1 "+"8.0.0" );
      // File.WriteAllText( path, output ); 
      // AssetDatabase.Refresh();
      // }
    }

    EditorGUILayout.Space( 10 );
    foldMultiTool = EditorGUILayout.Foldout( foldMultiTool, "Multi Tool" );
    if( foldMultiTool )
    {
      operationContext = (OperationContext)EditorGUILayout.EnumPopup( "Context", operationContext );

      FilterObjectName = EditorGUILayout.TextField( "Filter by Name", FilterObjectName );

      ProcessSubObjects = EditorGUILayout.Toggle( "Process SubObjects", ProcessSubObjects );
      FilterSubObjectName = EditorGUILayout.TextField( "SubobjectName", FilterSubObjectName );

      ReplacePrefab = EditorGUILayout.Toggle( "ReplacePrefab", ReplacePrefab );
      replacementPrefab = (GameObject)EditorGUILayout.ObjectField( "Replacement Prefab", replacementPrefab, typeof(GameObject), false, GUILayout.MinWidth( 50 ) );

      ReplaceSprite = EditorGUILayout.Toggle( "ReplaceSprite", ReplaceSprite );
      replacementSprite = (Sprite)EditorGUILayout.ObjectField( "Replacement Sprite", replacementSprite, typeof(Sprite), false );

      ReplaceMaterial = EditorGUILayout.Toggle( "Replace Material", ReplaceMaterial );
      replacementMaterial = (Material)EditorGUILayout.ObjectField( "Replacement Material", replacementMaterial, typeof(Material), false );

      AutoFixSprite = EditorGUILayout.Toggle( "Auto Fix Sprite", AutoFixSprite );

      PrefabStage prefabStage = PrefabStageUtility.GetCurrentPrefabStage();

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Process Objects" ) )
      {
        int count = 0;
        switch( operationContext )
        {
          case OperationContext.SELECTED:
            gos = new List<GameObject>( Selection.gameObjects );
            if( FilterObjectName.Length > 0 )
              for( int i = 0; i < gos.Count; i++ )
                if( !gos[i].name.StartsWith( FilterObjectName ) )
                  gos.RemoveAt( i-- );
            count = gos.Count;
            break;

          case OperationContext.SCENE:
            if( prefabStage != null )
            {
              ProcessSubObjects = true;
              gos = new List<GameObject>();
              gos.Add( prefabStage.prefabContentsRoot );
              assetPaths = new List<string>();
              assetPaths.Add( prefabStage.prefabAssetPath );
              count = 1;
            }
            else
            {
              gos = new List<GameObject>( FindObjectsOfType<GameObject>() );
              if( FilterObjectName.Length > 0 )
                for( int i = 0; i < gos.Count; i++ )
                  if( !gos[i].name.StartsWith( FilterObjectName ) )
                    gos.RemoveAt( i-- );
              count = gos.Count;
            }
            break;

          case OperationContext.PROJECT:
            // NOTE do not allow replacing prefabs with other prefab instances
            ReplacePrefab = false;
            assetPaths = new List<string>();
            guids = AssetDatabase.FindAssets( FilterObjectName + " t:prefab" );
            foreach( string guid in guids )
              assetPaths.Add( AssetDatabase.GUIDToAssetPath( guid ) );
            count = assetPaths.Count;
            break;
        }
        StartJob( "Processing...", count, delegate()
        {
          GameObject go = null;

          switch( operationContext )
          {
            case OperationContext.SELECTED:
            case OperationContext.SCENE:
              go = gos[index];
              break;

            case OperationContext.PROJECT:
              go = PrefabUtility.LoadPrefabContents( assetPaths[index] );
              break;
          }

          if( ProcessSubObjects )
          {
            Transform[] tfs = go.GetComponentsInChildren<Transform>();
            for( int i = 0; i < tfs.Length; i++ )
            {
              Transform tf = tfs[i];
              if( FilterSubObjectName.Length == 0 || tf.name.StartsWith( FilterSubObjectName ) )
                ProcessSingle( tf.gameObject );
            }
          }
          else
          {
            ProcessSingle( go );
          }

          if( operationContext == OperationContext.PROJECT )
          {
            PrefabUtility.SaveAsPrefabAsset( go, assetPaths[index] );
            PrefabUtility.UnloadPrefabContents( go );
          }

          if( prefabStage != null )
            EditorSceneManager.MarkSceneDirty( prefabStage.scene );
        }, AssetDatabase.SaveAssets );
      }
    }

    EditorGUILayout.Space( 10 );
    foldFont = EditorGUILayout.Foldout( foldFont, "Pixel Font" );
    if( foldFont )
    {
      fontAdvance = EditorGUILayout.IntField( "Character Advance", fontAdvance );
      // tested on Unity 2019.2.6f1
      fontImage = (Texture2D)EditorGUILayout.ObjectField( "Pixel Font Image", fontImage, typeof(Texture2D), false );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Create Pixel Font" ) )
      {
        string letter = " !\"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~";
        const int imageSize = 256;
        const int cols = 16;
        const int charWith = imageSize / cols;
        string output = "info font=\"Base 5\" size=32 bold=0 italic=0 charset=\"\" unicode=0 stretchH=100 smooth=1 aa=1 padding=0,0,0,0 spacing=2,2\n" + "common lineHeight=10 base=12 scaleW=" +
          imageSize + " scaleH=" + imageSize + " pages=1 packed=0\n" + "page id=0 file=\"" + fontImage.name + ".png\"\nchars count=" + letter.Length + "\n";
        for( int i = 0; i < letter.Length; i++ )
          output += "char id=" + i + " x=" + (i % cols) * charWith + " y=" + (i / cols) * charWith + " width=14 height=14 xoffset=1 yoffset=1 xadvance=" + fontAdvance + " page=0 chnl=0 letter=\"" +
            letter[i] + "\"\n";
        output += "kernings count=0";
        File.WriteAllText( Application.dataPath + "/font/" + fontImage.name + ".fnt", output );
        AssetDatabase.SaveAssets();

        string fntpath = Path.ChangeExtension( AssetDatabase.GetAssetPath( fontImage ), "fnt" );
        Debug.Log( fntpath );
        AssetDatabase.ImportAsset( fntpath, ImportAssetOptions.ForceUpdate | ImportAssetOptions.ForceSynchronousImport );
        AssetDatabase.Refresh();
        litefeel.BFImporter.DoImportBitmapFont( fntpath );

        // set default font values
        string fontsettings = Path.ChangeExtension( AssetDatabase.GetAssetPath( fontImage ), "fontsettings" );
        Font font = AssetDatabase.LoadAssetAtPath<Font>( fontsettings );
        SerializedObject so = new SerializedObject( font );
        so.Update();
        so.FindProperty( "m_AsciiStartOffset" ).intValue = 32;
        so.FindProperty( "m_Tracking" ).floatValue = 0.5f;
        so.FindProperty( "m_CharacterRects" ).GetArrayElementAtIndex( 0 ).FindPropertyRelative( "advance" ).floatValue = fontAdvance;
        so.ApplyModifiedProperties();
        so.SetIsDifferentCacheDirty();
        so.Update();
      }
    }

    EditorGUILayout.Space( 10 );
    foldSprite = EditorGUILayout.Foldout( foldSprite, "Sprite Library" );
    if( foldSprite )
    {
      sla = (SpriteLibraryAsset)EditorGUILayout.ObjectField( "sprite library", sla, typeof(SpriteLibraryAsset), false );
      category = EditorGUILayout.TextField( "category", category );
      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Add Selected Sprites" ) )
      {
        SortedList<string, Sprite> list = new SortedList<string, Sprite>();
        Object[] objects = Selection.GetFiltered<Object>( SelectionMode.Unfiltered );
        for( int i = 0; i < objects.Length; i++ )
        {
          int underscore = objects[i].name.LastIndexOf( '_' ) + 1;
          string index = objects[i].name.Substring( underscore, objects[i].name.Length - underscore );
          list.Add( index, (Sprite)objects[i] );
        }
        foreach( var pair in list )
          sla.AddCategoryLabel( pair.Value, category, pair.Key );

        EditorUtility.SetDirty( sla );
      }
    }
    EditorGUILayout.Space( 10 );

    foldTilemap = EditorGUILayout.Foldout( foldTilemap, "Aseprite Tilemap" );
    if( foldTilemap )
    {
      tileSize = EditorGUILayout.Vector2IntField( "Tile Size", tileSize );
      tilemapInScene = (Tilemap)EditorGUILayout.ObjectField( "tilemap in scene", tilemapInScene, typeof(Tilemap), true );
      tilemap = (Texture2D)EditorGUILayout.ObjectField( "tilemap", tilemap, typeof(Texture2D), false );
      tileset = (Texture2D)EditorGUILayout.ObjectField( "tileset", tileset, typeof(Texture2D), false );
      palette = (Texture2D)EditorGUILayout.ObjectField( "palette map", palette, typeof(Texture2D), false );
      /*paletteObject = (GameObject)EditorGUILayout.ObjectField( "palette object", paletteObject, typeof(GameObject), false );*/

      // Prerequisites: Aseprite has the ability to export images for this script to work. 
      // Align the level to a grid (16x16 for SNES games)
      // Run the script "Pack Similar Tiles" in the Aseprite File menu. This creates two files, a tilemap and a tileset.
      // The palette for the tilemap must have unique values for this script to work, so next run "Random Palette" on the tilemap.
      // DO NOT edit or "Remap" the palette. 
      // The palette image is necessary because this script does not import and read indexed images.
      // In the palette menu, select "Save Palette" and save to a PNG.
      // Now save all three files into your Unity project.

      if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "Create Scene from Aseprite Tilemap" ) )
      {
        // Make tilemap readable
        {
          string path = AssetDatabase.GetAssetPath( tilemap );
          TextureImporter ti0 = AssetImporter.GetAtPath( path ) as TextureImporter;
          ti0.isReadable = true;
          AssetDatabase.ImportAsset( path, ImportAssetOptions.ForceUpdate );
          EditorUtility.SetDirty( tilemap );
        }
        // Make palette readable
        {
          string path = AssetDatabase.GetAssetPath( palette );
          TextureImporter ti1 = AssetImporter.GetAtPath( path ) as TextureImporter;
          ti1.isReadable = true;
          AssetDatabase.ImportAsset( path, ImportAssetOptions.ForceUpdate );
          EditorUtility.SetDirty( palette );
        }

        paletteIndex = new Dictionary<string, int>();
        Color[] ppix = palette.GetPixels();
        int i = 0;
        for( int y = 0; y < palette.height; y++ )
        for( int x = 0; x < palette.width; x++, i++ )
        {
          paletteIndex[ColorUtility.ToHtmlStringRGB( ppix[x + (palette.height - 1 - y) * palette.width] )] = i;
        }

        string tilesetPath = AssetDatabase.GetAssetPath( tileset );
        TextureImporter ti = AssetImporter.GetAtPath( tilesetPath ) as TextureImporter;
        ti.isReadable = true;
        ti.spriteImportMode = SpriteImportMode.Multiple;
        List<SpriteMetaData> lsmd = new List<SpriteMetaData>();
        for( int y = tileset.height; y > 0; y -= tileSize.y )
        {
          for( int x = 0; x < tileset.width; x += tileSize.x )
          {
            SpriteMetaData smd = new SpriteMetaData();
            smd.pivot = new Vector2( 0.5f, 0.5f );
            smd.alignment = 9;
            // format the name into (Y,X) column-order because of alphabetical sorting of subassets.
            smd.name = ((tileset.height - y) / tileSize.y) + ", " + x / tileSize.x;
            smd.rect = new Rect( x, y - tileSize.y, tileSize.x, tileSize.y );
            lsmd.Add( smd );
          }
        }
        ti.spritesheet = lsmd.ToArray();
        AssetDatabase.ImportAsset( tilesetPath, ImportAssetOptions.ForceUpdate );
        EditorUtility.SetDirty( tileset );

        Object[] spriteTiles = AssetDatabase.LoadAllAssetsAtPath( AssetDatabase.GetAssetPath( tileset ) );
        Dictionary<string, Tile> tiles = new Dictionary<string, Tile>();
        Vector3Int pos = Vector3Int.zero;
        for( int x = 0; x < tilemap.width; x++ )
        for( int y = 0; y < tilemap.height; y++ )
        {
          pos.x = x;
          pos.y = y;
          string idx = ColorUtility.ToHtmlStringRGB( tilemap.GetPixel( x, y ) );
          int pindex = paletteIndex[idx];
          if( pindex == removeTileIndex )
          {
            tilemapInScene.SetTile( pos, null );
            continue;
          }
          Sprite newSprite = (Sprite)spriteTiles[paletteIndex[idx]];
          Tile tile;
          if( tiles.ContainsKey( idx ) )
            tile = tiles[idx];
          else
          {
            tile = ScriptableObject.CreateInstance<Tile>();
            tile.sprite = newSprite;
            tile.colliderType = Tile.ColliderType.Grid;
            tiles.Add( idx, tile );
          }
          //paletteObject.GetComponentInChildren<Tilemap>().SetTile( new Vector3Int( i, i % tilemap.width, 0 ), tile );
          tilemapInScene.SetTile( pos, tile );
        }

        EditorUtility.SetDirty( paletteObject );
      }
    }

    EditorGUILayout.EndScrollView();
  }

  // quad mesh dimensions
  Vector2 md;

  GameObject ReplaceObjectWithPrefabInstance( GameObject replaceThis, GameObject prefab )
  {
    GameObject go = (GameObject)PrefabUtility.InstantiatePrefab( prefab, replaceThis.transform.parent );
    go.transform.position = replaceThis.transform.position;
    go.transform.rotation = replaceThis.transform.rotation;
    SpriteRenderer sr = go.GetComponent<SpriteRenderer>();
    SpriteRenderer tfsr = replaceThis.GetComponent<SpriteRenderer>();
    if( sr != null && tfsr != null )
    {
      sr.size = tfsr.size;
      PrefabUtility.RecordPrefabInstancePropertyModifications( sr );
    }
    NavMeshObstacle gonob = go.GetComponent<NavMeshObstacle>();
    NavMeshObstacle nob = replaceThis.GetComponent<NavMeshObstacle>();
    if( gonob != null && nob != null )
    {
      gonob.size = nob.size;
      gonob.carving = nob.carving;
    }
    PrefabUtility.RecordPrefabInstancePropertyModifications( go.transform );
    DestroyImmediate( replaceThis.gameObject );
    return go;
  }

  void ProcessSingle( GameObject go )
  {
    if( ReplacePrefab && replacementPrefab != null )
      go = ReplaceObjectWithPrefabInstance( go, replacementPrefab );

    SpriteRenderer spr = go.GetComponent<SpriteRenderer>();
    if( spr != null )
    {
      if( ReplaceSprite && replacementSprite != null )
        spr.sprite = replacementSprite;
      if( replacementMaterial && replacementMaterial != null )
        spr.material = replacementMaterial;
      if( AutoFixSprite && spr.sprite != null && !spr.gameObject.name.Contains( "glyph" ) )
      {
        BoxCollider2D BoxCollider2D = spr.GetComponent<BoxCollider2D>();
        NavMeshObstacle NavMeshObstacle = spr.GetComponent<NavMeshObstacle>();
        Vector2 pivot = new Vector2( spr.sprite.pivot.x / spr.sprite.rect.width, spr.sprite.pivot.y / spr.sprite.rect.height );

        if( BoxCollider2D != null )
        {
          if( spr.gameObject.layer == LayerMask.NameToLayer( "twowayPlatform" ) )
          {
            BoxCollider2D.autoTiling = false;
            const float onewayPlatformThickness = 0.01f;
            BoxCollider2D.size = new Vector2( spr.size.x, onewayPlatformThickness );
            BoxCollider2D.offset = new Vector2( (0.5f - pivot.x) * BoxCollider2D.size.x, (0.5f - pivot.y) * BoxCollider2D.size.y );
          }
          else if( spr.gameObject.layer != LayerMask.NameToLayer( "entity" ) )
          {
            Vector2 autoTileSize = new Vector2( spr.sprite.textureRect.width / spr.sprite.pixelsPerUnit, spr.sprite.textureRect.height / spr.sprite.pixelsPerUnit );
            BoxCollider2D.size = BoxCollider2D.autoTiling ? autoTileSize : spr.size;
            BoxCollider2D.offset = new Vector2( (0.5f - pivot.x) * BoxCollider2D.size.x, (0.5f - pivot.y) * BoxCollider2D.size.y );
          }
        }

        if( NavMeshObstacle != null )
        {
          //NavMeshObstacle.carving = true;
          NavMeshObstacle.size = new Vector3( spr.size.x, spr.size.y, 1f );
          NavMeshObstacle.center = new Vector2( (0.5f - pivot.x) * NavMeshObstacle.size.x, (0.5f - pivot.y) * NavMeshObstacle.size.y );
        }
      }
    }
  }


  static void Build_MacOS()
  {
    buildMacOS = true;
    buildLinux = buildWindows = buildWebGL = false;
    Build();
  }

  static void Build()
  {
    //Application.logMessageReceived -= Global.CustomLogger;

    /*
    // Trying to get a scene build index from the build settings is hopeless.
    for( int i = 0; i < SceneManager.sceneCountInBuildSettings; i++ )
    {
      string scenepath = SceneUtility.GetScenePathByBuildIndex( i );
      if( scenepath != null )
      {
        int buildIndex = SceneManager.GetSceneByPath( scenepath ).buildIndex;
        ///
        // UNITYBUG: second scene in build settings has index of -1 !!
        ///
        if( buildIndex >= 0 )
        {
          buildnames.Add( scenepath );
          buildInfo.scenes.Add( new BuildMetaData.SceneMeta { buildIndex = buildIndex, name = Path.GetFileNameWithoutExtension( scenepath ) } );
        }
      }
    }*/

#if !DEVELOPMENT_BUILD
        System.Version v = System.Version.Parse( PlayerSettings.bundleVersion );
        PlayerSettings.bundleVersion = (new System.Version( v.Major, v.Minor, v.Build + 1 )).ToString();
#endif

    if( buildAutoSceneRefs )
      Util.AddAllScenesToBuildAndGlobalPrefab();

    BuildPlayerOptions bpo = new BuildPlayerOptions();
    List<string> buildpaths = new List<string>();
    for( int i = 0; i < EditorBuildSettings.scenes.Length; i++ )
      if( EditorBuildSettings.scenes[i].enabled )
        buildpaths.Add( EditorBuildSettings.scenes[i].path );
    bpo.scenes = buildpaths.ToArray();
    bpo.options = BuildOptions.CompressWithLz4;
    // YAML/text assets can be stripped out of a build when stripping is enabled, so disable it here.
    PlayerSettings.SetManagedStrippingLevel( BuildTargetGroup.Standalone, ManagedStrippingLevel.Disabled );

    if( buildLinux )
    {
      bpo.targetGroup = BuildTargetGroup.Standalone;
      bpo.target = BuildTarget.StandaloneLinux64;
      string outDir = Directory.GetParent( Application.dataPath ).FullName + "/build/Linux";
      outDir += "/Saga." + Util.Timestamp();
      Directory.CreateDirectory( outDir );
      bpo.locationPathName = outDir + "/" + "Saga" + ".x86_64";
      BuildPipeline.BuildPlayer( bpo );
      Debug.Log( bpo.locationPathName );
      // copy to shared folder
      string shareDir = System.Environment.GetFolderPath( System.Environment.SpecialFolder.UserProfile ) + "/SHARE";
      Util.DirectoryCopy( outDir, Path.Combine( shareDir, "Saga." + Util.Timestamp() ) );
    }
    if( buildWindows )
    {
      bpo.targetGroup = BuildTargetGroup.Standalone;
      bpo.target = BuildTarget.StandaloneWindows64;
      string outDir = Directory.GetParent( Application.dataPath ).FullName + "/build/Windows";
      outDir += "/Saga." + Util.Timestamp();
      Directory.CreateDirectory( outDir );
      bpo.locationPathName = outDir + "/" + "Saga" + ".exe";
      BuildReport report = BuildPipeline.BuildPlayer( bpo );
      Debug.Log( bpo.locationPathName );
    }
    if( buildWebGL )
    {
      bpo.targetGroup = BuildTargetGroup.WebGL;
      bpo.target = BuildTarget.WebGL;
      string outDir = Directory.GetParent( Application.dataPath ).FullName + "/build/WebGL";
      Directory.CreateDirectory( outDir );
      bpo.locationPathName = outDir + "/" + "Saga" + "." + Util.Timestamp();
      BuildPipeline.BuildPlayer( bpo );
      Debug.Log( bpo.locationPathName );
    }
    if( buildMacOS )
    {
      bpo.targetGroup = BuildTargetGroup.Standalone;
      bpo.target = BuildTarget.StandaloneOSX;
      /*PlayerSettings.SetScriptingBackend( BuildTargetGroup.Standalone, ScriptingImplementation.IL2CPP );*/
      string outDir = Directory.GetParent( Application.dataPath ).FullName + "/build/MacOS/";
      Directory.CreateDirectory( outDir );
      // the extension is replaced with ".app" by Unity
      bpo.locationPathName = outDir += "Saga" + "." + Util.Timestamp() + ".extension";
      BuildReport report = BuildPipeline.BuildPlayer( bpo );
      if( report.summary.result == BuildResult.Succeeded )
        Debug.Log( bpo.locationPathName );
      System.Diagnostics.Process.Start( "open", bpo.locationPathName );
    }
  }
}


#if false
void ClearGroundImages()
{
  string[] dirs = Directory.GetDirectories( Application.persistentDataPath );
  foreach( var dir in dirs )
  {
    Debug.Log( dir );
    string[] files = Directory.GetFiles( dir, "*.png" );
    foreach( var f in files )
    {
      File.Delete( f );
    }
  }
}

void ClearGroundOverlayImages()
{
  string[] dirs = Directory.GetDirectories( Application.persistentDataPath );
  foreach( var dir in dirs )
  {
    Debug.Log( dir );
    string[] files = Directory.GetFiles( dir, "*-dirt.png" );
    foreach( var f in files )
    {
      File.Delete( f );
    }
  }
}



// I wrote this for someone on the Unity forums
public class ProgressUpdateExample : EditorWindow
{
  [MenuItem("Tool/ProgressUpdateExample")]
  static void Init()
  {
    ProgressUpdateExample window = (ProgressUpdateExample)EditorWindow.GetWindow(typeof(ProgressUpdateExample));
    window.Show();
  }

  System.Action ProgressUpdate;
  bool processing = false;
  float progress = 0;
  int index = 0;
  int length = 0;

  List<GameObject> gos = new List<GameObject>();

  void OnGUI()
  {
    if( processing )
    {
      if( index == length )
      {
        processing = false;
        progress = 1;
        ProgressUpdate = null;
      }
      else
      {
        ProgressUpdate();
        progress = (float)index++ / (float)length;
      }
      // IMPORTANT: while processing, this call "drives" OnGUI to be called repeatedly instead of on-demand.
      Repaint();
    }

    EditorGUI.ProgressBar( EditorGUILayout.GetControlRect( false, 30 ), progress, "progress" );

    if( GUI.Button( EditorGUILayout.GetControlRect( false, 30 ), "List all Prefabs" ) )
    {
      // gather prefabs into list
      gos.Clear();
      string[] guids = AssetDatabase.FindAssets("t:prefab");
      foreach (string guid in guids)
      {
        GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>( AssetDatabase.GUIDToAssetPath( guid ) );
        gos.Add( prefab );
      }

      // initialize progress update
      length = gos.Count;
      index = 0;
      progress = 0;
      processing = true;
      ProgressUpdate = delegate() {
        GameObject go = gos[index];
        Debug.Log("prefab: " + go.name, go );
      };
    }
  }
}
#endif


#endif