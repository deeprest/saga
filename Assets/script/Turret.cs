﻿using System.Collections.Generic;
using UnityEngine;

public class Turret : Entity
{
  [Header( "Turret" )]
  public bool AutoFire;
  bool cAutoFire;
  public Vector2 AutoFireDirection;
  //static float IncrementalStartOffset;
  [SerializeField] Transform cannon;
  [SerializeField] float sightRange = 6;
  [SerializeField] float rotspeed = 20;

  public Transform shotOrigin;
  [SerializeField] WeaponDefinition WeaponDefinition;
  public Weapon weapon;

  [SerializeField] Transform sightOrigin;
  [SerializeField] float sightPulseInterval = 1;
  Timer returnToDefaultRotationTimer = new Timer();

  [SerializeField] float min = -90;
  [SerializeField] float max = 90;
  public float maxShootAngle = 5;
  public bool EnableLead;
  public bool EnableAimVariance;
  public float varianceAmount = 1;
  public bool varianceflag;

  // sight, target
  Transform targetPrev;
  Timer SightPulseTimer = new Timer();
  SortedList<float, EntityColliderPair> nearby = new SortedList<float, EntityColliderPair>();

  [SerializeField] IndexedColors indexedColors;

  protected override void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    base.OnDestroy();
    SightPulseTimer.Stop( false );
  }

  protected override void Start()
  {
    base.Start();
    UpdateLogic = UpdateTurret;
    UpdateCollision = null;
    if( indexedColors != null )
    {
      Global.instance.GetTeam( TeamFlags ).color.CopyTo( indexedColors.colors, 0 );
      indexedColors.ExplicitUpdate();
    }
    //IncrementalStartOffset = Mathf.Repeat( IncrementalStartOffset + 0.3f, 1f );
    weapon = WeaponDefinition.GetNewWeapon();
    weapon.OnAcquire( this );
  }

  void UpdateTurret()
  {
    if( !SightPulseTimer.IsActive )
    {
      nearby.Clear();
      int count = Physics2D.OverlapCircleNonAlloc( transform.position, sightRange, Global.ColliderResults, Global.EnemyInterestLayers );
      for( int i = 0; i < count; i++ )
      {
        Collider2D cld = Global.ColliderResults[i];
        Entity potentialTarget = Global.ColliderResults[i].GetComponentInParent<Entity>();
        if( potentialTarget != null && IsEnemyTeam( potentialTarget.TeamFlags ) )
        {
          float dist = Vector3.Distance( transform.position, cld.transform.position );
          if( !nearby.ContainsKey( dist ) )
            nearby.Add( dist, new EntityColliderPair { entity = potentialTarget, collider = cld } );
        }
      }
      SightPulseTimer.Start( this, sightPulseInterval );
    }

    if( cAutoFire != AutoFire )
    {
      cAutoFire = AutoFire;
      if( AutoFire )
        AutoFireOn( AutoFireDirection );
      else
        AutoFireOff();
    }

    if( AutoFire )
    {
      Vector2 local = transform.worldToLocalMatrix.MultiplyVector( AutoFireDirection );
      // prevent cannon from rotating outside of 180 degree range 
      float angle = Mathf.Clamp( Util.NormalizeAngle( Mathf.Rad2Deg * Mathf.Atan2( local.y, local.x ) - 90 ), min, max );
      cannon.localRotation = Quaternion.Euler( 0, 0, Mathf.MoveTowardsAngle( cannon.localRotation.eulerAngles.z, angle, rotspeed * Time.deltaTime ) );
      Vector2 aim = cannon.transform.up;
      if( Vector2.Angle( AutoFireDirection, aim ) < maxShootAngle )
        weapon.FireWeapon( this, shotOrigin.position, AutoFireDirection );
    }
    else
    {
      Entity visibleTarget = null;
      Vector2 targetpos;
      foreach( var pair in nearby )
      {
        if( pair.Value.collider == null )
          continue;
        targetpos = pair.Value.collider.bounds.center;
        // check line of sight to potential target
        hitCount = Physics2D.LinecastNonAlloc( sightOrigin.position, targetpos, Global.RaycastHits, Global.SightObstructionLayers );
        if( hitCount == 0 )
        {
          visibleTarget = pair.Value.entity;
          break;
        }
      }
      if( visibleTarget == null )
      {
        animator.Play( "idle" );
        if( targetPrev != null )
          returnToDefaultRotationTimer.Start( this, 4, null, null );
        targetPrev = null;
        if( !returnToDefaultRotationTimer.IsActive )
          cannon.localRotation = Quaternion.Euler( 0, 0, Mathf.MoveTowardsAngle( cannon.localRotation.eulerAngles.z, 0, rotspeed * Time.deltaTime ) );
      }
      else
      {
        targetPrev = visibleTarget.transform;
        animator.Play( "alert" );
        Vector3 tpos = visibleTarget.transform.position;
        Vector2 worldDeltaNormal = (tpos - cannon.position).normalized;

        if( EnableLead )
          tpos += (Vector3)visibleTarget.velocity * ((visibleTarget.transform.position - cannon.position).magnitude / WeaponDefinition.data.Speed);
        if( EnableAimVariance )
        {
          // have the aim position pingpong between two points near the target
          if( varianceflag )
            tpos += new Vector3( worldDeltaNormal.y, -worldDeltaNormal.x, 0 ) * varianceAmount;
          else
            tpos += new Vector3( -worldDeltaNormal.y, worldDeltaNormal.x, 0 ) * varianceAmount;
        }
        worldDeltaNormal = tpos - cannon.position;
        Vector2 local = transform.worldToLocalMatrix.MultiplyVector( worldDeltaNormal );
        // prevent cannon from rotating outside of 180 degree range
        float angle = Mathf.Clamp( Util.NormalizeAngle( Mathf.Rad2Deg * Mathf.Atan2( local.y, local.x ) - 90 ), min, max );
        cannon.localRotation = Quaternion.Euler( 0, 0, Mathf.MoveTowardsAngle( cannon.localRotation.eulerAngles.z, angle, rotspeed * Time.deltaTime ) );
        Vector2 aim = cannon.transform.up;
        /*if( target != null && visibleTarget.IsChildOf( PotentialTarget.transform ) )*/
        {
          if( Vector2.Angle( worldDeltaNormal, aim ) < maxShootAngle )
          {
            weapon.FireWeapon( this, shotOrigin.position, aim );
            varianceflag = !varianceflag;
          }
        }
      }
    }
    if( weapon != null )
      weapon.UpdateAbility();
  }


  public void AutoFireOn( Vector2 direction )
  {
    AutoFire = true;
    cAutoFire = true;
    AutoFireDirection = direction;
  }

  public void AutoFireOff()
  {
    AutoFire = false;
    cAutoFire = false;
  }

  public override Vector2 GetShotOriginPosition() { return shotOrigin.position; }
  public override Vector2 GetAimVector() { return cannon.transform.up; }
}