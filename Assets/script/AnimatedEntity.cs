﻿using UnityEngine;

// Animated carry object
public class AnimatedEntity : Entity
{
  Vector3 cachedPosition;

  protected override void Start()
  {
    base.Start();
    cachedPosition = transform.position;
  }
  
  // Must be called after animation update (LateUpdate),
  // and carried entities must update afterwards.
  public override void EntityUpdate()
  {
    Vector3 delta = transform.position - cachedPosition;
    if( delta.sqrMagnitude > 0 && Time.deltaTime > 0 )
      velocity = delta * (1f / Time.deltaTime);
    cachedPosition = transform.position;
  }
}