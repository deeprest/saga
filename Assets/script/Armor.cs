using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Armor : Entity { }
#if false
public class Armor : MonoBehaviour, IDamage
{
  public Entity parentEntity;
  public List<SpriteRenderer> spriteRenderers = new List<SpriteRenderer>();
  [EnumFlag]
  public TeamFlags TeamFlags;

  [Header( "Damage" )]
  public int Health = 3;
  public int MaxHealth = 5;
  public bool CanTakeDamage = true;
  [SerializeField] bool ContributeToHits = true;
  // ignore damage below this value
  public int DamageThreshold = 1;
  public bool ReflectIfBelowThreshold;
  // FLASH
  Timer flashTimer = new Timer();
  public float flashInterval = 0.05f;
  public int flashCount = 5;
  bool flip = false;
  readonly float flashOn = 1f;
  [Tooltip( "deal this damage on collision" )]
  public DamageDefinition contactDamageDefinition;

  [Header( "Death" )]
  public bool dead;
  public AudioClip soundDeath;
  public Entity.SpawnChance[] SpawnOnDeath;
  public UnityEvent EventDestroyed;

  void OnDestroy()
  {
    if( Global.IsQuiting )
      return;
    flashTimer.Stop( false );
    if( parentEntity != null )
    {
      Collider2D[] clds = GetComponentsInChildren<Collider2D>();
      SpriteRenderer[] srs = GetComponentsInChildren<SpriteRenderer>();
      parentEntity.Remove( clds, srs );
    }
    parentEntity = null;
    EventDestroyed?.Invoke();
  }

  public GameObject[] GetDeathSpawnObjects()
  {
    List<GameObject> gos = new List<GameObject>();
    if( SpawnOnDeath.Length > 0 )
    {
      int spawnChanceWeightTotal = 0;
      for( int i = 0; i < SpawnOnDeath.Length; i++ )
        spawnChanceWeightTotal += SpawnOnDeath[i].weight;
      int index = 0;
      int randy = Random.Range( 0, spawnChanceWeightTotal + 1 );
      int runningTotal = 0;
      for( index = 0; index < SpawnOnDeath.Length; index++ )
      {
        if( SpawnOnDeath[index].alwaysSpawn )
        {
          if( SpawnOnDeath[index].prefab != null )
            gos.Add( SpawnOnDeath[index].prefab );
          continue;
        }
        runningTotal += SpawnOnDeath[index].weight;
        if( runningTotal > randy )
        {
          if( SpawnOnDeath[index].prefab != null )
            gos.Add( SpawnOnDeath[index].prefab );
          break;
        }
      }
    }
    return gos.ToArray();
  }

  public virtual void AddHealth( int amount ) { Health = Mathf.Clamp( Health + amount, 0, MaxHealth ); }
  
  public void SetCanTakeDamage( bool value )
  {
    // This is needed for Unity Events
    CanTakeDamage = value;
  }

  public virtual void Die( Damage damage )
  {
    if( dead )
      return;
    dead = true;
    if( soundDeath != null )
      Global.instance.AudioOneShot( soundDeath, transform.position );
    if( Global.instance.explosion != null )
      Instantiate( Global.instance.explosion, transform.position, Quaternion.identity );

    GameObject[] prefab = GetDeathSpawnObjects();
    for( int i = 0; i < prefab.Length; i++ )
      Instantiate( prefab[i], transform.position, Quaternion.identity );
    Destroy( gameObject );
  }
  
  
  // public DamageResult CalculateDamageResult( Damage damage );
  // public void TakeDamage( Damage damage );
}
#endif