﻿using System.Collections.Generic;
using UnityEngine;
using Gizmos = Popcron.Gizmos;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor( typeof(PlayerController) )]
public class PlayerControllerEditor : Editor
{
  public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();
    PlayerController pc = target as PlayerController;
    if( GUI.Button( EditorGUILayout.GetControlRect(), "record" ) )
      pc.RecordBegin();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "end" ) )
      pc.RecordEnd();
    if( GUI.Button( EditorGUILayout.GetControlRect(), "play recording" ) )
      pc.PlaybackBegin();
  }
}
#endif


[System.Serializable]
public struct InputState
{
  // 1 byte
  public bool MoveLeft;
  public bool MoveRight;
  public bool MoveUp;
  public bool MoveDown;
  public bool Jump;
  public bool Dash;
  public bool Fire;
  public bool Charge;
  // 1 byte
  public bool Ability;
  public bool Interact;
  public bool NextWeapon;
  public bool NextAbility;
  public bool NOTUSED0;
  public bool NOTUSED1;
  public bool NOTUSED2;
  public bool NOTUSED3;
  // 8 byte
  public Vector2 Aim;
}


public class PlayerController : Controller
{
  Controls.BipedActionsActions BA;
  public float DirectionalCursorDistance = 3;
  // any persistent input variables
  Vector2 aimPosition = Vector2.zero;
  bool fire;
  public Vector2 aimDeltaSinceLastFrame;

  // init bindings to modify input struct
  public override void Awake()
  {
    base.Awake();
    BindControls();
    recordpath = Application.persistentDataPath + "/input.dat";
  }

  public void HACKSetSpeed( float speed )
  {
    if( pawn != null )
      pawn.OnControllerAssigned();
  }

  public override void AssignPawn( Pawn pwn )
  {
    base.AssignPawn( pwn );
    // I'm Mr. Meseeks, look at me!
    Global.instance.CameraController.LookTarget = this;
    if( pawn is PlayerBiped )
      EnableBipedControls();
  }

  public override void RemovePawn()
  {
    Global.instance.CameraController.LookTarget = null;
    pawn = null;
    Global.instance.Controls.BipedActions.Disable();
  }

  public void EnableBipedControls()
  {
    Global.instance.Controls.BipedActions.Enable();
    // reset the aim vector to avoid jumps
    aimPosition = Vector2.zero;
  }

  public void DisableBipedControls()
  {
    Global.instance.Controls.BipedActions.Disable();
  }

  private void BindControls()
  {
    BA = Global.instance.Controls.BipedActions;

    BA.NextAbility.performed += ( obj ) => input.NextAbility = true;
    BA.NextWeapon.performed += ( obj ) => input.NextWeapon = true;
    //BA.Charge.started += ( obj ) => input.Charge = true;
    BA.Interact.performed += ( obj ) => { input.Interact = true; };
    BA.Aim.performed += ( obj ) => { aimDeltaSinceLastFrame = obj.ReadValue<Vector2>(); };

    BA.Minimap.performed += ( obj ) => { Global.instance.ToggleMinimap(); };
  }

  // apply input to pawn
  public override void Update()
  {
    if( playback )
    {
      if( pawn == null || playbackIndex >= evt.Count )
        PlaybackEnd();
      else
      {
        pawn.transform.position = evt[playbackIndex].position;
        pawn.ApplyInput( evt[playbackIndex++].input );
      }
    }
    else if( pawn != null )
    {
      if( Cursor.lockState != CursorLockMode.None )
      {
        if( Global.instance.UsingGamepad )
        {
          Vector2 newposition = BA.Aim.ReadValue<Vector2>() * DirectionalCursorDistance;
          Vector2 clipped = ClipAimPositionToEdgeOfScreen( newposition );
          if( clipped.sqrMagnitude > 0.5f )
            aimPosition = clipped;
          else
            aimPosition = newposition;
        }
        else
        {
          Vector2 newposition = aimPosition + aimDeltaSinceLastFrame * Global.instance.CursorSensitivity;
          Vector2 clipped = ClipAimPositionToEdgeOfScreen( newposition );
          aimDeltaSinceLastFrame = Vector2.zero;
          // only clip to edge of screen when not close to the edge. This is to avoid letting the aim vector diminish to nothing
          // while running into the edge of the screen
          if( clipped.sqrMagnitude > 0.5f )
            aimPosition = clipped;
          else
            aimPosition = newposition;
          // keep cursor within CursorOuter radius
          if( Camera.main.orthographic )
            aimPosition = aimPosition.normalized * Mathf.Max( Mathf.Min( aimPosition.magnitude, Camera.main.orthographicSize * Camera.main.aspect * Global.instance.CursorOuter ), 0.01f );
          else
            aimPosition = aimPosition.normalized * Mathf.Max( Mathf.Min( aimPosition.magnitude, 5f * Global.instance.CursorOuter ), 0.01f );
        }
      }
      input.Aim = aimPosition;

      if( BA.MoveRight.ReadValue<float>() > 0.5f )
        input.MoveRight = true;
      if( BA.MoveLeft.ReadValue<float>() > 0.5f )
        input.MoveLeft = true;
      input.MoveDown = BA.Down.IsPressed();
      input.Jump = BA.Jump.IsPressed();
      input.Dash = BA.Dash.IsPressed();
      input.Fire = BA.Fire.IsPressed();
      input.Charge = BA.Charge.IsPressed();
      input.Ability = BA.Ability.IsPressed();

      Vector2 move = BA.Move.ReadValue<Vector2>();
      if( move.x > 0.5f )
        input.MoveRight = true;
      if( move.x < -0.5f )
        input.MoveLeft = true;
      if( move.y > 0.5f )
        input.MoveUp = true;
      if( move.y < -0.5f )
        input.MoveDown = true;

      pawn.ApplyInput( input );

      if( recording )
      {
        evt.Add( new RecordFrame
        {
          input = input,
          position = pawn.transform.position,
          timestamp = Time.unscaledTime - recordStartTime
        } );
      }
    }

    input = default;
  }

  public void FireOnlyInputMode()
  {
    Global.instance.Controls.BipedActions.Disable();
    Global.instance.Controls.BipedActions.Aim.Enable();
    Global.instance.Controls.BipedActions.Fire.Enable();
  }

  public void NormalInputMode() { Global.instance.Controls.BipedActions.Enable(); }

  public void OnPauseMenu() { Global.instance.Controls.BipedActions.Disable(); }

  public void OnUnpause() { Global.instance.Controls.BipedActions.Enable(); }

  Vector2 ClipAimPositionToEdgeOfScreen( Vector2 newPlayerLocalAimPosition )
  {
    // clip cursor to edge of screen
    Vector2 cpos = Camera.main.gameObject.transform.position;
    float chsy = Camera.main.orthographicSize;
    float chsx = Camera.main.orthographicSize * Camera.main.aspect;
    Vector2 UL = new Vector2( cpos.x - chsx, cpos.y + chsy );
    Vector2 UR = new Vector2( cpos.x + chsx, cpos.y + chsy );
    Vector2 LR = new Vector2( cpos.x + chsx, cpos.y - chsy );
    Vector2 LL = new Vector2( cpos.x - chsx, cpos.y - chsy );
    Gizmos.Line( UL, UR, Color.red );
    Gizmos.Line( UR, LR, Color.red );
    Gizmos.Line( LR, LL, Color.red );
    Gizmos.Line( LL, UL, Color.red );
    Vector2 cursorOrigin = pawn.transform.position;
    Vector2 NewAimPosition = cursorOrigin + newPlayerLocalAimPosition;
    Vector2 result = NewAimPosition;
    bool intersect = false;
    if( Util.LineSegementsIntersect( UL, UR, cursorOrigin, NewAimPosition, ref result ) )
    {
      intersect = true;
      result = UL + Util.Project2D( NewAimPosition - UL, UR - UL );
    }
    if( Util.LineSegementsIntersect( UR, LR, cursorOrigin, NewAimPosition, ref result ) )
    {
      intersect = true;
      result = UR + Util.Project2D( NewAimPosition - UR, LR - UR );
    }
    if( Util.LineSegementsIntersect( LR, LL, cursorOrigin, NewAimPosition, ref result ) )
    {
      intersect = true;
      result = LR + Util.Project2D( NewAimPosition - LR, LL - LR );
    }
    if( Util.LineSegementsIntersect( LL, UL, cursorOrigin, NewAimPosition, ref result ) )
    {
      intersect = true;
      result = LL + Util.Project2D( NewAimPosition - LL, UL - LL );
    }
    if( intersect )
    {
      Gizmos.Circle( result, .2f, Quaternion.identity, Color.red );
      Gizmos.Line( cursorOrigin, result, Color.red );
      return result - cursorOrigin;
    }
    Gizmos.Line( cursorOrigin, result, Color.cyan );
    return newPlayerLocalAimPosition;
  }

#region Record

  // todo record the initial state of all objects in the scene
  // store random seed / generation seed
  // store the scene index or sname

  // replace using frame index with timestamps. during playback, look ahead to the
  // state in the next keyframe and interpolate to it.

  // add chop drop event
  // store slo-motion begin/end events
  // store pause, menu active events (or ignore them during capture)

  struct RecordHeader
  {
    public Vector2 initialposition;
    // todo need all relevent pawn state

    /// scene ref / name to load
    /// load serialized scene state
  }

  const int HEADER_SIZE = 8;

  struct RecordFrame
  {
    public float timestamp;
    public InputState input;
    public Vector2 position;
  }

  // timestamp (4)
  // frame size = input buttons (2) + input aim (8)
  // position (8)
  const int FRAME_SIZE = 4 + (2 + 8) + 8;
  // todo is there a compile tile constant sizeof?
  //const int FRAME_SIZE = sizeof(RecordState);

  bool recording;
  bool playback;
  int playbackIndex = 0;
  float playbackStartTime;
  RecordHeader header;
  float recordStartTime;
  List<RecordFrame> evt;
  string recordpath;

  public void RecordToggle()
  {
    if( recording )
      RecordEnd();
    else
      RecordBegin();
  }

  public bool IsRecording() { return recording; }

  public void RecordBegin()
  {
    recording = true;
    evt = new List<RecordFrame>();
    header = new RecordHeader
    {
      initialposition = pawn.transform.position
    };
    recordStartTime = Time.unscaledTime;
  }

  public void RecordEnd()
  {
    recording = false;

    byte[] x;
    int p = 0;
    byte[] buffer = new byte[HEADER_SIZE + evt.Count * FRAME_SIZE];

    FileStream fs = File.Open( recordpath, FileMode.Create );
    x = System.BitConverter.GetBytes( header.initialposition.x );
    buffer[p] = x[0];
    buffer[p + 1] = x[1];
    buffer[p + 2] = x[2];
    buffer[p + 3] = x[3];
    p += 4;

    x = System.BitConverter.GetBytes( header.initialposition.y );
    buffer[p] = x[0];
    buffer[p + 1] = x[1];
    buffer[p + 2] = x[2];
    buffer[p + 3] = x[3];
    p += 4;

    for( int i = 0; i < evt.Count; i++ )
    {
      RecordFrame frame = evt[i];
      InputState input = frame.input;
      // 2 byte
      int value = (input.MoveLeft ? 0x1 : 0x0) << 0 | (input.MoveRight ? 0x1 : 0x0) << 1 | (input.MoveUp ? 0x1 : 0x0) << 2 | (input.MoveDown ? 0x1 : 0x0) << 3 | (input.Jump ? 0x1 : 0x0) << 4 | (input.Dash ? 0x1 : 0x0) << 5 | (input.Fire ? 0x1 : 0x0) << 6 | (input.Charge ? 0x1 : 0x0) << 7 | (input.Ability ? 0x1 : 0x0) << 8 | (input.Interact ? 0x1 : 0x0) << 9 | (input.NextWeapon ? 0x1 : 0x0) << 10 | (input.NextAbility ? 0x1 : 0x0) << 11 | (input.NOTUSED0 ? 0x1 : 0x0) << 12 | (input.NOTUSED0 ? 0x1 : 0x0) << 13 | (input.NOTUSED0 ? 0x1 : 0x0) << 14 | (input.NOTUSED0 ? 0x1 : 0x0) << 15;

      buffer[p] = (byte)value;
      buffer[p + 1] = (byte)(value >> 8);
      p += 2;

      x = System.BitConverter.GetBytes( input.Aim.x );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;

      x = System.BitConverter.GetBytes( input.Aim.y );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;

      // position
      x = System.BitConverter.GetBytes( frame.position.x );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;

      x = System.BitConverter.GetBytes( frame.position.y );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;

      x = System.BitConverter.GetBytes( frame.timestamp );
      buffer[p] = x[0];
      buffer[p + 1] = x[1];
      buffer[p + 2] = x[2];
      buffer[p + 3] = x[3];
      p += 4;
    }

    fs.Write( buffer, 0, buffer.Length );
    fs.Flush();
    fs.Close();
  }

  public void PlaybackToggle()
  {
    playback = !playback;
    if( playback )
      PlaybackBegin();
    else
      PlaybackEnd();
  }

  public void PlaybackBegin()
  {
    playback = true;
    playbackStartTime = Time.unscaledTime;

    evt = new List<RecordFrame>();
    playbackIndex = 0;

    byte[] buffer = File.ReadAllBytes( recordpath );
    // header
    pawn.transform.position = new Vector3( System.BitConverter.ToSingle( buffer, 0 ), System.BitConverter.ToSingle( buffer, 4 ), 0 );
    // frames
    for( int i = HEADER_SIZE; i < buffer.Length; i += FRAME_SIZE )
    {
      InputState state = new InputState
      {
        MoveLeft = ((buffer[i] >> 0) & 0x1) > 0,
        MoveRight = ((buffer[i] >> 1) & 0x1) > 0,
        MoveUp = ((buffer[i] >> 2) & 0x1) > 0,
        MoveDown = ((buffer[i] >> 3) & 0x1) > 0,
        Jump = ((buffer[i] >> 4) & 0x1) > 0,
        Dash = ((buffer[i] >> 5) & 0x1) > 0,
        Fire = ((buffer[i] >> 6) & 0x1) > 0,
        Charge = ((buffer[i] >> 7) & 0x1) > 0,

        Ability = ((buffer[i + 1] >> 0) & 0x1) > 0,
        Interact = ((buffer[i + 1] >> 1) & 0x1) > 0,
        NextWeapon = ((buffer[i + 1] >> 2) & 0x1) > 0,
        NextAbility = ((buffer[i + 1] >> 3) & 0x1) > 0,
        NOTUSED0 = ((buffer[i + 1] >> 4) & 0x1) > 0,
        NOTUSED1 = ((buffer[i + 1] >> 5) & 0x1) > 0,
        NOTUSED2 = ((buffer[i + 1] >> 6) & 0x1) > 0,
        NOTUSED3 = ((buffer[i + 1] >> 7) & 0x1) > 0,

        Aim = new Vector2( System.BitConverter.ToSingle( buffer, i + 2 ), System.BitConverter.ToSingle( buffer, i + 6 ) )
      };

      Vector2 pos = new Vector2( System.BitConverter.ToSingle( buffer, i + 10 ), System.BitConverter.ToSingle( buffer, i + 14 ) );
      float timestamp = System.BitConverter.ToSingle( buffer, i + 18 );

      evt.Add( new RecordFrame { timestamp = timestamp, input = state, position = pos } );
    }
  }

  public void PlaybackEnd() { playback = false; }

#endregion
}