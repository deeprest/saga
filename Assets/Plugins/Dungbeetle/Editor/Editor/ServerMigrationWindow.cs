using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class ServerMigrationWindow : EditorWindow {

    // Source connection
    private GUIContent sourceConnectionLabel;

    private GUIContent sourceDomainLabel;
    private GUIContent sourcePortLabel;
    private GUIContent sourcePasswordLabel;
    private GUIContent sourcePubKeyLabel;
    private string sourceDomain;
    private int sourcePort;
    private string sourcePassword;
    private string sourcePubKey;

    // Test source connection
    private GUIStyle testSourceConnectionResultStyle;
    private string testSourceConnectionResult;
    private bool sourceConnectionTested;
    private double sourceConnectionTestTime;
    private bool sourceConnectionConnected;
    private bool sourceConnectionChanged;

    private bool sourceSocketFailed;
    private string sourceSocketFailMsg;

    // Target connection
    private GUIContent targetConnectionLabel;

    private GUIContent targetDomainLabel;
    private GUIContent targetPortLabel;
    private GUIContent targetPasswordLabel;
    private GUIContent targetPubKeyLabel;
    private string targetDomain;
    private int targetPort;
    private string targetPassword;
    private string targetPubKey;

    // Test target connection
    private GUIStyle testTargetConnectionResultStyle;
    private string testTargetConnectionResult;
    private bool targetConnectionTested;
    private double targetConnectionTestTime;
    private bool targetConnectionConnected;
    private bool targetConnectionChanged;

    private bool targetSocketFailed;
    private string targetSocketFailMsg;

    // Global connection test
    private string testMigrationViabilityResult;
    private bool migrationViable;
    private double forcedConnectionTestTime;

    // Initialization
    private GUIContent initializationLabel;

    private GUIContent[] clearTargetDatabaseList;
    private GUIContent clearTargetDatabaseLabel;
    private int clearTargetDatabaseSelection;

    // Main Operation
    private GUIContent mainOperationLabel;

    private GUIContent[] relayDataList;
    private GUIContent relayDataLabel;
    private int relayDataSelection;

    // Project Merges
    private GUIContent projectMergesLabel;

    private bool fetchingSourceProjects = true;
    private bool fetchingTargetProjects = true;

    private ProjectRepresentation[] projectRepresentations;

    private int[] targetProjects;
    private GUIContent[] targetProjectGUIContents;

    private enum ProjectParticipation {
        Add,
        Merge,
        Ignore,
    }

    private class ProjectRepresentation {
        public int sourceId;
        public GUIContent name;
        public int targetSelection;
        public ProjectParticipation inclusion;

        public ProjectRepresentation(int sourceId, string name, ProjectParticipation inclusion, int targetSelection ) {
            this.sourceId = sourceId;
            this.name = new GUIContent(name);
            this.targetSelection = targetSelection;
            this.inclusion = inclusion;
        }

        public bool include => inclusion != ProjectParticipation.Ignore;
        public bool merge   => inclusion == ProjectParticipation.Merge;
    }

    private static ServerMigrationSettings.ProjectMigration[] GetProjectMigrations(ProjectRepresentation[] representations, int[] targetProjects) {
        return representations.Where(r => r.include).Select(rep =>
            new ServerMigrationSettings.ProjectMigration {
                sourceId = rep.sourceId,
                targetId = rep.merge ? targetProjects[rep.targetSelection] : -1
        }).ToArray();
    }

    // Cleanup
    private GUIContent cleanupLabel;

    private GUIContent[] clearSourceDatabaseList;
    private GUIContent clearSourceDatabaseLabel;
    private int clearSourceDatabaseSelection;

    // Execution

    private GUIContent readyToRunLabel;
    private bool readyToRun;

    private bool running;

    private bool serversRunning;
    private Thread runThread;
    private OutputBuffer runLog;
    private int repaintsNeeded;

    private class MigrationJob {

        public string sourceDomain;
        public int sourcePort;
        public string sourcePassword;
        public string sourcePubKey;
        public ServerMigrationSettings settings;
        public bool relay;
        public IOutput output;

        public MigrationJob(string sd, int sp, string pw, string pk, ServerMigrationSettings s, bool r, IOutput o) {
            sourceDomain = sd;
            sourcePort = sp;
            sourcePassword = pw;
            sourcePubKey = pk;
            settings = s;
            relay = r;
            output = o;
        }
    }

    [MenuItem ("Window/Dungbeetle/Tools/Server Migration", false, 130)]
    public static void Init () {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.ServerMigrationWindoww");
        var window = EditorWindow.GetWindow<ServerMigrationWindow>();
        if (setPos)
            window.position = new Rect(100f, 100f, 590f, 530f);
    }

    private void OnEnable () {
        /***************************************
        ** --- Source connection details --- ***
        ***************************************/

        sourceConnectionLabel = new GUIContent("Source server connection:");

        sourceDomainLabel = new GUIContent("Source server IP/Domain:");
        sourcePortLabel = new GUIContent("Source server port:");
        sourcePasswordLabel = new GUIContent("Source server developer password:");
        sourcePubKeyLabel = new GUIContent("Source server public key:");

        sourceDomain = Config.OverviewClientConfig.Domain;
        sourcePort = Config.OverviewClientConfig.Port;
        sourcePassword = Config.OverviewClientConfig.Password;
        sourcePubKey = Config.OverviewClientConfig.PubKey;

        /***************************************
        ** ---  Test source connection   --- ***
        ***************************************/

        testSourceConnectionResult = "Unknown";
        sourceConnectionTested = false;

        /***************************************
        ** --- Target connection details --- ***
        ***************************************/

        targetConnectionLabel = new GUIContent("Target server connection:");

        targetDomainLabel = new GUIContent("Target server IP/Domain:");
        targetPortLabel = new GUIContent("Target server port:");
        targetPasswordLabel = new GUIContent("Target server developer password:");
        targetPubKeyLabel = new GUIContent("Target server public key:");

        targetDomain = "127.0.0.1";
        targetPort = 15275;
        targetPassword = sourcePassword;
        targetPubKey = sourcePubKey;

        /***************************************
        ** ---  Test target connection   --- ***
        ***************************************/

        testTargetConnectionResult = "Unknown";
        targetConnectionTested = false;

        /***************************************
        ** ---   Test all connections    --- ***
        ***************************************/

        forcedConnectionTestTime = EditorApplication.timeSinceStartup + 60.0;

        /***********************************
        ** --- Clear target database --- ***
        ***********************************/

        initializationLabel = new GUIContent("Initialization:");
        clearTargetDatabaseSelection = 1;
        clearTargetDatabaseLabel = new
            GUIContent("Clear target database",
@"Clears the target database before migration.
Only do this if you really also want to delete
the existing projects on the target server.

For most use cases, select ""No"".");

        clearTargetDatabaseList = new[] {
            new GUIContent("Yes",
                           "Will remove all projects from\n" +
                           "target database before migrating."),
            new GUIContent("No",
                           "Will keep all projects in the target\n" +
                           "database and assign new project ids\n" +
                           "and project names if necessary.")
        };

        /**********************************
        ** ---    Main Operation    --- ***
        **********************************/

        mainOperationLabel = new GUIContent("Main Operation:");

        relayDataSelection = 1;
        relayDataLabel = new GUIContent("Use this machine as a proxy", "Relay the data during the migration.");
        relayDataList = new[] {new GUIContent("Yes"), new GUIContent("No")};

        /**********************************
        ** ---    Project Merges    --- ***
        **********************************/

        projectMergesLabel = new GUIContent("Project Migrations:");

        /**********************************
        ** --- Clear target database --- **
        **********************************/

        cleanupLabel = new GUIContent("Cleanup:");
        clearSourceDatabaseSelection = 1;
        clearSourceDatabaseLabel = new GUIContent("Clear source database", "Clears the source database\nafter migration succeeds.");

        clearSourceDatabaseList = new[] {
            new GUIContent("Yes", "Will clear the database on the\nsource server after migrating."),
            new GUIContent("No", "Will keep the source database.")
        };

        /***********************************
        ** --- Clear target database --- ***
        ***********************************/

        clearSourceDatabaseSelection = 1;
        clearSourceDatabaseLabel = new GUIContent("Clear source database", "Clears the source database\nafter migration succeeds.");

        clearSourceDatabaseList = new[] {
            new GUIContent("Yes", "Will clear the database on the\nsource server after migrating."),
            new GUIContent("No", "Will keep the source database.")
        };

        readyToRunLabel = new GUIContent("Check this when you're ready:");
        readyToRun = false;

        runLog = new OutputBuffer();
        runLog.Logged = Logged;

        forcedConnectionTestTime = EditorApplication.timeSinceStartup + 3.0;
    }

    private void OnGUI() {
        if (testSourceConnectionResultStyle == null)
            testSourceConnectionResultStyle = EditorStyles.boldLabel;
        if (testTargetConnectionResultStyle == null)
            testTargetConnectionResultStyle = EditorStyles.boldLabel;

        if (sourceConnectionChanged) {
            sourceConnectionChanged = false;
            testSourceConnectionResult = sourceConnectionConnected ? "Ready" : "Connection failed";
            testSourceConnectionResultStyle = sourceConnectionConnected ? DungbeetleGUIStyles.successLabel : DungbeetleGUIStyles.errorLabel;
        }
        if (targetConnectionChanged) {
            targetConnectionChanged = false;
            testTargetConnectionResult = targetConnectionConnected ? "Ready" : "Connection failed";
            testTargetConnectionResultStyle = targetConnectionConnected ? DungbeetleGUIStyles.successLabel : DungbeetleGUIStyles.errorLabel;
        }

        EditorGUIUtility.labelWidth = 230;

        GUI.enabled = !running;

        EditorGUILayout.Space();
        EditorGUILayout.LabelField(sourceConnectionLabel, DungbeetleGUIStyles.paragraphHeader);
        EditorGUI.indentLevel += 1;
        EditorGUI.BeginChangeCheck();
        sourceDomain = EditorGUILayout.TextField(sourceDomainLabel, sourceDomain);
        sourcePort = EditorGUILayout.IntField(sourcePortLabel, sourcePort);
        sourcePassword = EditorGUILayout.TextField(sourcePasswordLabel, sourcePassword);
        sourcePubKey = DungbeetleGUILayout.PubKeyField(sourcePubKeyLabel, sourcePubKey);
        if (EditorGUI.EndChangeCheck()) {
            sourceConnectionTested = false;
            sourceConnectionTestTime = EditorApplication.timeSinceStartup + 1.5;
            fetchingSourceProjects = true;
        }

        if (!sourceConnectionConnected || migrationViable)
            EditorGUILayout.LabelField(testSourceConnectionResult, testSourceConnectionResultStyle);
        if (sourceConnectionConnected && !migrationViable)
            EditorGUILayout.LabelField(testMigrationViabilityResult, DungbeetleGUIStyles.errorLabel);
        EditorGUI.indentLevel -= 1;

        EditorGUILayout.LabelField(targetConnectionLabel, DungbeetleGUIStyles.paragraphHeader);
        EditorGUI.indentLevel += 1;
        EditorGUI.BeginChangeCheck();
        targetDomain = EditorGUILayout.TextField(targetDomainLabel, targetDomain);
        targetPort = EditorGUILayout.IntField(targetPortLabel, targetPort);
        targetPassword = EditorGUILayout.TextField(targetPasswordLabel, targetPassword);
        targetPubKey = DungbeetleGUILayout.PubKeyField(targetPubKeyLabel, targetPubKey);
        if (EditorGUI.EndChangeCheck()) {
            targetConnectionTested = false;
            targetConnectionTestTime = EditorApplication.timeSinceStartup + 1.5;
            fetchingTargetProjects = true;
        }

        if (!targetConnectionConnected || migrationViable)
            EditorGUILayout.LabelField(testTargetConnectionResult, testTargetConnectionResultStyle);
        if (targetConnectionConnected && !migrationViable)
            EditorGUILayout.LabelField(testMigrationViabilityResult, DungbeetleGUIStyles.errorLabel);
        EditorGUI.indentLevel -= 1;
        EditorGUILayout.Space();

        EditorGUILayout.LabelField(initializationLabel, DungbeetleGUIStyles.paragraphHeader);
        EditorGUI.indentLevel += 1;
        clearTargetDatabaseSelection = EditorGUILayout.Popup(clearTargetDatabaseLabel, clearTargetDatabaseSelection, clearTargetDatabaseList);
        EditorGUI.indentLevel -= 1;

        EditorGUILayout.LabelField(mainOperationLabel, DungbeetleGUIStyles.paragraphHeader);
        EditorGUI.indentLevel += 1;
        EditorGUI.BeginChangeCheck();
        relayDataSelection = EditorGUILayout.Popup(relayDataLabel, relayDataSelection, relayDataList);
        if (EditorGUI.EndChangeCheck())
            targetConnectionTested = true;
        EditorGUI.indentLevel -= 1;

        EditorGUILayout.LabelField(projectMergesLabel, DungbeetleGUIStyles.paragraphHeader);
        EditorGUI.indentLevel += 1;
        var anyProjectsIncluded = false;
        foreach (var pr in projectRepresentations) {
            var mergeSelectionRect = EditorGUILayout.GetControlRect(false);

            var halfWidth = (mergeSelectionRect.width - 215f) / 2f;
            var labelRect                = new Rect(mergeSelectionRect.x,                    mergeSelectionRect.y,      215f, mergeSelectionRect.height);
            var firstMergeSelectionRect  = new Rect(mergeSelectionRect.x + 215f,             mergeSelectionRect.y, halfWidth, mergeSelectionRect.height);
            var secondMergeSelectionRect = new Rect(mergeSelectionRect.x + 215f + halfWidth, mergeSelectionRect.y, halfWidth, mergeSelectionRect.height);

            EditorGUI.LabelField(labelRect, pr.name);
            pr.inclusion = (ProjectParticipation) EditorGUI.EnumPopup(firstMergeSelectionRect, pr.inclusion);
            if (pr.merge)
                pr.targetSelection = EditorGUI.Popup(secondMergeSelectionRect, pr.targetSelection, targetProjectGUIContents);

            if (pr.include)
                anyProjectsIncluded = true;
        }

        EditorGUI.indentLevel -= 1;


        EditorGUILayout.LabelField(cleanupLabel, DungbeetleGUIStyles.paragraphHeader);
        EditorGUI.indentLevel += 1;
        clearSourceDatabaseSelection = EditorGUILayout.Popup(clearSourceDatabaseLabel, clearSourceDatabaseSelection, clearSourceDatabaseList);
        EditorGUI.indentLevel -= 1;

        EditorGUILayout.Space();
        if (!running) {
            var connectionsPrepared = sourceConnectionConnected && targetConnectionConnected && sourceConnectionTested && targetConnectionTested && migrationViable;
            GUI.enabled = connectionsPrepared && anyProjectsIncluded;
            readyToRun = EditorGUILayout.Toggle(readyToRunLabel, readyToRun && connectionsPrepared);
            GUI.enabled = readyToRun;
            if (GUILayout.Button("Run"))
                running = true;
        }
        else {
            EditorGUILayout.Toggle(readyToRunLabel, true);
            GUILayout.Button("Run");
            GUI.enabled = true;
            EditorGUILayout.Space();
            EditorGUILayout.TextArea(runLog.ToString());
        }

        EditorGUIUtility.labelWidth = 0;
    }

    private static IOutput unityDebug = new UnityDebug();

    private void Update() {
        if (Interlocked.CompareExchange(ref repaintsNeeded, 0, 1) == 1)
            Repaint();

        if (forcedConnectionTestTime < EditorApplication.timeSinceStartup) {
            forcedConnectionTestTime = EditorApplication.timeSinceStartup + 60.0;
            sourceConnectionTested = false;
            targetConnectionTested = false;
        }

        if (!sourceConnectionTested && sourceConnectionTestTime < EditorApplication.timeSinceStartup) {
            sourceConnectionTested = true;
            var connected = BugUtility.GetPing(sourceDomain, sourcePort, sourcePassword, sourcePubKey, ref sourceSocketFailed, ref sourceSocketFailMsg, unityDebug);

            if (connected)
                UpdateMigrationViability();
            sourceConnectionChanged = connected != sourceConnectionConnected;
            sourceConnectionConnected = connected;

            if (sourceConnectionChanged || fetchingSourceProjects) {
                fetchingSourceProjects = false;
                if (connected) {
                    var projects = BugUtility.GetProjects(false, sourceDomain, sourcePort, sourcePassword, sourcePubKey, ref sourceSocketFailed, ref sourceSocketFailMsg, unityDebug);
                    projectRepresentations = projects.Select(p => new ProjectRepresentation(p.id, p.name, ProjectParticipation.Ignore, 0)).ToArray();
                }
                else {
                    projectRepresentations = new ProjectRepresentation[0];
                }
            }

            Repaint();
        }

        if (!targetConnectionTested && targetConnectionTestTime < EditorApplication.timeSinceStartup) {
            targetConnectionTested = true;
            bool connected;
            if (relayDataSelection == 0)
                connected = BugUtility.GetPing(targetDomain, targetPort, targetPassword, targetPubKey, ref targetSocketFailed, ref targetSocketFailMsg, unityDebug);
            else //relayDataSelection == 1
                connected = BugUtility.GetPingability(sourceDomain, sourcePort, sourcePassword, sourcePubKey, targetDomain, targetPort, targetPassword,
                                                      targetPubKey, ref sourceSocketFailed, ref sourceSocketFailMsg, unityDebug);

            if (sourceConnectionConnected)
                UpdateMigrationViability();
            targetConnectionChanged = connected != targetConnectionConnected;
            targetConnectionConnected = connected;

            if (targetConnectionChanged || fetchingTargetProjects) {
                fetchingTargetProjects = false;
                if (connected) {
                    var fetchedProjects = BugUtility.GetProjects(false, targetDomain, targetPort, targetPassword, targetPubKey, ref targetSocketFailed, ref targetSocketFailMsg, unityDebug);
                    targetProjectGUIContents = new GUIContent[fetchedProjects.Count];
                    targetProjects = new int[fetchedProjects.Count];

                    for (int i = 0; i < fetchedProjects.Count; i++) {
                        var p = fetchedProjects[i];
                        targetProjectGUIContents[i] = new GUIContent(p.name);
                        targetProjects[i] = p.id;
                    }
                }
                else {
                    targetProjectGUIContents = new GUIContent[0];
                    targetProjects = new int[0];
                }
                foreach(var pr in projectRepresentations)
                    if (pr.merge && pr.targetSelection >= targetProjects.Length)
                        pr.targetSelection = 0;
            }

            Repaint();
        }

        if (running && !serversRunning) {
            serversRunning = true;
            var settings = new ServerMigrationSettings {
                targetDomain = targetDomain,
                targetPort = targetPort,
                targetPassword = targetPassword,
                targetPubKey = targetPubKey,
                projects = GetProjectMigrations(projectRepresentations, targetProjects),
                clearTargetDatabase = clearTargetDatabaseSelection == 0,
                clearSourceDatabase = clearSourceDatabaseSelection == 0
            };
            var relayData = relayDataSelection == 0;
            runThread = new Thread(StartRun);
            runThread.Start(new MigrationJob(sourceDomain, sourcePort, sourcePassword, sourcePubKey, settings, relayData, runLog));
        }
    }

    private void UpdateMigrationViability() {
        testMigrationViabilityResult = "Migration test: Unknown result";
        migrationViable = BugUtility.CheckTargetMigrationViability(sourceDomain, sourcePort, sourcePassword, sourcePubKey, targetDomain, targetPort, out testMigrationViabilityResult, ref sourceSocketFailed, ref sourceSocketFailMsg, unityDebug);
    }

    private void OnDestroy() {
        if (runThread != null) {
            runThread.Abort();
            while (runThread.IsAlive)
                Thread.Sleep(10);
            runThread.Join();
        }
    }

    private void Logged() {
        repaintsNeeded = 1;
    }

    private static void StartRun(object job) {
        var m = job as MigrationJob;

        bool   migrationSocketFailed = false;
        string migrationSocketFailMsg = null;
        if (m.relay)
            BugUtility.MigrateReturn(m.sourceDomain, m.sourcePort, m.sourcePassword, m.sourcePubKey, m.settings, m.output, ref migrationSocketFailed,
                                     ref migrationSocketFailMsg);
        else
            BugUtility.MigrateSendDirect(m.sourceDomain, m.sourcePort, m.sourcePassword, m.sourcePubKey, m.settings, m.output, ref migrationSocketFailed,
                                         ref migrationSocketFailMsg);
    }
}
}
