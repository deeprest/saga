using System;
using System.CodeDom.Compiler;
using System.IO;

using UnityEngine;
using UnityEditor;

using Linq = System.Linq.Enumerable;

namespace Dungbeetle {
public class ServerCompilationWindow : EditorWindow {

    [MenuItem ("Window/Dungbeetle/Tools/Build DLL or Server/Standalone Server", false, 120)]
    static void Init() {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.ServerCompilationWindoww");

        var window = EditorWindow.GetWindow <ServerCompilationWindow>();
        if (setPos)
            window.position = new Rect(100f, 100f, 320f, 240f);
        window.Show();
    }

    [MenuItem ("Window/Dungbeetle/Tools/Build DLL or Server/Standalone Server", true)]
    static bool SourceCodeAvailable() {
#if DUNGBEETLE_DLL
        return false;
#else
        return true;
#endif
    }

    private string resultString;

    void OnGUI() {
        if (GUILayout.Button("Compile"))
            Compile(ref resultString, GetPath);
        if (!string.IsNullOrEmpty(resultString))
            EditorGUILayout.TextArea(resultString);
    }

    static public string GetPath() {
        return EditorUtility.SaveFolderPanel("Select a folder to store server", System.Environment.GetEnvironmentVariable("USERPROFILE"), "");
    }

    static public void Compile (ref string resultStr, Func<string> getPath) {

        var serverSourceFileNames = new[] {
          "Assets/Plugins/Dungbeetle/Editor/Core/Bug.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/Server_Main_for_standalone.cs.txt",
          "Assets/Plugins/Dungbeetle/Editor/Core/ConfigField_for_standalone.cs.txt",
          "Assets/Plugins/Dungbeetle/Editor/Core/CustomConnection.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/DungbeetleDBBase.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/IDatabaseLink.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ImageFile.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/MySQLDatabaseLink.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/MySQLDatabaseLinkBase.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/Output.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/DungbeetlePath.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/Project.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ProjectDeveloper.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ProtocolAttribute.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/Server.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ServerService.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ServerServiceInstaller.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ServerArguments.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ServerMigrationSettings.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ServerPreferences.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/ServerStatus.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/SQLiteDatabaseLink.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/SQLiteDatabaseLinkBase.cs",
          "Assets/Plugins/Dungbeetle/Editor/Core/SymKeyCache.cs"
        };

        var sharedSourceFileNames = new[] {
          "Assets/Plugins/Dungbeetle/Scripts/Config/PasswordAttribute_for_standalone.cs.txt",
          "Assets/Plugins/Dungbeetle/Scripts/Config/PasswordField.cs",
          "Assets/Plugins/Dungbeetle/Scripts/Config/PubKeyAttribute_for_standalone.cs.txt",
          "Assets/Plugins/Dungbeetle/Scripts/Config/PubKeyField.cs",
          "Assets/Plugins/Dungbeetle/Scripts/PrivilegeLevel.cs",
          "Assets/Plugins/Dungbeetle/Scripts/BinaryWriterExtended.cs",
          "Assets/Plugins/Dungbeetle/Scripts/BugReport.cs",
          "Assets/Plugins/Dungbeetle/Scripts/CryptoStreamManager.cs",
          "Assets/Plugins/Dungbeetle/Scripts/DungbeetleNetwork.cs",
          "Assets/Plugins/Dungbeetle/Scripts/DungbeetleNetworkFactory.cs",
          "Assets/Plugins/Dungbeetle/Scripts/DungbeetleTcpClient.cs",
          "Assets/Plugins/Dungbeetle/Scripts/IOutput.cs",
          "Assets/Plugins/Dungbeetle/Scripts/ProtocolException.cs",
        };

        var destinationPath = getPath();

        if (!string.IsNullOrEmpty(destinationPath)) {
            var prefix = destinationPath + "/";

            if (Application.platform == RuntimePlatform.OSXEditor) {
            }
            else if (Application.platform == RuntimePlatform.WindowsEditor) {
                File.Copy("Assets/Plugins/Dungbeetle/Editor/SQLite/Win_x64/sqlite3.dll", $"{prefix}sqlite3.dll", true);
            }
            else if (Application.isEditor) { // Assume linux
            }

            File.Copy("Assets/Plugins/YamlDotNet/YamlDotNet.dll",                            $"{prefix}YamlDotNet.dll",         true);
            File.Copy("Assets/Plugins/Dungbeetle/Editor/MySql.Data.dll",                     $"{prefix}MySql.Data.dll",         true);
            File.Copy("Assets/Plugins/Dungbeetle/Editor/SQLite/System.Data.SQLite.dll",      $"{prefix}System.Data.SQLite.dll", true);

            ServerPreferences.SaveToFile($"{prefix}{DungbeetlePath.DefaultPreferencesFileName}",
              Config.Defaults.Port.port,
              Config.Defaults.ReporterPassword.password,
              Config.Defaults.DeveloperPassword.password,
              Config.Defaults.PrivKey.key);


            CompilerParameters cp = new CompilerParameters();
            cp.GenerateExecutable = true;
            cp.OutputAssembly = $"{prefix}DungbeetleServer.exe";
            cp.GenerateInMemory = false;
            cp.CompilerOptions = "/optimize";
            cp.MainClass = "Server_Main";

            cp.ReferencedAssemblies.Add( "Assets/Plugins/Dungbeetle/Editor/MySql.Data.dll" );
            cp.ReferencedAssemblies.Add( "Assets/Plugins/Dungbeetle/Editor/SQLite/System.Data.SQLite.dll" );
            cp.ReferencedAssemblies.Add( "Assets/Plugins/YamlDotNet/YamlDotNet.dll" );
            cp.ReferencedAssemblies.Add( "System.ServiceProcess.dll" );
            cp.ReferencedAssemblies.Add( "System.Data.dll" );
            cp.ReferencedAssemblies.Add( "System.Core.dll" );
            cp.ReferencedAssemblies.Add( "System.dll" );

            cp.CompilerOptions = "-d:DUNGBEETLE_SERVER_STANDALONE /optimize";

            var cr = new Microsoft.CSharp.CSharpCodeProvider().CompileAssemblyFromFile(cp, Linq.ToArray(Linq.Concat(serverSourceFileNames, sharedSourceFileNames)));

            if (cr.Errors.Count > 0) {
                var errorStringBuilder = new System.Text.StringBuilder();
                foreach (var error in cr.Errors)
                    errorStringBuilder.AppendLine(error.ToString());
                resultStr = errorStringBuilder.ToString();
            }
            else {
                EditorUtility.RevealInFinder(prefix);
                resultStr = "Success!";
            }
        }
    }
}
}
