using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class BugDetailsWindow : EditorWindow {

    private BugDetailsGUI gui;

    private BugOverviewWindow _parentOverview;
    public BugOverviewWindow parentOverview {
        get {
            return _parentOverview;
        }
        set {
            _parentOverview = value;
            LoadGUI();
        }
    }

    public static BugDetailsWindow Init(Bug b, BugOverviewWindow ov) {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.BugDetailsWindoww");
        var window = GetWindow<BugDetailsWindow>($"Details {b.id}", true);
        if (setPos)
            window.position = new Rect(100f, 100f, 450f, 680f);

        var new_gui = new BugDetailsGUI();
        new_gui.SetBug(b, true);

        window.gui = new_gui;
        window.parentOverview = ov;

        return window;
    }

    private void OnEnable() {
        LoadGUI();
    }

    private void LoadGUI() {
        if (gui != null)
            gui.SetParentOverview(_parentOverview);
    }

    private bool shuttingDown;

    private void OnGUI() {
        if (gui != null && !gui.closed)
            gui.OnGUI();
        else
            CloseDown();
    }

    private void CloseDown() {
        if (!shuttingDown) {
            shuttingDown = true;
            EditorApplication.delayCall += Close;
        }
    }

    public void OnBugChanged(Bug bug) {
        if (gui != null) {
            gui.OnBugChanged(bug);
            RepaintIfNeeded();
        }
    }

    public void OnBugDeleted(Bug b) {
        if (gui != null) {
            gui.OnBugDeleted(b);
            if (gui.closed)
                CloseDown();
        }
    }

    private void RepaintIfNeeded() {
        if (gui.needRepaint) {
            gui.needRepaint = false;
            Repaint();
        }
    }

}
}