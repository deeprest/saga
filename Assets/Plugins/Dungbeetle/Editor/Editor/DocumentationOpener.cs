using System.IO;
using UnityEngine;
using UnityEditor;

public static class DocumentationOpener {
    private const string indexPath  = "Assets/Dungbeetle/.Documentation/introduction.htm";
    private const string indexPath2 = "Assets/Dungbeetle/Documentation/.Documentation/introduction.htm";

    [MenuItem("Help/Dungbeetle Documentation", false, 50)]
    public static void OpenDocs() {
        if (File.Exists(indexPath))
            Application.OpenURL(Path.GetFullPath(indexPath));
        else if (File.Exists(indexPath2))
            Application.OpenURL(Path.GetFullPath(indexPath2));
        else
            EditorUtility.DisplayDialog("Documentation not found",
                                        "Before you can use the documentation, you must unzip the file 'Assets/Dungbeetle/Documentation.zip'", "Ok");
    }

    [MenuItem("Window/Dungbeetle/Documentation", false, 200)]
    public static void OpenDocs2() {
        OpenDocs();
    }
}