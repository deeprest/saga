using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

[CustomPropertyDrawer(typeof(PubKeyAttribute))]
public class PubKeyFieldDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);

        EditorGUI.BeginChangeCheck();
        var newKey = DungbeetleGUI.PubKeyField(position, label, property.stringValue);
        if (EditorGUI.EndChangeCheck()) {
            property.stringValue = newKey;
            EditorServer.ClearCookies();
            BugUtility.InvalidateNetworkDetails();
        }

        EditorGUI.EndProperty();
    }
}
}