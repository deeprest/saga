using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public static class ConfigWindowStyles {
    private static GUIContent _projectLabel;
    public static GUIContent ProjectLabel {
        get {
            return _projectLabel != null ? _projectLabel : (_projectLabel = new GUIContent("Project:"));
        }
    }

    private static GUIContent _developersLabel;
    public static GUIContent DevelopersLabel {
        get {
            return _developersLabel != null ? _developersLabel : (_developersLabel = new GUIContent("Developers:"));
        }
    }

    private static GUIStyle _categoryLabel;
    public static GUIStyle CategoryLabel {
        get {
            return _categoryLabel != null ? _categoryLabel : (_categoryLabel = categoryLabel());
        }
    }

    private static GUIStyle _developerField;
    public static GUIStyle DeveloperField {
        get {
            return _developerField != null ? _developerField : (_developerField = developerField());
        }
    }

    private static GUIStyle _categoryBG;
    public static GUIStyle CategoryBG {
        get {
            return _categoryBG != null ? _categoryBG : (_categoryBG = new GUIStyle("CN EntryBackEven"));
        }
    }

    private static GUIStyle _categoryScrollView;
    public static GUIStyle CategoryScrollView {
        get {
            return _categoryScrollView != null ? _categoryScrollView : (_categoryScrollView = new GUIStyle("PreferencesSectionBox"));
        }
    }

    private static GUIStyle _headerLabel;
    public static GUIStyle HeaderLabel {
        get {
            return _headerLabel != null ? _headerLabel : (_headerLabel = headerLabel());
        }
    }

    private static GUIStyle _projectHeader;
    public static GUIStyle ProjectHeader {
        get {
            return _projectHeader != null ? _projectHeader : (_projectHeader = projectHeader());
        }
    }

    private static GUIStyle _header;
    public static GUIStyle Header {
        get {
            return _header != null ? _header : (_header = header());
        }
    }

    private static string blackSuffix = EditorGUIUtility.isProSkin ? string.Empty : "_black";
    public static Texture2D FieldDefaultIcon = fieldDefaultIcon();
    public static Texture2D FieldOverrideIcon = fieldOverrideIcon();

    public static GUIContent FieldGUIContent(string text , ConfigWindow.FieldType type) {
        if (type == ConfigWindow.FieldType.Default)
            return new GUIContent(text, FieldOverrideIcon);
        else if (type == ConfigWindow.FieldType.Inherit)
            return new GUIContent(text, FieldDefaultIcon, "Value is inherited from the default page");
        else // if (type == ConfigWindow.FieldType.Override)
            return new GUIContent(text, FieldOverrideIcon, "Overrides any default value for this field");
    }

    private static Texture2D fieldDefaultIcon() {
        return AssetDatabase.LoadAssetAtPath<Texture2D>($"Assets/Plugins/Dungbeetle/Editor/Icons/InheritDefault12{blackSuffix}.png");
    }

    private static Texture2D fieldOverrideIcon() {
        return AssetDatabase.LoadAssetAtPath<Texture2D>($"Assets/Plugins/Dungbeetle/Editor/Icons/Override12{blackSuffix}.png");
    }

    private static GUIStyle categoryLabel() {
        var result = new GUIStyle("PreferencesSection");
        result.onNormal.textColor = Color.white;
        return result;
    }

    private static GUIStyle developerField() {
        var result = new GUIStyle(EditorStyles.textField);
        result.margin.left = 0;
        return result;
    }

    private static GUIStyle headerLabel() {
        var result = new GUIStyle(EditorStyles.label);
        result.fontSize = 18;
        result.margin.top = 10;
        result.fixedHeight = 24;
        return result;
    }

    private static GUIStyle projectHeader() {
        var result = new GUIStyle(EditorStyles.textField);
        result.fontStyle = FontStyle.Bold;
        result.fontSize = 18;
        result.margin.top = 10;
        return result;
    }

    private static GUIStyle header() {
        var result = new GUIStyle(EditorStyles.largeLabel);
        result.fontStyle = FontStyle.Bold;
        result.fontSize = 18;
        result.margin.top = 10;
        return result;
    }
}
}
