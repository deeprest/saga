using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

namespace Dungbeetle {

public class ConfigWindow : EditorWindow {

    [MenuItem("Window/Dungbeetle/Connection", false, 101)]
    public static void CreateWindow() {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.ConfigWindoww");
        var window = GetWindow<ConfigWindow>(true, "Connection");
        if (setPos)
            window.position = new Rect(100f, 100f, 800f, 180f);
        // ConfigWindow.instance.Init();
        window.minSize = new Vector2(600f, 180f);
        window.Show();
    }

    public static void RepaintAll() {
        var windows = Resources.FindObjectsOfTypeAll<ConfigWindow>();
        foreach (var window in windows)
            window.Repaint();
    }

    public enum FieldType {
        Default,
        Inherit,
        Override,
    }

    private class ConfigWindowCategory {
        public string name;
        public Action drawAction;
        public ConfigWindowCategory(string n, Action a) {
            name = n;
            drawAction = a;
        }
    }

    private List<ConfigWindowCategory> categories;

    [SerializeField]
    private int selectedIndex;
    [SerializeField]
    private Vector2 scrollPos;

    private class ConfigProperty {
        protected SerializedObject serializedObject;
        protected SerializedProperty serializedProperty;
        protected GUIContent guiContent;

        protected ConfigProperty(ScriptableObject target, string fieldName)
            : this(target, fieldName, ObjectNames.NicifyVariableName(fieldName)) {}

        protected ConfigProperty(ScriptableObject target, string fieldName, string displayName)
            : this (target, fieldName, displayName, FieldType.Default) { }

        protected ConfigProperty(ScriptableObject target, string fieldName, string displayName, FieldType fieldType) {
            serializedObject   = new SerializedObject(target);
            serializedProperty = serializedObject.FindProperty(fieldName);
            guiContent         = ConfigWindowStyles.FieldGUIContent(displayName, fieldType);
        }

        public void Update() {
            serializedObject.Update();
        }

        public virtual void OnGUI() {
            EditorGUILayout.PropertyField(serializedProperty, guiContent);
        }

        public void ApplyModifiedProperties() {
            serializedObject.ApplyModifiedProperties();
        }

        public static implicit operator SerializedProperty(ConfigProperty p) {
            return p.serializedProperty;
        }

        public static implicit operator SerializedObject(ConfigProperty p) {
            return p.serializedObject;
        }

        public static implicit operator GUIContent(ConfigProperty p) {
            return p.guiContent;
        }
    }

    private class ConfigDefaultProperty : ConfigProperty {
        public ConfigDefaultProperty(ScriptableObject target, string fieldName)
            : base(target, fieldName) { }

        public ConfigDefaultProperty(ScriptableObject target, string fieldName, string displayName)
            : base(target, fieldName, displayName) { }
    }

    private class ConfigOverridableProperty : ConfigProperty {
        SerializedObject   parentSerializedObject;
        SerializedProperty parentProperty;
        ScriptableObject   defaultTarget;
        string             customObjectName;
        string             fieldName;
        bool               useDefault;

        private GUIContent noLabelContent = GUIContent.none;

        private GUIStyle _labelStyle;
        private GUIStyle labelStyle {
            get {
                if (_labelStyle == null)
                    _labelStyle = useDefault ? EditorStyles.label : EditorStyles.boldLabel;
                return _labelStyle;
            }
        }

        public ConfigOverridableProperty(SerializedObject parent, string propName, ScriptableObject @default, string fn, string objectNamePrefix) :
            this(parent, propName, @default, fn, objectNamePrefix, ObjectNames.NicifyVariableName(fn)) {}

        public ConfigOverridableProperty (SerializedObject parent, string propName, ScriptableObject @default, string fn, string objectNamePrefix, string displayName) :
            base((ScriptableObject) parent.FindProperty(propName).objectReferenceValue,
                 fn,
                 displayName,
                 parent.FindProperty(propName).objectReferenceValue == @default ? FieldType.Inherit : FieldType.Override
            )
        {
            var parentProp = parent.FindProperty(propName);
            var target = parentProp.objectReferenceValue;

            var _useDefault = target == @default;

            Init(parent, parentProp, @default, displayName, fn, objectNamePrefix, _useDefault);
        }

        private void Init(SerializedObject parent, SerializedProperty parentProp, ScriptableObject @default, string displayName, string fn,
                          string objectNamePrefix, bool _useDefault) {
            parentSerializedObject = parent;
            parentProperty = parentProp;
            defaultTarget = @default;
            customObjectName = $"{objectNamePrefix} {displayName}";
            fieldName = fn;
            useDefault = _useDefault;
        }

        public override void OnGUI() {
            if (useDefault) {
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(serializedProperty, guiContent);
                if (EditorGUI.EndChangeCheck()) {
                    useDefault = false;
                    var newTarget = Config.GetNewField(customObjectName, defaultTarget);
                    var newSerializedObject = new SerializedObject(newTarget);
                    newSerializedObject.CopyFromSerializedProperty(serializedProperty);
                    var newSerializedProperty = newSerializedObject.FindProperty(fieldName);

                    serializedObject.Update(); // Get rid of the change - don't apply to the default

                    parentProperty.objectReferenceValue = newTarget;
                    parentSerializedObject.ApplyModifiedProperties();

                    serializedObject   = newSerializedObject;
                    serializedProperty = newSerializedProperty;
                    guiContent         = ConfigWindowStyles.FieldGUIContent(guiContent.text, FieldType.Override);
                    GUIUtility.ExitGUI();
                }
            }
            else {
                GUILayout.BeginHorizontal();

                var rect = EditorGUILayout.GetControlRect();
                rect = EditorGUI.PrefixLabel(rect, guiContent, labelStyle);
                EditorGUI.PropertyField(rect, this, noLabelContent);

                var resetRect = GUILayoutUtility.GetRect(0f, EditorGUIUtility.singleLineHeight, EditorStyles.miniButton, GUILayout.Height(16f),
                                                         GUILayout.Width(50f));
                var reset = GUI.Button(resetRect, "Reset");
                GUILayout.EndHorizontal();

                if (reset && EditorUtility.DisplayDialog($"Reset field", "Field: {customObjectName}\n\nThe value from the default page will be used instead.", "Reset", "Cancel")) {
                    useDefault = true;

                    Config.DeleteField((ScriptableObject) parentProperty.objectReferenceValue);

                    serializedObject   = new SerializedObject(defaultTarget);
                    serializedProperty = serializedObject.FindProperty(fieldName);
                    guiContent         = ConfigWindowStyles.FieldGUIContent(guiContent.text, FieldType.Inherit);

                    parentProperty.objectReferenceValue = defaultTarget;
                    parentSerializedObject.ApplyModifiedProperties();
                    GUIUtility.ExitGUI();
                }
            }
        }

        public static implicit operator SerializedProperty(ConfigOverridableProperty p) {
            return p.serializedProperty;
        }

        public static implicit operator SerializedObject(ConfigOverridableProperty p) {
            return p.serializedObject;
        }

        public static implicit operator GUIContent(ConfigOverridableProperty p) {
            return p.guiContent;
        }
    }

    private class ConfigDynamicBoolProperty : ConfigProperty {
        public Action<bool> action;

        public ConfigDynamicBoolProperty(ScriptableObject target, string fieldName, Action<bool> _action)
            : base (target, fieldName) {
            Init(_action);
        }

        public ConfigDynamicBoolProperty(ScriptableObject target, string fieldName, string displayName, Action<bool> _action)
            : base(target, fieldName, displayName) {
            Init(_action);
        }

        private void Init(Action<bool> _action) {
            action = _action;
        }

        public override void OnGUI() {
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(serializedProperty, guiContent);
            if (EditorGUI.EndChangeCheck())
                action(serializedProperty.boolValue);
        }
    }

    private class ConfigConnectionDefaultProperty : ConfigDefaultProperty {
        private Action action;

        public ConfigConnectionDefaultProperty(ScriptableObject target, string fieldName, Action _action) : base(target, fieldName) {
            Init(_action);
        }

        private void Init(Action _action) {
            action = _action;
        }

        public override void OnGUI() {
            var oRef = serializedObject.targetObject;
            var cc = Config.OverviewClientConfig;
            var analyze = oRef == cc.Domain || oRef == cc.Port; // If we're changing the ports or domain currently being used to connect
            if (analyze)
                EditorGUI.BeginChangeCheck();
            base.OnGUI();
            if (analyze && EditorGUI.EndChangeCheck())
                action();
        }
    }

    private class ConfigConnectionOverridableProperty : ConfigOverridableProperty {
        private Action action;

        public ConfigConnectionOverridableProperty(SerializedObject parent, string propName, ScriptableObject @default, string fn, string objectNamePrefix, Action _action)
            : base(parent, propName, @default, fn, objectNamePrefix) {
            Init(_action);
        }

        private void Init(Action _action) {
            action = _action;
        }

        public override void OnGUI() {
            EditorGUI.BeginChangeCheck(); // Any change will override the ports/domain used to connect, no need to check the reference. See ConfigConnectionDefaultProperty.OnGUI
            base.OnGUI();
            if (EditorGUI.EndChangeCheck())
                action();
        }
    }

    private List<ConfigProperty> defaultProperties      = new List<ConfigProperty>();
    private List<ConfigProperty> reporterProperties     = new List<ConfigProperty>();
    private List<ConfigProperty> overviewProperties     = new List<ConfigProperty>();
    private List<ConfigProperty> editorServerProperties = new List<ConfigProperty>();

    private void OnEnable() {
        ProjectIdAttributeDrawer.projectsNeedRefresh = true;
        ProjectIdAttributeDrawer.serverHasChanged = false;

        categories = new List<ConfigWindowCategory> {
            new ConfigWindowCategory("Default", DrawDefaultProperties),
            new ConfigWindowCategory("Game Builds", DrawReporterProperties),
            new ConfigWindowCategory("Editor Windows", DrawOverviewProperties),
            new ConfigWindowCategory("Editor Server", DrawEditorServerProperties)
        };

        var d = Config.Defaults;
        defaultProperties = new List<ConfigProperty>(8) {
            new ConfigConnectionDefaultProperty(d.Domain, "domain", RetryNetwork),
            new ConfigConnectionDefaultProperty(d.Port, "port", RetryNetwork),
            new ConfigDefaultProperty          (d.PubKey, "key", "Public Key"),
            new ConfigDefaultProperty          (d.PrivKey, "key", "Private Key"),
            new ConfigDefaultProperty          (d.ReporterPassword, "password", "Reporter Password"),
            new ConfigDefaultProperty          (d.DeveloperPassword, "password", "Developer Password"),
            new ConfigDefaultProperty          (d.ProjectId, "projectId", "Project ID")
        };

        var rConfig = new SerializedObject(Config.ReporterClientConfig);
        reporterProperties = new List<ConfigProperty>(5) {
            new ConfigOverridableProperty(rConfig, "Domain", d.Domain, "domain", "Reporter"),
            new ConfigOverridableProperty(rConfig, "Port", d.Port, "port", "Reporter"),
            new ConfigOverridableProperty(rConfig, "PubKey", d.PubKey, "key", "Reporter", "Public Key"),
            new ConfigOverridableProperty(rConfig, "Password", d.ReporterPassword, "password", "Reporter", "Reporter Password"),
            new ConfigOverridableProperty(rConfig, "ProjectId", d.ProjectId, "projectId", "Reporter", "Project ID")
        };

        var dConfig = new SerializedObject(Config.OverviewClientConfig);
        overviewProperties = new List<ConfigProperty>(5) {
            new ConfigConnectionOverridableProperty(dConfig, "Domain", d.Domain, "domain", "Overview", RetryNetwork),
            new ConfigConnectionOverridableProperty(dConfig, "Port", d.Port, "port", "Overview", RetryNetwork),
            new ConfigOverridableProperty(dConfig, "PubKey", d.PubKey, "key", "Overview", "Public Key"),
            new ConfigOverridableProperty(dConfig, "Password", d.DeveloperPassword, "password", "Overview", "Developer Password"),
            new ConfigOverridableProperty(dConfig, "ProjectId", d.ProjectId, "projectId", "Overview", "Project ID")
        };

        var esConfig = new SerializedObject(Config.EditorServerConfig);
        editorServerProperties = new List<ConfigProperty>(5) {
            new ConfigOverridableProperty(esConfig, "Port", d.Port, "port", "Editor Server"),
            new ConfigOverridableProperty(esConfig, "PrivKey", d.PrivKey, "key", "Editor Server", "Private Key"),
            new ConfigDynamicBoolProperty(d.EditorServerEnabled, "value", "Editor Server Enabled", ControlEditorServer)
        };
    }

    private void DrawDefaultProperties() {
        DrawProperties(defaultProperties);
    }

    private void DrawReporterProperties() {
        DrawProperties(reporterProperties);
    }

    private void DrawOverviewProperties() {
        DrawProperties(overviewProperties);
    }

    private void DrawEditorServerProperties() {
        DrawProperties(editorServerProperties);
    }

    private void ControlEditorServer(bool runEditorServer) {
        if (runEditorServer)
            EditorServer.Run();
        else
            EditorServer.Stop();
    }

    private void RetryNetwork() {
        ProjectIdAttributeDrawer.serverHasChanged = true;
        ClearOverviewWindow();
    }

    private void ClearOverviewWindow() {
        var overviewWindow = BugOverviewWindow.window;
        var overviewGUI = overviewWindow ? overviewWindow.gui : null;
        if (overviewGUI != null) {
            overviewGUI.Clear();
            overviewWindow.Repaint();
        }
    }

    private void DrawProperties(List<ConfigProperty> properties) {
        foreach (var p in properties)
            p.Update();
        foreach (var cp in properties)
            cp.OnGUI();
        foreach (var p in properties)
            p.ApplyModifiedProperties();
    }

    private void OnGUI() {
        GUILayout.BeginHorizontal();

        //###############################################
        //#                Left bar                    ##
        //###############################################

        GUILayout.BeginVertical(ConfigWindowStyles.CategoryScrollView, GUILayout.Width(80f));
        GUILayout.Space(10f);

        GUIContent content;
        Rect rect;

        for (int i = 0; i < categories.Count; i++) {
            var configCategory = categories[i];
            content = new GUIContent(configCategory.name);

            rect = GUILayoutUtility.GetRect(content, ConfigWindowStyles.CategoryLabel, GUILayout.ExpandWidth(true));

            if (selectedIndex == i && Event.current.type == EventType.Repaint)
                ConfigWindowStyles.CategoryBG.Draw(rect, false, false, true, false);

            EditorGUI.BeginChangeCheck();
            if (GUI.Toggle(rect, selectedIndex == i, content, ConfigWindowStyles.CategoryLabel)) {
                selectedIndex = i;
                scrollPos = Vector2.zero;
            }

            if (EditorGUI.EndChangeCheck())
                GUIUtility.keyboardControl = 0;
        }

        var prefCategory2 = categories[Mathf.Clamp(selectedIndex, 0, categories.Count - 1)];

        GUILayout.EndVertical();
        GUILayout.Space(10f);

        //###############################################
        //#           Draw selected page               ##
        //###############################################

        GUILayout.BeginVertical();

        GUILayout.Label(prefCategory2.name, ConfigWindowStyles.Header);
        GUILayout.Space(5f);

        scrollPos = GUILayout.BeginScrollView(scrollPos);
        prefCategory2.drawAction?.Invoke();

        GUILayout.Space(5f);
        GUILayout.EndScrollView();

        GUILayout.EndVertical();

        GUILayout.EndHorizontal();
    }
}
}
