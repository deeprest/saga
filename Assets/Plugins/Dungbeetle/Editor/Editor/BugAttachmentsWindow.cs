using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class BugAttachmentsWindow : EditorWindow {

    private BugAttachmentsGUI gui;

    public static void Init(Bug b) {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.BugAttachmentsWindoww");
        var window = GetWindow<BugAttachmentsWindow>($"Attachments {b.id}", true);
        if (setPos)
            window.position = new Rect(100f, 100f, 320f, 240f);
        var new_gui = new BugAttachmentsGUI();
        new_gui.SetBug(b);

        window.gui = new_gui;
    }

    public void OnGUI() {
        if (gui == null)
            Close();
        else
            gui.OnGUI();
    }
}
}