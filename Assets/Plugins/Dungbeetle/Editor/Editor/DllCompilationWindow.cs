using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using UnityEngine;
using UnityEditor;

namespace Dungbeetle {
public class DllCompilationWindow : EditorWindow {

    [MenuItem ("Window/Dungbeetle/Tools/Build DLL or Server/Dll Version of Dungbeetle", false, 121)]
    static void Init() {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.DllCompilationWindoww");
        var window = EditorWindow.GetWindow <DllCompilationWindow>();
        if (setPos)
            window.position = new Rect(100f, 100f, 320f, 240f);
        window.Show();
    }

    [MenuItem ("Window/Dungbeetle/Tools/Build DLL or Server/Dll Version of Dungbeetle", true)]
    static bool SourceCodeAvailable() {
#if DUNGBEETLE_DLL
        return false;
#else
        return true;
#endif
    }

    private string resultString;

    private void SourceSearch (string sDir, List<string> target) {
        foreach (var f in Directory.GetFiles(sDir, "*.cs"))
            target.Add(f);
        foreach (var d in Directory.GetDirectories(sDir)) {
            SourceSearch(d, target);
        }
    }

    private void DocSearch (string sDir, SearchOption searchOption, List<string> target) {
        foreach (var f in Directory.GetFiles(sDir, "*.cs", searchOption))
            target.Add(f);
        foreach (var f in Directory.GetFiles(sDir, "*.cs.meta", searchOption))
            target.Add(f);
        foreach (var f in Directory.GetFiles(sDir, "*.txt", searchOption))
            target.Add(f);
        foreach (var f in Directory.GetFiles(sDir, "*.txt.meta", searchOption))
            target.Add(f);
        foreach (var f in Directory.GetFiles(sDir, "*.png", searchOption))
            target.Add(f);
        foreach (var f in Directory.GetFiles(sDir, "*.png.meta", searchOption))
            target.Add(f);
        foreach (var f in Directory.GetFiles(sDir, "*.guiskin", searchOption))
            target.Add(f);
        foreach (var f in Directory.GetFiles(sDir, "*.guiskin.meta", searchOption))
            target.Add(f);
    }

    private void HtmlSearch (string sDir, List<string> target) {
        foreach (var f in Directory.GetFiles(sDir, "*"))
            if (!f.EndsWith(".meta"))
                target.Add(f);
    }

    private void IconSearch (string sDir, string tDir, List<string> target) {
        foreach (var f in Directory.GetFiles(sDir, "*.png"))
            target.Add(f);
        foreach (var f in Directory.GetFiles(sDir, "*.png.meta"))
            if (!File.Exists("$tDir/$(Path.GetFileName(f))")) // Don't overwrite meta files. Causes trouble with permissions.
                target.Add(f);
    }

    void OnGUI() {
        if (GUILayout.Button("Compile")) {
            resultString = string.Empty;

            /*************************************
            *                                    *
            *          Unity Common Libs         *
            *                                    *
            *************************************/

            var destinationPath = EditorUtility.SaveFolderPanel("Select a folder to store dll", System.Environment.GetEnvironmentVariable("USERPROFILE"), string.Empty);

            if (string.IsNullOrEmpty(destinationPath))
                return;

            var destMainPath         = $"{destinationPath}/Dungbeetle";
            var destAssetsPath       = $"{destinationPath}/Dungbeetle/Assets";
            var destHtmlPath         = $"{destinationPath}/Dungbeetle/.Documentation";
            var destPluginsPath      = $"{destinationPath}/Plugins/Dungbeetle";
            var destEditorPath       = $"{destinationPath}/Plugins/Dungbeetle/Editor";
            var destIconsPath        = $"{destinationPath}/Plugins/Dungbeetle/Editor/Icons";
            var destSqlitePath       = $"{destinationPath}/Plugins/Dungbeetle/Editor/SQLite";
            var destSqliteNativePath = $"{destinationPath}/Plugins/Dungbeetle/Editor/SQLite/Win_x64";

            var assetsPluginsPath      = "Assets/Plugins/Dungbeetle";
            var assetsEditorPath       = "Assets/Plugins/Dungbeetle/Editor";
            var assetsIconsPath        = "Assets/Plugins/Dungbeetle/Editor/Icons";
            var assetsSqlitePath       = "Assets/Plugins/Dungbeetle/Editor/SQLite";
            var assetsSqliteNativePath = "Assets/Plugins/Dungbeetle/Editor/SQLite/Win_x64";

             Directory.CreateDirectory(destSqliteNativePath);

            // Non-editor DLLs
            File.Copy($"{assetsPluginsPath}/YamlDotNet.dll",        $"{destPluginsPath}/YamlDotNet.dll",      true);

            // Editor DLLs
            File.Copy($"{assetsEditorPath}/MySql.Data.dll",         $"{destEditorPath}/MySql.Data.dll",         true);
            File.Copy($"{assetsSqlitePath}/System.Data.SQLite.dll", $"{destSqlitePath}/System.Data.SQLite.dll", true);
            if (Application.platform == RuntimePlatform.WindowsEditor)
                File.Copy($"{assetsSqliteNativePath}/sqlite3.dll", $"{destSqliteNativePath}/sqlite3.dll", true);

            /*************************************
            *                                    *
            *    Example Code + Documentation    *
            *              + Icons               *
            *                                    *
            *************************************/

            var sources = new List<string>();

            DocSearch("Assets/Dungbeetle", SearchOption.TopDirectoryOnly, sources);
            Directory.CreateDirectory(destMainPath);
            foreach (var fn in sources)
                File.Copy(fn, $"{destMainPath}/{Path.GetFileName(fn)}", true);

            sources.Clear();
            DocSearch("Assets/Dungbeetle/Assets", SearchOption.AllDirectories, sources);
            Directory.CreateDirectory(destAssetsPath);
            foreach (var fn in sources)
                File.Copy(fn, $"{destAssetsPath}/{Path.GetFileName(fn)}", true);

            var defaultDocPath = "Assets/Dungbeetle/.Documentation";
            // The secondary path is necessary because people might unzip the
            // Documentation.zip to './Documentation/' instead of directly into './'
            var secondaryDocPath = "Assets/Dungbeetle/Documentation/.Documentation";
            var docPath          = Directory.Exists(defaultDocPath) ? defaultDocPath : secondaryDocPath;
            if (Directory.Exists(docPath)) {
                sources.Clear();
                HtmlSearch (docPath, sources);
                Directory.CreateDirectory(destHtmlPath);
                foreach (var fn in sources)
                    File.Copy(fn, $"{destHtmlPath}/{Path.GetFileName(fn)}", true);
            }

            sources.Clear();
            IconSearch(assetsIconsPath, destIconsPath, sources);
            Directory.CreateDirectory(destIconsPath);
            foreach (var fn in sources)
                File.Copy(fn, $"{destIconsPath}/{Path.GetFileName(fn)}", true);

            /*************************************
            *                                    *
            *      Plain UnityEngine Sources     *
            *                                    *
            *************************************/

            CompilerParameters cp = new CompilerParameters();
            cp.OutputAssembly = $"{destPluginsPath}/Dungbeetle.dll";

            cp.ReferencedAssemblies.Add( "System.dll" );
            cp.ReferencedAssemblies.Add( "System.Core.dll" );

            cp.ReferencedAssemblies.Add( $"{EditorApplication.applicationContentsPath}/Managed/UnityEngine.dll" );
            cp.ReferencedAssemblies.Add( $"{assetsPluginsPath}/YamlDotNet.dll" );

            sources.Clear();
            SourceSearch("Assets/Plugins/Dungbeetle/Scripts", sources);

            cp.CompilerOptions = "-d:DUNGBEETLE_DLL /optimize";

            cp.IncludeDebugInformation = true;

            var cr = new Microsoft.CSharp.CSharpCodeProvider().CompileAssemblyFromFile(cp, sources.ToArray());

            if(cr.Errors.Count > 0) {
                // Display compilation errors.
                var errorBuilder = new System.Text.StringBuilder();
                errorBuilder.AppendLine("Errors building.");
                foreach (CompilerError ce in cr.Errors)
                    errorBuilder.AppendLine($"  {ce.ToString()}");
                resultString = errorBuilder.ToString();
                return;
            }

            /*************************************
            *                                    *
            *         UnityEditor Sources        *
            *                                    *
            *************************************/

            cp = new CompilerParameters();
            cp.OutputAssembly = $"{destEditorPath}/Dungbeetle.Editor.dll";
            cp.GenerateInMemory = false;

            cp.ReferencedAssemblies.Add( "System.dll" );
            cp.ReferencedAssemblies.Add( "System.Core.dll" );
            cp.ReferencedAssemblies.Add( "System.Data.dll" );
            cp.ReferencedAssemblies.Add( "System.ServiceProcess.dll" );

            cp.ReferencedAssemblies.Add( $"{assetsEditorPath}/MySql.Data.dll" );
            cp.ReferencedAssemblies.Add( $"{assetsSqlitePath}/System.Data.SQLite.dll" );

            cp.ReferencedAssemblies.Add( $"{EditorApplication.applicationContentsPath}/Managed/UnityEngine.dll" );
            cp.ReferencedAssemblies.Add( $"{EditorApplication.applicationContentsPath}/Managed/UnityEditor.dll" );
            cp.ReferencedAssemblies.Add( $"{assetsPluginsPath}/YamlDotNet.dll" );
            cp.ReferencedAssemblies.Add( $"{destPluginsPath}/Dungbeetle.dll" );

            sources.Clear();
            SourceSearch("Assets/Plugins/Dungbeetle/Editor", sources);

            cp.CompilerOptions = "-d:DUNGBEETLE_DLL /optimize";

            cp.IncludeDebugInformation = true;

            cr = new Microsoft.CSharp.CSharpCodeProvider().CompileAssemblyFromFile(cp, sources.ToArray());

            if(cr.Errors.Count > 0) {
                // Display compilation errors.
                var errorBuilder = new System.Text.StringBuilder();
                errorBuilder.AppendLine("Errors building.");
                foreach (CompilerError ce in cr.Errors)
                    errorBuilder.AppendLine($"  {ce.ToString()}");
                resultString = errorBuilder.ToString();
                return;
            }

            if (string.IsNullOrEmpty(resultString)) {
                EditorUtility.RevealInFinder(destinationPath + "/");
                resultString = "Success!";
            }
        }
        if (!string.IsNullOrEmpty(resultString))
            EditorGUILayout.TextArea(resultString);
    }
}
}
