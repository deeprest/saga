using System;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

[CustomEditor(typeof(Config))]
public class ConfigEditor : Editor {

    [NonSerialized]
    private GUIContent _unhideGUIContent;
    private GUIContent unhideGUIContent {
        get {
            if (_unhideGUIContent == null)
                _unhideGUIContent = new GUIContent("Unhide asset parts (expert mode)", "Unhide sub assets in the project window.");
            return _unhideGUIContent;
        }
    }

    [NonSerialized]
    private GUIContent _hideGUIContent;
    private GUIContent hideGUIContent {
        get {
            if (_hideGUIContent == null)
                _hideGUIContent = new GUIContent("Hide asset parts", "Hide sub assets in the project window.");
            return _hideGUIContent;
        }
    }

    private Config config;

    private void OnEnable() {
        config = (Config) target;
    }

    public override void OnInspectorGUI() {
        DrawDefaultInspector();

        if (GUILayout.Button("Open Connection Config"))
            ConfigWindow.CreateWindow();

        var currentHideflags = HideFlags.None;

        using (var subAssetsEnumerator = config.subAssets) {
            while (subAssetsEnumerator.MoveNext()) {
                var asset = subAssetsEnumerator.Current;
                currentHideflags = asset.hideFlags;
                break;
            }
        }

        var nextHideflags = currentHideflags;
        if (currentHideflags == HideFlags.HideInHierarchy && GUILayout.Button(unhideGUIContent))
            nextHideflags = HideFlags.None;
        if (currentHideflags == HideFlags.None && GUILayout.Button(hideGUIContent))
            nextHideflags = HideFlags.HideInHierarchy;

        if (nextHideflags != currentHideflags) {
            using(var subAssetsEnumerator = config.subAssets) {
                while (subAssetsEnumerator.MoveNext()) {
                    var asset = subAssetsEnumerator.Current;
                    asset.hideFlags = nextHideflags;
                }
            }

            EditorUtility.SetDirty(config);
            AssetDatabase.SaveAssets();
        }
    }
}
}