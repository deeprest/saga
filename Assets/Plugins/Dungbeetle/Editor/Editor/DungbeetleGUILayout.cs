using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class DungbeetleGUILayout {

    public static string PubKeyField(GUIContent label, string pubKey) {
        var rect = EditorGUILayout.GetControlRect();
        return DungbeetleGUI.PubKeyField(rect, label, pubKey);
    }

    public static string PubKeyField(string pubKey) {
        var rect = GUILayoutUtility.GetRect(0f, EditorGUIUtility.singleLineHeight);
        return DungbeetleGUI.PubKeyField(rect, pubKey);
    }

    public static string PrivKeyField(GUIContent label, string privKey) {
        var rect = EditorGUILayout.GetControlRect();
        return DungbeetleGUI.PrivKeyField(rect, label, privKey);
    }

    public static string PrivKeyField(string privKey) {
        var rect = GUILayoutUtility.GetRect(0f, EditorGUIUtility.singleLineHeight);
        return DungbeetleGUI.PrivKeyField(rect, privKey);
    }
}
}