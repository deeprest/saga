using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class BugAssignWindow : EditorWindow, IBugAssign {

    private BugAssignGUI gui;

    private IBugOverview _parentOverview;
    public IBugOverview parentOverview {
        get { return _parentOverview; }
        set {
            _parentOverview = value;
            LoadGUI();
        }
    }

    public static BugAssignWindow Init(Bug b, BugAssignAction a, IBugOverview ov) {
        var v = System.Enum.GetName(typeof(BugAssignAction), a);

        var setPos = !EditorPrefs.HasKey("Dungbeetle.BugAssignWindoww");
        var window = GetWindow<BugAssignWindow>($"{v} #{b.id}", true);
        if (setPos)
            window.position = new Rect(100f, 100f, 320f, 345f);

        var new_gui = new BugAssignGUI();
        new_gui.SetActionVerb(v);
        new_gui.SetBug(b, a);

        window.gui = new_gui;
        window.parentOverview = ov;

        return window;
    }

    private void OnEnable() {
        LoadGUI();
    }

    private void LoadGUI() {
        if (gui != null) {
            gui.window = this;
            gui.SetParentOverview(_parentOverview);
        }
    }

    private void OnGUI() {
        if (gui != null)
        gui.OnGUI();
    }

    public void CloseWindow() {
        Close();
    }
}
}