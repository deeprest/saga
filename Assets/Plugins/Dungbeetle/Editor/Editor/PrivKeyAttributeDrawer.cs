using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

[CustomPropertyDrawer(typeof(PrivKeyAttribute))]
public class PrivKeyFieldDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);

        EditorGUI.BeginChangeCheck();
        var newKey = DungbeetleGUI.PrivKeyField(position, label, property.stringValue);
        if (EditorGUI.EndChangeCheck()) {
            property.stringValue = newKey;
            EditorServer.ClearCookies();
            BugUtility.InvalidateNetworkDetails();
        }

        EditorGUI.EndProperty();
    }
}
}