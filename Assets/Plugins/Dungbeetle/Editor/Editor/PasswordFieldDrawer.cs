using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

[CustomPropertyDrawer(typeof(PasswordAttribute))]
public class PasswordDrawer : PropertyDrawer {

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        EditorGUI.BeginProperty(position, label, property);

        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        EditorGUI.BeginChangeCheck();
        var newString = EditorGUI.TextField(position, property.stringValue);
        if (EditorGUI.EndChangeCheck()) {
            property.stringValue = newString;

            EditorServer.ClearCookies();
            BugUtility.InvalidateNetworkDetails();
        }
        EditorGUI.EndProperty();
    }
}
}