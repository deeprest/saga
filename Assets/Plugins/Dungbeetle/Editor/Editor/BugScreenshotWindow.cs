using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class BugScreenshotWindow : EditorWindow, IBugScreenshot {

    private BugScreenshotGUI gui;

    private IBugOverview _parentOverview;
    private IBugOverview parentOverview {
        get {
            return _parentOverview;
        }
        set {
            _parentOverview = value;
            LoadGUI();
        }
    }

    public static void Init(Bug b, IBugOverview ov) {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.BugScreenshotWindoww");
        var window = EditorWindow.GetWindow<BugScreenshotWindow>("Screenshot", true);
        if (setPos)
            window.position = new Rect(100f, 100f, 853f, 480f);

        var new_gui = new BugScreenshotGUI();
        new_gui.SetBug(b);
        new_gui.window = window;

        window.gui = new_gui;
        window.parentOverview = ov;
    }

    private void OnEnable() {
        LoadGUI();
    }

    private void LoadGUI() {
        if (gui != null) {
            gui.window = this;
            gui.SetParentOverview(_parentOverview);
        }
    }

    private void OnGUI() {
        if (Event.current.type == EventType.MouseDown)
            Close();
        gui.OnGUI();
    }

    public void SetWindowDimension(int width, int height) {
        position = new Rect(position.x, position.y, width, height);
    }
}
}