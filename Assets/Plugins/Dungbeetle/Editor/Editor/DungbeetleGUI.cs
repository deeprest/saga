using UnityEngine;
using UnityEditor;
using System.Security.Cryptography;

namespace Dungbeetle {

public class DungbeetleGUI {

    public static string PubKeyField(Rect position, GUIContent label, string pubKey) {
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        return PubKeyField(position, pubKey);
    }

    public static string PubKeyField(Rect position, string pubKey) {
        var labelWidth = 65;
        var keyRect = new Rect(position.x, position.y, labelWidth, position.height);
        var buttonWidth = (position.width - keyRect.width) / 2;
        var copyRect    = new Rect(keyRect.xMax,  position.y, buttonWidth, position.height);
        var pasteRect   = new Rect(copyRect.xMax, position.y, buttonWidth, position.height);

        var displayString = $"{pubKey.GetHashCode():X8}";

        GUI.Label(keyRect, displayString, EditorStyles.label);

        var guiPrevChange = GUI.changed;
        if (GUI.Button(copyRect, "Copy to Clipboard")) {
            EditorGUIUtility.systemCopyBuffer = pubKey;
            GUI.changed = guiPrevChange;
        }

        guiPrevChange = GUI.changed;
        if (GUI.Button(pasteRect, "Paste from Clipboard")) {
            var cb = EditorGUIUtility.systemCopyBuffer;
            try {
                var rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(cb); // test for illegal format before applying new string
                var result = rsa.ToXmlString(false);
                GUI.changed = true;
                return result;
            }
            catch {
                EditorUtility.DisplayDialog("Invalid Key", "Could not paste! Invalid format.", "Ok");
                GUI.changed = guiPrevChange;
            }
        }

        return pubKey;
    }

    public static string PrivKeyField(Rect position, GUIContent label, string privKey) {
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
        return PrivKeyField(position, privKey);
    }

    public static string PrivKeyField(Rect position, string privKey) {
        var labelWidth = 65;
        var keyRect = new Rect(position.x, position.y, labelWidth, position.height);
        var buttonWidth = (position.width - keyRect.width) / 3;
        var copyRect     = new Rect(keyRect.xMax,   position.y, buttonWidth, position.height);
        var pasteRect    = new Rect(copyRect.xMax,  position.y, buttonWidth, position.height);
        var generateRect = new Rect(pasteRect.xMax, position.y, buttonWidth, position.height);

        var displayString = $"{privKey.GetHashCode():X8}";

        var guiPrevChange = GUI.changed;
        GUI.Label(keyRect, displayString);
        if (GUI.Button(copyRect, "Copy to Clipboard")) {
            EditorGUIUtility.systemCopyBuffer = privKey;
            GUI.changed = guiPrevChange;
        }

        var result = privKey;

        guiPrevChange = GUI.changed;
        if (GUI.Button(pasteRect, "Paste from Clipboard")) {
            var cb = EditorGUIUtility.systemCopyBuffer;
            try {
                var rsa = new RSACryptoServiceProvider();
                rsa.FromXmlString(cb); // test for illegal format before applying new string
                if (rsa.PublicOnly)
                    Debug.LogError("Failed to paste private key: Found public key, where private key was expected");
                else if (EditorUtility.DisplayDialog("Pasting from Clipboard",
                                                     "Your private key will be overwritten.\n\nThis action can not be undone!", "Paste", "Cancel")) {
                    GUI.changed = true;
                    result = cb;
                }
                else
                    GUI.changed = guiPrevChange;
            }
            catch (System.Exception e) {
                EditorUtility.DisplayDialog("Invalid Key", "Could not paste! Invalid format.\n" + e.Message, "Ok");
                GUI.changed = guiPrevChange;
            }
        }

        guiPrevChange = GUI.changed;
        if (GUI.Button(generateRect, "Regenerate")) {
            if (EditorUtility.DisplayDialog(
              "Generate new key",
              @"Really generate new key?

 - Your private key will be deleted and replaced.
 - Your public key will no longer match your
   private key.
 - You will no longer be able to connect to
   your previously compiled servers, unless
   you also replace their private keys. This
   can cause trouble for builds that rely on
   those private keys as well.

 - Note!
    To re-align your pubkey, you can paste the
    private key. The private part will be
    removed automatically.

This action can not be undone!", "Yes, delete previous key", "Cancel")) {
                var rsa = new RSACryptoServiceProvider();
                result = rsa.ToXmlString(true);
                GUI.changed = true;
            }
            else
                GUI.changed = guiPrevChange;
        }
        return result;
    }
}
}