using System;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class ConfirmationPopup {

    private string queryText;
    private Action action;

    public bool complete => _complete;
    public bool _complete;
    private bool confirmed;

    public ConfirmationPopup(string queryText, Action action) {
        this.queryText = queryText;
        this.action = action;
    }

    public void Finish() {
        if (confirmed)
            action();
    }

    public void OnGUI() {
        EditorGUILayout.LabelField(queryText, DungbeetleGUIStyles.seriousWarningMessage, GUILayout.ExpandWidth(true));
        GUILayout.Space(70);

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Cancel"))
            _complete = true;
        if (GUILayout.Button("Ok")) {
            _complete = true;
            confirmed = true;
        }

        EditorGUILayout.EndHorizontal();
    }

}
}