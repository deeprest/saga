using System.Threading;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class EditorServerWindow : EditorWindow {

    private int repaintsNeeded; // pretend bool, for interlocked operations, which don't support bool

    private void OnEnable() {
        QueueRepaint();
        EditorServer.RegisterExclusiveOnLoggedEvent(QueueRepaint);
    }

    private void QueueRepaint() {
        repaintsNeeded = 1;
    }

    private void OnDisable() {
        EditorServer.ClearOnLoggedEvents();
    }

    private void OnInspectorUpdate() {
        if (Interlocked.CompareExchange(ref repaintsNeeded, 0, 1) == 1) {
            AdjustWindowToInput();
        }
    }

    private void AdjustWindowToInput() {
        Repaint();
        EditorServer.ScrollDown();
    }

    private SerializedObject _serializedObject;
    private SerializedObject serializedObject {
        get {
            if (_serializedObject == null)
                _serializedObject = new SerializedObject(Config.EditorServerConfig.Enabled);
            return _serializedObject;
        }
    }

    private SerializedProperty _serializedProperty;
    private SerializedProperty serializedProperty {
        get {
            if(_serializedProperty == null)
                _serializedProperty = serializedObject.FindProperty("value");
            return _serializedProperty;
        }
    }

    private void OnGUI() {
        serializedObject.Update();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(serializedProperty, new GUIContent("Editor Server Enabled"));
        if (EditorGUI.EndChangeCheck()) {
            if (serializedProperty.boolValue)
                EditorServer.Run();
            else
                EditorServer.Stop();
        }

        serializedObject.ApplyModifiedProperties();
        EditorServer.ServerGUI(position.height - 20, position.width);
    }

    [MenuItem ("Window/Dungbeetle/Tools/In-Editor Server Log", false, 125)]
    private static void Init () {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.EditorServerWindoww");
        var window = EditorWindow.GetWindow<EditorServerWindow>();
        if (setPos)
            window.position = new Rect(100f, 100f, 320f, 240f);
        window.Show();
    }
}
}