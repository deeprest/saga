using System;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

[InitializeOnLoad]
public static class BuildNameArkLoader
{

    const string BuildInfoAssetPath = "Assets/external/Dungbeetle/BuildName.asset";
    
    static BuildNameArkLoader() {
        EditorApplication.playModeStateChanged -= LoadOrUnloadIfPlaying;
        EditorApplication.playModeStateChanged += LoadOrUnloadIfPlaying;

        EditorApplication.delayCall -= EnsureArkExists;
        EditorApplication.delayCall += EnsureArkExists;
    }

    private static void LoadOrUnloadIfPlaying(PlayModeStateChange state) {
        if (state == PlayModeStateChange.EnteredPlayMode)
            Load(true);
        else if (state == PlayModeStateChange.ExitingPlayMode)
            Load(false);
    }

    private static void Load(bool loadIn) {
        if (loadIn) {
            if (BuildNameArk.ark == null) {
                // Shouldn't happen because of the static constructor, but is a part of the pattern
                BuildNameArk.ark = AssetDatabase.LoadAssetAtPath<BuildNameArk>(BuildInfoAssetPath);
            }
        }
        else if(BuildNameArk.ark != null) {
            Resources.UnloadAsset(BuildNameArk.ark);
            BuildNameArk.ark = null;
        }
    }

    private static void EnsureArkExists() {
        var getSoMethod = typeof(PlayerSettings).GetMethod("GetSerializedObject", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        if (getSoMethod == null)
            throw new Exception("The type PlayerSettings doesn't have the private static GetSerializedObject!");

        var so = (SerializedObject) getSoMethod.Invoke(null, new object[0]);

        so.Update();
        var assets = so.FindProperty("preloadedAssets");

        BuildNameArk.ark = AssetDatabase.LoadAssetAtPath<BuildNameArk>(BuildInfoAssetPath);
        if (BuildNameArk.ark != null) {
            // Update the build name each recompile
            BuildNameArk.ark.buildName.Update();

            // Check that the ark is in the player settings
            var found = false;
            int i = 0;

            while (!found && i < assets.arraySize) {
                found = assets.GetArrayElementAtIndex(i++).objectReferenceValue == BuildNameArk.ark;
            }
            if (!found) {
                assets.InsertArrayElementAtIndex(i);
                assets.GetArrayElementAtIndex(i).objectReferenceValue = BuildNameArk.ark;
                so.ApplyModifiedProperties();
            }
        }
        else {
            // New Ark
            BuildNameArk.ark = ScriptableObject.CreateInstance<BuildNameArk>();
            BuildNameArk.ark.buildName = new AutomaticBuildName(string.Empty, true, true, true, true);
            AssetDatabase.CreateAsset(BuildNameArk.ark, BuildInfoAssetPath);

            // Stuff it into playerSettings
            var lastIndex = assets.arraySize;
            assets.InsertArrayElementAtIndex(lastIndex);
            assets.GetArrayElementAtIndex(lastIndex).objectReferenceValue = BuildNameArk.ark;

            so.ApplyModifiedProperties();
        }
    }
}
}
