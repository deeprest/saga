using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class ProjectManagementWindow : EditorWindow {

    [MenuItem ("Window/Dungbeetle/Projects", false, 102)]
    public static void Init () {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.ProjectManagementWindoww");
        var window = GetWindow<ProjectManagementWindow>(true, "Projects");
        if (setPos)
            window.position = new Rect(100f, 100f, 630f, 400f);
        window.Show();
    }

    private ConfirmationPopup confirmationPopup;
    private int selectedIndex = -1;

    private Vector2 scrollPos;

    private bool socketFailed;
    private string socketFailMsg;
    private bool socketFailGui;

    private bool requesting;

    private GUIAwareMessage requestingGMsg = default(GUIAwareMessage);

    private List<Project> projects;

    private ushort failedPort;
    private string failedDomain;

    private static Dictionary<int, string> devControlNames = new Dictionary<int, string>();
    private static bool focusNewDev;
    private static bool focusNewProject;

    private struct GUIAwareMessage {
        public bool laidOut;
        public bool painted;
        public bool ready;

        public void GUIUpdate() {
            if (!laidOut)
                laidOut = Event.current.type == EventType.Layout;
            else if(!painted)
                painted = Event.current.type == EventType.Repaint;
            else
                ready = true;
        }

        public void Reset() {
            laidOut = false;
            painted = false;
            ready = false;
        }
    }

    private void OnEnable() {
        requesting = true;
        confirmationPopup = null;
    }

    private void OnGUI() {
        if (Config.EditorServerConfig.Enabled && !EditorServer.Running()) {
            // ProjectManagementWindow open during Unity startup
            EditorGUILayout.LabelField("Waiting for contact with server...");
            return;
        }

        if (CheckSocketFailGUI())
            return;

        if (requesting) {
            requestingGMsg.GUIUpdate();
            if (requestingGMsg.ready) {
                requesting = false;
                requestingGMsg.Reset();
                RequestProjectsFromServer();
                if (CheckSocketFailGUI())
                    return;
            }
            else {
                if (requestingGMsg.laidOut)
                    EditorGUILayout.LabelField("Requesting projects from server...");
                if (requestingGMsg.painted)
                    Repaint();
                return;
            }
        }

        GUILayout.BeginHorizontal();

        //###############################################
        //#                Left bar                    ##
        //###############################################

        GUILayout.BeginVertical(ConfigWindowStyles.CategoryScrollView, GUILayout.Width(80f));

        GUILayout.Space(10f);

        GUIContent content;
        Rect rect;

        for (int i = 0; i < projects.Count; i++) {
            content = new GUIContent(projects[i].name);

            rect = GUILayoutUtility.GetRect(content, ConfigWindowStyles.CategoryLabel, GUILayout.ExpandWidth(true));

            if (selectedIndex == i && Event.current.type == EventType.Repaint)
                ConfigWindowStyles.CategoryBG.Draw(rect, false, false, true, false);

            EditorGUI.BeginChangeCheck();
            if (GUI.Toggle(rect, selectedIndex == i, content, ConfigWindowStyles.CategoryLabel))
                selectedIndex = i;
            if (EditorGUI.EndChangeCheck()) {
                scrollPos = Vector2.zero;
                GUIUtility.keyboardControl = 0;
            }
        }

        if (focusNewProject && Event.current.type == EventType.Repaint) {
            focusNewProject = false;
            EditorGUI.FocusTextInControl("Project Name");
        }

        if (GUILayout.Button("Add")) {
            selectedIndex = projects.Count;
            projects.Add(new Project {
                id = -1,
                name = string.Empty,
                developers = new List<ProjectDeveloper>()
            });
            focusNewProject = true;
        }

        GUILayout.EndVertical();
        GUILayout.Space(10f);

        //###############################################
        //#               Project page                 ##
        //###############################################

        if (confirmationPopup != null) {
            if (confirmationPopup.complete) {
                confirmationPopup.Finish();
                confirmationPopup = null;
            }
            else {
                GUILayout.BeginVertical();
                confirmationPopup.OnGUI();
                GUILayout.EndVertical();
                return;
            }
        }

        if (-1 < selectedIndex && selectedIndex < projects.Count) {
            var project = projects[selectedIndex];

            GUILayout.BeginVertical();

            GUILayout.BeginHorizontal();
            var prevLabelWidth = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 80f;
            var projectRect = EditorGUILayout.GetControlRect(true, 24f, ConfigWindowStyles.HeaderLabel);
            projectRect = EditorGUI.PrefixLabel(projectRect, ConfigWindowStyles.ProjectLabel, ConfigWindowStyles.HeaderLabel);

            EditorGUI.BeginChangeCheck();
            PrepareDelayedControl("Project Name", projectRect);
            project.name = EditorGUI.DelayedTextField(projectRect, project.name, ConfigWindowStyles.ProjectHeader);
            if (EditorGUI.EndChangeCheck()) {
                if (project.id == -1)
                    project.id = BugUtility.AddProject(project.name, ref socketFailed, ref socketFailMsg);
                else
                    BugUtility.RenameProject(project.id, project.name, ref socketFailed, ref socketFailMsg);
            }

            EditorGUIUtility.labelWidth = prevLabelWidth;
            GUILayout.EndHorizontal();
            GUILayout.Space(15f);

            scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.Height(Mathf.Max(Screen.height - 50f, 20f)));

            EditorGUILayout.LabelField(ConfigWindowStyles.DevelopersLabel);

            EditorGUI.indentLevel += 1;

            EditorGUIUtility.labelWidth = 0f;

            for (int i = 0; i < project.developers.Count; i++) {
                var deletionRect = EditorGUILayout.GetControlRect(false);
                var developerRect = deletionRect;

                // developerRect.x += 15 * EditorGUI.indentLevel # DelayedTextField indents extra
                developerRect.width -= /*15 * EditorGUI.indentLevel +*/ 60f;
                deletionRect.xMin = developerRect.xMax + 1;
                deletionRect.width = 60f;

                var dev = project.developers[i];
                var devId = dev.id;
                string devControlName;
                if (!devControlNames.TryGetValue(devId, out devControlName))
                    devControlName = $"dev{devId}";

                devControlNames[devId] = devControlName;

                EditorGUI.BeginChangeCheck();
                PrepareDelayedControl(devControlName, developerRect);
                dev.name = EditorGUI.DelayedTextField(developerRect, dev.name, ConfigWindowStyles.DeveloperField);
                if (EditorGUI.EndChangeCheck()) {
                    if (devId == -1)
                        dev.id = BugUtility.AddDeveloper(project.id, dev.name, ref socketFailed, ref socketFailMsg);
                    else
                        BugUtility.RenameDeveloper(devId, dev.name, ref socketFailed, ref socketFailMsg);
                }

                if (GUI.Button(deletionRect, "Remove")) {
                    project.developers.RemoveAt(i);
                    BugUtility.DeleteDeveloper(devId, ref socketFailed, ref socketFailMsg);
                }
            }

            if (focusNewDev) {
                focusNewDev = false;
                EditorGUI.FocusTextInControl("dev-1");
            }

            var addRect = EditorGUILayout.GetControlRect(false);
            addRect.x += 15 * EditorGUI.indentLevel;
            addRect.width = 60;
            if (GUI.Button(addRect, "Add")) {
                focusNewDev = true;
                project.developers.Add(new ProjectDeveloper(-1, string.Empty));
            }

            EditorGUI.indentLevel -= 1;

            GUILayout.Space(10f);

            EditorGUILayout.LabelField("Actions:");
            EditorGUI.indentLevel += 1;
            var dRect = EditorGUILayout.GetControlRect(false);
            dRect.x += 15 * EditorGUI.indentLevel;
            dRect.width = 100;
            if (GUI.Button(dRect, "Delete project")) {
                var projectId = project.id;
                confirmationPopup = new ConfirmationPopup($"This will DELETE project {project.name}!\n\nConfirm?", () => {
                    BugUtility.DeleteProject(projectId, ref socketFailed, ref socketFailMsg);
                    projects.RemoveAt(selectedIndex);
                    selectedIndex -= 1;
                });
            }

            EditorGUI.indentLevel -= 1;

            GUILayout.EndScrollView();

            GUILayout.EndVertical();
        }

        GUILayout.EndHorizontal();

        if (socketFailed) {
            failedPort = Config.OverviewClientConfig.Port;
            failedDomain = Config.OverviewClientConfig.Domain;
        }
    }

    private bool CheckSocketFailGUI() {
        if (socketFailed) {
            socketFailGui = socketFailGui || Event.current.type == EventType.Layout;
            if (socketFailGui) {
                var reconnect = GUILayout.Button("Reconnect");
                GUILayout.Label($"Attempted to connect to {failedDomain}:{failedPort}: {socketFailMsg}", errorLabelStyle);
                if (reconnect) {
                    socketFailed = false;
                    socketFailGui = false;
                    requesting = true;
                }
            }
            return true;
        }
        return false;
    }

    private void PrepareDelayedControl(string controlName, Rect controlRect) {
        GUI.SetNextControlName(controlName);
        DoubleCheckDelayedUnfocus(controlName, controlRect);
    }

    private void DoubleCheckDelayedUnfocus(string controlName, Rect controlRect) {
        // This is just a little extra help to get the delayed text
        // fields to yield their data if they have actually been
        // focused.
        if (Event.current.type == EventType.MouseDown) {
            if (GUI.GetNameOfFocusedControl() == controlName)
                if (!controlRect.Contains(Event.current.mousePosition))
                    GUI.FocusControl(null);
        }
    }

    private void RequestProjectsFromServer() {
        projects = BugUtility.GetProjects(true, ref socketFailed, ref socketFailMsg, 3000);
        if (socketFailed) {
            failedPort = Config.OverviewClientConfig.Port;
            failedDomain = Config.OverviewClientConfig.Domain;
        }
    }

    private bool _errorLabelStyleExists;
    private GUIStyle _errorLabelStyle;
    private GUIStyle errorLabelStyle {
        get {
            if(!_errorLabelStyleExists) {
                _errorLabelStyleExists = true;
                _errorLabelStyle = "label";
                _errorLabelStyle.wordWrap = true;
            }
            return _errorLabelStyle;
        }
    }
}
}
