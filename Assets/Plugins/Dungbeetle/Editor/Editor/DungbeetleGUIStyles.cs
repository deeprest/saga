using UnityEngine;

namespace Dungbeetle {

public static class DungbeetleGUIStyles {

    private static GUIStyle _errorLabel;
    public static GUIStyle errorLabel {
        get {
            if (_errorLabel == null) {
                _errorLabel = new GUIStyle(GUI.skin.label);
                _errorLabel.fontStyle = FontStyle.Bold;
                if (UnityEditor.EditorGUIUtility.isProSkin)
                    _errorLabel.normal.textColor = new Color32(255, 75, 75, 255);
                else
                    _errorLabel.normal.textColor = new Color32(157, 19, 19, 255);
            }

            return _errorLabel;
        }
    }

    private static GUIStyle _seriousWarningMessage;
    public static GUIStyle seriousWarningMessage {
        get {
            if (_seriousWarningMessage == null) {
                _seriousWarningMessage = "NotificationText";
                _seriousWarningMessage.fixedWidth = 0f;
                _seriousWarningMessage.padding.left = 0;
                _seriousWarningMessage.padding.right = 0;
                _seriousWarningMessage.margin.left = 4;
                _seriousWarningMessage.margin.right = 4;
                if (UnityEditor.EditorGUIUtility.isProSkin)
                    _seriousWarningMessage.normal.textColor = new Color32(255, 75, 75, 255);
                else
                    _seriousWarningMessage.normal.textColor = new Color32(157, 19, 19, 255);
            }

            return _seriousWarningMessage;
        }
    }

    private static GUIStyle _successLabel;
    public static GUIStyle successLabel {
        get {
            if (_successLabel == null) {
                _successLabel = new GUIStyle(GUI.skin.label);
                _successLabel.fontStyle = FontStyle.Bold;
                if (UnityEditor.EditorGUIUtility.isProSkin)
                    _successLabel.normal.textColor = new Color32(0, 180, 0, 255);
                else
                    _successLabel.normal.textColor = new Color32(0, 105, 0, 255);
            }
            return _successLabel;
        }
    }

    private static GUIStyle _paragraphHeader;
    public static GUIStyle paragraphHeader {
        get {
            if (_paragraphHeader == null) {
                _paragraphHeader = new GUIStyle(GUI.skin.label);
                _paragraphHeader.fontStyle = FontStyle.Bold;
            }
            return _paragraphHeader;
        }
    }
}
}