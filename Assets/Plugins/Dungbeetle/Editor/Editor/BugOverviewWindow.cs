using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class BugOverviewWindow : EditorWindow, IBugOverview {

    public BugOverviewGUI gui;
    public bool isDestroyed;

    public Bug superPositionBug;
    public Bug superPositionNextBug;
    public Bug superPositionPreviousBug;

    private List<BugAssignWindow> assignWindows;
    private List<BugDetailsWindow> detailsWindows;

    public static BugOverviewWindow window;

    [MenuItem("Window/Dungbeetle/Bug Reports", false, 100)]
    public static void Init() {
        var setPos = !EditorPrefs.HasKey("Dungbeetle.BugOverviewWindoww");
        window = GetWindow<BugOverviewWindow>("Dungbeetle", true);
        if (setPos)
            window.position = new Rect(100f, 100f, 925f, 310f);
        window.LoadGUI();
    }

    private void LoadGUI() {
        window = this;
        if (gui != null)
            gui.Load();
        else
            gui = new BugOverviewGUI();

        if (assignWindows == null)
            assignWindows = new List<BugAssignWindow>();
        if (detailsWindows == null)
            detailsWindows = new List<BugDetailsWindow>();

        if (gui != null && !ReferenceEquals(gui.window, this)) {
            gui.window = this;

            var newAssignWindows = new List<BugAssignWindow>();
            foreach(var w in assignWindows) {
                if (w != null) {
                    w.parentOverview = this;
                    newAssignWindows.Add(w);
                }
            }

            assignWindows = newAssignWindows;

            var newDetailsWindows = new List<BugDetailsWindow>();
            foreach(var w in detailsWindows) {
                if (w != null) {
                    w.parentOverview = this;
                    newDetailsWindows.Add(w);
                }
            }

            detailsWindows = newDetailsWindows;
        }
    }

    private void OnEnable() => LoadGUI();

    private void OnDestroy() => isDestroyed = true;

    public bool IsDestroyed() => isDestroyed;

    public List<ProjectDeveloper> DeveloperListExplicit() => gui.DeveloperListExplicit(false);

    public string DeveloperName(int id) => gui.DeveloperName(id);

    public void OnGUI() => gui.OnGUI();

    public void Refresh() => Repaint();

    public void ChangeBugState(Bug b) {
        gui.MoveBug(b);
        OnBugChanged(b, false);
    }

    public void RemoveFromList(Bug b) {
        gui.RemoveFromList(b);
        OnBugChanged(b, true);
    }

    public void RegisterSuperPositionBug(Bug b) {
        superPositionBug         = b;
        superPositionPreviousBug = gui.window_previous_bug(b);
        superPositionNextBug     = gui.window_next_bug(b);
    }

    public void UpdateBugComment(int id, string comment) {
        gui.UpdateBugComment(id, comment);
    }

    public void UpdatePriority(int id, string p) {
        gui.UpdatePriority(id, p);
    }

    public void UpdateContactResponsible(int id, int r) {
        gui.UpdateContactResponsible(id, r);
    }

    public void UpdateBugFixer(Bug b) {
        gui.UpdateBugFixer(b);
        OnBugChanged(b, false);
    }

    private void OnBugChanged(Bug bug, bool deleted) {
        foreach(var w in detailsWindows) {
            if (w == null)
                continue;

            if (deleted)
                w.OnBugDeleted(bug);
            else
                w.OnBugChanged(bug);
        }
        Refresh();
    }

    public void ShowOverview() { }

    public void ShowScreenshot(Bug b) {
        BugScreenshotWindow.Init(b, this);
    }

    public void ShowAssigner(Bug b, BugAssignAction a) {
        var w = BugAssignWindow.Init(b, a, this);

        if (assignWindows.Contains(w))
            assignWindows.Add(w);
    }

    public void ShowDetails(Bug b) {
        var w = BugDetailsWindow.Init(b, this);
        if (!detailsWindows.Contains(w))
            detailsWindows.Add(w);
    }

    public void ShowDetailsPrevious(Bug b) {
        if (b == superPositionBug && superPositionPreviousBug != null) {
            ShowDetails(superPositionPreviousBug);
            superPositionBug = superPositionPreviousBug = superPositionNextBug = null;
        }
        else {
            var previous_bug = gui.window_previous_bug(b);
            if (previous_bug != null)
                ShowDetails(previous_bug);
        }
    }

    public void ShowDetailsNext(Bug b) {
        if (b == superPositionBug && superPositionNextBug != null) {
            ShowDetails(superPositionNextBug);
            superPositionBug = superPositionPreviousBug = superPositionNextBug = null;
        }
        else {
            var next_bug = gui.window_next_bug(b);
            if (next_bug != null)
                ShowDetails(next_bug);
        }
    }

    public void ShowAttachments(Bug b) {
        BugAttachmentsWindow.Init(b);
    }
}
}