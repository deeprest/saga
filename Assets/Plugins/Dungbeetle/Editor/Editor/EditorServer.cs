using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

[InitializeOnLoad]
public class EditorServer : ScriptableObject {

    static EditorServer() {
        EditorApplication.delayCall += EditorServerClassInitialize;
    }

    private static void EditorServerClassInitialize() {
        var servers = Resources.FindObjectsOfTypeAll<EditorServer>();
        var serversCount = servers.Length;
        if (serversCount > 1)
            Debug.LogError("Something has gone wrong, because two Dungbeetle server objects exist!");
        else {
            if (serversCount == 1)
                _instance = servers[0];
            else
                _instance = CreateInstance<EditorServer>();
            if (hangingLoggedEvent != null) {
                _instance.serverGUICore.Logged = hangingLoggedEvent; // Override old event listeners
                hangingLoggedEvent = null;
            }
        }
        _classInitialized = true;
    }

    private static EditorServer _instance;
    private static bool _classInitialized;
    private static ServerGUICore.LoggedEvent hangingLoggedEvent;

    public static bool ClassInitialized => _classInitialized;

    public static bool Running() {
        return _instance != null && _instance.running;
    }

    public static void RegisterExclusiveOnLoggedEvent(ServerGUICore.LoggedEvent f) {
        if (_instance && _instance.serverGUICore != null)
            _instance.serverGUICore.Logged = f; // Override old event listeners
        else
            // RegisterExclusiveOnLoggedEvent happens during window 'OnEnable' which might be before 'EditorServerClassInitialize,
            // where _instance is created/initialized
            hangingLoggedEvent = f;
    }

    public static void ClearOnLoggedEvents() {
        if (_instance != null && _instance.serverGUICore != null) {
            _instance.serverGUICore.Logged = null;
            hangingLoggedEvent = null;
        }
    }

    public static void ServerGUI(float height, float width) {
        if (_instance != null && _instance.serverGUICore != null)
            _instance.serverGUICore.ServerGUI(height, width);
        else if (_instance != null)
            GUILayout.TextArea("EditorServer not initialized", GUILayout.ExpandHeight(true));
        else
            GUILayout.TextArea("Server not running.", GUILayout.ExpandHeight(true));
    }

    public static void ScrollDown() {
        if (_instance != null && _instance.serverGUICore != null)
            _instance.serverGUICore.ScrollDown();
    }

    public static void Run() {
        _instance.Run_(true);
    }

    public static void Stop() {
        _instance.Stop_(true);
    }

    public static void ClearCookies() {
        _instance.serverGUICore.ClearCookies();
    }

    [SerializeField]
    private ServerGUICore serverGUICore;
    [SerializeField]
    private bool running; // For catching errors

    public void Run_(bool explicitly) {
        if (!running) {
            serverGUICore.Run(explicitly);
            running = true;
        }
    }

    public void Stop_(bool explicitly) {
        if (running) {
            serverGUICore.Stop(explicitly);
            running = false;
        }
    }

    private void OnEnable() {
        hideFlags = HideFlags.HideAndDontSave;
        if (serverGUICore == null)
            serverGUICore = new ServerGUICore();
        if (Config.EditorServerConfig.Enabled)
            Run_(false);
    }

    private void OnDisable() {
        Stop_(false);
    }
}
}