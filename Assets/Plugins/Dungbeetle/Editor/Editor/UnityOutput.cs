using UnityEngine;

namespace Dungbeetle {
    public class UnityDebug : IOutput {
        public void Log(string s) => UnityEngine.Debug.Log(s);
    }
}
