using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

[CustomEditor(typeof(ReporterConnectionConfig))]
public class ReporterConnectionConfigEditor : Editor {

    public override void OnInspectorGUI() {
        DrawDefaultInspector();
        var rcc = (ReporterConnectionConfig) target;
        if (rcc.Connected) {
            if (GUILayout.Button("Open Connection Config"))
                ConfigWindow.CreateWindow();
        }
        else if (GUILayout.Button("Reconnect with Config.asset")) {
            ReporterConnectionConfig.ConnectConfig(Config.ReporterClientConfig);
        }
    }
}
}
