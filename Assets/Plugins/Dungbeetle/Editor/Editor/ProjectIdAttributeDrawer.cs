using System.Linq;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

[CustomPropertyDrawer (typeof(ProjectIdAttribute))]
public class ProjectIdAttributeDrawer : PropertyDrawer {

    public static bool projectsNeedRefresh;
    public static bool serverHasChanged;

    private bool socketFailed;
    private string socketFailMsg;

    private string[] _allProjectNames;
    private string[] allProjectNames {
        get {
            if (_allProjects == null)
                GetDataFromServer();
            return _allProjectNames;
        }
    }

    private Project[] _allProjects;
    private Project[] allProjects {
        get {
            if (_allProjects == null)
                GetDataFromServer();
            return _allProjects;
        }
    }

    private GUIContent noLabelContent = GUIContent.none;

    private void GetDataFromServer() {
        socketFailed = false; // Even get a chance to un-fail the socket
        var _projects = BugUtility.GetProjects(false, ref socketFailed, ref socketFailMsg, 3000);
        projectsNeedRefresh = false;
        if (socketFailed)
            return;

        _allProjects = _projects.ToArray();
        _allProjectNames = _allProjects.Select(p => p.name).ToArray();
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
        var originalWidth = position.width;

        if (!label.Equals(noLabelContent)) {
            position.width = EditorGUIUtility.labelWidth;
            EditorGUI.LabelField(position, label);
            position.x += position.width;
            position.width = originalWidth - position.width;
        }

        if (serverHasChanged) {
            if (GUI.Button(position, "Reconnect and reload projects")) {
                serverHasChanged = false;
                projectsNeedRefresh = true;
            }
            return;
        }

        if (projectsNeedRefresh) {
            projectsNeedRefresh = false;
            GetDataFromServer();
        }

        if (socketFailed)
            EditorGUI.LabelField(position, socketFailMsg);
        else if (allProjects == null || allProjects.Length == 0 || allProjectNames == null || allProjectNames.Length == 0)
            EditorGUI.LabelField(position, "No projects found!");
        else {
            var currentIdx = 0; //if we're not in the list of projects, become the first project
            var currentID = property.intValue;
            var found = false;

            for (int i = 0; i < allProjects.Length; i++) {
                if (allProjects[i].id == currentID) {
                    currentIdx = i;
                    found = true;
                }
            }

            EditorGUI.BeginChangeCheck();
            var newIdx = EditorGUI.Popup(position, currentIdx, allProjectNames);

            if (EditorGUI.EndChangeCheck()) {
                var overviewWindow = BugOverviewWindow.window;
                var overviewGUI = overviewWindow != null ? overviewWindow.gui : null;
                if (overviewGUI != null) {
                    overviewGUI.Clear();
                    overviewWindow.Repaint();
                }
            }

            if (newIdx != currentIdx || !found) {
                property.intValue = allProjects[newIdx].id;
            }
        }
    }
}
}