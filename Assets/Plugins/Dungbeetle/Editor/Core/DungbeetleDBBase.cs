using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;

namespace Dungbeetle {
public static class IDbCommandExtension {
    public static void AddWithValue<T>(this IDbCommand cmd, string name, T value) {
        var parameter = cmd.CreateParameter();
        parameter.ParameterName = name;
        parameter.Value = value;
        cmd.Parameters.Add(parameter);
    }
}

public abstract class DungbeetleDBBase : IDatabaseLink {
    protected IOutput output;

    public DungbeetleDBBase(IOutput o) {
        output = o;
    }

    protected abstract IDbCommand Command(string query, IDbConnection connection);
    protected abstract IDbConnection Connection(string connectionString);
    protected abstract bool HandleException(Exception e);

    protected abstract string Db_Connection_String { get; }
    protected abstract bool   Db_Bug_Autoincrement { get; }
    protected abstract string DB_INT               { get; }
    protected abstract string DB_SHORT_STRING      { get; }
    protected abstract string DB_TEXT              { get; }
    protected abstract string DB_AUTOINCREMENT     { get; }
    protected abstract string DB_TABLE_SUFFIX      { get; }
    protected abstract string DB_SUBSTRING         { get; }

    protected abstract void CreateAndUseDBTable(IDbConnection connection, string dbName);
    public abstract bool DatabaseAligned();

    private class SafeStringReader {
        private IDataReader rdr;

        public SafeStringReader(IDataReader r) {
            rdr = r;
        }

        public string this[int i] => rdr.IsDBNull(i) ? string.Empty : rdr.GetString(i);
    }

    /***************************************************
    **                                                **
    **              Utility Methods                   **
    **                                                **
    ***************************************************/

    protected void UsingConnection(Action<IDbConnection> action) {
        IDbConnection connection = null;
        try {
            connection = Connection(Db_Connection_String);
            connection.Open();
            action(connection);
        }
        catch (Exception e) {
            if (!HandleException(e))
                throw;
        }
        finally {
            connection?.Dispose();
        }
    }

    protected void ReadyQuery(string query, Action<IDbCommand> cmdAction) {
        UsingConnection(connection => {
            using (var cmd = Command(query, connection))
                cmdAction(cmd);
        });
    }

    protected void ReadyQuery(string query, Action<IDbCommand, IDataReader> cmdAction) {
        UsingConnection(connection => {
            using (var cmd = Command(query, connection))
            using (var rdr = cmd.ExecuteReader())
                cmdAction(cmd, rdr);
        });
    }

    protected T ReadyQuery<T>(string query, Func<IDbCommand, T> cmdAction) {
        var result = default(T);
        UsingConnection(connection => {
            using (var cmd = Command(query, connection))
                result = cmdAction(cmd);
        });

        return result;
    }

    protected T ReadyQuery<T>(string query, Func<IDbCommand, IDataReader, T> cmdAction) {
        var result = default(T);
        UsingConnection(connection => {
            using (var cmd = Command(query, connection))
            using (var rdr = cmd.ExecuteReader())
                result = cmdAction(cmd, rdr);
        });

        return result;
    }

    protected void NonQuery(string query, IDbConnection connection) {
        using (var cmd = Command(query, connection))
            cmd.ExecuteNonQuery();
    }

    private List<Bug> GetBugs(int projectId, string querySuffix) {
        var bugs = new List<Bug>();
        UsingConnection(connection => {
            var query = $"SELECT id, state, time, address, email, imagename, priority, scene, build_name, description, comment, " +
                        $"accept_responsible, fix_responsible, fix_source, resolution_responsible, contact_responsible " +
                        $"FROM bug WHERE project_id={projectId} AND {querySuffix}";
            using (var cmd = Command(query, connection))
            using (var rdr = cmd.ExecuteReader()) {
                var s = new SafeStringReader(rdr);
                while (rdr.Read()) {
                    var state = (BugState) rdr.GetInt32(1);

                    bugs.Add(new Bug(Convert.ToInt32(rdr[0]), projectId, state, rdr.GetDateTime(2), s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], Convert.ToInt32(rdr[11]),
                                     Convert.ToInt32(rdr[12]), s[13], Convert.ToInt32(rdr[14]), Convert.ToInt32(rdr[15])));
                }
            }
        });
        return bugs;
    }

    public void UpdateBug<T> (int projectId, int id, string parameterName, T parameterValue) {
        var command = $"UPDATE bug SET {parameterName}=@param1 WHERE project_id=@projectId AND id=@bugId";
        ReadyQuery(command, cmd => {
            cmd.AddWithValue("@projectId", projectId);
            cmd.AddWithValue("@bugId", id);
            cmd.AddWithValue("@param1", parameterValue);
            cmd.ExecuteNonQuery();
        });
    }

    public void TableDefinitions(IDbConnection connection, string dbName) {
        NonQuery("BEGIN", connection);
        CreateAndUseDBTable(connection, dbName);

        NonQuery($@"
CREATE TABLE project (
  id {DB_INT} PRIMARY KEY {DB_AUTOINCREMENT},
  name {DB_SHORT_STRING} UNIQUE
){DB_TABLE_SUFFIX}", connection);

        NonQuery("INSERT INTO project (name) VALUES ('default')", connection);

        var autoIncrementSegment = Db_Bug_Autoincrement ? $" {DB_AUTOINCREMENT}" : "";

        NonQuery($@"
CREATE TABLE bug (
  project_id {DB_INT} NOT NULL,
  id {DB_INT} NOT NULL{autoIncrementSegment},
  state {DB_INT} NOT NULL DEFAULT 0,
  time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  address {DB_SHORT_STRING},
  email {DB_SHORT_STRING},
  imagename {DB_SHORT_STRING},
  priority {DB_SHORT_STRING},
  scene {DB_SHORT_STRING},
  build_name {DB_SHORT_STRING},
  description {DB_TEXT},
  comment {DB_TEXT},
  accept_responsible {DB_INT},
  fix_responsible {DB_INT},
  fix_source {DB_SHORT_STRING},
  resolution_responsible {DB_INT},
  contact_responsible {DB_INT},
  PRIMARY KEY (project_id, id),
  FOREIGN KEY (project_id) REFERENCES project(id)
){DB_TABLE_SUFFIX}", connection);

        NonQuery($@"
CREATE TABLE developer (
  id {DB_INT} PRIMARY KEY {DB_AUTOINCREMENT},
  project_id {DB_INT} NOT NULL,
  name {DB_SHORT_STRING},
  FOREIGN KEY(project_id) REFERENCES project(id)
){DB_TABLE_SUFFIX}", connection);

        NonQuery($@"
CREATE TABLE attachment (
  id {DB_INT} PRIMARY KEY {DB_AUTOINCREMENT},
  project_id {DB_INT} NOT NULL,
  bug_id {DB_INT} NOT NULL,
  content {DB_TEXT},
  FOREIGN KEY(project_id, bug_id) REFERENCES bug(project_id, id)
){DB_TABLE_SUFFIX}", connection);

        NonQuery("COMMIT", connection);
    }

    public abstract int LastProjectId(IDbCommand cmd);
    public abstract int LastDeveloperId(IDbCommand cmd);
    public abstract int LastBugId(IDbCommand cmd);
    public abstract string InsertBugQuery(bool reuseId, bool reuseState);

    /***************************************************
    **                                                **
    **              IDatabaseLink Methods             **
    **                                                **
    ***************************************************/

    public int CreateMergedDeveloper(int projectId, string name) {
        var query = "SELECT EXISTS(SELECT 1 FROM developer WHERE project_id=@projectId AND name=@name LIMIT 1)";

        return ReadyQuery(query, cmd => {
            int developerId;
            cmd.AddWithValue("@projectId", projectId);
            cmd.AddWithValue("@name", name);
            var exists = Convert.ToInt32(cmd.ExecuteScalar()) == 1;
            if (exists) {
                cmd.CommandText = "SELECT id FROM developer WHERE project_id=@projectId AND name=@name";
                developerId = Convert.ToInt32(cmd.ExecuteScalar());
            }
            else {
                cmd.CommandText = "INSERT INTO developer (project_id, name) VALUES (@projectId, @name)";
                cmd.ExecuteNonQuery();
                developerId = LastDeveloperId(cmd);
            }

            return developerId;
        });
    }

    public int CreateNewDeveloper(int projectId, string name) {
        var query = "INSERT INTO developer (project_id, name) VALUES (@projectId, @name)";

        return ReadyQuery(query, cmd => {
            cmd.AddWithValue("@projectId", projectId);
            cmd.AddWithValue("@name", name);
            cmd.ExecuteNonQuery();
            return LastDeveloperId(cmd);
        });
    }

    public int CreateNewProject(string name) {
        var query = "INSERT INTO project (name) VALUES (@name)";

        return ReadyQuery(query, cmd => {
            cmd.AddWithValue("@name", name);
            cmd.ExecuteNonQuery();
            var projectId = LastProjectId(cmd);
            return projectId;
        });
    }

    public int CreateUniqueProject(string name) {
        var query = "SELECT EXISTS(SELECT 1 FROM project WHERE name=@name LIMIT 1)";

        return ReadyQuery(query, cmd => {
            while (true) {
                cmd.AddWithValue("@name", name);
                var exists = Convert.ToInt32(cmd.ExecuteScalar()) == 1;
                if (!exists)
                    break;
                cmd.Parameters.Clear();
                var m = Regex.Match(name, "(.*\b)([0-9]+)");

                if (m.Success)
                    name = $"{m.Groups[1]}{int.Parse(m.Groups[2].ToString()) + 1}";
                else
                    name = $"{name} 1";
            }

            cmd.CommandText = "INSERT INTO project (name) VALUES (@name)";
            cmd.ExecuteNonQuery();
            return LastProjectId(cmd);
        });
    }

    public void DeleteBug(int projectId, int id) {
        UsingConnection(connection => {
            NonQuery("BEGIN", connection);

            using (var cmd = Command("DELETE FROM bug WHERE project_id=@projectId AND id=@bugId", connection)) {
                cmd.AddWithValue("@projectId", projectId);
                cmd.AddWithValue("@bugId", id);
                cmd.ExecuteNonQuery();
            }

            using (var cmd = Command("DELETE FROM attachment WHERE project_id=@projectId AND bug_id=@bugId", connection)) {
                cmd.AddWithValue("@projectId", projectId);
                cmd.AddWithValue("@bugId", id);
                cmd.ExecuteNonQuery();
            }

            NonQuery("COMMIT", connection);
        });
    }

    public void DeleteDeveloper(int id) {
        var query = "DELETE FROM developer WHERE id=@id";
        ReadyQuery(query, cmd => {
            cmd.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();
        });
    }

    private void DeleteImageFilesInProject(int projectId, IDbConnection connection) {
        var query = "SELECT imagename FROM bug WHERE project_id=@projectId";
        using (var cmd = Command(query, connection)) {
            cmd.AddWithValue("@projectId", projectId);
            using (var rdr = cmd.ExecuteReader()) {
                while (rdr.Read()) {
                    try {
                        File.Delete(ImageFile.GetExistingPath((string) rdr[0]));
                    }
                    catch (Exception e) {
                        output.Log($"Failed to delete {(rdr[0])}: {(e.Message)}");
                    }
                }
            }
        }
    }

    public void DeleteProject(int projectId) {
        UsingConnection(connection => {
            DeleteImageFilesInProject(projectId, connection);
            NonQuery("BEGIN", connection);
            NonQuery($"DELETE FROM attachment WHERE project_id={projectId}", connection);
            NonQuery($"DELETE FROM bug WHERE project_id={projectId}", connection);
            NonQuery($"DELETE FROM project WHERE id={projectId}", connection);
            NonQuery($"DELETE FROM developer WHERE project_id={projectId}", connection);
            NonQuery("COMMIT", connection);
        });
    }

    public List<Bug> GetAssigned(int projectId) {
        return GetBugs(projectId, "state=1");
    }


    public GetAttachmentResults GetAttachments(int projectId, int id, int maxLength) {
        var query = string.Format($"SELECT id, {DB_SUBSTRING} FROM attachment WHERE project_id=@projectId AND bug_id=@id", maxLength);
        return ReadyQuery(query, cmd => {
            cmd.AddWithValue("@projectId", projectId);
            cmd.AddWithValue("@id", id);
            using (var rdr = cmd.ExecuteReader()) {
                var attachment_contents = new List<string>();
                var attachment_ids = new List<int>();
                while (rdr.Read()) {
                    attachment_ids.Add(rdr.GetInt32(0));
                    attachment_contents.Add((string) rdr[1]);
                }

                return new GetAttachmentResults {
                    attachment_contents = attachment_contents,
                    attachment_ids = attachment_ids
                };
            }
        });
    }

    public string GetAttachment(int id) {
        var query = "SELECT content FROM attachment WHERE id=@id";
        return ReadyQuery(query, cmd => {
            cmd.AddWithValue("@id", id);
            using (var rdr = cmd.ExecuteReader())
                return rdr.Read() ? (string) rdr[0] : "";
        });
    }

    public int GetBugCount(params int[] projects) {
        var query = $"SELECT COUNT(*) FROM bug WHERE project_id IN ({string.Join(",", projects)})";
        return ReadyQuery(query, cmd => Convert.ToInt32(cmd.ExecuteScalar()));
    }

    public List<Bug> GetBugsForMigration(int[] projectIds, int limit, int offset) {
        var query = $"SELECT id, project_id, state, time, address, email, imagename, priority, scene, build_name, description, comment, " +
                    $"accept_responsible, fix_responsible, fix_source, resolution_responsible, contact_responsible FROM bug WHERE project_id IN " +
                    $"({string.Join(",", projectIds)}) LIMIT @limit OFFSET @offset";

        return ReadyQuery(query, cmd => {
            cmd.AddWithValue("@limit", limit);
            cmd.AddWithValue("@offset", offset);

            var bugs = new List<Bug>(limit);
            using (var rdr = cmd.ExecuteReader()) {
                var s = new SafeStringReader(rdr);
                while (rdr.Read()) {
                    var state = (BugState) rdr.GetInt32(2);
                    bugs.Add(new Bug(Convert.ToInt32(rdr[0]), Convert.ToInt32(rdr[1]), state, rdr.GetDateTime(3), s[4], s[5], s[6], s[7], s[8], s[9], s[10], s[11], Convert.ToInt32(rdr[12]),
                                     Convert.ToInt32(rdr[13]), s[14], Convert.ToInt32(rdr[15]), Convert.ToInt32(rdr[16])));
                }
            }

            cmd.Parameters.Clear();
            cmd.CommandText = "SELECT content FROM attachment WHERE bug_id=@bugId AND project_id=@projectId";

            foreach (var bug in bugs) {
                cmd.AddWithValue("@bugId", bug.id);
                cmd.AddWithValue("@projectId", bug.projectId);
                using (var attachmentRdr = cmd.ExecuteReader()) {
                    var attachments = new List<string>();
                    while (attachmentRdr.Read())
                        attachments.Add((string) attachmentRdr[0]);
                    bug.attachments = attachments;
                }

                cmd.Parameters.Clear();
            }

            return bugs;
        });
    }

    public List<Bug> GetFixed(int projectId) => GetBugs(projectId, "state=2");

    public List<Bug> GetForFixer(int projectId, int fixer) {
        var query = $"SELECT id, state, time, address, email, imagename, priority, scene, build_name, description, comment, accept_responsible, " +
                    $"fix_responsible, fix_source, resolution_responsible, contact_responsible FROM bug WHERE project_id={projectId} AND state=1 AND " +
                    $"fix_responsible={fixer}";

        return ReadyQuery(query, cmd => {
            var bugs = new List<Bug>();
            using (var rdr = cmd.ExecuteReader()) {
                var s = new SafeStringReader(rdr);
                while (rdr.Read()) {
                    var state = (BugState) rdr.GetInt32(1);
                    bugs.Add(new Bug(Convert.ToInt32(rdr[0]), projectId, state, rdr.GetDateTime(2), s[3], s[4], s[5], s[6], s[7], s[8], s[9], s[10], Convert.ToInt32(rdr[11]),
                                     Convert.ToInt32(rdr[12]), s[13], Convert.ToInt32(rdr[14]), Convert.ToInt32(rdr[15])));
                }
            }

            return bugs;
        });
    }

    public string GetImageName(int projectId, int id) {
        var query = "SELECT imagename FROM bug WHERE project_id=@projectId AND id=@id";

        return ReadyQuery(query, cmd => {
            cmd.AddWithValue("@projectId", projectId);
            cmd.AddWithValue("@id", id);
            using (var rdr = cmd.ExecuteReader()) {
                if (rdr.Read())
                    return (string) rdr[0];
                else
                    return "";
            }
        });
    }

    public List<Bug> GetOpen(int projectId) => GetBugs(projectId, "state <> 3");

    public List<ProjectDeveloper> GetProjectDevelopers(int projectId) {
        var query = $"SELECT id, name FROM developer WHERE project_id={projectId}";
        return ReadyQuery(query, cmd => {
            var devs = new List<ProjectDeveloper>();
            using (IDataReader rdr = cmd.ExecuteReader())
                while (rdr.Read())
                    devs.Add(new ProjectDeveloper(rdr.GetInt32(0), (string) rdr[1]));
            return devs;
        });
    }

    public List<Project> GetProjects(bool includeDevelopers) {
        return GetProjectsSubset(includeDevelopers, string.Empty);
    }

    public List<Project> GetProjects(bool includeDevelopers, int[] projectIds) {
        return GetProjectsSubset(includeDevelopers, $"WHERE id IN ({string.Join(",", projectIds)})");
    }

    private List<Project> GetProjectsSubset(bool includeDevelopers, string suffix) {
        var projects = new List<Project>();

        UsingConnection(connection => {
            var query = $"SELECT id, name FROM project {suffix}";

            using (var cmd = Command(query, connection))
            using (var rdr = cmd.ExecuteReader()) {
                while (rdr.Read()) {
                    var project = new Project {
                        id = rdr.GetInt32(0),
                        name = rdr[1] is string ? (string) rdr[1] : "Unnamed Database", //happens if the database is NULL. The string NULL is handled poorly.
                        developers = new List<ProjectDeveloper>()
                    };
                    projects.Add(project);
                }
            }

            if (includeDevelopers) {
                foreach (var project in projects) {
                    query = $"SELECT id, name FROM developer WHERE project_id={project.id}";

                    using (var cmd = Command(query, connection))
                    using (var rdr = cmd.ExecuteReader()) {
                        while (rdr.Read())
                            project.developers.Add(new ProjectDeveloper(rdr.GetInt32(0), (string) rdr[1]));
                    }
                }
            }
        });

        return projects;
    }

    public List<Bug> GetResolved(int projectId)
        => GetBugs(projectId, "state=3");

    public List<Bug> GetUnassigned(int projectId)
        => GetBugs(projectId, "state=0");

    public void MigrateBug(Bug bug, bool reuseId)
        => InsertBug(bug, reuseId, true);

    public void RenameDeveloper(int id, string name) {
        var query = "UPDATE developer SET name=@name WHERE id=@id";
        ReadyQuery(query, cmd => {
            cmd.AddWithValue("@name", name);
            cmd.AddWithValue("@id", id);
            cmd.ExecuteNonQuery();
        });
    }

    public void RenameProject(int projectId, string name) {
        var query = "UPDATE project SET name=@name WHERE id=@projectId";
        ReadyQuery(query, cmd => {
            cmd.AddWithValue("@name", name);
            cmd.AddWithValue("@id", projectId);
            cmd.ExecuteNonQuery();
        });
    }

    public void ResetDatabase() {
        UsingConnection(connection => {
            NonQuery("BEGIN", connection);
            NonQuery("DELETE FROM attachment", connection);
            NonQuery("DELETE FROM bug", connection);
            NonQuery("DELETE FROM project", connection);
            NonQuery("DELETE FROM developer", connection);
            NonQuery("COMMIT", connection);
        });
    }

    public void InsertBug(Bug bug) {
        InsertBug(bug, false, false);
    }

    public void InsertBug(Bug bug, bool reuseId, bool reuseState) {
        UsingConnection(connection => {
            NonQuery("BEGIN", connection);
            var query = InsertBugQuery(reuseId, reuseState);
            var cmd = connection.CreateCommand();
            cmd.CommandText = query;

            if (reuseId)
                cmd.AddWithValue("@bugId", bug.id);
            if (reuseState)
                cmd.AddWithValue("@bugState", bug.state);
            cmd.AddWithValue("@projectId",              bug.projectId);
            cmd.AddWithValue("@address",                bug.address);
            cmd.AddWithValue("@email",                  bug.email);
            cmd.AddWithValue("@imageName",              bug.imageName);
            cmd.AddWithValue("@priority",               bug.priority);
            cmd.AddWithValue("@scene",                  bug.scene);
            cmd.AddWithValue("@build_name",             bug.build_name);
            cmd.AddWithValue("@description",            bug.description);
            cmd.AddWithValue("@comment",                bug.comment);
            cmd.AddWithValue("@accept_responsible",     bug.accept_responsible);
            cmd.AddWithValue("@fix_responsible",        bug.fix_responsible);
            cmd.AddWithValue("@fix_source",             bug.fix_source);
            cmd.AddWithValue("@resolution_responsible", bug.resolution_responsible);
            cmd.AddWithValue("@contact_responsible",    bug.contact_responsible);
            cmd.ExecuteNonQuery();

            var bugId = reuseId ? bug.id : LastBugId(cmd);

            cmd.Parameters.Clear();

            query = "INSERT INTO attachment (project_id, bug_id, content) VALUES(@projectId, @bugId, @content)";
            cmd = connection.CreateCommand();
            cmd.CommandText = query;
            cmd.Prepare();

            cmd.AddWithValue("@projectId", bug.projectId);
            cmd.AddWithValue("@bugId", bugId);

            var contentParam = cmd.CreateParameter();
            contentParam.ParameterName = "@content";
            cmd.Parameters.Add(contentParam);

            foreach (var attachment in bug.attachments) {
                contentParam.Value = attachment;
                cmd.ExecuteNonQuery();
            }

            cmd.CommandText = "COMMIT";
            cmd.ExecuteNonQuery();
        });
    }

    public void UpdateComment(int projectId, int id, string comment)
        => UpdateBug(projectId, id, "comment", comment);

    public void UpdateContactResponsible(int projectId, int id, int contact_responsible)
        => UpdateBug(projectId, id, "contact_responsible", contact_responsible);

    public void UpdatePriority(int projectId, int id, string priority)
        => UpdateBug(projectId, id, "priority", priority);

    public void UpdateReopen(int projectId, int id)
        => UpdateBug(projectId, id, "state", 0);

    public void UpdateResponsible(int projectId, int id, int state, string comment, string priority, int accept_responsible, int fix_responsible, string fix_source,
                                  int resolution_responsible) {
        var query = $"UPDATE bug SET state=@state, comment=@comment, priority=@priority, accept_responsible=@accept_responsible, " +
                    $"fix_responsible=@fix_responsible, fix_source=@fix_source, resolution_responsible=@resolution_responsible WHERE " +
                    $"project_id=@projectId AND id=@id";

        ReadyQuery(query, cmd => {
            cmd.AddWithValue("@projectId", projectId);
            cmd.AddWithValue("@id", id);
            cmd.AddWithValue("@state", state);
            cmd.AddWithValue("@comment", comment);
            cmd.AddWithValue("@priority", priority);
            cmd.AddWithValue("@accept_responsible", accept_responsible);
            cmd.AddWithValue("@fix_responsible", fix_responsible);
            cmd.AddWithValue("@fix_source", fix_source);
            cmd.AddWithValue("@resolution_responsible", resolution_responsible);
            cmd.ExecuteNonQuery();
        });
    }
}
}
