using System;
using System.Data;

namespace Dungbeetle {

public class MySQLDatabaseLink : MySQLDatabaseLinkBase {

    public MySQLDatabaseLink(IOutput o) : base(o) { }

    public bool DatabaseUserExists(string dbURL, string dbUser, string dbPassword) {
        IDbConnection connection = null;

        try {
            connection = Connection($"SERVER={dbURL};UID={dbUser};PASSWORD={dbPassword};");
            connection.Open();
            return true;
        }
        catch (Exception e) {
            HandleException(e);
        }
        finally {
            connection?.Close();
        }

        return false;
    }

    public bool DatabaseExists(string dbURL, string dbUser, string dbPassword, string dbName) {
        IDbConnection connection = null;

        try {
            connection = Connection($"SERVER={dbURL};UID={dbUser};PASSWORD={dbPassword};");
            connection.Open();

            var cmd = Command($"SELECT schema_name FROM information_schema.schemata WHERE schema_name = '{dbName}'", connection);
            using (var rdr = cmd.ExecuteReader())
                return rdr.Read();
        }
        catch (Exception e) {
            HandleException(e);
        }
        finally {
            connection?.Close();
        }

        return false;
    }

    public void CreateDatabase(string dbURL, string dbUser, string dbPassword, string dbName) {
        IDbConnection connection = null;

        try {
            connection = Connection($"SERVER={dbURL};UID={dbUser};PASSWORD={dbPassword};");
            connection.Open();
            TableDefinitions(connection, dbName);
        }
        catch (Exception e) {
            HandleException(e);
        }
        finally {
            connection?.Close();
        }
    }
}
}
