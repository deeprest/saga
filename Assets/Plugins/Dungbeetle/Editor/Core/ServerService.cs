using System.ServiceProcess;
using System.Threading;

namespace Dungbeetle {

public class ServerService : ServiceBase {

    public ServerService() {
        this.ServiceName = "Dungbeetle";
        this.CanStop = true;
        this.CanPauseAndContinue = true;
        this.AutoLog = true;
    }

    protected override void OnStart(string[] args) {
        var key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Services\\Dungbeetle");
        DungbeetlePath.Log         = key.GetValue("LogFile") as string;
        DungbeetlePath.Preferences = key.GetValue("Preferences") as string;
        DungbeetlePath.Home        = key.GetValue("Home") as string;
        key.Close();

        new Thread(Run).Start();
    }

    public  void Run() {
        var o = new FileOutput(DungbeetlePath.Log);
        o.Log("Running as a service");
        Server.Run(o, true, true);
    }

    protected override void OnStop() {
        Server.Stop(true);
    }
}
}