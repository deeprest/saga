using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Threading;

namespace Dungbeetle {
static class Server {
    private static Dictionary<string, Action<CryptoStreamManager, TcpClient>> reporter;
    private static Dictionary<string, Action<CryptoStreamManager, TcpClient>> developer;

    static Server() {
        reporter  = new Dictionary<string, Action<CryptoStreamManager, TcpClient>>();
        developer = new Dictionary<string, Action<CryptoStreamManager, TcpClient>>();

        reporter.Add("New2", New2);
        reporter.Add("New", New);

        developer.Add("Assigned-Bugs", AssignedBugs);
        developer.Add("Assigned-Bugs-By-Fixer", AssignedBugsByFixer);
        developer.Add("Attachments", Attachments);
        developer.Add("Attachment", Attachment);
        developer.Add("BugChanges", BugChanges);
        developer.Add("BugPriority", BugPriority);
        developer.Add("ContactResponsible", ContactResponsible);
        developer.Add("DeleteBug", DeleteBug);
        developer.Add("Fixed-Bugs", FixedBugs);
        developer.Add("Image", Image);
        developer.Add("MigrateIpTargetCheck", MigrateIpTargetCheck);
        developer.Add("MigrateSend", MigrateSend);
        developer.Add("MigrateReturn", MigrateReturn);
        developer.Add("MigrateReceive", MigrateReceive);
        developer.Add("New2", New2);
        developer.Add("New", New);
        developer.Add("Open-Bugs", OpenBugs);
        developer.Add("Ping", Ping);
        developer.Add("RelayedPing", RelayedPing);
        developer.Add("Reopen", Reopen);
        developer.Add("Resolved-Bugs", ResolvedBugs);
        developer.Add("GetProjects", GetProjects);
        developer.Add("GetProjectNames", GetProjectNames);
        developer.Add("AddProject", AddProject);
        developer.Add("DeleteProject", DeleteProject);
        developer.Add("RenameProject", RenameProject);
        developer.Add("AddDeveloper", AddDeveloper);
        developer.Add("DeleteDeveloper", DeleteDeveloper);
        developer.Add("RenameDeveloper", RenameDeveloper);
        developer.Add("Unassigned-Bugs", UnassignedBugs);
        developer.Add("Update", Update);
        developer.Add("GetDevelopers", GetDevelopers);
    }

    private const string programName =
#if DUNGBEETLE_SERVER_STANDALONE
        "Server";
#else
        "Editor server";
#endif

    private static TcpListener listener;
    private static bool serverStopped = true;
    private static bool serverStopping = false;

    // Thread safe objects
    private static readonly HashSet<string> silentKeywords = GenerateIgnoredCommands();
    private static SymKeyCache symKeyCache;
    private static IOutput output;
    private static IDatabaseLink dbLink;

    public static void Run(IOutput o, bool catchThread, bool explicitly) {
        if (!serverStopped)
            return;

        output = o;
        symKeyCache = new SymKeyCache();

#if DUNGBEETLE_SERVER_STANDALONE
        var mode = ServerPreferences.GetMode();
#else
        var mode = ServerDatabaseMode.SQLite;
#endif

        string dbName = default(string);
        if (mode == ServerDatabaseMode.SQLite) {
            SetUpSQLite(o);
        }
        else {
            bool failedToSetup;
            SetUpMySql(o, out failedToSetup, out dbName);
            if (failedToSetup)
                return;
        }

        if (!dbLink.DatabaseAligned()) {
            output.Log($"Database `{dbName}` exists, but the tables are wrong");
            symKeyCache.Stop();
            return;
        }

#if DUNGBEETLE_SERVER_STANDALONE
        var port = ServerPreferences.GetPort();
        if (Type.GetType ("Mono.Runtime") == null) {
            try {
                listener = new TcpListener(IPAddress.IPv6Any, port);
                listener.Server.SetSocketOption(SocketOptionLevel.IPv6, (SocketOptionName) 27,  false);
            }
            catch (SocketException) {
                listener.Stop();
                output.Log("Falling back to IPv4");
                listener = new TcpListener(IPAddress.Any, port);
            }
        }
        else {
            listener = new TcpListener(IPAddress.Any, port);
        }
#else
        ushort port = Config.EditorServerConfig.Port;
        listener = new TcpListener(IPAddress.Any, port);
#endif

        try {
            listener.Start();
            if (explicitly)
                output.Log($"{programName} listening to port {port}");
            AcceptClients();
            serverStopped = false;

            if (catchThread)
                while (true)
                    Thread.Sleep(1000);
        }
        catch (SocketException e) {
            if (e.ErrorCode == 10048)
                output.Log($"Error: The server couldn't start. Port {port} is already in use.\n\tAre you running both an in-editor server and standalone?\n");
            else
                output.Log("Error: The server couldn't start. Couldn't open a socket.\n");
            output.Log(e.ToString());
        }
        finally {
            symKeyCache.Stop();
        }
    }

    private static void SetUpSQLite(IOutput o) {
        dbLink = new SQLiteDatabaseLink(o);
        var sqliteLink = dbLink as SQLiteDatabaseLink;
        if (!sqliteLink.DatabaseExists()) {
            output.Log($"Creating database {DungbeetlePath.Database}");
            sqliteLink.CreateDatabase();
        }
    }

    private static void SetUpMySql(IOutput o, out bool failedToSetup, out string dbName) {
        dbLink = new MySQLDatabaseLink(o);
        var mySQLLink = (MySQLDatabaseLink) dbLink;
        var url = ServerPreferences.GetDbUrl();
        var user = ServerPreferences.GetDbUserName();
        var pw = ServerPreferences.GetDbPassword();
        dbName = ServerPreferences.GetDbName();

        var result = mySQLLink.DatabaseUserExists(url, user, pw);

        if (result) {
            result = mySQLLink.DatabaseExists(url, user, pw, dbName);
            if (!result) {
                output.Log($"Creating database `{dbName}`");
                mySQLLink.CreateDatabase(url, user, pw, dbName);
            }

            mySQLLink.databaseURL = url;
            mySQLLink.databaseUser = user;
            mySQLLink.databasePassword = pw;
            mySQLLink.databaseName = dbName;
            failedToSetup = false;
        }
        else {
            output.Log($"Database user '{user}' doesn't exist");
            symKeyCache.Stop();
            failedToSetup = true;
        }
    }

    public static void ClearCookies() => symKeyCache?.Clear();

    public static void Stop(bool explicitly) {
        if (serverStopped)
            return;
        if (explicitly)
            output?.Log($"{programName} shutting down\n");

        serverStopping = true;
        symKeyCache?.Stop();
        listener?.Stop();

        while (serverStopping)
            Thread.Sleep(50);
    }

    private static HashSet<string> GenerateIgnoredCommands() =>
        new HashSet<string> {
            "Ping",
            "RelayedPing",
            "MigrateIpTargetCheck"
        };

    private static void AcceptClients() {
        listener.BeginAcceptTcpClient(HandleRequest, listener);
    }

    private static void HandleRequest(IAsyncResult res) {
        if (serverStopping) {
            serverStopping = false;
            serverStopped = true;
            return;
        }

        AcceptClients();

        var cur_listener = (TcpListener) res.AsyncState;
        var client = cur_listener.EndAcceptTcpClient(res);

        var stream = client.GetStream();
        var reader = new BinaryReader(stream);
        var writer = new BinaryWriter(stream);

        try {
            PrivilegeLevel privilege = PrivilegeLevel.Reporter;
            var negotiationAccepted = false;
            using (var network = NegotiateAccess(reader, writer, client, ref privilege, ref negotiationAccepted)) {
                if (negotiationAccepted) {
                    var request = network.ReadString();
                    if (privilege == PrivilegeLevel.Developer && developer.ContainsKey(request)) {
                        if (!silentKeywords.Contains(request))
                            output.Log($"Request: '{request}'");
                        developer[request](network, client);
                    }
                    else if (privilege == PrivilegeLevel.Reporter && reporter.ContainsKey(request)) {
                        if (!silentKeywords.Contains(request))
                            output.Log($"Request: '{request}'");
                        reporter[request](network, client);
                    }
                    else {
                        output.Log($"Not recognized for {privilege}: '{request}'");
                    }
                }
            }
        }
        catch (CryptographicException) {
            output.Log("Decryption failed. Request denied.");
        }
        catch (Exception e) {
            output.Log(e.ToString());
        }

        reader.Close();
        writer.Close();
    }

    private static CryptoStreamManager NegotiateAccess (BinaryReader reader, BinaryWriter writer, TcpClient client, ref PrivilegeLevel privilege, ref bool accepted) {
        var clientKeyString = reader.ReadString();

        if (clientKeyString == "Key") {
            writer.Write("Send key");
            var identifier = reader.ReadString();

            var remoteAddr = ((IPEndPoint) client.Client.RemoteEndPoint).Address;
            byte[] key = null;
            if (symKeyCache.TryGet(remoteAddr, identifier, ref privilege, ref key)) {
                writer.Write("Key accepted");
                accepted = true;
                return new CryptoStreamManager(client.GetStream(), key, output);
            }
            else {
                writer.Write("Key expired");
            }
        }
        else if (clientKeyString != "GetKey") {
            output.Log($"Not recognized: '{clientKeyString}'");
            return null;
        }

        writer.Write("Send password");
        if (ReadPassword(reader, ref privilege)) {
            writer.Write(privilege == PrivilegeLevel.Reporter ? "Reporter accepted" : "Developer accepted");
            var identifier = reader.ReadString();
            var cm = ReadAndCreateCryptoManager(reader, client.GetStream());

            var remoteAddr = ((IPEndPoint) client.Client.RemoteEndPoint).Address;
            symKeyCache.Add(remoteAddr, identifier, privilege, cm.Key);
            accepted = true;
            return cm;
        }
        else {
            writer.Write("Password denied");
            return null;
        }
    }

    private static bool ReadPassword(BinaryReader reader, ref PrivilegeLevel privilege) {
        var passwordLength = reader.ReadInt32();
        const int maxPasswordLength = 1 << 14;
        passwordLength = passwordLength < maxPasswordLength ? passwordLength : maxPasswordLength;
        var password = reader.ReadBytes(passwordLength);

#if DUNGBEETLE_SERVER_STANDALONE
        var storedPrivateKey = ServerPreferences.GetPrivateKey();
#else
        var storedPrivateKey = Config.EditorServerConfig.PrivKey;
#endif

        var rsa = new RSACryptoServiceProvider();
        rsa.FromXmlString(storedPrivateKey);
        var decryptedPwd = System.Text.Encoding.ASCII.GetString(rsa.Decrypt(password, false));

#if DUNGBEETLE_SERVER_STANDALONE
        var isReporterPassword = decryptedPwd.Equals(ServerPreferences.GetReporterPassword());
#else
        var isReporterPassword = decryptedPwd.Equals(Config.Defaults.ReporterPassword.password);
#endif

        if (isReporterPassword)
            return true;
        else {
#if DUNGBEETLE_SERVER_STANDALONE
            var isDeveloperPassword = decryptedPwd.Equals(ServerPreferences.GetDeveloperPassword());
#else
            var isDeveloperPassword = decryptedPwd.Equals(Config.Defaults.DeveloperPassword.password);
#endif
            if (isDeveloperPassword) {
                privilege = PrivilegeLevel.Developer;
                return true;
            }
            else
                return false;
        }
    }

    public static CryptoStreamManager ReadAndCreateCryptoManager(BinaryReader reader, NetworkStream stream) {
        var symKeyLength = reader.ReadInt32();
        const int maxSymKeyLength = 1 << 14;
        if (symKeyLength > maxSymKeyLength)
            symKeyLength = maxSymKeyLength;

        var symKey = reader.ReadBytes(symKeyLength);

        var rsa = new RSACryptoServiceProvider();
#if DUNGBEETLE_SERVER_STANDALONE
        rsa.FromXmlString(ServerPreferences.GetPrivateKey());
#else
        rsa.FromXmlString(Config.EditorServerConfig.PrivKey.key);
#endif

        var symKeyDecrypted = rsa.Decrypt(symKey, false);
        return new CryptoStreamManager(stream, symKeyDecrypted, output);
    }

    private static void PackAndSend(List<Bug> bugs_to_return, CryptoStreamManager network) {
        var serialized_bugs = bugs_to_return.Select(b => b.ToYaml()).ToList();
        network.Write(serialized_bugs);
    }

    [ProtocolKeyword(Alias = "Assigned-Bugs")]
    private static void AssignedBugs(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        PackAndSend(dbLink.GetAssigned(projectId), network);
    }

    [ProtocolKeyword(Alias = "Assigned-Bugs-By-Fixer")]
    private static void AssignedBugsByFixer(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var fixer = network.ReadInt32();
        PackAndSend(dbLink.GetForFixer(projectId, fixer), network);
    }

    [ProtocolKeyword]
    private static void Attachments(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var id = network.ReadInt32();
        var maxLength = network.ReadInt32();

        var attachments = dbLink.GetAttachments(projectId, id, maxLength);

        network.Write(attachments.attachment_ids);
        network.Write(attachments.attachment_contents);
    }

    [ProtocolKeyword]
    private static void Attachment(CryptoStreamManager network, TcpClient client) {
        var id = network.ReadInt32();
        var attachment = dbLink.GetAttachment(id);
        network.Write(attachment);
    }

    [ProtocolKeyword]
    private static void BugChanges(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var id = network.ReadInt32();
        var state = network.ReadInt32();
        var comment = network.ReadString();
        var priority = network.ReadString();
        var accept_responsible = network.ReadInt32();
        var fix_responsible = network.ReadInt32();
        var fix_source = network.ReadString();
        var resolution_responsible = network.ReadInt32();
        dbLink.UpdateResponsible(projectId, id, state, comment, priority, accept_responsible, fix_responsible, fix_source, resolution_responsible);
    }

    [ProtocolKeyword]
    private static void BugPriority(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var id = network.ReadInt32();
        var priority = network.ReadString();
        dbLink.UpdatePriority(projectId, id, priority);
    }

    [ProtocolKeyword]
    private static void ContactResponsible(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var id = network.ReadInt32();
        var contact_responsible = network.ReadInt32();
        dbLink.UpdateContactResponsible(projectId, id, contact_responsible);
    }

    [ProtocolKeyword]
    private static void DeleteBug(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var id = network.ReadInt32();
        var imageName = dbLink.GetImageName(projectId, id);
        dbLink.DeleteBug(projectId, id);

        if (imageName != null)
            File.Delete(ImageFile.GetExistingPath(imageName));
    }

    [ProtocolKeyword(Alias = "Fixed-Bugs")]
    private static void FixedBugs(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        PackAndSend(dbLink.GetFixed(projectId), network);
    }

    [ProtocolKeyword]
    private static void Image(CryptoStreamManager network, TcpClient client) {
        var fileName = network.ReadString();
        var fileBytes = File.ReadAllBytes(ImageFile.GetExistingPath(fileName));
        network.Write(fileBytes);
    }

    [ProtocolKeyword]
    private static void MigrateIpTargetCheck(CryptoStreamManager network, TcpClient client) {
        var domain = network.ReadString();
        var port = network.ReadInt32();

#if DUNGBEETLE_SERVER_STANDALONE
        var localPort = ServerPreferences.GetPort();
#else
        ushort localPort = Config.EditorServerConfig.Port;
#endif

        if (port != localPort)
            network.Write("Accepted");
        else {
            var acceptable = true;
            var resultWritten = false;
            try {
                var hostIPs = Dns.GetHostAddresses(domain);
                var localIPs = Dns.GetHostAddresses(Dns.GetHostName());

                foreach (var hostIP in hostIPs) {
                    acceptable = acceptable && !IPAddress.IsLoopback(hostIP);
                    foreach (var localIP in localIPs)
                        acceptable = acceptable && !hostIP.Equals(localIP);
                }
            }
            catch (Exception e) {
                resultWritten = true;
                var result = $"DSN lookup error: {e.Message}";
                output.Log(result);
                network.Write(result);
            }

            if (!resultWritten) {
                if (acceptable)
                    network.Write("Accepted");
                else
                    network.Write("Same-Host");
            }
        }
    }

    [ProtocolKeyword]
    private static void MigrateSend(CryptoStreamManager network, TcpClient _) {
        var settings = ServerMigrationSettings.FromYaml(network.ReadString());

        string connectionErrorMessage = null;
        try {
            using (var client = CustomConnection.Connection(settings.targetDomain, settings.targetPort, 5000))
            using (var target_network = DungbeetleNetworkFactory.Create(client, settings.targetPassword, settings.targetPubKey, PrivilegeLevel.Developer, output)) {
                target_network.Write("MigrateReceive");
                target_network.Write(settings.ToYaml());
                MigrateCompleteSourceTasks(settings, target_network, network);
            }
        }
        catch (SocketException e) {
            connectionErrorMessage = $"Couldn't connect to {settings.targetDomain}:{settings.targetPort}\n{e.Message}";
        }
        catch (ProtocolException e) {
            connectionErrorMessage = $"Couldn't connect to {settings.targetDomain}:{settings.targetPort}\n{e.Message}";
        }
        catch (EndOfStreamException e) {
            connectionErrorMessage = $"Couldn't connect to {settings.targetDomain}:{settings.targetPort}\n{e.Message}";
        }

        if (connectionErrorMessage != null)
            output.Log(connectionErrorMessage);
    }

    [ProtocolKeyword]
    private static void MigrateReturn(CryptoStreamManager network, TcpClient client) {
        var settings = ServerMigrationSettings.FromYaml(network.ReadString());
        MigrateCompleteSourceTasks(settings, network, null);
    }

    private static void MigrateCompleteSourceTasks(ServerMigrationSettings settings, CryptoStreamManager network, CryptoStreamManager report_network) {
        MigrationSourceLog("Transfering projects", network, report_network);

        var migratingProjects = settings.projects.Select(pm => pm.sourceId).ToArray();

        var projects = dbLink.GetProjects(true, migratingProjects);

        network.Write(projects.Count);
        foreach (var p in projects)
            network.Write(p.ToYaml());

        MigrationSourceLog("Transfering bugs", network, report_network);

        var num_bugs = dbLink.GetBugCount(migratingProjects);
        var image_names = new List<string>(num_bugs);
        var bug_page_size = 100;

        network.Write(num_bugs);

        var bug_offset = 0;
        while (bug_offset < num_bugs) {
            var bugs = dbLink.GetBugsForMigration(migratingProjects, bug_page_size, bug_offset);
            var page_offset = 0;
            while (page_offset < bugs.Count) {
                image_names.Add(bugs[page_offset].imageName);
                network.Write(Bug.bugFormatVersion);
                network.Write(bugs[page_offset].ToYaml());
                ;
                page_offset += 1;
            }

            bugs.Clear();
            bug_offset += bug_page_size;
        }

        MigrationSourceLog("Transfering images", network, report_network);
        network.Write(image_names.Count);
        foreach (var image_name in image_names) {
            var fileBytes = File.ReadAllBytes(ImageFile.GetExistingPath(image_name));
            network.Write(image_name);
            network.Write(fileBytes);
        }

        var status = network.ReadString();

        if (status == "Migration succeeded") {
            if (settings.clearSourceDatabase) {
                MigrationSourceLog("Clearing source database", network, report_network);
                dbLink.ResetDatabase();
                try {
                    ImageFile.DeleteAll();
                }
                catch (Exception e) {
                    output.Log($"Failed to delete screenshots: {e.Message}");
                }
            }

            MigrationSourceLog("Migration succeeded", network, report_network);
        }
        else {
            MigrationSourceLog("Source didn't signal \"Migration succeeded\". Source database was not cleared.", network, report_network);
        }
    }

    private static void MigrationSourceLog(string msg, CryptoStreamManager client, CryptoStreamManager target) {
        output.Log(msg);
        client.Write(msg);
        target?.Write(msg);
    }


    [ProtocolKeyword]
    private static void MigrateReceive(CryptoStreamManager network, TcpClient client) {
        var settings = ServerMigrationSettings.FromYaml(network.ReadString());

        if (settings.clearTargetDatabase) {
            output.Log("Clearing database");
            dbLink.ResetDatabase();
            try {
                ImageFile.DeleteAll();
            }
            catch (Exception e) {
                output.Log($"Failed to delete screenshots: {e.Message}");
            }
        }

        var status = network.ReadString(); // "Transfering projects"
        output.Log(status);

        var projectMergesByOldId = new Dictionary<int, bool>();
        foreach (var mp in settings.projects)
            projectMergesByOldId[mp.sourceId] = mp.targetId != -1;

        var projectsCount = network.ReadInt32();
        var projectIdChanges = new Dictionary<int, int>();
        var developerIdChanges = new Dictionary<int, int>();
        var projectMergesByNewId = new Dictionary<int, bool>();
        for (int i = 0; i < projectsCount; i++) {
            var project = Project.FromYaml(network.ReadString());
            var oldId = project.id;
            int newId;
            bool merge;
            projectMergesByOldId.TryGetValue(oldId, out merge);
            if (merge)
                settings.TryGetProjectId(oldId, out newId);
            else
                newId = dbLink.CreateUniqueProject(project.name);
            projectMergesByNewId[newId] = merge;
            if (newId != oldId)
                projectIdChanges[oldId] = newId;

            foreach (var _developer in project.developers) {
                int newDevId;
                if (merge)
                    newDevId = dbLink.CreateMergedDeveloper(newId, _developer.name);
                else
                    newDevId = dbLink.CreateNewDeveloper(newId, _developer.name);
                developerIdChanges[_developer.id] = newDevId;
            }
        }

        status = network.ReadString(); // "Transfering bugs"
        output.Log(status);

        var num_bugs = network.ReadInt32();
        var new_image_name_changes = new Dictionary<string, string>();
        var new_image_names = new HashSet<string>();
        var bug_page_size = 100;

        var bug_page = new List<Bug>(bug_page_size);

        var bug_i = 0;
        while (bug_i < num_bugs) {
            var num_page_bugs = num_bugs - bug_i >= bug_page_size ? bug_page_size : num_bugs - bug_i;
            var bug_page_i = 0;
            while (bug_page_i < num_page_bugs) {
                var bug_version = network.ReadString();
                var bug = Bug.FromYaml(network.ReadString(), bug_version);

                var newImageName = ImageFile.UniqueNameConservatively(new_image_names, bug.imageName);
                new_image_names.Add(newImageName);
                new_image_name_changes[bug.imageName] = newImageName;
                bug.imageName = newImageName;

                int projectId;
                if (projectIdChanges.TryGetValue(bug.projectId, out projectId))
                    bug.projectId = projectId;

                int developerId;
                if (developerIdChanges.TryGetValue(bug.accept_responsible, out developerId))
                    bug.accept_responsible = developerId;
                if (developerIdChanges.TryGetValue(bug.fix_responsible, out developerId))
                    bug.fix_responsible = developerId;
                if (developerIdChanges.TryGetValue(bug.resolution_responsible, out developerId))
                    bug.resolution_responsible = developerId;
                if (developerIdChanges.TryGetValue(bug.contact_responsible, out developerId))
                    bug.contact_responsible = developerId;

                bug_page.Add(bug);
                bug_i += 1;
                bug_page_i += 1;
            }

            var inMerge = false;
            foreach (var bug in bug_page) {
                projectMergesByNewId.TryGetValue(bug.projectId, out inMerge);
                dbLink.MigrateBug(bug, !inMerge);
            }

            bug_page.Clear();
        }

        status = network.ReadString(); // "Transfering images"
        output.Log(status);
        var num_images = network.ReadInt32();
        for (int i = 0; i < num_images; i++) {
            var originalImageName = network.ReadString();
            var imageBytes = network.ReadBytes();

            string newImageName;
            if (new_image_name_changes.TryGetValue(originalImageName, out newImageName))
                File.WriteAllBytes(ImageFile.GetValidPath(newImageName), imageBytes);
            else
                output.Log($"Error, couldn't allocate new file name for '{originalImageName}', maybe it didn't belong to a bug?");
        }

        network.Write("Migration succeeded");

        if (settings.clearSourceDatabase) {
            status = network.ReadString(); // "Clearing source database"
            output.Log(status);
        }

        status = network.ReadString(); // "Migration succeeded"
        if (status == "Migration succeeded")
            output.Log(status);
        else
            output.Log($"Source server failed with message: {status}");
    }

    [ProtocolKeyword(Alias = "New2", Privilege = PrivilegeLevel.Reporter)]
    private static void New2(CryptoStreamManager network, TcpClient client) {
        // In New2, attachments are sent separately, so they don't impact serialization.

        var version = network.ReadString();
        //output.Log($"BugReport version is: {version}");

        var bug = new Bug(BugReport.FromYaml(network.ReadString(), version));
        bug.attachments = new List<string>();
        // Handle the array with care. Something is fishy with the ienumerable and ReadStringArray
        var attachmentsArray = network.ReadStringArray();
        var i = 0;
        while (i < attachmentsArray.Length)
            bug.attachments.Add(attachmentsArray[i++]);

        var bugAuthor = string.IsNullOrWhiteSpace(bug.email) ? "unknown" : bug.email;
        output.Log($"Bug by {bugAuthor}: {bug.description}");

        var imageBytes = network.ReadBytes();
        bug.imageName = ImageFile.UniqueName();
        File.WriteAllBytes(ImageFile.GetValidPath(bug.imageName), imageBytes);

        var address = IPAddress.Parse(((IPEndPoint) client.Client.RemoteEndPoint).Address.ToString()).ToString();
        bug.address = address;

        dbLink.InsertBug(bug);
        network.Write("Accepted");
    }

    [ProtocolKeyword(Alias = "New", Privilege = PrivilegeLevel.Reporter)]
    private static void New(CryptoStreamManager network, TcpClient client) {
        var version = network.ReadString();
        //output.Log($"BugReport version is: {version}")

        var serialized_bug = network.ReadString();
        var bug = new Bug(BugReport.FromYaml(serialized_bug, version));
        var email = String.IsNullOrEmpty(bug.email) ? "unknown" : bug.email;
        output.Log($"Bug by {email}: {bug.description}");

        var imageBytes = network.ReadBytes();
        bug.imageName = ImageFile.UniqueName();
        File.WriteAllBytes(ImageFile.GetValidPath(bug.imageName), imageBytes);

        var address = IPAddress.Parse(((IPEndPoint) client.Client.RemoteEndPoint).Address.ToString()).ToString();
        bug.address = address;

        dbLink.InsertBug(bug);
        network.Write("Accepted");
    }

    [ProtocolKeyword(Alias = "Open-Bugs")]
    private static void OpenBugs(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        PackAndSend(dbLink.GetOpen(projectId), network);
    }

    [ProtocolKeyword]
    private static void Ping(CryptoStreamManager network, TcpClient client)
        => network.Write("Pong");

    [ProtocolKeyword]
    private static void RelayedPing(CryptoStreamManager network, TcpClient _) {
        var domain = network.ReadString();
        var port = network.ReadInt32();
        var password = network.ReadString();
        var pubKey = network.ReadString();
        var pingable = false;
        string connectionErrorMessage = null;
        try {
            using (var client = CustomConnection.Connection(domain, port, 1000))
            using (var target_network = DungbeetleNetworkFactory.Create(client, password, pubKey, PrivilegeLevel.Developer, output)) {
                target_network.Write("Ping");
                pingable = target_network.ReadString() == "Pong";
            }
        }
        catch (SocketException e) {
            connectionErrorMessage = $"Couldn't ping {domain}:{port}\n{e.Message}";
        }
        catch (ProtocolException e) {
            connectionErrorMessage = $"Couldn't ping {domain}:{port}\n{e.Message}";
        }
        catch (EndOfStreamException e) {
            connectionErrorMessage = $"Couldn't ping {domain}:{port}\n{e.Message}";
        }

        if (connectionErrorMessage != null) {
            output.Log(connectionErrorMessage);
            network.Write(connectionErrorMessage);
        }
        else if (!pingable) {
            output.Log("Target server didn't return 'Pong' as expected");
            network.Write("Error");
        }
        else
            network.Write("RelayedPong");
    }

    [ProtocolKeyword]
    private static void Reopen(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var id = network.ReadInt32();
        dbLink.UpdateReopen(projectId, id);
    }

    [ProtocolKeyword(Alias = "Resolved-Bugs")]
    private static void ResolvedBugs(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        PackAndSend(dbLink.GetResolved(projectId), network);
    }

    [ProtocolKeyword]
    private static void GetProjects(CryptoStreamManager network, TcpClient client) {
        RespondWithProjects(true, network);
    }

    [ProtocolKeyword]
    private static void GetProjectNames(CryptoStreamManager network, TcpClient client) {
        RespondWithProjects(false, network);
    }

    private static void RespondWithProjects(bool includeDevelopers, CryptoStreamManager network) {
        network.Write(dbLink.GetProjects(includeDevelopers).Select(p => p.ToYaml()).ToList());
    }

    [ProtocolKeyword]
    private static void AddProject(CryptoStreamManager network, TcpClient client) {
        var projectName = network.ReadString();
        var projectId = dbLink.CreateNewProject(projectName);
        network.Write(projectId);
    }


    [ProtocolKeyword]
    private static void DeleteProject(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        dbLink.DeleteProject(projectId);
    }

    [ProtocolKeyword]
    private static void RenameProject(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var projectName = network.ReadString();
        dbLink.RenameProject(projectId, projectName);
    }

    [ProtocolKeyword]
    private static void AddDeveloper(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var name = network.ReadString();
        var id = dbLink.CreateNewDeveloper(projectId, name);
        network.Write(id);
    }

    [ProtocolKeyword]
    private static void DeleteDeveloper(CryptoStreamManager network, TcpClient client) {
        var id = network.ReadInt32();
        dbLink.DeleteDeveloper(id);
    }

    [ProtocolKeyword]
    private static void RenameDeveloper(CryptoStreamManager network, TcpClient client) {
        var id = network.ReadInt32();
        var name = network.ReadString();
        dbLink.RenameDeveloper(id, name);
    }

    [ProtocolKeyword(Alias = "Unassigned-Bugs")]
    private static void UnassignedBugs(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        PackAndSend(dbLink.GetUnassigned(projectId), network);
    }

    [ProtocolKeyword]
    private static void Update(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        var id = network.ReadInt32();
        var comment = network.ReadString();
        dbLink.UpdateComment(projectId, id, comment);
    }

    [ProtocolKeyword]
    private static void GetDevelopers(CryptoStreamManager network, TcpClient client) {
        var projectId = network.ReadInt32();
        network.Write( dbLink.GetProjectDevelopers(projectId).Select(d => d.ToYaml()).ToList() );
    }
}
}
