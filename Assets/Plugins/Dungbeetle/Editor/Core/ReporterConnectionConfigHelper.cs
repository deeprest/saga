using System.IO;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {
[InitializeOnLoad]
public class ReporterConnectionConfigHelper : ScriptableObject {
    const string configPath = "Assets/Plugins/Dungbeetle/Resources/ReporterConnectionConfig.asset";

    static ReporterConnectionConfigHelper() {
        ReporterConnectionConfig.setDirty += EditorUtility.SetDirty;
        ReporterConnectionConfig.loadConfig += LoadConfig;
    }

    private static ReporterConnectionConfig LoadConfig() {
        ReporterConnectionConfig _config =
            AssetDatabase.LoadAssetAtPath<ReporterConnectionConfig>(configPath);

        if (_config == null) {
            if (!Directory.Exists("Assets/Plugins/Dungbeetle/Resources"))
                AssetDatabase.CreateFolder("Assets/Plugins/Dungbeetle", "Resources");
            _config = CreateInstance<ReporterConnectionConfig>();
            AssetDatabase.CreateAsset(_config, configPath);
        }

        return _config;
    }
}
}