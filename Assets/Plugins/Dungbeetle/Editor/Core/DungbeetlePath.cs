namespace Dungbeetle {
public static class DungbeetlePath {
    public const string DefaultPreferencesFileName = "ServerPreferences.yaml";

    private static string c(string a, string b) => System.IO.Path.Combine(a, b);

    private static string home;
    private static string screenshots;
    private static string database;
    private static string log;
    private static string preferences;

    private static bool customPreferences = false;
    private static bool customLog = false;

    static DungbeetlePath() {
#if !DUNGBEETLE_SERVER_STANDALONE
        //<Project Folder>/DungbeetleServer/..
        home = "DungbeetleServer";
#else
        //Next to the executable
        home = System.IO.Directory.GetCurrentDirectory();
#endif
        ApplyHome();
    }

    private static void ApplyHome() {
        screenshots = c(home, "Screenshots");
        database = c(home, "DungbeetleDatabase.sqlite");

        if (!customLog)
            log = c(home, "DungbeetleServer.log");
        if (!customPreferences)
            preferences = c(home, DefaultPreferencesFileName);
    }

    public static string Home {
        get { return home; }
        set {
            home = value;
            ApplyHome();
        }
    }

    public static string Screenshots(string fileName)
        => c(screenshots, fileName);

    public static string Database => database;

    public static string Log {
        get { return log; }
        set {
            log = value;
            customLog = true;
        }
    }

    public static string Preferences {
        get { return preferences; }
        set {
            preferences = value;
            customPreferences = true;
        }
    }
}
}