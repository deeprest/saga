namespace Dungbeetle {
public class ProtocolKeywordAttribute : System.Attribute {
    public PrivilegeLevel Privilege { get; set; }
    public string         Alias     { get; set; }
}

public class ProtocolAttribute : System.Attribute {
}
}
