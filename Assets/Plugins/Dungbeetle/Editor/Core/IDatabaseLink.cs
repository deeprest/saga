using System;
using System.Collections.Generic;

namespace Dungbeetle
{
    interface IDatabaseLink
    {
        int CreateMergedDeveloper(int projectId, string name);
        int CreateNewDeveloper(int projectId, string name);
        int CreateNewProject(string name);
        int CreateUniqueProject(string name);
        bool DatabaseAligned();
        void DeleteBug(int projectId, int id);
        void DeleteDeveloper(int id);
        void DeleteProject(int id);
        List<Bug> GetAssigned(int projectId);
        GetAttachmentResults GetAttachments(int projectId, int id, int maxLength);
        string GetAttachment(int id);
        int GetBugCount(params int[] projectIds);
        List<Bug> GetBugsForMigration(int[] projectIds, int limit, int offset);
        List<Bug> GetFixed(int projectId);
        List<Bug> GetForFixer(int projectId, int fixer);
        string GetImageName(int projectId, int id);
        List<Bug> GetOpen(int projectId);
        List<ProjectDeveloper> GetProjectDevelopers(int projectId);
        List<Project> GetProjects(bool includeDevelopers);
        List<Project> GetProjects(bool includeDevelopers, int[] projectIds);
        List<Bug> GetResolved(int projectId);
        List<Bug> GetUnassigned(int projectId);
        void InsertBug(Bug bug);
        void MigrateBug(Bug bug, bool reuseId);
        void RenameDeveloper(int id, string name);
        void RenameProject(int projectId, string name);
        void ResetDatabase();
        void UpdateComment(int projectId, int id, string comment);
        void UpdateContactResponsible(int projectId, int id, int contact_responsible);
        void UpdatePriority(int projectId, int id, string priority);
        void UpdateReopen(int projectId, int id);
        void UpdateResponsible(int projectId, int id, Int32 state, string comment, string priority, int accept_responsible, int fix_responsible,
            string fix_source, int resolution_responsible);
    }

    public struct GetAttachmentResults {
        public List<string> attachment_contents;
        public List<int> attachment_ids;
    }
}
