namespace Dungbeetle {
[System.Flags]
public enum ServerArgumentsResult {
    Ready     = 1 << 0,
    LogToFile = 1 << 1,
    Install   = 1 << 2,
    Uninstall = 1 << 3,
}

public static class ServerArguments {
    private const string helpString = @"Usage: DungbeetleServer.exe [OPTION]...

Options:
  -l <path>, --logfile=<path>      Write log to file instead of stdout.
  -p <path>, --preferences=<path>  Use given preferences file.
                                   (default: ServerPreferences.yaml in
                                    the current home directory)
  -h <path>, --home=<path>         Store database and screenshots folder
                                   at the given path.
                                   (default: current working directory)
             --help                Show this help page.
             --store-in-registry   Try to install logfile, preferences
                                   and home paths in the registry on
                                   windows. (The keys are used when the
                                   server runs as a service).
             --uninstall           Remove the path to the logfile,
                                   preferences and home from the registry
                                   on windows.
";


    public static ServerArgumentsResult ParseArguments(IOutput o, string[] argv) {
        var activate_log = false;
        var show_help = false;
        var install = false;
        var uninstall = false;
        if (argv.Length > 0) {
            var i = 0;
            while (i < argv.Length) {
                var a = argv[i];
                if (a == "--help") {
                    show_help = true;
                    i += 1;
                    continue;
                }
                else if (a == "--store-in-registry") {
                    var platform = (int) System.Environment.OSVersion.Platform;
                    if (platform == 4 || platform == 6 || platform == 128) {
                        o.Log("The --store-in-registry option is for Windows only.");
                        return 0;
                    }
                    else
                        install = true;

                    i += 1;
                    continue;
                }
                else if (a == "--uninstall") {
                    var platform = (int) System.Environment.OSVersion.Platform;
                    if (platform == 4 || platform == 6 || platform == 128) {
                        o.Log("The --uninstall option is for Windows only.");
                        return 0;
                    }
                    else
                        uninstall = true;

                    i += 1;
                    continue;
                }

                var i_next = i + 1;
                var one_spent = false;
                var two_spent = false;
                if (i_next < argv.Length) {
                    var next = argv[i_next];
                    two_spent = true;
                    if (a == "-l" || a == "--logfile") {
                        DungbeetlePath.Log = next;
                        activate_log = true;
                    }
                    else if (a == "-p" || a == "--preferences")
                        DungbeetlePath.Preferences = next;
                    else if (a == "-h" || a == "--home")
                        DungbeetlePath.Home = next;
                    else
                        two_spent = false;

                    if (two_spent)
                        i = i_next;
                }

                if (!two_spent) {
                    if (a.Length > 2 && a[0] == '-' && a[1] == '-') {
                        var equalsPos = a.IndexOf('=');
                        if (equalsPos != -1 && equalsPos != a.Length - 1) {
                            var word = a.Substring(2, equalsPos - 2);
                            var value = a.Substring(equalsPos + 1);
                            one_spent = true;
                            if (word == "logfile") {
                                DungbeetlePath.Log = value;
                                activate_log = true;
                            }
                            else if (word == "preferences")
                                DungbeetlePath.Preferences = value;
                            else if (word == "home")
                                DungbeetlePath.Home = value;
                            else
                                one_spent = false;
                        }
                    }
                }

                if (!(one_spent || two_spent))
                    o.Log($"Didn't understand argument: '{a}'");
                i += 1;
            }

            if (show_help) {
                o.Log(helpString);
                return 0;
            }
            else if (install)
                return ServerArgumentsResult.Install;
            else if (uninstall)
                return ServerArgumentsResult.Uninstall;
            else {
                if (activate_log)
                    return ServerArgumentsResult.Ready | ServerArgumentsResult.LogToFile;
                else
                    return ServerArgumentsResult.Ready;
            }
        }
        else {
            return ServerArgumentsResult.Ready;
        }
    }
}
}
