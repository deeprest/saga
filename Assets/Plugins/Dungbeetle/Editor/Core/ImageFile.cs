using System;
using System.Collections.Generic;
using System.IO;

namespace Dungbeetle {
public static class ImageFile {

    public static string UniqueName() {
        var result = RandomName();
        while (File.Exists(ScreenshotPath(result)))
            result = RandomName();
        return result;
    }

    public static string UniqueNameConservatively(ICollection<string> usedNames, string fileName) {
        while (usedNames.Contains(fileName) || File.Exists(ScreenshotPath(fileName)))
            fileName = RandomName();
        return fileName;
    }

    public static string GetValidPath(string fileName) {
        EnsureFolderExists();
        return ScreenshotPath(fileName);
    }

    public static string GetExistingPath(string fileName) =>
        ScreenshotPath(fileName);

    public static void DeleteAll() =>
        Directory.Delete(DungbeetlePath.Screenshots(""), true);

    private static string RandomName() {
        var randomSource = Path.GetRandomFileName();
        return randomSource.Substring(0, 8) + randomSource.Substring(9);
    }

    private static void EnsureFolderExists() {
        var folder = DungbeetlePath.Screenshots("");
        if (!Directory.Exists(folder))
            Directory.CreateDirectory(folder);
    }

    private static string ScreenshotPath(string fileName) =>
        DungbeetlePath.Screenshots($"{fileName}.png");
}
}
