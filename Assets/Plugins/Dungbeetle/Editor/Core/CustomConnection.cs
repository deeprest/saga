using System;
using System.Net.Sockets;

namespace Dungbeetle {
public static class CustomConnection {
    public static TcpClient Connection(string domain, int port, int timeout) {
        var dClient = new DungbeetleTcpClient(domain, port);
        var asyncResult = dClient.BeginConnect();

        if (!asyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(timeout)))
            throw new SocketException(10060);

        dClient.EndConnect(asyncResult);
        return dClient.TcpClient;
    }
}
}