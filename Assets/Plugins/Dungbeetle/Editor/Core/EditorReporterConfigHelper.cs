using UnityEngine;
using UnityEditor;

namespace Dungbeetle {
public class EditorReporterConfigHelper : IEditorReporterConfigHelper {
    private const string editorHomePath = "Assets/Plugins/Dungbeetle/Editor";
    private const string configFolderName = "Config";
    private const string configFileName = "EditorReporterConfig.asset";

    private static string configPath
        => $"{editorHomePath}/{configFolderName}/{configFileName}";

    [InitializeOnLoadMethod]
    static void Init() {
        EditorReporterConfig.SetHelper(new EditorReporterConfigHelper());
    }

    public EditorReporterConfig GetConfig() {
        var result = AssetDatabase.LoadMainAssetAtPath(configPath);
        if (result == null)
            result = NewConfig();
        return (EditorReporterConfig) result;
    }


    private EditorReporterConfig NewConfig() {
        var newConfig = ScriptableObject.CreateInstance<EditorReporterConfig>();

        if (!System.IO.Directory.Exists($"{editorHomePath}/{configFolderName}"))
            AssetDatabase.CreateFolder(editorHomePath, configFolderName);
        AssetDatabase.CreateAsset(newConfig, configPath);

        EditorUtility.SetDirty(newConfig);
        AssetDatabase.SaveAssets();

        EditorGUIUtility.PingObject(newConfig);

        return newConfig;
    }

    public void MarkDirty(EditorReporterConfig config)
        => EditorUtility.SetDirty(config);
}
}
