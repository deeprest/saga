using System.Collections.Generic;
using System.IO;
using YamlDotNet.Serialization;

namespace Dungbeetle {
public class ServerMigrationSettings {
    public class ProjectMigration {
        public int sourceId { get; set; }
        public int targetId { get; set; }
    }

    private Dictionary<int, int> migrationRules;

    private Dictionary<int, int> CreateMigrationRules() {
        var result = new Dictionary<int, int>(projects.Length);
        foreach (var pm in projects)
            result[pm.sourceId] = pm.targetId;
        return result;
    }

    public bool TryGetProjectId(int sourceId, out int targetId) {
        if (migrationRules == null)
            migrationRules = CreateMigrationRules();
        return migrationRules.TryGetValue(sourceId, out targetId);
    }

    public string targetDomain { get; set; }
    public int targetPort { get; set; }
    public string targetPassword { get; set; }
    public string targetPubKey { get; set; }

    public ProjectMigration[] projects { get; set; }
    public bool clearTargetDatabase { get; set; }
    public bool clearSourceDatabase { get; set; }

    public override string ToString() {
        return $"ServerMigrationSettings{{{targetDomain}:{targetPort}:${targetPassword.Substring(8)}...:{targetPubKey.Substring(8)}... - " +
               $"(clearTargetDB({clearTargetDatabase}), clearSourceDB({clearSourceDatabase}))}}";
    }

    public string ToYaml() {
        var o = new StringWriter();
        var s = new SerializerBuilder().DisableAliases().Build();
        s.Serialize(o, this);
        return o.ToString();
    }

    public static ServerMigrationSettings FromYaml(string source) {
        var input = new StringReader(source);
        var deserializer = new DeserializerBuilder().Build();
        return deserializer.Deserialize<ServerMigrationSettings>(input);
    }
}
}