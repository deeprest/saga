using System.Collections.Generic;

namespace Dungbeetle {
public static class ServerStatus {

    public static bool ProjectsEqual(List<Project> left, List<Project> right) {
        if (left.Count != right.Count)
            return false;

        for (int i = 0; i < left.Count; i++)
            if (!left[i].Equals(right[i]))
                return false;

        return true;
    }

}
}