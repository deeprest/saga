using System;
using System.Data;
using System.Data.SQLite;

namespace Dungbeetle {
public class SQLiteDatabaseLinkBase : DungbeetleDBBase {

    private static readonly string DBConnectionString = $"Data Source={DungbeetlePath.Database};Pooling=True;";


    public SQLiteDatabaseLinkBase(IOutput o) : base(o) { }

    protected override IDbCommand Command(string query, IDbConnection connection) {
        return new SQLiteCommand(query, (SQLiteConnection) connection);
    }

    protected override IDbConnection Connection(string connectionString) {
        return new SQLiteConnection(connectionString);
    }

    protected override bool HandleException (Exception e) {
        var me = e as SQLiteException;
        if (me != null) {
            output.Log($"Couldn't open '{DungbeetlePath.Database}': {me.Message}");
            return true;
        }
        return false;
    }

    protected override string Db_Connection_String => DBConnectionString;
    protected override bool Db_Bug_Autoincrement => false;
    protected override string DB_INT => "INTEGER";
    protected override string DB_SHORT_STRING => "TEXT";
    protected override string DB_TEXT => "TEXT";
    protected override string DB_AUTOINCREMENT => "AUTOINCREMENT";
    protected override string DB_TABLE_SUFFIX => "";
    protected override string DB_SUBSTRING => "SUBSTR(content, -{0})";

    protected override void CreateAndUseDBTable(IDbConnection connection, string dbName) {
        //There aren't multiple databases in SQLite
    }

    public override bool DatabaseAligned() {
        var isAligned = true;

        UsingConnection(connection => {
            foreach (var table_name in new [] { "project", "bug", "developer", "attachment"}) {
                using (var cmd = Command("SELECT * FROM sqlite_master WHERE type='table' AND name=@tableName", connection)) {
                    cmd.AddWithValue("@tableName", table_name);
                    using (var reader = cmd.ExecuteReader())
                        if (!reader.Read())
                            if (!reader.Read()) {
                                isAligned = false;
                                return;
                            }
                }
            }
        });

        return isAligned;
    }

    public override int LastProjectId(IDbCommand cmd) {
        cmd.Parameters.Clear();
        cmd.CommandText = "SELECT id FROM project WHERE rowid=last_insert_rowid()";
        return Convert.ToInt32(cmd.ExecuteScalar());
    }

    public override int LastDeveloperId(IDbCommand cmd) {
        cmd.Parameters.Clear();
        cmd.CommandText = "SELECT id FROM developer WHERE rowid=last_insert_rowid()";
        return Convert.ToInt32(cmd.ExecuteScalar());
    }

    public override int LastBugId(IDbCommand cmd) {
        cmd.Parameters.Clear();
        cmd.CommandText = "SELECT id FROM bug WHERE rowid=last_insert_rowid()";
        return Convert.ToInt32(cmd.ExecuteScalar());
    }

    public override string InsertBugQuery(bool reuseId, bool reuseState) {
        var stateLabel = reuseState ? "state, "     : "";
        var stateField = reuseState ? "@bugState, " : "";

        //id will always be inserted explicitly in SQLite, because there's two keys
        string query;
        if (reuseId)
            query = $"INSERT INTO bug (id, project_id, {stateLabel}address, email, imagename, priority, scene, build_name, description, comment, " +
                    $"accept_responsible, fix_responsible, fix_source, resolution_responsible, contact_responsible) VALUES (@bugId, @projectId, " +
                    $"{stateField}@address, @email, @imageName, @priority, @scene, @build_name, @description, @comment, @accept_responsible, " +
                    $"@fix_responsible, @fix_source, @resolution_responsible, @contact_responsible)";
        else
            query = $"INSERT INTO bug (id, project_id, {stateLabel}address, email, imagename, priority, scene, build_name, description, comment, " +
                    $"accept_responsible, fix_responsible, fix_source, resolution_responsible, contact_responsible) SELECT IFNULL(MAX(id), 0)+1, " +
                    $"@projectId, {stateField}@address, @email, @imageName, @priority, @scene, @build_name, @description, @comment, @accept_responsible, " +
                    $"@fix_responsible, @fix_source, @resolution_responsible, @contact_responsible FROM bug WHERE project_id=@projectId";
        return query;
    }
}
}
