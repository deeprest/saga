using System.IO;
using System.Data.SQLite;

namespace Dungbeetle {

public class SQLiteDatabaseLink : SQLiteDatabaseLinkBase {
    public SQLiteDatabaseLink(IOutput o) : base(o) { }

    public bool DatabaseExists() {
        if (!File.Exists(DungbeetlePath.Database))
            return false;

        return ReadyQuery("SELECT * FROM sqlite_master WHERE type='table' and name=@name", cmd => {
            cmd.AddWithValue("@name", "project");

            using (var reader = cmd.ExecuteReader())
                return reader.Read();
        });
    }

    public void CreateDatabase() {
        //CreateDirectory does nothing if the directory exists.
        Directory.CreateDirectory(DungbeetlePath.Home);
        if (!File.Exists(DungbeetlePath.Database))
            SQLiteConnection.CreateFile(DungbeetlePath.Database);

        UsingConnection(connection => {
            TableDefinitions(connection, null);
        });
    }
}

}
