using System;
using System.Data;
using MySql.Data.MySqlClient;

namespace Dungbeetle {

public class MySQLDatabaseLinkBase : DungbeetleDBBase {

    public string databaseURL;
    public string databaseUser;
    public string databasePassword;
    public string databaseName;

    public MySQLDatabaseLinkBase(IOutput o) : base(o) { }

    protected override IDbCommand Command(string query, IDbConnection connection) {
        return new MySqlCommand(query, (MySqlConnection) connection);
    }

    protected override IDbConnection Connection(string connectionString) {
        return new MySqlConnection(connectionString);
    }

    protected override bool HandleException(Exception e) {
        var me = e as MySqlException;
        if (me != null) {
            if (me.Number == 0) {
                output.Log("Cannot connect to server for some reason");
                output.Log(me.ToString());
            }
            else if (me.Number == 1045) {
                output.Log("Invalid username and/or password");
            }
            else {
                output.Log($"Failed to open connection {me.Number}");
                output.Log(me.ToString());
            }
            return true;
        }
        return false;
    }

    protected override string Db_Connection_String => $"SERVER={databaseURL};DATABASE={databaseName};UID={databaseUser};PASSWORD={databasePassword};";

    protected override bool Db_Bug_Autoincrement => true;

    protected override string DB_INT => "INT(11)";

    protected override string DB_SHORT_STRING => "VARCHAR(255)";

    protected override string DB_TEXT => "MEDIUMTEXT";

    protected override string DB_AUTOINCREMENT => "AUTO_INCREMENT";

    protected override string DB_TABLE_SUFFIX => " ENGINE=MyISAM DEFAULT CHARSET=latin1";

    protected override string DB_SUBSTRING => "RIGHT(content, {0})";

    protected override void CreateAndUseDBTable(IDbConnection connection, string dbName) {
        NonQuery($"CREATE DATABASE `{dbName}`", connection);
        NonQuery($"USE `{dbName}`", connection);
    }

    public override bool DatabaseAligned() {
        var isAligned = true;
        UsingConnection(connection => {
            foreach (var table_name in new[] {"project", "bug", "developer", "attachment"}) {
                using (var cmd = Command("SHOW TABLES LIKE @tableName", connection)) {
                    cmd.AddWithValue("@tableName", table_name);
                    using (var reader = cmd.ExecuteReader())
                        if (!reader.Read()) {
                            isAligned = false;
                            return;
                        }

                }
            }
        });
        return isAligned;
    }

    public override int LastProjectId(IDbCommand cmd) {
        return (int) ((MySqlCommand) cmd).LastInsertedId;
    }

    public override int LastDeveloperId(IDbCommand cmd) {
        return (int) ((MySqlCommand) cmd).LastInsertedId;
    }

    public override int LastBugId(IDbCommand cmd) {
        return (int) ((MySqlCommand) cmd).LastInsertedId;
    }

    public override string InsertBugQuery(bool reuseId, bool reuseState) {
        var idLabel    = reuseId    ? "id, "        : "";
        var idField    = reuseId    ? "@bugId, "    : "";
        var stateLabel = reuseState ? "state, "     : "";
        var stateField = reuseState ? "@bugState, " : "";

        return $"INSERT INTO bug ({idLabel}project_id, {stateLabel}address, email, imagename, priority, scene, build_name, description, comment, " +
               $"accept_responsible, fix_responsible, fix_source, resolution_responsible, contact_responsible) VALUES({idField}@projectId, " +
               $"{stateField}@address, @email, @imageName, @priority, @scene, @build_name, @description, @comment, @accept_responsible, @fix_responsible, " +
               $"@fix_source, @resolution_responsible, @contact_responsible)";
    }
}
}
