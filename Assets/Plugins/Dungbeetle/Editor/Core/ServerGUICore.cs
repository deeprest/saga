using UnityEngine;
using UnityEditor;

namespace Dungbeetle {
public class ServerGUICore {

    [SerializeField]
    private OutputBuffer serverLog = new OutputBuffer();
    [SerializeField]
    private Vector2 serverScrollPos = Vector2.zero;
    [SerializeField]
    private bool serverExplicitlyStarted;

    public LoggedEvent Logged;
    public delegate void LoggedEvent();

    public ServerGUICore() {
        serverLog.Logged = OnLogged; // Override old event listeners
    }

    private void OnLogged() {
        Logged?.Invoke();
    }

    public void Run(bool explicitly) {
        Server.Run(serverLog, false, explicitly || !serverExplicitlyStarted); // Make the explict announcement the first time
        serverExplicitlyStarted = true;
    }

    public void Stop(bool explicitly) {
        Server.Stop(explicitly);
    }

    public void ClearCookies() {
        Server.ClearCookies();
    }

    public void ClearHistory() {
        serverLog.Clear();
    }

    public void ServerGUI(float height, float width) {
        //Handle context mouse clicks (right click)
        var evt = Event.current;
        if (evt.type == EventType.ContextClick) {
            var menu = new GenericMenu();
            menu.AddItem(new GUIContent("Clear"), false, ClearHistory);
            menu.ShowAsContext();
            evt.Use();
        }

        var s = serverLog.ToString();
        serverScrollPos = GUILayout.BeginScrollView(serverScrollPos, GUILayout.Height(height));
        GUILayout.Box(s, EditorStyles.textField, GUILayout.ExpandHeight(true));

        GUILayout.EndScrollView();
    }

    public void ScrollDown() {
        serverScrollPos.y = Mathf.Infinity;
    }
}
}