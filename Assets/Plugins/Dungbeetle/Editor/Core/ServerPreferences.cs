using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using YamlDotNet.Serialization;

namespace Dungbeetle {
public enum ServerDatabaseMode {
    SQLite,
    MYSql
}

public class ServerPreferences {
    public static ServerDatabaseMode GetMode()              => instance.content.mode;
    public static ushort             GetPort()              => instance.content.port;
    public static string             GetDbUserName()        => instance.content.dbUserName;
    public static string             GetDbPassword()        => instance.content.dbPassword;
    public static ushort             GetDbPort()            => instance.content.dbPort;
    public static string             GetDbUrl()             => instance.content.dbUrl;
    public static string             GetDbName()            => instance.content.dbName;
    public static string             GetReporterPassword()  => instance.content.reporterPassword;
    public static string             GetDeveloperPassword() => instance.content.developerPassword;
    public static string             GetPrivateKey()        => instance.content.privateKey;

    public static void SetMode(ServerDatabaseMode value) {
        instance.content.changed = instance.content.mode != value;
        instance.content.mode = value;
    }

    public static void SetPort(ushort value) {
        instance.content.changed = instance.content.port != value;
        instance.content.port = value;
    }

    public static void SetDbUserName(string value) {
        instance.content.changed = instance.content.dbUserName != value;
        instance.content.dbUserName = value;
    }

    public static void SetDbPassword(string value) {
        instance.content.changed = instance.content.dbPassword != value;
        instance.content.dbPassword = value;
    }

    public static void SetDbPort(ushort value) {
        instance.content.changed = instance.content.dbPort != value;
        instance.content.dbPort = value;
    }

    public static void SetDbUrl(string value) {
        instance.content.changed = instance.content.dbUrl != value;
        instance.content.dbUrl = value;
    }

    public static void SetDbName(string value) {
        instance.content.changed = instance.content.dbName != value;
        instance.content.dbName = value;
    }

    public static void SetReporterPassword(string value) {
        instance.content.changed = instance.content.reporterPassword != value;
        instance.content.reporterPassword = value;
    }

    public static void SetDeveloperPassword(string value) {
        instance.content.changed = instance.content.developerPassword != value;
        instance.content.developerPassword = value;
    }

    public static void SetPrivateKey(string value) {
        instance.content.changed = instance.content.privateKey != value;
        instance.content.privateKey = value;
    }

    public static void SaveIfChanged() {
        instance.Save(false);
    }

    public static void SaveToFile (string targetPath, ushort port, string rPasswd, string devPasswd, string privKey) {
        var tempInstance     = new ServerPreferences();
        tempInstance.content = new ServerPreferencesContent(true);
        tempInstance.content.port              = port;
        tempInstance.content.reporterPassword  = rPasswd;
        tempInstance.content.developerPassword = devPasswd;
        tempInstance.content.privateKey        = privKey;
        tempInstance.Save(true, targetPath);
    }

    private static ServerPreferences instance {
        get {
            if (_instance == null) {
                if (File.Exists(DungbeetlePath.Preferences)) {
                    using (var fileStreamReader = new StreamReader(new FileStream(DungbeetlePath.Preferences, FileMode.Open, FileAccess.Read))) {
                        var deserializer = new DeserializerBuilder().Build();
                        _instance = new ServerPreferences();
                        _instance.content = deserializer.Deserialize<ServerPreferencesContent>(fileStreamReader);
                    }
                }
                else {
                    _instance = new ServerPreferences();
                    _instance.content = new ServerPreferencesContent(true);
                    _instance.Save(true);
                }
            }

            return _instance;
        }
    }
    private static ServerPreferences _instance;

    private ServerPreferences() { }

    private ServerPreferencesContent content;

    private void Save(bool forced) => Save(forced, DungbeetlePath.Preferences);

    private void Save(bool forced, string pathSpec) {
        if (content.changed || forced) {
            content.changed = false;
            var serializer = new SerializerBuilder().EmitDefaults().Build();
            var stringBuilder = new StringBuilder();
            serializer.Serialize(new StringWriter(stringBuilder), content);

            Directory.CreateDirectory(Path.GetDirectoryName(pathSpec));
            File.WriteAllText(pathSpec, stringBuilder.ToString());
        }
    }

    private const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    private static readonly Random randomProvider = new Random();

    public static string RandomPassword(int length) {
        var res = new StringBuilder();
        while (0 < length--)
            res.Append(valid[randomProvider.Next(valid.Length)]);
        return res.ToString();
    }

    private class ServerPreferencesContent {
        public ServerPreferencesContent() {
            //  For Yaml deserialized objects;
        }

        public ServerPreferencesContent(bool isNew /* always true */) {
            mode = ServerDatabaseMode.SQLite;
            port = 15275;
            dbUserName = "";
            dbPassword = "";
            dbPort = 3306;
            dbUrl = "localhost";
            dbName = "dungbeetle";

            reporterPassword = RandomPassword(16);
            developerPassword = RandomPassword(16);

            var rsaProvider = new RSACryptoServiceProvider();
            privateKey = rsaProvider.ToXmlString(true);
        }

        public ServerDatabaseMode mode { get; set; }
        public ushort port { get; set; }
        public string dbUserName { get; set; }
        public string dbPassword { get; set; }
        public ushort dbPort { get; set; }
        public string dbUrl { get; set; }
        public string dbName { get; set; }

        public string reporterPassword { get; set; }
        public string developerPassword { get; set; }
        public string privateKey { get; set; }

        [YamlIgnore]
        public bool changed;

        public override string ToString() {
            return $"{mode}, {port}, {dbUserName}, {dbPassword}, {dbPort}, {dbUrl}, {dbName}";
        }
    }
}
}
