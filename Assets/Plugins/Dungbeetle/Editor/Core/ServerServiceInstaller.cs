using System;
using System.Security;

namespace Dungbeetle {
public static class ServerServiceInstaller {
    public static void Install(IOutput o) {
        try {
            var key = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("SYSTEM\\CurrentControlSet\\Services\\Dungbeetle");
            key.SetValue("LogFile", DungbeetlePath.Log);
            key.SetValue("Preferences", DungbeetlePath.Preferences);
            key.SetValue("Home", DungbeetlePath.Home);
            key.Close();

            o.Log("Installed keys in the registry under 'HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Dungbeetle'.");
        }
        catch (SecurityException) {
            o.Log("Failed to install keys in the registry under 'HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Dungbeetle'.\n" +
                  "Insufficient permissions. Are you running the program as an administrator?");
        }
        catch (UnauthorizedAccessException) {
            o.Log("Failed to install keys in the registry under 'HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Dungbeetle'.\n" +
                  "Insufficient permissions. Are you running the program as an administrator?");
        }
    }

    public static void Uninstall(IOutput o) {
        try {
            var key = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Services\\Dungbeetle", true);
            if (key != null) {
                key.DeleteValue("LogFile", false);
                key.DeleteValue("Preferences", false);
                key.DeleteValue("Home", false);
                var delete = key.GetValueNames().Length == 0;
                key.Close();

                if (delete)
                    Microsoft.Win32.Registry.LocalMachine.DeleteSubKey("SYSTEM\\CurrentControlSet\\Services\\Dungbeetle");
                o.Log("Uninstalled keys from the registry under 'HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Dungbeetle'.");
            }
            else
                o.Log("No registry keys were installed.");
        }
        catch (SecurityException) {
            o.Log("Failed to uninstall registry keys. Are you running the program as an administrator?");
        }
    }
}
}