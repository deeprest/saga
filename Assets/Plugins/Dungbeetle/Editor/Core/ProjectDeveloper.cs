using System;
using System.IO;
using System.Text;
using YamlDotNet.Serialization;

namespace Dungbeetle {
public class ProjectDeveloper : IEquatable<ProjectDeveloper> {
    public const string Any = "Any";

    public int id { get; set; }
    public string name { get; set; }

    public ProjectDeveloper() {
        // Needs to exist due to yaml-serialization. Don't call manually, as that could give a duplicate id!
    }

    public ProjectDeveloper(int id, string name) {
        this.id = id;
        this.name = name;
    }

    public bool Equals(ProjectDeveloper other) {
        if (other == null)
            return false;
        return id == other.id && string.Equals(name, other.name, StringComparison.Ordinal);
    }

    public override bool Equals(object obj) {
        if (obj is ProjectDeveloper)
            return Equals((ProjectDeveloper) obj);
        return false;
    }

    public override int GetHashCode() {
        unchecked {
            return (id * 397) ^ (name != null ? name.GetHashCode() : 0);
        }
    }

    public string ToYaml() {
        var serializer = new SerializerBuilder().Build();
        var stringBuilder = new StringBuilder();
        var stringWriter = new StringWriter(stringBuilder);
        serializer.Serialize(stringWriter, this);
        return stringBuilder.ToString();
    }

    public static ProjectDeveloper FromYaml(string source) {
        var input = new StringReader(source);
        var deserializer = new DeserializerBuilder().Build();
        return deserializer.Deserialize<ProjectDeveloper>(input);
    }

    public new ProjectDeveloper MemberwiseClone() {
        return (ProjectDeveloper) base.MemberwiseClone();
    }
}
}