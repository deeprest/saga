using System;
using System.Net;
using System.Threading;
using System.Collections.Generic;

namespace Dungbeetle {
public class SymKeyCacheKey : IEquatable<SymKeyCacheKey> {
    private readonly IPAddress address;
    private readonly string userString;

    public SymKeyCacheKey(IPAddress a, string u) {
        address = a;
        userString = u;
    }

    public override int GetHashCode() {
        return address.GetHashCode() ^ userString.GetHashCode();
    }

    public override bool Equals(object obj) {
        var key = obj as SymKeyCacheKey;
        return key != null && Equals(key);
    }

    public bool Equals(SymKeyCacheKey other) {
        return other != null && address.Equals(other.address) && userString.Equals(other.userString);
    }
}

public class SymKeyElement {
    public SymKeyCacheKey ClientKey;
    public byte[] Key;
    public PrivilegeLevel Privilege;

    private DateTime time;

    public bool Expired => (DateTime.Now - time).Minutes > 10f;

    public void Refresh() => time = DateTime.Now;

    public SymKeyElement(SymKeyCacheKey cKey, PrivilegeLevel privilege, byte[] key) {
        ClientKey = cKey;
        Privilege = privilege;
        Key = key;
        Refresh();
    }
}

public class SymKeyCache {
    private LinkedList<SymKeyElement> sortedKeys;
    private Dictionary<SymKeyCacheKey, LinkedListNode<SymKeyElement>> keyIndex;

    private object localLock;
    private object sleepLock;

    private bool running = true;

    public SymKeyCache() {
        sortedKeys = new LinkedList<SymKeyElement>();
        keyIndex = new Dictionary<SymKeyCacheKey, LinkedListNode<SymKeyElement>>();
        localLock = new object();
        sleepLock = new object();

        new Thread(ExpireOldElements).Start();
    }

    public void Add(IPAddress a, string clientKeyStr, PrivilegeLevel privilege, byte[] key) {
        lock (localLock) {
            var newClientKey = new SymKeyCacheKey(a, clientKeyStr);
            var newElement = new SymKeyElement(newClientKey, privilege, key);
            var node = sortedKeys.AddFirst(newElement);
            keyIndex[newClientKey] = node;
        }
    }

    public bool TryGet(IPAddress a, string clientKeyStr, ref PrivilegeLevel privilege, ref byte[] key) {
        lock (localLock) {
            var clientKey = new SymKeyCacheKey(a, clientKeyStr);
            if (keyIndex.ContainsKey(clientKey)) {
                var element = keyIndex[clientKey].Value;
                sortedKeys.Remove(element);
                sortedKeys.AddFirst(element);
                element.Refresh();
                privilege = element.Privilege;
                key = element.Key;
                return true;
            }
            else
                return false;
        }
    }

    private void ExpireOldElements() {
        while (running) {
            lock (sleepLock) {
                Monitor.Wait(sleepLock, TimeSpan.FromSeconds(5));
                lock (localLock) {
                    var current = sortedKeys.Last;
                    while (current != null && current.Value.Expired) {
                        keyIndex.Remove(current.Value.ClientKey);
                        var previous = current.Previous;
                        sortedKeys.Remove(current);
                        current = previous;
                    }
                }
            }
        }
    }

    public void Stop() {
        running = false;
        lock (sleepLock) {
            Monitor.Pulse(sleepLock);
        }
    }

    public void Clear() {
        lock (localLock) {
            sortedKeys.Clear();
            keyIndex.Clear();
        }
    }
}
}
