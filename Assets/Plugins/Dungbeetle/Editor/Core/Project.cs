using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using YamlDotNet.Serialization;

namespace Dungbeetle {
    public class Project : IEquatable<Project> {
        public int id { get; set; }
        public string name { get; set; }
        public List<ProjectDeveloper> developers { get; set; }

        public bool Equals(Project other) {
            if (other == null)
                return false;
            if (id != other.id)
                return false;
            if (name != other.name)
                return false;
            return developers.SequenceEqual(other.developers);
        }

        public static List<Project> CloneList(List<Project> original) {
            var result = new List<Project>(original.Count);
            for (int i = 0; i < original.Count; i++) {
                var pClone = new Project {
                    id = original[i].id,
                    name = original[i].name,
                    developers = new List<ProjectDeveloper>()
                };

                for (int j = 0; j < original[i].developers.Count; j++) {
                    pClone.developers.Add(original[i].developers[j].MemberwiseClone());
                }
            }

            return result;
        }

        public override string ToString() {
            return $"{id}: {name} ({(developers.Count)} developers)";
        }

        public string ToYaml() {
            var serializer = new SerializerBuilder().Build();
            var stringBuilder = new StringBuilder();
            var stringWriter = new StringWriter(stringBuilder);
            serializer.Serialize(stringWriter, this);
            return stringBuilder.ToString();
        }

        public static Project FromYaml(string source) {
            var input = new StringReader(source);
            var deserializer = new DeserializerBuilder().Build();
            return deserializer.Deserialize<Project>(input);
        }
    }
}