using System;
using System.Collections.Generic;
using System.IO;
using YamlDotNet.RepresentationModel;
using YamlDotNet.Serialization;

namespace Dungbeetle {
public class SerializedBugParent : SerializedBugReportParent {
    public SerializedBug Bug { get; set; }
}

public class SerializedBug : SerializedBugReport {
    public string id                     { get; set; }
    public string state                  { get; set; }
    public DateTime time                 { get; set; }
    public string address                { get; set; }
    public string imageName              { get; set; }
    public string priority               { get; set; }
    public string comment                { get; set; }
    public string accept_responsible     { get; set; }
    public string fix_responsible        { get; set; }
    public string fix_source             { get; set; }
    public string resolution_responsible { get; set; }
    public string contact_responsible    { get; set; }

    public SerializedBug(Bug bug) : base(bug) {
        id        = bug.id.ToString();
        state     = ((int) (bug.state)).ToString();
        time      = bug.time;
        address   = bug.address;
        imageName = bug.imageName;
        priority  = bug.priority;
        comment   = bug.comment;
        accept_responsible     = bug.accept_responsible.ToString();
        fix_responsible        = bug.fix_responsible.ToString();
        fix_source             = bug.fix_source;
        resolution_responsible = bug.resolution_responsible.ToString();
        contact_responsible    = bug.contact_responsible.ToString();
    }
}

//Carefull! Used for serialization and db.
public enum BugState {
    New,
    Assigned,
    Fixed,
    Resolved,
}

public class Bug : BugReport {
    public static readonly string bugFormatVersion = "2.0";

    public int      id;
    public BugState state;
    public DateTime time;
    public string   address;
    public string   imageName;
    public string   priority;
    public string   comment;
    public int      accept_responsible;
    public int      fix_responsible;
    public string   fix_source;
    public int      resolution_responsible;
    public int      contact_responsible;

    public string timeAsString; //only needed because Unity can't serialize DateTime

    public Bug() {
        attachments = new List<string>();
    }

    public Bug(BugReport report)
        : this(0, report.projectId, BugState.New, new DateTime(), "", report.email, "", "Low", report.scene, report.build_name, report.description, "", -1, -1, "", -1, -1, report.attachments) { }

    public Bug(int iden, int p, BugState s, DateTime t, string addr, string em, string img_name, string pr, string sc, string bn, string desc, string cmt, int acc_r, int fix_r, string fix_s, int res_r, int contact_r)
        : this(iden, p, s, t, addr, em, img_name, pr, sc, bn, desc, cmt, acc_r, fix_r, fix_s, res_r, contact_r, new List<string>()) { }

    public Bug(int iden, int p, BugState s, DateTime t, string addr, string em, string img_name, string pr, string sc, string bn, string desc, string cmt, int acc_r, int fix_r, string fix_s, int res_r, int contact_r, List<string> att) : base(p, em, sc, bn, desc, att) {
        id           = iden;
        state        = s;
        time         = t;
        timeAsString = time.ToLocalTime().ToString("yyyy-MM-dd HH:mm");
        address      = addr;
        imageName    = img_name;
        priority     = pr;
        comment      = cmt;
        accept_responsible     = acc_r;
        fix_responsible        = fix_r;
        fix_source             = fix_s;
        resolution_responsible = res_r;
        contact_responsible    = contact_r;
    }

    public void SetPriority              (string p) => priority = p;
    public void SetContactResponsible    (int    r) => contact_responsible = r;
    public void SetAcceptResponsible     (int    r) => accept_responsible = r;
    public void SetFixResponsible        (int    r) => fix_responsible = r;
    public void SetResolutionResponsible (int    r) => resolution_responsible = r;

    public override string ToString() => $"Bug{{{id} - {state} - {email} - {description} }}";

    public override string ToYaml() {
        var b = new SerializedBugParent {Bug = new SerializedBug(this)};
        var o = new StringWriter();
        var s = new SerializerBuilder().DisableAliases().Build();

        s.Serialize(o, b);
        return o.ToString();
    }

    public new static Bug FromYaml(string source, string version) {
        var invalid = version != bugFormatVersion;

        var yaml = new YamlStream();
        yaml.Load(new StringReader(source));
        var mapping = (YamlMappingNode) yaml.Documents[0].RootNode;

        var bug = new Bug();
        foreach (var root in mapping.Children)
        foreach (var entry in (YamlMappingNode) root.Value)
            bug.AssignKey(entry.Key.ToString(), entry.Value);

        if (invalid)
            bug.UpgradeFrom(version);

        return bug;
    }

    protected override bool AssignKey(string key, YamlNode valueNode) {
        if (base.AssignKey(key, valueNode))
            return true;

        int buf;
        var value = valueNode.ToString();
        switch (key) {
            case "id":
                if (int.TryParse(value, out buf))
                    id = buf;
                break;
            case "state":
                if (int.TryParse(value, out buf))
                    state = (BugState) buf;
                break;
            case "time":
                using (var timeReader = new StringReader(value)) {
                    time = new DeserializerBuilder().Build().Deserialize<DateTime>(timeReader);
                    timeAsString = time.ToLocalTime().ToString("yyyy-MM-dd HH:mm");
                }

                break;
            case "address":
                address = value;
                break;
            case "imageName":
                imageName = value;
                break;
            case "priority":
                priority = value;
                break;
            case "comment":
                comment = value;
                break;
            case "accept_responsible":
                if (int.TryParse(value, out buf))
                    accept_responsible = buf;
                break;
            case "fix_responsible":
                if (int.TryParse(value, out buf))
                    fix_responsible = buf;
                break;
            case "fix_source":
                fix_source = value;
                break;
            case "resolution_responsible":
                if (int.TryParse(value, out buf))
                    resolution_responsible = buf;
                break;
            case "contact_responsible":
                if (int.TryParse(value, out buf))
                    contact_responsible = buf;
                break;
            default:
                return false;
        }

        return true;
    }
}
}
