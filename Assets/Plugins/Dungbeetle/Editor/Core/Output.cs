using System;

namespace Dungbeetle
{
    public class ConsoleOutput : IOutput
    {
        public void Log(string s) => Console.WriteLine(s);
    }

    public class FileOutput : IOutput
    {
        private string path;

        public FileOutput(string p)
        {
            path = p;
        }

        public void Log(string s)
        {
            try
            {
                using (var sw = System.IO.File.AppendText(path))
                    sw.WriteLine(s);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Could not log to {path}");
                Console.WriteLine(e.Message);
                Console.WriteLine($"The message was: {s}");
            }
        }
    }
}
