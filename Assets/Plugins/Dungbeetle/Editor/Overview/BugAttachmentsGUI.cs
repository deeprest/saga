using System.IO;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class BugAttachmentsGUI {

    private Bug bug;

    private bool socketFailed;
    private string socketFailMsg;

    private Vector2 scrollPos;
    private int attachmentIndex;

    public void OnGUI() {
        if (socketFailed) {
            GUILayout.Label($"Failed to fetch attachments for bug {bug.id}.\n" +
                            $"Please connect to the network, then reopen this window.\n\n" +
                            $"Attempted to connect to {Config.OverviewClientConfig.Domain}:{Config.OverviewClientConfig.Port}: {socketFailMsg}");
            return;
        }

        if (attachmentIndex < 0)
            attachmentIndex = 0;
        else if(bug.attachments != null && bug.attachments.Count <= attachmentIndex) {
            attachmentIndex = bug.attachments.Count - 1;
        }

        var style = new GUIStyle(GUI.skin.label);
        style.fontSize = 20;

        if (bug == null) {
            GUILayout.Label("Fetching attachments from server", style);
            return;
        }

        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height - 20)); // make room for the tab

        GUILayout.Label($"Bug {bug.id} attachments", style);

        GUILayout.BeginHorizontal();
        GUI.enabled = attachmentIndex > 0;
        if (GUILayout.Button("Previous")) {
            --attachmentIndex;
            GUIUtility.keyboardControl = 0; // Allow text areas to update their content
            GUIUtility.hotControl = 0;
        }

        GUI.enabled = true;

        if (GUILayout.Button("Save"))
            SaveFullAttachment(bug.attachmentIds[attachmentIndex]);
        GUI.enabled = attachmentIndex < bug.attachments.Count - 1;
        if (GUILayout.Button("Next")) {
            ++attachmentIndex;
            GUIUtility.keyboardControl = 0;
            GUIUtility.hotControl = 0;
        }

        GUI.enabled = true;
        GUILayout.EndHorizontal();

        scrollPos = GUILayout.BeginScrollView(scrollPos);

        if (bug.attachments != null && 0 <= attachmentIndex && attachmentIndex < bug.attachments.Count)
            EditorGUILayout.TextArea(bug.attachments[attachmentIndex]);
        else
            EditorGUILayout.TextArea("No viewable attachments");

        GUILayout.EndScrollView();

        GUILayout.EndArea();
    }

    private void SaveFullAttachment(int id) {
        var attachment = BugUtility.GetFullAttachment(id, ref socketFailed, ref socketFailMsg);
        var path = EditorUtility.SaveFilePanel("File to save", System.Environment.GetEnvironmentVariable("USERPROFILE"), $"Attachment {id}", "txt");
        if (!socketFailed && !string.IsNullOrEmpty(path))
            File.WriteAllText(path, attachment);
    }

    public void SetBug(Bug b) {
#if UNITY_2018_3_OR_NEWER
        var previewLimit = (1 << 16) - 2;
#else
        var previewLimit = (1 << 14) - 2;
#endif
        var attachments = BugUtility.GetAttachments(b, previewLimit, ref socketFailed, ref socketFailMsg);

        if (!socketFailed) {
            b.attachments   = attachments.attachmentsList;
            b.attachmentIds = attachments.idsList;
        }

        bug = b;
    }
}
}
