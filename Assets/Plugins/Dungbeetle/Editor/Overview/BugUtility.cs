using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using UnityEngine;

namespace Dungbeetle {
static class BugUtility {

    private static bool validFactory;
    private static DungbeetleNetworkFactory _networkFactory;
    public static DungbeetleNetworkFactory networkFactory {
        get {
            if (!validFactory) {
                _networkFactory = new DungbeetleNetworkFactory(Config.OverviewClientConfig.Password, Config.OverviewClientConfig.PubKey, PrivilegeLevel.Developer);
                validFactory = true;
            }

            return _networkFactory;
        }
    }

    public static void InvalidateNetworkDetails() {
        validFactory = false;
    }

    public static List<Bug> RequestBugs(string request, int extra, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network((client, network) => {
            network.Write(request);

            uint project = Config.OverviewClientConfig.ProjectId;

            network.Write((int) project);
            if (extra != -1)
                network.Write(extra);

            var bugStringArray = network.ReadStringArray();
            var result = new List<Bug>(bugStringArray.Length);
            foreach (var bugString in bugStringArray)
                result.Add(Bug.FromYaml(bugString, Bug.bugFormatVersion));

            return result;
        }, ref socketFailed, ref socketFailMsg);
    }

    public static void AssignBug(Bug bug, ref bool socketFailed, ref string socketFailMsg) {
        bug.state = BugState.Assigned;
        BugUtility.ChangeResponsible(bug, ref socketFailed, ref socketFailMsg);
    }

    public static void FixBug(Bug bug, ref bool socketFailed, ref string socketFailMsg) {
        bug.state = BugState.Fixed;
        BugUtility.ChangeResponsible(bug, ref socketFailed, ref socketFailMsg);
    }

    public static void ResolveBug(Bug bug, ref bool socketFailed, ref string socketFailMsg) {
        bug.state = BugState.Resolved;
        BugUtility.ChangeResponsible(bug, ref socketFailed, ref socketFailMsg);
    }

    public static void ReopenBug(Bug bug) {
        developer_network((client, network) => {
            network.Write("Reopen");
            network.Write(bug.projectId);
            network.Write(bug.id);
        });
    }

    public static void DeleteBug(Bug bug) {
        developer_network((client, network) => {
            network.Write("DeleteBug");
            network.Write(bug.projectId);
            network.Write(bug.id);
        });
    }

    public static List<ProjectDeveloper> GetAllProjectDevelopers(uint projectId, out bool socketFailed, out string socketFailMsg) {
        socketFailed = false;
        socketFailMsg = "";

        var defaultResult = new List<ProjectDeveloper>();
        return developer_network(defaultResult, (client, network) => {
            network.Write("GetDevelopers");
            network.Write((int) projectId);
            var developerStringArray = network.ReadStringArray();
            var result = new List<ProjectDeveloper>(developerStringArray.Length);
            foreach (var developerString in developerStringArray)
                result.Add(ProjectDeveloper.FromYaml(developerString));
            return result;
        }, ref socketFailed, ref socketFailMsg);
    }

    public static bool GetPing(string domain, int port, string password, string pubKey, ref bool socketFailed, ref string socketFailMsg, IOutput output) {
        return custom_network(domain, port, password, pubKey, Dungbeetle.PrivilegeLevel.Developer, (client, network) => {
            network.Write("Ping");
            return network.ReadString() == "Pong";
        }, ref socketFailed, ref socketFailMsg, output);
    }

    public static bool GetPingability(string sourceDomain, int sourcePort, string sourcePassword, string sourcePubKey, string targetDomain, int targetPort, string targetPassword, string targetPubKey, ref bool socketFailed, ref string socketFailMsg, IOutput output) {
        return custom_network(sourceDomain, sourcePort, sourcePassword, sourcePubKey, PrivilegeLevel.Developer, (client, network) => {
            network.Write("RelayedPing");
            network.Write(targetDomain);
            network.Write(targetPort);
            network.Write(targetPassword);
            network.Write(targetPubKey);
            return network.ReadString() == "RelayedPong";
        }, ref socketFailed, ref socketFailMsg, output);
    }

    public static bool CheckTargetMigrationViability(string domain, int port, string password, string pubKey, string targetDomain, int targetPort, out string error, ref bool socketFailed, ref string socketFailMsg, IOutput output) {
        string networkError = "";
        var result = custom_network(domain, port, password, pubKey, PrivilegeLevel.Developer, (client, network) => {
            network.Write("MigrateIpTargetCheck");
            network.Write(targetDomain);
            network.Write(targetPort);
            var networkResponse = network.ReadString();
            if (networkResponse == "Accepted")
                return true;
            else {
                networkError = networkResponse;
                return false;
            }
        }, ref socketFailed, ref socketFailMsg, output);

        error = networkError;
        return result;
    }

    public static void SetBugPriority(Bug bug, BugPriority priority, ref bool socketFailed, ref string socketFailMsg) {
        bug.SetPriority(Enum.GetName(typeof(BugPriority), priority));

        developer_network((client, network) => {
            network.Write("BugPriority");
            network.Write(bug.projectId);
            network.Write(bug.id);
            network.Write(bug.priority);
        }, ref socketFailed, ref socketFailMsg);
    }


    public static void SetBugContactResponsible(Bug bug, int responsible, ref bool socketFailed, ref string socketFailMsg) {
        bug.SetContactResponsible(responsible);
        developer_network((client, network) => {
            network.Write("ContactResponsible");
            network.Write(bug.projectId);
            network.Write(bug.id);
            network.Write(bug.contact_responsible);
        }, ref socketFailed, ref socketFailMsg);
    }

    public static void SetBugComment(Bug bug, string comment, ref bool socketFailed, ref string socketFailMsg) {
        bug.comment = comment;
        developer_network((client, network) => {
            network.Write("Update");
            network.Write(bug.projectId);
            network.Write(bug.id);
            network.Write(bug.comment);
        }, ref socketFailed, ref socketFailMsg);
    }

    public static void ChangeResponsible(Bug bug, ref bool socketFailed, ref string socketFailMsg) {
        developer_network((client, network) => {
            network.Write("BugChanges");
            network.Write(bug.projectId);
            network.Write(bug.id);
            network.Write((int) bug.state);
            network.Write(bug.comment);
            network.Write(bug.priority);
            network.Write(bug.accept_responsible);
            network.Write(bug.fix_responsible);
            network.Write(bug.fix_source);
            network.Write(bug.resolution_responsible);
        }, ref socketFailed, ref socketFailMsg);
    }

    public static Texture2D GetTexture(Bug bug, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network((client, network) => {
            network.Write("Image");
            network.Write(bug.imageName);
            var responseData = network.ReadBytes();
            var responseAsTexture = new Texture2D(4, 4, TextureFormat.RGB24, false);
            ImageConversion.LoadImage(responseAsTexture, responseData);
            return responseAsTexture;
        }, ref socketFailed, ref socketFailMsg);
    }

    public struct GetAttachmentsResult {
        public List<int> idsList;
        public List<string> attachmentsList;
    }

    public static GetAttachmentsResult GetAttachments(Bug b, int maxAttachmentLength, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network((client, network) => {
            network.Write("Attachments");
            network.Write(b.projectId);
            network.Write(b.id);
            network.Write(maxAttachmentLength);

            return new GetAttachmentsResult {
                idsList = new List<int>(network.ReadIntArray()),
                attachmentsList = new List<string>(network.ReadStringArray())
            };
        }, ref socketFailed, ref socketFailMsg);
    }

    public static string GetFullAttachment(int attachmentId, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network((client, network) => {
            network.Write("Attachment");
            network.Write(attachmentId);
            return network.ReadString();
        }, ref socketFailed, ref socketFailMsg);
    }

    public static List<Project> GetProjects(bool includeDevelopers, ref bool socketFailed, ref string socketFailMsg, int timeout) {
        return developer_network_timeout(timeout, (client, network) => {
            network.Write(includeDevelopers ? "GetProjects" : "GetProjectNames");

            var projectStringArray = network.ReadStringArray();
            var result = new List<Project>(projectStringArray.Length);
            foreach (var projectString in projectStringArray)
                result.Add(Project.FromYaml(projectString));
            return result;
        }, ref socketFailed, ref socketFailMsg);
    }

    public static List<Project> GetProjects(bool includeDevelopers, string domain, int port, string password, string pubKey, ref bool socketFailed, ref string socketFailMsg, IOutput output) {
        return custom_network(domain, port, password, pubKey, PrivilegeLevel.Developer, (client, network) => {
            network.Write(includeDevelopers ? "GetProjects" : "GetProjectNames");

            var projectStringArray = network.ReadStringArray();
            var result = new List<Project>(projectStringArray.Length);
            foreach (var projectString in projectStringArray)
                result.Add(Project.FromYaml(projectString));
            return result;
        }, ref socketFailed, ref socketFailMsg, output);
    }

    public static int AddProject(string name, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network((client, network) => {
            network.Write("AddProject");
            network.Write(name);
            return network.ReadInt32();
        }, ref socketFailed, ref socketFailMsg);
    }

    public static void DeleteProject(int id, ref bool socketFailed, ref string socketFailMsg) {
        developer_network((client, network) => {
            network.Write("DeleteProject");
            network.Write(id);
        }, ref socketFailed, ref socketFailMsg);
    }


    public static void RenameProject(int id, string name, ref bool socketFailed, ref string socketFailMsg) {
        developer_network((client, network) => {
            network.Write("RenameProject");
            network.Write(id);
            network.Write(name);
        }, ref socketFailed, ref socketFailMsg);
    }

    public static int AddDeveloper(int projectId, string name, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network((client, network) => {
            network.Write("AddDeveloper");
            network.Write(projectId);
            network.Write(name);
            return network.ReadInt32();
        }, ref socketFailed, ref socketFailMsg);
    }

    public static void DeleteDeveloper(int id, ref bool socketFailed, ref string socketFailMsg) {
        developer_network((client, network) => {
            network.Write("DeleteDeveloper");
            network.Write(id);
        }, ref socketFailed, ref socketFailMsg);
    }

    public static void RenameDeveloper(int id, string name, ref bool socketFailed, ref string socketFailMsg) {
        developer_network((client, network) => {
            network.Write("RenameDeveloper");
            network.Write(id);
            network.Write(name);
        }, ref socketFailed, ref socketFailMsg);
    }

    public static void MigrateSendDirect(string domain, int port, string password, string pubKey, ServerMigrationSettings settings, IOutput output, ref bool socketFailed, ref string socketFailMsg) {
        custom_network(domain, port, password, pubKey, PrivilegeLevel.Developer, (client, network) => {
            network.Write("MigrateSend");
            network.Write(settings.ToYaml());

            output.Log(network.ReadString()); // "Transfering projects"
            output.Log(network.ReadString()); // "Transfering bugs"
            output.Log(network.ReadString()); // "Transfering images
            if (settings.clearSourceDatabase)
                output.Log(network.ReadString()); // "Clearing source database"

            var result = network.ReadString();
            if (result != "Migration succeeded")
                output.Log($"Source server failed with message: {result}");
            else
                output.Log(result);
        }, ref socketFailed, ref socketFailMsg, output);
    }

    public static void MigrateReturn(string domain, int port, string password, string pubKey, ServerMigrationSettings settings, IOutput output, ref bool socketFailed, ref string socketFailMsg) {
        try {
            using (var client = CustomConnection.Connection(domain, port, 5000))
            using (var network = DungbeetleNetworkFactory.Create(client, password, pubKey, PrivilegeLevel.Developer, output))
            using (var target = CustomConnection.Connection(settings.targetDomain, settings.targetPort, 5000))
            using (var target_network = DungbeetleNetworkFactory.Create(target, settings.targetPassword, settings.targetPubKey, PrivilegeLevel.Developer, output)) {
                network.Write("MigrateReturn");
                network.Write(settings.ToYaml());

                target_network.Write("MigrateReceive");
                target_network.Write(settings.ToYaml());

                //Following protocol two-way
                var p = new MigrationProxyUtility(network, target_network);

                output.Log(p.PassOnString()); // "Transfering projects"

                var projectsCount = p.PassOnInt32();
                for (int i = 0; i < projectsCount; i++)
                    p.PassOnString();

                output.Log(p.PassOnString()); // "Transfering bugs"

                var num_bugs = p.PassOnInt32();

                for (int i = 0; i < num_bugs; i++) {
                    p.PassOnString(); // Bug version
                    p.PassOnString(); // Bug
                }

                output.Log(p.PassOnString()); // "Transfering images"

                for (int i = 0; i < p.PassOnInt32(); i++) {
                    p.PassOnString(); // imageName
                    p.PassOnBytes();
                }

                var status = target_network.ReadString(); // "Migration succeeded"
                network.Write(status);

                if (settings.clearSourceDatabase)
                    output.Log(p.PassOnString()); // "Clearing source database"

                status = p.PassOnString(); // "Migration succeeded"
                if (status == "Migration succeeded")
                    output.Log(status);
                else
                    output.Log($"Target server failed with message: {status}");
            }
        }
        catch (SocketException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
    }

    private class MigrationProxyUtility {
        private DungbeetleNetwork _sourceNetwork;
        private DungbeetleNetwork _targetNetwork;

        public MigrationProxyUtility(DungbeetleNetwork sourceNetwork, DungbeetleNetwork targetNetwork) {
            _sourceNetwork = sourceNetwork;
            _targetNetwork = targetNetwork;
        }

        public int PassOnInt32() {
            var result = _sourceNetwork.ReadInt32();
            _targetNetwork.Write(result);
            return result;
        }

        public string PassOnString() {
            var result = _sourceNetwork.ReadString();
            _targetNetwork.Write(result);
            return result;
        }

        public byte[] PassOnBytes() {
            var result = _sourceNetwork.ReadBytes();
            _targetNetwork.Write(result);
            return result;
        }
    }

    private static T custom_network<T>(string domain, int port, string password, string pubKey, PrivilegeLevel privilegeLevel, Func<TcpClient, DungbeetleNetwork, T> action, ref bool socketFailed, ref string socketFailMsg, IOutput output) {
        T result = default(T);
        try {
            using (var client = CustomConnection.Connection(domain, port, 1000))
            using (var network = DungbeetleNetworkFactory.Create(client, password, pubKey, privilegeLevel, output))  {
                result = action(client, network);
            }
        }
        catch (SocketException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
        catch (ProtocolException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
        catch (EndOfStreamException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }

        return result;
    }

    private static void custom_network(string domain, int port, string password, string pubKey, PrivilegeLevel privilegeLevel, Action<TcpClient, DungbeetleNetwork> action, ref bool socketFailed, ref string socketFailMsg, IOutput output) {
        try {
            using (var client = CustomConnection.Connection(domain, port, 1000))
            using (var network = DungbeetleNetworkFactory.Create(client, password, pubKey, privilegeLevel, output))  {
                action(client, network);
            }
        }
        catch (SocketException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
        catch (ProtocolException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
        catch (EndOfStreamException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
    }

    private static T developer_network<T>(T defaultResult, Func<TcpClient, DungbeetleNetwork, T> action, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network_timeout(defaultResult, 10000, action, ref socketFailed, ref socketFailMsg);
    }

    private static T developer_network<T>(Func<TcpClient, DungbeetleNetwork, T> action, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network_timeout(10000, action, ref socketFailed, ref socketFailMsg);
    }

    private static T developer_network_timeout<T>(int timeoutMilliseconds, Func<TcpClient, DungbeetleNetwork, T> action, ref bool socketFailed, ref string socketFailMsg) {
        return developer_network_timeout(default(T), timeoutMilliseconds, action, ref socketFailed, ref socketFailMsg);
    }

    private static IOutput unityDebug = new UnityDebug();

    private static T developer_network_timeout<T>(T defaultResult, int timeoutMilliseconds, Func<TcpClient, DungbeetleNetwork, T> action, ref bool socketFailed, ref string socketFailMsg) {
        var t = defaultResult;
        try {
            using (var client = OverviewServerConnection.Connection(timeoutMilliseconds))
            using (var network = networkFactory.Create(client, unityDebug)) {
                t = action(client, network);
            }
        }
        catch (SocketException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
        catch (ProtocolException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
        catch (EndOfStreamException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }

        return t;
    }

    private static void developer_network(Action<TcpClient, DungbeetleNetwork> action) {
        bool discard = false;
        string discard2 = null;
        developer_network_timeout(10000, action, ref discard, ref discard2);
    }

    private static void developer_network(Action<TcpClient, DungbeetleNetwork> action, ref bool socketFailed, ref string socketFailMsg) {
        developer_network_timeout(10000, action, ref socketFailed, ref socketFailMsg);
    }

    private static void developer_network_timeout(int timeoutMilliseconds, Action<TcpClient, DungbeetleNetwork> action, ref bool socketFailed, ref string socketFailMsg) {
        try {
            using (var client = OverviewServerConnection.Connection(timeoutMilliseconds))
            using (var network = networkFactory.Create(client, unityDebug)) {
                action(client, network);
            }
        }
        catch (SocketException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
        catch (ProtocolException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
        catch (EndOfStreamException e) {
            socketFailed = true;
            socketFailMsg = e.Message;
        }
    }
}
}
