using System;
using UnityEngine;

namespace Dungbeetle {
public class BugGUIButton {
    private string _name;
    public string name {
        get { return _name; }
        set { _name = value; }
    }

    private Action<Bug> _action;
    public Action<Bug> action {
        get { return _action; }
        set { _action = value; }
    }

    private bool _colorize;
    public bool colorize {
        get { return _colorize; }
        set { _colorize = value; }
    }

    private Color _color;
    public Color color {
        get { return _color; }
        set { _color = value; }
    }

    private int _weight = 1;
    public int weight {
        get { return _weight; }
        set { _weight = value; }
    }

    private bool _need_confirmation;
    public bool need_confirmation {
        get { return _need_confirmation; }
        set { _need_confirmation = value; }
    }

    private bool _need_screenshot;
    public bool need_screenshot {
        get { return _need_screenshot; }
        set { _need_screenshot = value; }
    }

    private float confirmTime;
    private int confirmBug;

    public void DrawSelf(Bug bug, Rect rect) {
        var prev_color = GUI.color;
        var cur_time = System.Environment.TickCount / 1000f;

        if (colorize)
            GUI.color = prev_color * color;
        else
            GUI.color = prev_color;

        if (need_confirmation) {
            if (cur_time - confirmTime < 3f && bug.id == confirmBug) {
                if (GUI.Button(rect, "Confirm"))
                    action(bug);
            }
            else if (GUI.Button(rect, name)) {
                confirmTime = cur_time;
                confirmBug = bug.id;
            }
        }
        else {
            GUI.enabled = !string.IsNullOrWhiteSpace(bug.imageName) || !need_screenshot;

            if (GUI.Button(rect, name))
                action(bug);
            GUI.enabled = true;
        }

        GUI.color = prev_color;
    }
}
}