using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Dungbeetle {

[Serializable]
public class OutputBuffer : IOutput {

    [SerializeField]
    private List<string> log;
    [SerializeField]
    private string sum = "";

    private object _log_lock = new object();
    private int dirty = 0;

    public LoggedEvent Logged; // Careful! 'Logged' may be called via any thread
    public delegate void LoggedEvent();

    public OutputBuffer() {
        log = new List<string>();
    }

    public void Clear() { // May be accessed by any thread
        lock(_log_lock) {
            log.Clear();
            SetDirty();
        }
    }

    public void Log(string s) { // May be accessed by any thread
        lock(_log_lock){
            log.Add(s);
            if (log.Count > 500)
                log.RemoveRange(0, log.Count - 500);
            SetDirty();
        }

        Logged?.Invoke();
    }

    public override string ToString() {
        lock(_log_lock) {
            if (CheckAndResetDirtyStatus()) // This doesn't actually need to be thread safe, these days, inside of the critical section
                sum = string.Join("\n", log);
        }
        return sum;
    }

    private void SetDirty() {
        Interlocked.CompareExchange(ref dirty, 1, 0); // Sets dirty to 1, only if it is 0, atomically
    }

    private bool CheckAndResetDirtyStatus() {
        return Interlocked.CompareExchange(ref dirty, 0, 1) == 1; //Atomically reset 'dirty'
    }
}

}
