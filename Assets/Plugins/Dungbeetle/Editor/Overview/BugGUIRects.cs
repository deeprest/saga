using UnityEngine;

namespace Dungbeetle {
public class BugGUIRects {
    public const float id_rect_width = 32f;
    public const float pri_rect_width = 20f;

    private Rect _root_rect;
    public Rect root_rect { get { return _root_rect; } set { _root_rect = value; } }

    private Rect _id_rect;
    public Rect id_rect { get { return _id_rect; } set { _id_rect = value; } }

    private Rect _pri_rect;
    public Rect pri_rect { get { return _pri_rect; } set { _pri_rect = value; } }

    private Rect _desc_rect;
    public Rect desc_rect { get { return _desc_rect; } set { _desc_rect = value; } }

    private Rect _cmt_rect;
    public Rect cmt_rect { get { return _cmt_rect; } set { _cmt_rect = value; } }

    private Rect[] _label_rects;
    public Rect[] label_rects { get { return _label_rects; } set { _label_rects = value; } }

    private Rect[] _button_rects;
    public Rect[] button_rects { get { return _button_rects; } set { _button_rects = value; } }

    public void UseY(float y) {
        _root_rect.y = y;
        _id_rect.y = y;
        _pri_rect.y = y;
        _desc_rect.y = y;
        _cmt_rect.y = y;

        for (int j = 0; j < _label_rects.Length; j++)
            _label_rects[j].y = y;

        for (int j = 0; j < _button_rects.Length; j++)
            _button_rects[j].y = y;
    }

    public void UseHeight(float height) {
        _root_rect.height = height;
        _id_rect.height = height;
        _pri_rect.height = height;
        _desc_rect.height = height;
        _cmt_rect.height = height;

        for (int j = 0; j < _label_rects.Length; j++)
            _label_rects[j].height = height;

        for (int j = 0; j < _button_rects.Length; j++)
            _button_rects[j].height = height;
    }
}
}