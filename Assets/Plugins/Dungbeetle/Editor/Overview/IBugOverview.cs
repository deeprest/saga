using System.Collections.Generic;

namespace Dungbeetle {
public interface IBugOverview {
    void ChangeBugState(Bug b);
    List<ProjectDeveloper> DeveloperListExplicit();
    string DeveloperName(int id);
    bool IsDestroyed();
    void Refresh();
    void RemoveFromList(Bug b);
    void RegisterSuperPositionBug(Bug b);
    void ShowOverview();
    void ShowScreenshot(Bug b);
    void ShowAssigner(Bug b, BugAssignAction a);
    void ShowDetails(Bug b);
    void ShowDetailsNext(Bug b);
    void ShowDetailsPrevious(Bug b);
    void ShowAttachments(Bug b);
    void UpdateBugComment(int id, string comment);
    void UpdatePriority(int id, string p);
    void UpdateContactResponsible(int id, int r);
    void UpdateBugFixer(Bug b);
}
}