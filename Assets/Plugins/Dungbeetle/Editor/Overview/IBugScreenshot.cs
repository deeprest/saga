public interface IBugScreenshot {
    void SetWindowDimension(int width, int height);
}