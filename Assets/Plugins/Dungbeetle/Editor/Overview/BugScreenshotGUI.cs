using UnityEngine;

namespace Dungbeetle {

public class BugScreenshotGUI {

    [System.NonSerialized]
    public IBugScreenshot window;


    private bool socketFailed;
    private string socketFailMsg;

    private Bug bug;
    private Texture2D responseAsTexture;
    private bool initializedSize;

    private IBugOverview parentOverview;

    public void OnGUI() {
        if (socketFailed) {
            GUILayout.Label($"Failed to download screenshot image for bug {bug.id}.\n" +
                            $"Please connect to the network, then reopen this window.\n\n" +
                            $"Attempted to connect to {Config.OverviewClientConfig.Domain}:{Config.OverviewClientConfig.Port}: {socketFailMsg}");
            return;
        }

        OnGUI(responseAsTexture.width, responseAsTexture.height);
    }

    private void OnGUI(int width, int height) {
        if (!initializedSize) {
            if (window != null) {
                initializedSize = true;
                window.SetWindowDimension(width, height);
            }
        }

        var r = new Rect(0f, 0f, width, height);
        GUI.DrawTexture(r, responseAsTexture, ScaleMode.ScaleToFit);

        if (Event.current.type == EventType.MouseDown)
            parentOverview.ShowOverview();
    }

    public void SetBug(Bug b) {
        bug = b;
        responseAsTexture = BugUtility.GetTexture(bug, ref socketFailed, ref socketFailMsg);
    }

    public void SetParentOverview(IBugOverview ovg) {
        parentOverview = ovg;
    }

}
}