using UnityEditor;

namespace Dungbeetle {
public static class BugEditorConfig {
    public static int defaultDeveloper {
        get { return EditorPrefs.GetInt("Dungbeetle_DefaultDeveloper", -1); }
        set { EditorPrefs.SetInt("Dungbeetle_DefaultDeveloper", value); }
    }

    public static int defaultFixer {
        get { return EditorPrefs.GetInt("Dungbeetle_DefaultFixer", 0); }
        set { EditorPrefs.SetInt("Dungbeetle_DefaultFixer", value); }
    }

    public static int defaultAcceptResponsible {
        get { return EditorPrefs.GetInt("Dungbeetle_DefaultAcceptResponsible", 0); }
        set { EditorPrefs.SetInt("Dungbeetle_DefaultAcceptResponsible", value); }
    }

    public static int defaultFixResponsible {
        get { return EditorPrefs.GetInt("Dungbeetle_DefaultFixResponsible", 0); }
        set { EditorPrefs.SetInt("Dungbeetle_DefaultFixResponsible", value); }
    }

    public static int defaultResolutionResponsible {
        get { return EditorPrefs.GetInt("Dungbeetle_DefaultResolutionResponsible", 0); }
        set { EditorPrefs.SetInt("Dungbeetle_DefaultResolutionResponsible", value); }
    }

    public static string defaultFixSource => $"{System.Environment.MachineName} {System.DateTime.Now:yyyy-MM-dd HH:mm}";
}
}