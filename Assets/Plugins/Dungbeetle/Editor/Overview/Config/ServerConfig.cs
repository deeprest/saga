using System.Collections.Generic;
using UnityEngine;

namespace Dungbeetle {
public class ServerConfig : ScriptableObject {
    public BoolField Enabled;
    public PortField Port;
    public PrivKeyField PrivKey;

    public IEnumerator<Object> subAssets {
        get {
            yield return Enabled;
            yield return Port;
            yield return PrivKey;
        }
    }
}
}