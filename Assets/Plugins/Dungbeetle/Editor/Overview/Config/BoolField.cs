namespace Dungbeetle {
public class BoolField : UnityEngine.ScriptableObject {
    public bool value;
    public static implicit operator bool(BoolField f) => f.value;
}
}