using System.Collections.Generic;
using System.IO;
using Dungbeetle;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {
public class Config : ScriptableObject {
    [SerializeField] private CompleteConfig defaults;
    [SerializeField] private ClientConfig   reporterClientConfig;
    [SerializeField] private ClientConfig   overviewClientConfig;
    [SerializeField] private ServerConfig   editorServerConfig;

    public static CompleteConfig Defaults             => config.defaults;
    public static ClientConfig   ReporterClientConfig => config.reporterClientConfig;
    public static ClientConfig   OverviewClientConfig => config.overviewClientConfig;
    public static ServerConfig   EditorServerConfig   => config.editorServerConfig;

    private const string editorHomePath = "Assets/Plugins/Dungbeetle/Editor";
    private const string configFolderName = "Config";
    private const string configFileName = "Config.asset";
    private static readonly string configPath = $"{editorHomePath}/{configFolderName}/{configFileName}";

    private static Config _config;

    private static Config config {
        get {
            if (_config == null) {
                _config = (Config) AssetDatabase.LoadMainAssetAtPath(configPath);
                if (_config != null)
                    _config.CheckForBrokenReferences();
                else
                    _config = CreateNew();
            }

            return _config;
        }
    }

    private void CheckForBrokenReferences() {
        if (defaults == null || reporterClientConfig == null || overviewClientConfig == null || editorServerConfig == null) {
            Debug.LogWarning("Config.asset seems to contain broken references. Check its meta file. Delete Config.asset if you want to regenerate it.");
        }
    }

    public static ScriptableObject GetNewField(string name, ScriptableObject original) {
        var newField = Instantiate(original);
        newField.name = name;
        newField.hideFlags = HideFlags.HideInHierarchy;
        AssetDatabase.AddObjectToAsset(newField, config);
        return newField;
    }

    public static void DeleteField(ScriptableObject o) {
        DestroyImmediate(o, true);
        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(config));
    }

    public static Config CreateNew() {
        var newConfig = CreateInstance<Config>();

        if (!Directory.Exists($"{editorHomePath}/{configFolderName}"))
            AssetDatabase.CreateFolder(editorHomePath, configFolderName);
        AssetDatabase.CreateAsset(newConfig, configPath);

        var newDefaultConfig  = CreateField<CompleteConfig>("Default Config", newConfig);
        var newReporterConfig = CreateField<ClientConfig>("Reporter Config", newConfig);
        var newOverviewConfig = CreateField<ClientConfig>("Overview Config", newConfig);
        var newServerConfig   = CreateField<ServerConfig>("Server Config", newConfig);

        var newDomain = CreateField<DomainField>("Default Domain", newConfig);
        var newPort = CreateField<PortField>("Default Port", newConfig);
        var newPrivKey = CreateField<PrivKeyField>("Default Private Key", newConfig);
        var newPubKey = CreateField<PubKeyField>("Default Public Key", newConfig);
        var newReporterPasswd = CreateField<PasswordField>("Default Reporter Password", newConfig);
        var newDeveloperPasswd = CreateField<PasswordField>("Default Developer Password", newConfig);
        var newProjectId = CreateField<ProjectIdField>("Default Project ID", newConfig);
        var newEditorServerEnabled = CreateField<BoolField>("Default Enable Editor Server", newConfig);

        var rsaProvider = new System.Security.Cryptography.RSACryptoServiceProvider();

        // Generate config defaults
        newDomain.domain = "localhost";
        newPort.port = 15275;
        newPrivKey.key = rsaProvider.ToXmlString(true);
        newPubKey.key = rsaProvider.ToXmlString(false);
        newReporterPasswd.password = ServerPreferences.RandomPassword(16);
        newDeveloperPasswd.password = ServerPreferences.RandomPassword(16);
        newProjectId.projectId = 1; //Database is 1-indexed
        newEditorServerEnabled.value = true;

        newConfig.defaults = newDefaultConfig;
        newConfig.reporterClientConfig = newReporterConfig;
        newConfig.overviewClientConfig = newOverviewConfig;
        newConfig.editorServerConfig = newServerConfig;

        newDefaultConfig.Domain = newDomain;
        newDefaultConfig.Port = newPort;
        newDefaultConfig.PubKey = newPubKey;
        newDefaultConfig.PrivKey = newPrivKey;
        newDefaultConfig.ReporterPassword = newReporterPasswd;
        newDefaultConfig.DeveloperPassword = newDeveloperPasswd;
        newDefaultConfig.ProjectId = newProjectId;
        newDefaultConfig.EditorServerEnabled = newEditorServerEnabled;

        newReporterConfig.Domain = newDomain;
        newReporterConfig.Port = newPort;
        newReporterConfig.PubKey = newPubKey;
        newReporterConfig.Password = newReporterPasswd;
        newReporterConfig.ProjectId = newProjectId;

        newOverviewConfig.Domain = newDomain;
        newOverviewConfig.Port = newPort;
        newOverviewConfig.PubKey = newPubKey;
        newOverviewConfig.Password = newDeveloperPasswd;
        newOverviewConfig.ProjectId = newProjectId;

        newServerConfig.Enabled = newEditorServerEnabled;
        newServerConfig.Port = newPort;
        newServerConfig.PrivKey = newPrivKey;

        ReporterConnectionConfig.ConnectConfig(newReporterConfig);

        EditorUtility.SetDirty(newConfig);
        AssetDatabase.SaveAssets();

        EditorGUIUtility.PingObject(newConfig);

        return newConfig;
    }

    static T CreateField<T>(string name, ScriptableObject mainAsset) where T : ScriptableObject {
        var newField = CreateInstance<T>();
        newField.name = name;
        newField.hideFlags = HideFlags.HideInHierarchy;
        AssetDatabase.AddObjectToAsset(newField, mainAsset);
        return newField;
    }


    public IEnumerator<Object> subAssets {
        get {
            if (defaults != null) {
                yield return defaults;
                using (var subAssetsEnumerator = defaults.subAssets)
                    while (subAssetsEnumerator.MoveNext())
                        yield return subAssetsEnumerator.Current;
            }

            if (reporterClientConfig != null) {
                yield return reporterClientConfig;
                using (var subAssetsEnumerator = reporterClientConfig.subAssets)
                    while (subAssetsEnumerator.MoveNext())
                        yield return subAssetsEnumerator.Current;
            }

            if (overviewClientConfig != null) {
                yield return overviewClientConfig;
                using (var subAssetsEnumerator = overviewClientConfig.subAssets)
                    while (subAssetsEnumerator.MoveNext())
                        yield return subAssetsEnumerator.Current;
            }

            if (editorServerConfig != null) {
                yield return editorServerConfig;
                using (var subAssetsEnumerator = editorServerConfig.subAssets)
                    while (subAssetsEnumerator.MoveNext())
                        yield return subAssetsEnumerator.Current;
            }
        }
    }
}
}
