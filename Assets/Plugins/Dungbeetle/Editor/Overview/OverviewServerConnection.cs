using System;
using System.Net.Sockets;

namespace Dungbeetle {

public static class OverviewServerConnection {

    public static TcpClient Connection(int timeout) {
        ushort port = Config.OverviewClientConfig.Port;
        var dClient = new DungbeetleTcpClient(Config.OverviewClientConfig.Domain, port);
        var asyncResult = dClient.BeginConnect();
        if (!asyncResult.AsyncWaitHandle.WaitOne(TimeSpan.FromMilliseconds(timeout)))
            throw new SocketException(10060);
        dClient.EndConnect(asyncResult);
        return dClient.TcpClient;
    }
}
}