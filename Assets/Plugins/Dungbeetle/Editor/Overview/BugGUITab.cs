namespace Dungbeetle {
public class BugGUITab {

    private string _name;
    public string name { get { return _name; } set { _name = value; } }

    private BugGUIButton[] _buttons;
    public BugGUIButton[] buttons { get { return _buttons; } set { _buttons = value; } }

    private bool _show_state;
    public bool show_state { get { return _show_state; } set { _show_state = value; } }

    private bool _show_acceptor;
    public bool show_acceptor { get { return _show_acceptor; } set { _show_acceptor = value; } }

    private bool _show_fixer;
    public bool show_fixer { get { return _show_fixer; } set { _show_fixer = value; } }

    private bool _show_fixer_fiter;
    public bool show_fixer_fiter { get { return _show_fixer_fiter; } set { _show_fixer_fiter = value; } }

    private bool _show_fix_source;
    public bool show_fix_source { get { return _show_fix_source; } set { _show_fix_source = value; } }

    private bool _show_resolver;
    public bool show_resolver { get { return _show_resolver; } set { _show_resolver = value; } }

    private bool _show_contact;
    public bool show_contact { get { return _show_contact; } set { _show_contact = value; } }
}
}