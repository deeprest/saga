using System;
using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

using static Dungbeetle.BugAssignAction;

namespace Dungbeetle {

public enum BugAssignAction {
    Assign,
    AssignFix,
    AssignFixResolve,
    Reassign,
    Fix,
    Break,
    Resolve,
}

public enum BugPriority {
    Low,
    High,
    Critical,
    Longterm,
    Declined,
}

public interface IBugAssign {
    void CloseWindow();
}

public class BugAssignGUI {

    [System.NonSerialized]
    public IBugAssign window;

    private Vector2 scrollPos;

    private Bug bug;
    private List<ProjectDeveloper> allDevelopers;
    private string[] allDevelopersStringArray;
    private BugAssignAction action;
    private string action_verb;

    private bool get_priority;
    private bool get_accept_responsible;
    private bool get_fix_responsible;
    private bool get_fix_source;
    private bool get_resolution_responsible;

    private BugPriority priority;
    private int         accept_responsible;
    private int         fix_responsible;
    private string      fix_source;
    private int         resolution_responsible;
    private string      comment;

    private bool   socketFailed;
    private string socketFailMsg;

    private enum FieldIndex {
        AcceptResponsible,
        FixResponsible,
        ResolutionResponsible,
    }

    private IBugOverview parentOverview;

    public void OnGUI() {
        if (socketFailed) {
            GUILayout.Label($"Failed to update bug #{bug.id}.\nPlease connect to the network, then reopen this window.\n\nAttempted to connect to " +
                            $"{Config.OverviewClientConfig.Domain}:{Config.OverviewClientConfig.Port}: {socketFailMsg}");
            return;
        }

        if (parentOverview == null)
            parentOverview = BugOverviewWindow.window;

        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));

        var style = new GUIStyle(GUI.skin.label);
        style.fontSize = 20;
        GUILayout.Label($"Bug #{bug.id}", style);

        scrollPos = GUILayout.BeginScrollView(scrollPos);

        style        = new GUIStyle(GUI.skin.textArea);
        style.margin = new RectOffset(10, 10, 10, 0);

        GUI.enabled = false;
        GUILayout.TextArea(bug.description, style);
        GUI.enabled = true;

        GUILayout.Space(5f);
        GUILayout.Label("Comment:");
        comment = GUILayout.TextArea(comment, style);

        GUILayout.Space(15f);

        if (get_priority) {
            GUILayout.Label("Priority:");
            priority = (BugPriority) EditorGUILayout.EnumPopup(priority);
        }

        // @TODO: This stuff can be factored
        if (get_accept_responsible) {
            GUILayout.Label("Accept responsible:");
            if (allDevelopers.Count == 0) {
                EditorGUILayout.LabelField("  No Developers to assign to!");
            }
            else {
                var selected = allDevelopers.FindIndex(d => d.id == accept_responsible);
                if (selected < 0 || selected > allDevelopers.Count)
                    selected = 0;

                accept_responsible = allDevelopers[EditorGUILayout.Popup(selected, allDevelopersStringArray)].id;
            }
        }

        if (get_fix_responsible) {
            GUILayout.Label("Fix responsible:");
            if(allDevelopers.Count == 0)
                EditorGUILayout.LabelField("  No Developers to assign to!");
            else {
                var selected = allDevelopers.FindIndex(d => d.id == fix_responsible);
                if (selected < 0 || selected > allDevelopers.Count)
                    selected = 0;

                fix_responsible = allDevelopers[EditorGUILayout.Popup(selected, allDevelopersStringArray)].id;
            }
        }

        if (get_fix_source) {
            GUILayout.Label("Fix source:");
            fix_source = GUILayout.TextField(fix_source);
        }

        if (get_resolution_responsible) {
            GUILayout.Label("Resolution responsible:");
            if (allDevelopers.Count == 0)
                UnityEditor.EditorGUILayout.LabelField("  No Developers to assign to!");
            else {
                var selected = allDevelopers.FindIndex(d => d.id == resolution_responsible);
                if (selected < 0 || selected > allDevelopers.Count)
                    selected = 0;

                resolution_responsible = allDevelopers[EditorGUILayout.Popup(selected, allDevelopersStringArray)].id;
            }
        }

        GUILayout.Space(15f);

        var acceptable_form =
            (!get_accept_responsible     || allDevelopers.Exists(d => d.id == accept_responsible)) &&
            (!get_fix_responsible        || allDevelopers.Exists(d => d.id == fix_responsible)) &&
            (!get_fix_source             || !string.IsNullOrEmpty(fix_source)) &&
            (!get_resolution_responsible || allDevelopers.Exists(d => d.id == resolution_responsible));

        GUILayout.BeginHorizontal();

        GUI.enabled = acceptable_form;
        if (GUILayout.Button(action_verb)) {

            if (get_accept_responsible || get_fix_responsible || get_resolution_responsible) {
                if (get_accept_responsible)
                    BugEditorConfig.defaultAcceptResponsible = accept_responsible;
                if (get_fix_responsible)
                    BugEditorConfig.defaultFixResponsible = fix_responsible;
                if (get_resolution_responsible)
                    BugEditorConfig.defaultResolutionResponsible = resolution_responsible;
            }

            bug.SetPriority(System.Enum.GetName(typeof(BugPriority), priority));
            if (get_accept_responsible)
                bug.SetAcceptResponsible(accept_responsible);
            if (get_fix_responsible)
                bug.SetFixResponsible(fix_responsible);
            if (get_fix_source)
                bug.fix_source = fix_source;
            if (get_resolution_responsible)
                bug.SetResolutionResponsible(resolution_responsible);
            bug.comment = comment;

            switch(action) {
                case BugAssignAction.Assign:
                case BugAssignAction.Reassign:
                case BugAssignAction.Break:
                    BugUtility.AssignBug(bug, ref socketFailed, ref socketFailMsg);
                    break;
                case BugAssignAction.Fix:
                case BugAssignAction.AssignFix:
                    BugUtility.FixBug(bug, ref socketFailed, ref socketFailMsg);
                    break;
                case BugAssignAction.Resolve:
                case BugAssignAction.AssignFixResolve:
                    BugUtility.ResolveBug(bug, ref socketFailed, ref socketFailMsg);
                    break;
            }

            if (!socketFailed) {
                if (parentOverview != null) {
                    if (action != BugAssignAction.Reassign)
                        parentOverview.ChangeBugState(bug);
                    else
                        parentOverview.UpdateBugFixer(bug);
                }

                if (window != null)
                    window.CloseWindow();
            }
        }

        GUI.enabled = true;

        if (GUILayout.Button("Cancel")) {
            if (parentOverview != null)
                parentOverview.ShowOverview();

            if (window != null)
                window.CloseWindow();
        }

        GUILayout.EndHorizontal();

        GUILayout.EndScrollView();
        GUILayout.EndArea();
    }

    public void SetParentOverview(IBugOverview ov) {
        parentOverview = ov;
        if (parentOverview == null)
            parentOverview = BugOverviewWindow.window;

        if (parentOverview != null)
            allDevelopers = parentOverview.DeveloperListExplicit();
        else
            allDevelopers = BugUtility.GetAllProjectDevelopers(Config.ReporterClientConfig.ProjectId, out socketFailed, out socketFailMsg);
        allDevelopersStringArray = allDevelopers.Select(dev => dev.name).ToArray();
    }

    public void SetActionVerb(string v) {
        action_verb = v;
    }

    public void SetBug(Bug b, BugAssignAction a) {
        action = a;

        get_priority               = a == Assign           || a == Reassign;
        get_accept_responsible     = a == Assign           || a == AssignFix || a == AssignFixResolve || a == Reassign;
        get_fix_responsible        = a == Assign           || a == AssignFix || a == AssignFixResolve || a == Reassign;
        get_fix_source             = a == Fix              || a == AssignFix || a == AssignFixResolve;
        get_resolution_responsible = a == Break            || a == Resolve   || a == AssignFixResolve;

        bug = b;
        try {
            if (get_priority) {
                try {
                    priority = (BugPriority) Enum.Parse(typeof(BugPriority), bug.priority);
                }
                catch {
                    priority = BugPriority.Low;
                }
            }
            if (get_accept_responsible) {
                if (bug.accept_responsible != -1)
                    accept_responsible = bug.accept_responsible;
                else
                    accept_responsible = BugEditorConfig.defaultAcceptResponsible;
            }
            if (get_fix_responsible) {
                if (bug.fix_responsible != -1)
                    fix_responsible = bug.fix_responsible;
                else
                    fix_responsible = BugEditorConfig.defaultFixResponsible;
            }
            if (get_fix_source) {
                if (bug.fix_source != "")
                    fix_source = bug.fix_source;
                else
                    fix_source = BugEditorConfig.defaultFixSource;
            }
            if (get_resolution_responsible) {
                if (bug.resolution_responsible != -1)
                    resolution_responsible = bug.resolution_responsible;
                else
                    resolution_responsible = BugEditorConfig.defaultResolutionResponsible;
            }
            comment = bug.comment;
        }
        catch(Exception e) {
            Debug.LogException(e);
        }
    }
}
}