using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace Dungbeetle {
public class BugOverviewGUI {

    [NonSerialized]
    public IBugOverview window;

    public List<ProjectDeveloper> DeveloperListExplicit(bool force) {
        if (developerListDirty || force)
            RebuildDeveloperList();
        return developerList;
    }

    private void RebuildDeveloperList() {
        developerListDirty = false;
        developerList = BugUtility.GetAllProjectDevelopers(Config.OverviewClientConfig.ProjectId, out socketFailed, out socketFailMsg);
        developerStringArray = new string[developerList.Count + 1];
        developerStringArray[0] = "Any";
        for (int i = 1; i < developerStringArray.Length; i++)
            developerStringArray[i] = developerList[i - 1].name;

        if (socketFailed) {
            failedPort = Config.OverviewClientConfig.Port;
            failedDomain = Config.OverviewClientConfig.Domain;
        }
    }

    public string DeveloperName(int id) {
        string result;
        if (this.developerNameDict.TryGetValue(id, out result))
            return result;
        else
            return "unknown";
    }

    private int currentTab {
        get { return EditorReporterConfig.SelectedTab; }
        set { EditorReporterConfig.SelectedTab = value; }
    }

    private bool sortPositiveDirection = true;
    // Store these as fields to make comparison work
    private Comparison<Bug> _idSort;
    private Comparison<Bug> _priSort;
    private Comparison<Bug> _descSort;
    private Comparison<Bug> idSort   => _idSort   ?? (_idSort   = CompareByID);
    private Comparison<Bug> priSort  => _priSort  ?? (_priSort  = CompareByPriority);
    private Comparison<Bug> descSort => _descSort ?? (_descSort = CompareByDescription);

    private Comparison<Bug> _sortFunc;
    private Comparison<Bug> sortFunc => _sortFunc ?? (_sortFunc = CompareByID);

    private int developer;
    private List<ProjectDeveloper> developerList;
    private bool developerListDirty;
    private string[] developerStringArray;

    private Dictionary<int, string> _developerNameDict;
    private Dictionary<int, string> developerNameDict {
        get {
            if(_developerNameDict == null) {
                _developerNameDict = new Dictionary<int, string>();
                foreach (var d in developerList)
                    _developerNameDict[d.id] = d.name;
                _developerNameDict[-1] = "";
            }

            return _developerNameDict;
        }
    }

    private List<Bug> bugs = new List<Bug>();
    private string filter = "";
    private IEnumerable<Bug> filtered_bugs {
        get {
            if (!string.IsNullOrWhiteSpace(filter)) {
                Regex regex = null;
                bool regexFailed = false;
                try {
                    regex = new Regex(filter, RegexOptions.IgnoreCase);
                }
                catch {
                    regexFailed = true;
                }

                foreach (var bug in bugs) {
                    if (regexFailed ||
                        regex.IsMatch(bug.id.ToString()) ||
                        regex.IsMatch(bug.time.ToString()) ||
                        regex.IsMatch(bug.address) ||
                        regex.IsMatch(bug.email) ||
                        regex.IsMatch(bug.priority) ||
                        regex.IsMatch(bug.scene) ||
                        regex.IsMatch(bug.build_name) ||
                        regex.IsMatch(bug.description) ||
                        regex.IsMatch(bug.comment) ||
                        regex.IsMatch(bug.fix_source))
                        yield return bug;
                }
            }
            else {
                foreach (var bug in bugs) {
                    yield return bug;
                }
            }
        }
    }

    private IEnumerable<Bug> fixer_bugs {
        get {
            foreach (var bug in filtered_bugs)
                if (developer == -1 || developer == bug.fix_responsible)
                    yield return bug;
        }
    }

    private IEnumerable<Bug> window_bugs {
        get {
            foreach (var bug in currentTab == 2 ? fixer_bugs : filtered_bugs)
                yield return bug;
        }
    }

    public Bug window_previous_bug(Bug bug) {
        Bug previous = null;
        foreach (var wb in window_bugs) {
            if (wb == bug)
                return previous;
            previous = wb;
        }

        return null;
    }

    public Bug window_next_bug(Bug bug) {
        var found = false;
        foreach (var wb in window_bugs) {
            if (found)
                return wb;
            found = wb == bug;
        }

        return null;
    }

    private int view_bugs_length;

    private Vector2 scrollPos;

    public BugOverviewGUI() {
        _bugGUITabs = null; // Action doesn't survive reloads. This causes a rebuild from the setter
    }

    public void Load() {
        _bugGUITabs = null;
    }

    private bool gotAllAssignedBugs;
    private bool socketFailed;
    private string socketFailMsg;
    private List<Bug> deletedBugs = new List<Bug>();
    private List<Bug> addedBugs = new List<Bug>();

    private ushort failedPort;
    private string failedDomain;

    private bool emptySet;

    private bool readyForFullGUI;

    public void OnGUI() {
        if (!readyForFullGUI) {
            /*The first frame would be white if OnGUI didn't return fast enough,
            * and the second frame will probably last a few seconds, this draws
            * a default color frame first.
            */
            GUILayout.Label("Loading...");

            if (EditorServer.ClassInitialized && Event.current.type == EventType.Repaint)
                readyForFullGUI = true;
            window.Refresh();
            return;
        }

        var newCurrentTab = GUILayout.SelectionGrid(currentTab, new[] {"All Open", "Unassigned", "Assigned", "Fixed", "Resolved"}, 5, tabStyle);

        if (socketFailed) {
            var reconnect = RequestButton("Reconnect");
            GUILayout.Label($"Attempted to connect to {failedDomain}:{failedPort}: {socketFailMsg}", errorLabelStyle);
            if (reconnect) {
                tabNeedRefresh = true;
                socketFailed = false;
                UpdateProjectIDDrawers();
            }

            return;
        }

        if (_bugGUITabs == null) {
            // Assembly reload detected!
            developer = BugEditorConfig.defaultDeveloper;
            RequestBugsForTab(currentTab);
        }

        if (RequestRefreshTab(newCurrentTab))
            RequestBugsForTab(newCurrentTab);

        BugDeveloperFilterGUI(currentTab);
        BugSearchFilterGUI();

        var scrollRect = GUILayoutUtility.GetRect(0f, 0f, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        scrollRect.x += 5;
        scrollRect.width -= 5;

        var contentWidth = view_bugs_length > scrollRect.height
                               ? scrollRect.width - GUI.skin.verticalScrollbar.fixedWidth
                               : scrollRect.width;

        scrollPos = GUI.BeginScrollView(scrollRect, scrollPos, new Rect(0f, 0f, contentWidth, view_bugs_length));

        var labelRect = new Rect(0, 0, contentWidth, 16);
        var bugRectY = 16;

        var doLayout = Event.current.type == EventType.Layout;
        if (doLayout) // Descriptions row
            view_bugs_length = 16;

        var tab_rects = GetBugGUIRects(currentTab, labelRect);
        BugGUILabels(currentTab, tab_rects, scrollPos, scrollRect);

        // Filter deleted and added bugs before any layout related actions happen.
        foreach(var bug in deletedBugs)
            bugs.RemoveAll(b => b.id == bug.id);
        deletedBugs.Clear();

        foreach(var addedBug in addedBugs)
            if (!bugs.Exists(b => b.id == addedBug.id))
                bugs.Add(addedBug);
        addedBugs.Clear();

        tab_rects.UseHeight(29);
        if (currentTab == 2) { //tab 2 is "assigned"
            foreach(var bug in fixer_bugs) {
                if (doLayout)
                    view_bugs_length += 29; 
                tab_rects.UseY(bugRectY);
                BugGUI(bug, currentTab, tab_rects, scrollPos, scrollRect);
                bugRectY += 29;
            }
        }
        else {
            foreach(var bug in filtered_bugs) {
                if (doLayout)
                    view_bugs_length += 29; 
                tab_rects.UseY(bugRectY); 
                BugGUI(bug, currentTab, tab_rects, scrollPos, scrollRect);
                bugRectY += 29;
            }
        }

        GUI.EndScrollView();
    }

    private void RequestBugsForTab(int tab) {
        switch (tab) {
            case 0:
                RequestBugs("Open-Bugs");
                break;
            case 1:
                RequestBugs("Unassigned-Bugs");
                break;
            case 2:
                if (developer >= 0)
                    RequestBugs("Assigned-Bugs-By-Fixer", developer);
                else
                    RequestBugs("Assigned-Bugs");
                break;
            case 3:
                RequestBugs("Fixed-Bugs");
                break;
            case 4:
                RequestBugs("Resolved-Bugs");
                break;
        }
    }

    private void RequestBugs(string request, int extra = -1) {
        bugs.Clear();
        bugs = BugUtility.RequestBugs(request, extra, ref socketFailed, ref socketFailMsg);
        if (socketFailed) {
            failedPort = Config.OverviewClientConfig.Port;
            failedDomain = Config.OverviewClientConfig.Domain;
            bugs = new List<Bug>();
        }
        else {
            DeveloperListExplicit(true);
            emptySet = bugs.Count == 0;
            _developerNameDict = null; // Causes rebuild of developerNameDict
            bugs.Sort(sortFunc);
            gotAllAssignedBugs = request == "Assigned-Bugs";
        }
    }

    private int CompareByID(Bug a, Bug b) {
        return sortPositiveDirection ? a.id.CompareTo(b.id) : b.id.CompareTo(a.id);
    }

    private int CompareByString(string aString, string bString, int aId, int bId) {
        var stringDiff = sortPositiveDirection ? string.Compare(aString, bString, StringComparison.Ordinal) : string.Compare(bString, aString, StringComparison.Ordinal);
        return stringDiff == 0 ? aId.CompareTo(bId) : stringDiff;
    }

    private int CompareByPriority(Bug a, Bug b) {
        var aPriority = GetBugPriorityValue((BugPriority) Enum.Parse(typeof(BugPriority), a.priority));
        var bPriority = GetBugPriorityValue((BugPriority) Enum.Parse(typeof(BugPriority), b.priority));

        var diff = aPriority.CompareTo(bPriority);
        return diff == 0             ? a.id.CompareTo(b.id) :
               sortPositiveDirection ?  diff :
                                       -diff  ;
    }

    // The ordering of the enum is Low, High, Critical, Longterm, Declined. This method allows for sorting with Critical on top, which we want
    private int GetBugPriorityValue(BugPriority priority) {
        switch (priority) {
            case BugPriority.Declined:
                return 0;
            case BugPriority.Longterm:
                return 1;
            case BugPriority.Low:
                return 2;
            case BugPriority.High:
                return 3;
            case BugPriority.Critical:
                return 4;
            default:
                throw new ArgumentOutOfRangeException(nameof(priority), priority, $"Unknown {nameof(BugPriority)}: {priority}");
        }
    }

    private int CompareByDescription(Bug a, Bug b) {
        return CompareByString(a.description, b.description, a.id, b.id);
    }

    private void SetSortFunc(Comparison<Bug> comparison) {
        if (_sortFunc == comparison)
            sortPositiveDirection = !sortPositiveDirection;
        else
            _sortFunc = comparison;
        bugs.Sort(sortFunc);
    }

    public void Clear() {
        bugs = new List<Bug>();
        emptySet = false;
    }

    private bool RequestButton(string text) {
        var connectionInfo = new GUIContent($"{Config.OverviewClientConfig.Domain}:{Config.OverviewClientConfig.Port}");
        var connectionInfo2 = $"Project {Config.OverviewClientConfig.ProjectId.projectId}";
        var buttonRect = GUILayoutUtility.GetRect(0f, 16f);
        buttonRect.x += 5f;
        buttonRect.width -= 5f;
        var secondWidth = 80f;

        GUI.skin.label.fontStyle = FontStyle.Italic;
        var firstWidth = Mathf.Min(GUI.skin.label.CalcSize(connectionInfo).x + 4f, (buttonRect.width - secondWidth) * 0.4f);
        var thirdWidth = buttonRect.width - firstWidth - secondWidth;

        var buttonLabelRect  = new Rect(buttonRect.x,                            buttonRect.y, firstWidth  - 4f, buttonRect.height);
        var buttonLabel2Rect = new Rect(buttonRect.x + firstWidth,               buttonRect.y, secondWidth - 4f, buttonRect.height);
        buttonRect           = new Rect(buttonRect.x + firstWidth + secondWidth, buttonRect.y, thirdWidth,       buttonRect.height);

        GUILayout.BeginHorizontal();
        GUI.Label(buttonLabelRect, connectionInfo);

        GUI.skin.label.fontStyle = FontStyle.Bold;
        GUI.Label(buttonLabel2Rect, connectionInfo2);

        GUI.skin.label.fontStyle = FontStyle.Normal;

        var result = GUI.Button(buttonRect, text, refreshStyle);
        GUILayout.EndHorizontal();
        return result;
    }

    private bool RequestRefreshTab(int tab) {
        var refresh      = bugs.Count != 0 || emptySet;
        var refreshLingo = refresh ? "Refresh" : "Load";

        if (RequestButton(refreshLingo) || tab != currentTab) {
            tabNeedRefresh = true;
            if (!refresh)
                UpdateProjectIDDrawers();
        }

        if (Event.current.type != EventType.Repaint)
            RefreshTab(tab);

        if (tabRefreshed) {
            tabRefreshed = false;
            return true;
        }

        return false;
    }

    private static void UpdateProjectIDDrawers() {
        ProjectIdAttributeDrawer.projectsNeedRefresh = true;
        ProjectIdAttributeDrawer.serverHasChanged = false;
        ConfigWindow.RepaintAll();
    }

    private bool tabNeedRefresh; //Some condition for initializing a tab has been fulfilled
    private bool tabRefreshed; // currentTab has been set by RefreshTab and refreshing should be requested
    private void RefreshTab(int tab) {
        if (tabNeedRefresh) {
            currentTab = tab;
            tabNeedRefresh = false;
            tabRefreshed = true;
        }
    }

    private BugGUIButton _detailsButton;
    private BugGUIButton _screenshotButton;
    private BugGUIButton _assignBugButton;
    private BugGUIButton _reassignBugButton;
    private BugGUIButton _fixBugButton;
    private BugGUIButton _assignFixBugButton;
    private BugGUIButton _assignFixResolveBugButton;
    private BugGUIButton _breakBugButton;
    private BugGUIButton _resolveBugButton;
    private BugGUIButton _reopenBugButton;
    private BugGUIButton _unassignBugButton;
    private BugGUIButton _deleteBugButton;

    private BugGUIButton detailsButton {
        get {
            if (_detailsButton?.action == null)
                _detailsButton = new BugGUIButton {name = "Details", action = BugDetails};
            return _detailsButton;
        }
    }

    private BugGUIButton screenshotButton {
        get {
            if (_screenshotButton?.action == null)
                _screenshotButton = new BugGUIButton {name = "Screenshot", action = BugScreenshot, need_screenshot = true };
            return _screenshotButton;
        }
    }

    public BugGUIButton assignBugButton {
        get {
            if (_assignBugButton?.action == null)
                _assignBugButton = new BugGUIButton {name = "Assign", action = AssignBug, color = Color.green, colorize = true};
            return _assignBugButton;
        }
    }

    public BugGUIButton reassignBugButton {
        get {
            if (_reassignBugButton?.action == null)
                _reassignBugButton = new BugGUIButton {name = "Reassign", action = ReassignBug, color = Color.yellow, colorize = true};
            return _reassignBugButton;
        }
    }

    public BugGUIButton fixBugButton {
        get {
            if (_fixBugButton?.action == null)
                _fixBugButton = new BugGUIButton {name = "Fix", action = FixBug, color = Color.green, colorize = true};
            return _fixBugButton;
        }
    }

    public BugGUIButton assignFixBugButton {
        get {
            if (_assignFixBugButton?.action == null)
                _assignFixBugButton = new BugGUIButton {name = "Fix", action = AssignFixBug, color = Color.yellow, colorize = true};
            return _assignFixBugButton;
        }
    }

    public BugGUIButton assignFixResolveBugButton {
        get {
            if (_assignFixResolveBugButton?.action == null)
                _assignFixResolveBugButton = new BugGUIButton {name = "Resolve", action = AssignFixResolveBug, color = Color.yellow, colorize = true};
            return _assignFixResolveBugButton;
        }
    }

    public BugGUIButton breakBugButton {
        get {
            if (_breakBugButton?.action == null)
                _breakBugButton = new BugGUIButton {name = "Break", action = BreakBug, color = Color.yellow, colorize = true};
            return _breakBugButton;
        }
    }

    public BugGUIButton resolveBugButton {
        get {
            if (_resolveBugButton?.action == null)
                _resolveBugButton = new BugGUIButton {name = "Resolve", action = ResolveBug, color = Color.green, colorize = true};
            return _resolveBugButton;
        }
    }

    public BugGUIButton reopenBugButton {
        get {
            if (_reopenBugButton?.action == null)
                _reopenBugButton = new BugGUIButton {name = "Reopen", action = ReopenBug, color = Color.yellow, colorize = true};
            return _reopenBugButton;
        }
    }

    public BugGUIButton unassignBugButton {
        get {
            if (_unassignBugButton?.action == null)
                _unassignBugButton = new BugGUIButton {name = "Unassign", action = ReopenBug, color = Color.yellow, colorize = true};
            return _unassignBugButton;
        }
    }

    public BugGUIButton deleteBugButton {
        get {
            if (_deleteBugButton?.action == null)
                _deleteBugButton = new BugGUIButton { name = "Delete", action = DeleteBug, color = Color.red, colorize = true, need_confirmation = true };
            return _deleteBugButton;
        }
    }

    private BugGUITab[] _bugGUITabs;
    private BugGUITab[] bugGUITabs {
        get {
            if (_bugGUITabs != null)
                return _bugGUITabs;

            _bugGUITabs = new[] {
                new BugGUITab {
                    name = "All Open",
                    show_state = true,
                    show_fixer = true,
                    buttons = new[] {
                        detailsButton,
                        screenshotButton
                    }
                },
                new BugGUITab {
                    name = "Unassigned",
                    buttons = new[] {
                        detailsButton,
                        screenshotButton,
                        assignBugButton,
                        assignFixBugButton,
                        assignFixResolveBugButton,
                        deleteBugButton
                    }
                },
                new BugGUITab {
                    name = "Assigned",
                    show_acceptor = true,
                    show_fixer_fiter = true,
                    show_resolver = true,
                    buttons = new[] {
                        detailsButton,
                        screenshotButton,
                        unassignBugButton,
                        reassignBugButton,
                        fixBugButton
                    }
                },
                new BugGUITab {
                    name = "Fixed",
                    show_fixer = true,
                    show_fix_source = true,
                    buttons = new[] {
                        detailsButton,
                        screenshotButton,
                        breakBugButton,
                        resolveBugButton
                    }
                },
                new BugGUITab {
                    name = "Resolved",
                    show_resolver = true,
                    show_contact = true,
                    buttons = new[] {
                        detailsButton,
                        screenshotButton,
                        reopenBugButton
                    }
                }
            };

            return _bugGUITabs;
        }
    }

    private bool _sortButtonStyleExists;
    private GUIStyle _sortButtonStyle;
    private GUIStyle sortButtonStyle {
        get {
            if (!_sortButtonStyleExists) {
                _sortButtonStyleExists = true;
                _sortButtonStyle = new GUIStyle(GUI.skin.button) {
                    margin = {top = 7},
                    normal = {background = null},
                    active = {background = null},
                    clipping = TextClipping.Overflow,
                    alignment = TextAnchor.MiddleLeft
                };
            }

            return _sortButtonStyle;
        }
    }

    private bool _tabStyleExists;
    private GUIStyle _tabStyle;
    private GUIStyle tabStyle {
        get {
            if (!_tabStyleExists) {
                _tabStyleExists = true;
                _tabStyle = "dragTab";
            }

            return _tabStyle;
        }
    }

    private bool _errorLabelStyleExists;
    private GUIStyle _errorLabelStyle;
    private GUIStyle errorLabelStyle {
        get {
            if (!_errorLabelStyleExists) {
                _errorLabelStyleExists = true;
                _errorLabelStyle = "label";
                _errorLabelStyle.wordWrap = true;
            }
            return _errorLabelStyle;
        }
    }

    private bool _searchStyleExists;
    private GUIStyle _searchStyle;
    private GUIStyle searchStyle {
        get {
            if (! _searchStyleExists) {
                _searchStyleExists = true;
                _searchStyle = "SearchTextField";
                _searchStyle.margin.bottom = 3;
            }
            return _searchStyle;
        }
    }

    private bool _refreshStyleExists;
    private GUIStyle _refreshStyle;
    private GUIStyle refreshStyle {
        get {
            if (!_refreshStyleExists) {
                _refreshStyleExists = true;
                _refreshStyle = "Button";
                _refreshStyle.margin.top = 5;
                _refreshStyle.margin.bottom = 3;
            }

            return _refreshStyle;
        }
    }


    private BugGUIRects GetBugGUIRects(int tab_index, Rect root_rect) {
        var tab = bugGUITabs[tab_index];

        var total_width_points = 5 + tab.buttons.Length;

        var label_width_points = 0;
        if (tab.show_state)      label_width_points += 1;
        if (tab.show_acceptor)   label_width_points += 1;
        if (tab.show_fixer)      label_width_points += 1;
        if (tab.show_fix_source) label_width_points += 1;
        if (tab.show_resolver)   label_width_points += 1;
        if (tab.show_contact)    label_width_points += 1;
        total_width_points += label_width_points;

        var id_field_width = BugGUIRects.id_rect_width;
        var pri_field_width = BugGUIRects.pri_rect_width;
        var static_fields_width = id_field_width + pri_field_width;

        var dynamic_width = root_rect.width - static_fields_width;
        var unit_width = dynamic_width / total_width_points;

        var id_rect = root_rect;
        id_rect.width = id_field_width;

        var pri_rect = root_rect;
        pri_rect.width = pri_field_width;
        pri_rect.x += id_field_width;

        var second_rect = root_rect;
        second_rect.width = dynamic_width * 3f / total_width_points;
        second_rect.x += static_fields_width;

        var third_rect = second_rect;
        third_rect.x += second_rect.width;
        third_rect.width = dynamic_width * 2f / total_width_points;

        var label_rects = new Rect[label_width_points];
        var button_rects = new Rect[tab.buttons.Length];

        var offset = static_fields_width + 5 * unit_width;

        for (int i = 0; i < label_width_points; i++) {
            label_rects[i] = new Rect(offset, root_rect.yMin, unit_width, root_rect.height);
            offset += unit_width;
        }

        for (int i = 0; i < button_rects.Length; i++) {
            var w = tab.buttons[i].weight * unit_width;
            button_rects[i] = new Rect(offset, root_rect.yMin, w, root_rect.height);
            offset += w;
        }

        return new BugGUIRects {
            root_rect = root_rect,
            id_rect = id_rect,
            pri_rect = pri_rect,
            desc_rect = second_rect,
            cmt_rect = third_rect,
            label_rects = label_rects,
            button_rects = button_rects
        };
    }

    private void BugDeveloperFilterGUI(int tab_index) {
        if (tab_index >= bugGUITabs.Length) {
            Debug.LogWarning($"bugGUITabs is {bugGUITabs.Length} long, index is {tab_index}");
            return;
        }

        var tab = bugGUITabs[tab_index];
        if (tab.show_fixer_fiter) {
            var developerIndex = 0;
            var _developerList = DeveloperListExplicit(false);
            for (int i = 0; i < _developerList.Count; i++) {
                if (_developerList[i].id == developer) {
                    developerIndex = i + 1;  // account for the 'any' entry at the beginning of the string array
                    break;
                }
            }

            EditorGUI.BeginChangeCheck();
            developerIndex = EditorGUILayout.Popup(developerIndex, developerStringArray);
            if (EditorGUI.EndChangeCheck()) {
                if (!gotAllAssignedBugs || developerIndex == 0)
                    RequestBugs("Assigned-Bugs");
                developer = developerIndex != 0 ? _developerList[developerIndex - 1].id : -1;
                BugEditorConfig.defaultDeveloper = developer;
            }
        }
    }

    private void BugSearchFilterGUI() {
        filter = EditorGUILayout.TextField(filter, searchStyle);
    }

    private void BugGUILabels(int tab_index, BugGUIRects rects, Vector2 scrollPos, Rect scrollRect) {
        var tab = bugGUITabs[tab_index];

        if (!new Rect(scrollPos.x, scrollPos.y, scrollRect.width, scrollRect.height).Overlaps(rects.root_rect)) // Don't draw outside the view
            return;

        if (GUI.Button(rects.id_rect, "ID", sortButtonStyle))
            SetSortFunc(idSort);
        if (GUI.Button(rects.pri_rect, "Pr", sortButtonStyle))
            SetSortFunc(priSort);
        if (GUI.Button(rects.desc_rect, "Description", sortButtonStyle))
            SetSortFunc(descSort);
        GUI.Label(rects.cmt_rect, "Comments");

        var label_index = 0;
        if (tab.show_state) {
            GUI.Label(rects.label_rects[label_index], "State");
            label_index += 1;
        } if (tab.show_acceptor) {
            GUI.Label(rects.label_rects[label_index], "Acceptor");
            label_index += 1;
        } if (tab.show_fixer) {
            GUI.Label(rects.label_rects[label_index], "Fixer");
            label_index += 1;
        } if (tab.show_fix_source) {
            GUI.Label(rects.label_rects[label_index], "Fix Source");
            label_index += 1;
        } if (tab.show_resolver) {
            GUI.Label(rects.label_rects[label_index], "Resolver");
            label_index += 1;
        } if (tab.show_contact) {
            GUI.Label(rects.label_rects[label_index], "Contact");
            //label_index += 1
        }
    }

    private Dictionary<string, GUIContent> priorityIcons;
    private GUIContent priorityIcon(Bug bug) {
        if (priorityIcons == null || priorityIcons.Count == 0) {
            priorityIcons = new Dictionary<string, GUIContent>();
            var enumNames = Enum.GetNames(typeof(BugPriority));
            for (int i = 0; i < enumNames.Length; i++) {
                priorityIcons[enumNames[i]] = new GUIContent("", priorityIconTextures[i]);
            }
        }

        GUIContent result;
        if (priorityIcons.TryGetValue(bug.priority, out result))
            return result;

        Debug.LogWarning($"Found unknown priority #{bug.id}: {bug.priority}");
        return GUIContent.none;
    }

    private Texture[] _priorityIconTextures;
    private Texture[] priorityIconTextures {
        get {
            if (_priorityIconTextures == null || _priorityIconTextures.Length == 0) {
                var numUniquePriorities = Enum.GetValues(typeof(BugPriority)).Length;
                _priorityIconTextures = new Texture[numUniquePriorities];
                for (int i = 0; i < numUniquePriorities; i++) {
                    _priorityIconTextures[i] = AssetDatabase.LoadAssetAtPath<Texture>(BugPriorityIcon.paths[i]);
                }
            }

            return _priorityIconTextures;
        }
    }

    private string[] _bugStateStrings;
    private string[] bugStateStrings {
        get {
            if (_bugStateStrings == null || _bugStateStrings.Length == 0)
                _bugStateStrings = Enum.GetNames(typeof(BugState));
            return _bugStateStrings;
        }
    }

    private Dictionary<int, string> _bugIDStrings;
    private string bugIDStrings(Bug bug) {
        if (_bugIDStrings == null)
            _bugIDStrings = new Dictionary<int, string>();

        string result;
        if (_bugIDStrings.TryGetValue(bug.id, out result))
            return result;

        result = bug.id.ToString();
        _bugIDStrings[bug.id] = result;
        return result;
    }

    private void BugGUI(Bug bug, int tab_index, BugGUIRects rects, Vector2 scrollPos, Rect scrollRect) {
        var tab = bugGUITabs[tab_index];

        if (!new Rect(scrollPos.x, scrollPos.y, scrollRect.width, scrollRect.height).Overlaps(rects.root_rect)) // Don't draw outside the view
            return;

        var c = new Color(GUI.color.r, GUI.color.g, GUI.color.b, GUI.color.a);

        GUI.enabled = false;
        // Dirty hack from:
        //   http://answers.unity3d.com/questions/19786/how-do-i-modify-the-disabled-gui-style.html
        GUI.color = new Color(c.r, c.g, c.b, 2);
        GUI.Label(rects.id_rect, bugIDStrings(bug));
        GUI.Label(rects.pri_rect, priorityIcon(bug));
        //did_wrap = EditorStyles.textField.wordWrap
        //EditorStyles.textField.wordWrap = true
        var did_wrap = GUI.skin.textField.wordWrap;
        GUI.skin.textField.wordWrap = true;
        GUI.TextArea(rects.desc_rect, bug.description);
        GUI.TextArea(rects.cmt_rect, bug.comment);
        //EditorStyles.textField.wordWrap = did_wrap
        GUI.skin.textField.wordWrap = did_wrap;
        // Undo hack
        GUI.color = c;
        GUI.enabled = true;

        var label_index = 0;
        if (tab.show_state) {
            GUI.Label(rects.label_rects[label_index], bugStateStrings[(int) bug.state]);
            label_index += 1;
        } if (tab.show_acceptor) {
            GUI.Label(rects.label_rects[label_index], DeveloperName(bug.accept_responsible));
            label_index += 1;
        } if (tab.show_fixer) {
            GUI.Label(rects.label_rects[label_index], DeveloperName(bug.fix_responsible));
            label_index += 1;
        } if (tab.show_fix_source) {
            GUI.Label(rects.label_rects[label_index], bug.fix_source);
            label_index += 1;
        } if (tab.show_resolver) {
            GUI.Label(rects.label_rects[label_index], DeveloperName(bug.resolution_responsible));
            label_index += 1;
        } if (tab.show_contact) {
            GUI.Label(rects.label_rects[label_index], DeveloperName(bug.contact_responsible));
            //label_index += 1
        }

        for (int i = 0; i < tab.buttons.Length; i++) {
            var rect = rects.button_rects[i];
            tab.buttons[i].DrawSelf(bug, rect);
        }
    }

    private void BugDetails(Bug bug) {
        window.ShowDetails(bug);
    }

    private void BugScreenshot(Bug bug) {
        window.ShowScreenshot(bug);
    }

    private void AssignBug(Bug bug) {
        window.ShowAssigner(bug, BugAssignAction.Assign);
    }

    private void AssignFixBug(Bug bug) {
        window.ShowAssigner(bug, BugAssignAction.AssignFix);
    }

    private void AssignFixResolveBug(Bug bug) {
        window.ShowAssigner(bug, BugAssignAction.AssignFixResolve);
    }

    private void ReassignBug(Bug bug) {
        window.ShowAssigner(bug, BugAssignAction.Reassign);
    }

    private void FixBug(Bug bug) {
        window.ShowAssigner(bug, BugAssignAction.Fix);
    }

    private void BreakBug(Bug bug) {
        window.ShowAssigner(bug, BugAssignAction.Break);
    }

    private void ResolveBug(Bug bug) {
        window.ShowAssigner(bug, BugAssignAction.Resolve);
    }

    public void RemoveFromList(Bug bug) {
        window.RegisterSuperPositionBug(bug);
        deletedBugs.Add(bug);
    }

    public void MoveBug(Bug bug) {
        if (currentTab == 0 && bug.state != BugState.Resolved || bug.state == (BugState) currentTab - 1) {
            addedBugs.Add(bug);
        }
        else {
            window.RegisterSuperPositionBug(bug);
            deletedBugs.Add(bug);
        }
    }

    public void UpdateBugComment(int id, string comment) {
        foreach(var b in bugs) {
            // bugs is cloned when recompiling, using id instead
            if (b.id == id) {
                b.comment = comment;
                return;
            }
        }
    }

    public void UpdatePriority(int id, string p) {
        foreach (var b in bugs) {
            // bugs is cloned when recompiling, using id instead
            if (b.id == id) {
                b.SetPriority(p);
                return;
            }
        }
    }

    public void UpdateContactResponsible(int id, int r) {
        foreach (var b in bugs) {
            // bugs is cloned when recompiling, using id instead
            if (b.id == id) {
                b.SetContactResponsible(r);
                return;
            }
        }
    }

    public void UpdateBugFixer(Bug bug) {
        foreach(var b in bugs) {
            // bugs is cloned when recompiling, using id instead
            if (b.id == bug.id) {
                b.SetFixResponsible(bug.fix_responsible);
                return;
            }
        }
    }

    private void ReopenBug(Bug bug) {
        bug.state = BugState.New;
        BugUtility.ReopenBug(bug);
        window.ChangeBugState(bug);
    }

    private void DeleteBug(Bug bug) {
        BugUtility.DeleteBug(bug);
        window.RemoveFromList(bug);
    }

}
}

