using UnityEditor;

namespace Dungbeetle {

public static class BugPriorityIcon {

    private const string iconPath = "Assets/Plugins/Dungbeetle/Editor/Icons/";
    private static readonly string blackSuffix = EditorGUIUtility.isProSkin ? string.Empty : "_black";

    public static readonly string[] paths = GeneratePaths();

    private static string[] GeneratePaths() {
        var names = System.Enum.GetNames(typeof(BugPriority));
        var length = names.Length;
        var result = new string[length];
        while (length > 0) {
            length -= 1;
            result[length] = FilterPath(names[length]);
        }

        return result;
    }

    private static string FilterPath(string s) {
        return $"{iconPath}{s}16{blackSuffix}.png";
    }
}
}