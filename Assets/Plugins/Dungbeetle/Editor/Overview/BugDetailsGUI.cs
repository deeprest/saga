using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Dungbeetle {

public class BugDetailsGUI {

    private enum BugDetailsFailure {
        Comment,
        ContactResponsible,
        GetDevelopers,
        Priority,
        Screenshot,
    }

    private Bug bug;
    private GUIContent descriptionGUIContent;
    private Texture2D responseAsTexture;

    private List<ProjectDeveloper> developerList;
    private string[] developerStringArray;

    private Dictionary<int, string> _developerNameDict;
    private Dictionary<int, string>  developerNameDict {
        get {
            if (_developerNameDict == null) {
                _developerNameDict = new Dictionary<int, string>();
                foreach (var d in developerList)
                    _developerNameDict[d.id] = d.name;
                _developerNameDict[-1] = "";
            }
            return _developerNameDict;
        }
    }

    private BugPriority  priority;
    private int contact_responsible = -1;
    private Vector2  scrollPos;

    private bool  socketFailed;
    private string  socketFailMsg;
    private BugDetailsFailure socketFailType;

    private ushort failedPort;
    private string failedDomain;

    private enum PopupName{
        ContactResponsible,
        Priority,
    }

    private IBugOverview parentOverview;

    private void SocketFailureLabel(string msg) {
        GUILayout.Label($"{msg}\nPlease connect to the network, then reopen this window.\n\nAttempted to connect to " +
                        $"{Config.OverviewClientConfig.Domain}:{Config.OverviewClientConfig.Port}: {socketFailMsg}");
    }

    public bool  closed;
    public bool  needRepaint;

    public void OnGUI() {
        var style = new GUIStyle(GUI.skin.label) {
            fontSize = 20
        };

        if (parentOverview == null || parentOverview.IsDestroyed())
            parentOverview = BugOverviewWindow.window;

        if (socketFailed) {
            if (socketFailType == BugDetailsFailure.Comment)
                SocketFailureLabel($"Failed ({failedDomain}:{failedPort}) update comment for bug {bug.id}.");
            else if (socketFailType == BugDetailsFailure.ContactResponsible)
                SocketFailureLabel($"Failed ({failedDomain}:{failedPort}) update contact responsible for bug {bug.id}.");
            else if (socketFailType == BugDetailsFailure.GetDevelopers)
                SocketFailureLabel($"Failed ({failedDomain}:{failedPort}) fetching developers in project {bug.projectId}.");
            else if (socketFailType == BugDetailsFailure.Priority)
                SocketFailureLabel($"Failed ({failedDomain}:{failedPort}) update priority for bug {bug.id}.");
            else // socketFailType == BugDetailsFailure.Screenshot:
                SocketFailureLabel($"Failed ({failedDomain}:{failedPort}) fetching screenshot image for bug {bug.id}.");
            return;
        }

        GUILayout.BeginArea(new Rect(0, 0, Screen.width / EditorGUIUtility.pixelsPerPoint, Screen.height / EditorGUIUtility.pixelsPerPoint - 22f));

        GUILayout.Label($"Bug {bug.id}", style);

        if (parentOverview != null) {
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("<"))
                parentOverview.ShowDetailsPrevious(bug);
            if (GUILayout.Button(">"))
                parentOverview.ShowDetailsNext(bug);
            GUILayout.EndHorizontal();
        }

        scrollPos = GUILayout.BeginScrollView(scrollPos);

        GUILayout.Space(5f);

        if (responseAsTexture) {
            var responseRect = GUILayoutUtility.GetRect(0f, 200f);
            GUI.DrawTexture(responseRect, responseAsTexture, ScaleMode.ScaleToFit);
        }

        GUILayout.Space(5f);

        style = new GUIStyle(GUI.skin.textArea);
        style.margin = new RectOffset(10, 10, 10, 0);

        var r = GUILayoutUtility.GetRect(descriptionGUIContent, style);
        EditorGUI.SelectableLabel(r, bug.description, style);

        GUILayout.Space(5f);
        EditorGUILayout.LabelField("Comment:");
        bug.comment = EditorGUILayout.TextArea(bug.comment, style);

        if (GUILayout.Button("Save comment")) {
            BugUtility.SetBugComment(bug, bug.comment, ref socketFailed, ref socketFailMsg);
            if (socketFailed) {
                socketFailType = BugDetailsFailure.Comment;
                failedPort = Config.OverviewClientConfig.Port;
                failedDomain = Config.OverviewClientConfig.Domain;
            }
            else {
                Debug.Log($"Sent update to server: Bug {bug.id}");
                if (parentOverview != null) {
                    parentOverview.UpdateBugComment(bug.id, bug.comment);
                    parentOverview.Refresh();
                }
            }
        }

        GUILayout.Space(5f);

        GUILayout.BeginHorizontal();
        GUILayout.BeginVertical();
        GUILayout.Label("State:");
        GUILayout.Label("Time:");
        GUILayout.Label("IP:");
        GUILayout.Label("Email:");
        GUILayout.Label("Priority:");
        GUILayout.Label("Scene:");
        GUILayout.Label("Build:");
        GUILayout.Label("Accept responsible:");
        GUILayout.Label("Fix responsible:");
        GUILayout.Label("Fix source:");
        GUILayout.Label("Resolution responsible:");
        GUILayout.Label("Contact responsible:");
        GUILayout.EndVertical();

        GUILayout.BeginVertical();
        GUILayout.Label(System.Enum.GetName(typeof(BugState), bug.state));
        GUILayout.Label(bug.timeAsString);
        GUILayout.Label(bug.address);
        GUILayout.Label(bug.email);

        var old_priority = priority;

        priority = (BugPriority) EditorGUILayout.EnumPopup(priority);

        GUILayout.Label(bug.scene);
        GUILayout.Label(bug.build_name);
        GUILayout.Label(parentOverview != null ? parentOverview.DeveloperName(bug.accept_responsible) : developerNameDict[bug.accept_responsible]);
        GUILayout.Label(parentOverview != null ? parentOverview.DeveloperName(bug.fix_responsible) : developerNameDict[bug.fix_responsible]);
        GUILayout.Label(bug.fix_source);
        GUILayout.Label(parentOverview != null ? parentOverview.DeveloperName(bug.resolution_responsible) : developerNameDict[bug.resolution_responsible]);

        var contactResponsibleIndex = 0;
        for (int i = 0; i < developerList.Count; i++) {
            if (developerList[i].id == contact_responsible) {
                contactResponsibleIndex = i + 1; // account for the 'none' entry at the beginning of the string array
                break;
            }
        }

        var old_contact_responsible = contact_responsible;

        EditorGUI.BeginChangeCheck();
        contactResponsibleIndex = EditorGUILayout.Popup(contactResponsibleIndex, developerStringArray);
        if (EditorGUI.EndChangeCheck()) {
            if (contactResponsibleIndex > 0)
                contact_responsible = developerList[contactResponsibleIndex - 1].id;
            else
                contact_responsible = -1;
        }

        GUILayout.EndVertical();
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Attachments")) {
            if (parentOverview != null)
                parentOverview.ShowAttachments(bug);
            else
                BugAttachmentsWindow.Init(bug);
        }

        if (GUILayout.Button("Screenshot")) {
            if (parentOverview != null)
                parentOverview.ShowScreenshot(bug);
            else {
                BugScreenshotWindow.Init(bug, null);
            }
        }

        if (parentOverview is BugOverviewWindow) {
            var overview = parentOverview as BugOverviewWindow;
            var actionButtonRow = GUILayoutUtility.GetRect(0f, 18f);

            actionButtonRow.x += 4;
            actionButtonRow.width -= 8;

            if (bug.state == BugState.New) {
                actionButtonRow.width /= 4;
                overview.gui.assignBugButton.DrawSelf(bug, actionButtonRow);
                actionButtonRow.x += actionButtonRow.width;
                overview.gui.assignFixBugButton.DrawSelf(bug, actionButtonRow);
                actionButtonRow.x += actionButtonRow.width;
                overview.gui.assignFixResolveBugButton.DrawSelf(bug, actionButtonRow);
                actionButtonRow.x += actionButtonRow.width;
                overview.gui.deleteBugButton.DrawSelf(bug, actionButtonRow);
            }
            else if(bug.state == BugState.Assigned) {
                actionButtonRow.width /= 3;
                overview.gui.unassignBugButton.DrawSelf(bug, actionButtonRow);
                actionButtonRow.x += actionButtonRow.width;
                overview.gui.reassignBugButton.DrawSelf(bug, actionButtonRow);
                actionButtonRow.x += actionButtonRow.width;
                overview.gui.fixBugButton.DrawSelf(bug, actionButtonRow);
            }
            else if(bug.state == BugState.Fixed) {
                actionButtonRow.width /= 2;
                overview.gui.breakBugButton.DrawSelf(bug, actionButtonRow);
                actionButtonRow.x += actionButtonRow.width;
                overview.gui.resolveBugButton.DrawSelf(bug, actionButtonRow);
            }
            else if (bug.state == BugState.Resolved) {
                overview.gui.reopenBugButton.DrawSelf(bug, actionButtonRow);
            }
        }

        GUILayout.EndScrollView();

        if (priority != old_priority) {
            BugUtility.SetBugPriority(bug, priority, ref socketFailed, ref socketFailMsg);
            if (socketFailed) {
                socketFailType = BugDetailsFailure.Priority;
                failedPort = Config.OverviewClientConfig.Port;
                failedDomain = Config.OverviewClientConfig.Domain;
            }
            else if(parentOverview != null) {
                parentOverview.UpdatePriority(bug.id, bug.priority);
                parentOverview.Refresh();
            }
        }

        if (contact_responsible != old_contact_responsible) {
            BugUtility.SetBugContactResponsible(bug, contact_responsible, ref socketFailed, ref socketFailMsg);
            if (socketFailed) {
                socketFailType = BugDetailsFailure.ContactResponsible;
                failedPort = Config.OverviewClientConfig.Port;
                failedDomain = Config.OverviewClientConfig.Domain;
            }
            else if(parentOverview != null) {
                parentOverview.UpdateContactResponsible(bug.id, bug.contact_responsible);
                parentOverview.Refresh();
            }
        }

        GUILayout.EndArea();
    }

    public void SetParentOverview(IBugOverview ov) {
        parentOverview = ov;
        if (parentOverview == null || parentOverview.IsDestroyed())
            parentOverview = BugOverviewWindow.window;

        if (parentOverview != null)
            developerList = parentOverview.DeveloperListExplicit(); // Save one network request cycle by using already obtained data
        else if (developerList == null) {
            developerList = BugUtility.GetAllProjectDevelopers(Config.OverviewClientConfig.ProjectId, out socketFailed, out socketFailMsg);
            if (socketFailed) {
                socketFailType = BugDetailsFailure.GetDevelopers;
                failedPort = Config.OverviewClientConfig.Port;
                failedDomain = Config.OverviewClientConfig.Domain;
            }
        }

        developerStringArray = new string[developerList.Count + 1];
        developerStringArray[0] = "None";

        for (int i = 1; i < developerStringArray.Length; i++) {
            developerStringArray[i] = developerList[i - 1].name;
        }
    }

    public void SetBug(Bug b, bool fetchScreenshot ) {
        EditorGUIUtility.editingTextField = false;

        bug = b;
        descriptionGUIContent = new GUIContent(bug.description);

        if (fetchScreenshot) {
            responseAsTexture = BugUtility.GetTexture(bug, ref socketFailed, ref socketFailMsg);
            responseAsTexture.hideFlags = HideFlags.DontSaveInEditor;
            if (socketFailed) {
                socketFailType = BugDetailsFailure.Screenshot;
                failedPort = Config.OverviewClientConfig.Port;
                failedDomain = Config.OverviewClientConfig.Domain;
            }
        }

        TryCachePriority(bug);

        contact_responsible = bug.contact_responsible;
    }

    public void OnBugDeleted(Bug b) {
        closed = bug.id == b.id;
    }

    public void OnBugChanged(Bug b) {
        if (bug.id == b.id) {
            if (bug != b)
                SetBug(b, false);
            else
                TryCachePriority(b); // Cache might be invalid after bug change
            needRepaint = true;
        }
    }

    private void TryCachePriority(Bug b) {
        try {
            priority = (BugPriority) System.Enum.Parse(typeof(BugPriority), bug.priority);
        }
        catch {
            priority = BugPriority.Low;
        }
    }


}
}
