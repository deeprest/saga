using System.Collections.Generic;
using UnityEngine;

namespace Dungbeetle {
public class CompleteConfig : ScriptableObject {
    public DomainField Domain;
    public PortField Port;
    public PubKeyField PubKey;
    public PrivKeyField PrivKey;
    public PasswordField ReporterPassword;
    public PasswordField DeveloperPassword;
    public ProjectIdField ProjectId;
    public BoolField EditorServerEnabled;

    public IEnumerator<Object> subAssets {
        get {
            yield return Domain;
            yield return Port;
            yield return PubKey;
            yield return PrivKey;
            yield return ReporterPassword;
            yield return DeveloperPassword;
            yield return ProjectId;
            yield return EditorServerEnabled;
        }
    }
}
}