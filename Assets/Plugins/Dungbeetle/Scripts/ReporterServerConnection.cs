using System.Net.Sockets;

namespace Dungbeetle {
static class ReporterServerConnection {

    public static TcpClient Connection() {
        var dClient = new DungbeetleTcpClient(ReporterConnectionConfig.Domain,
                                              ReporterConnectionConfig.Port   );
        dClient.Connect();
        return dClient.TcpClient;
    }
}
}
