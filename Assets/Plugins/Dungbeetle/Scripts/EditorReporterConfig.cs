using UnityEngine;

namespace Dungbeetle {
public interface IEditorReporterConfigHelper {
    EditorReporterConfig GetConfig();
    void MarkDirty (EditorReporterConfig config);
}

public class EditorReporterConfig : ScriptableObject {

    private static EditorReporterConfig config {
        get {
            if (helper != null && _config == null)
                _config = helper.GetConfig();
            if (_config == null)
                _config = CreateInstance<EditorReporterConfig>();
            return _config;
        }
    }
    private static EditorReporterConfig _config;

    private static IEditorReporterConfigHelper helper;
    public static void SetHelper (IEditorReporterConfigHelper newHelper) {
        helper = newHelper;
    }

    public string defaultReporter;
    public int selectedTab = 1;

    public static string DefaultReporter {
        get { return config.defaultReporter; }
        set {
            config.defaultReporter = value;
            helper?.MarkDirty(config);
        }
    }

    public static int SelectedTab {
        get { return config.selectedTab; }
        set {
            config.selectedTab = value;
            helper?.MarkDirty(config);
        }
    }

}
}
