using System;
using System.Collections.Generic;
using System.IO;

using YamlDotNet.RepresentationModel;
using YamlDotNet.Serialization;

namespace Dungbeetle {
public class SerializedBugReportParent {

    public SerializedBugReport BugReport { get { return bugReport; } set { bugReport = value; } }
    private SerializedBugReport bugReport;
}

public class SerializedBugReport {

    private string  _projectId,
                    _email,
                    _scene,
                    _build_name,
                    _description;
    public string[] _attachments;

    public string   projectId   { get { return _projectId;   } set { _projectId   = value; } }
    public string   email       { get { return _email;       } set { _email       = value; } }
    public string   scene       { get { return _scene;       } set { _scene       = value; } }
    public string   build_name  { get { return _build_name;  } set { _build_name  = value; } }
    public string   description { get { return _description; } set { _description = value; } }
    public string[] attachments { get { return _attachments; } set { _attachments = value; } }

    public SerializedBugReport (BugReport bugReport) {
        this._projectId       = bugReport.projectId.ToString();
        this._email           = bugReport.email;
        this._scene           = bugReport.scene;
        this._build_name      = bugReport.build_name;
        this._description     = bugReport.description;
        this._attachments     = bugReport.attachments.ToArray();
    }
}

public class BugReport {

    public static readonly string bugReportFormatVersion = "1.0";

    public int          projectId;
    public string       email;
    public string       scene;
    public string       build_name;
    public string       description;
    public List<string> attachments;
    public List<int>    attachmentIds; // Used by BugAttachmentsGUI for cache

    public BugReport () {
        this.attachments = new List<string>();
    }

    public BugReport (int p, string em, string sc, string bn, string desc, params string[] att) {
        this.projectId = p;
        this.email = em;
        this.scene = sc;
        this.build_name = bn;
        this.description = desc;
        this.attachments = new List<string>(att);
    }

    public BugReport (int p, string em, string sc, string bn, string desc, List<string> att) {
        this.projectId = p;
        this.email = em;
        this.scene = sc;
        this.build_name = bn;
        this.description = desc;
        this.attachments = att;
    }

    // Could be private, if it weren't for the warnings
    private BugReport (int p, string em, string sc, string bn, string desc) {
        this.projectId = p;
        this.email = em;
        this.scene = sc;
        this.build_name = bn;
        this.description = desc;
    }

    public override string ToString() => $"BugReport{{{email} - {description}}}";

    public virtual string ToYaml() {
        var b = new SerializedBugReportParent { BugReport = new SerializedBugReport(this) };
        var o = new StringWriter();
        var s = new SerializerBuilder().DisableAliases().Build();
        s.Serialize(o, b);
        return o.ToString();
    }

    public static BugReport FromYaml (string source, string version) {
        var invalid = version != bugReportFormatVersion;

        var yaml = new YamlStream();
        yaml.Load(new StringReader(source));
        var mapping = (YamlMappingNode) yaml.Documents[0].RootNode;

        var bugReport = new BugReport();
        foreach (var root in mapping.Children)
            foreach (var entry in (YamlMappingNode) root.Value)
                bugReport.AssignKey(entry.Key.ToString(), entry.Value);
        if (invalid)
            bugReport.UpgradeFrom(version);
        return bugReport;
    }

    protected virtual bool AssignKey (string key, YamlNode valueNode) {
        switch (key) {
            case "projectId":
                int.TryParse(valueNode.ToString(), out projectId);
                break;
            case "email":
                email = valueNode.ToString();
                break;
            case "scene":
                scene = valueNode.ToString();
                break;
            case "build_name":
                build_name = valueNode.ToString();
                break;
            case "description":
                description = valueNode.ToString();
                break;
            case "attachments":
                attachments = new List<string>();
                foreach (var a in (YamlSequenceNode) valueNode)
                    attachments.Add(a.ToString());
                break;
            default:
                return false;
        }
        return true;
    }

    public virtual void UpgradeFrom (string version) {}
}
}
