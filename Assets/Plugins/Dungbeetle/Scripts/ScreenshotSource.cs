using System;
using System.Collections;
using System.IO;

using UnityEngine;

namespace Dungbeetle {
public class ScreenshotSource {

    private Texture2D texture;

    private string path;
    private bool captured;
    private bool loaded;

    private byte[] bytesShrunk {
        get {
            if (_bytesShrunk == null) {
                var aspectR = (float) texture.width / (float) texture.height;
                var textureSmall = new Texture2D((int)(aspectR*480), 480);
                TextureScale.Bilinear(texture, textureSmall, null);
                _bytesShrunk = ImageConversion.EncodeToPNG(textureSmall);
            }
            return _bytesShrunk;
        }
    }
    private byte[] _bytesShrunk;

    private void LoadFromBytes (byte[] bytes) {
        texture = new Texture2D(2, 2);
        ImageConversion.LoadImage(texture, bytes);
        loaded = true;
    }

    private void LoadFromPath (string imagePath) {
        LoadFromBytes(File.ReadAllBytes(imagePath));
    }

    private void EnsureCaptured() {
        if (!captured)
            throw new FileNotFoundException(path);
        if (!loaded)
            LoadFromPath(path);
    }

    public byte[]    Bytes       { get { EnsureCaptured(); return this.bytesShrunk; } }
    public Texture2D FullTexture { get { EnsureCaptured(); return this.texture;     } }

    public bool Ready => captured || (captured = File.Exists(path));

    public class WaitForSource : IEnumerator {

        private ScreenshotSource source;
        private float endTime;
        private float nextCheck;

        private bool done;

        public WaitForSource (ScreenshotSource source, float timeout) {
            this.source    = source;
            this.endTime   = Time.realtimeSinceStartup + timeout;
            this.nextCheck = Time.realtimeSinceStartup - 0.001f;
        }

        public object Current => null;

        public bool MoveNext() {
            if (done || Time.realtimeSinceStartup > endTime)
                return false;
            if (Time.realtimeSinceStartup <= nextCheck)
                return true;

            nextCheck = Time.realtimeSinceStartup + 0.3f;
            done = source.Ready;
            return !done;
        }

        public void Reset() {}
    }

    public WaitForSource Wait (float timeout) => new WaitForSource(this, timeout);

    public void Dispose() {
        try {
            if (captured)
                File.Delete(path);
            Directory.Delete(Application.temporaryCachePath + "/Bug Report Screenshots/");
            Directory.Delete(Application.temporaryCachePath);
        }
        catch (Exception) {

        }
    }

    public ScreenshotSource () {
        this.path = Application.temporaryCachePath + "/Bug Report Screenshots/";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);
        var day    = System.DateTime.Now.Day;
        var hour   = System.DateTime.Now.Hour;
        var minute = System.DateTime.Now.Minute;
        var second = System.DateTime.Now.Second;
        path = $"{path}{day} {hour} {minute} {second}.png";
            ScreenCapture.CaptureScreenshot(path);
    }
}
}
