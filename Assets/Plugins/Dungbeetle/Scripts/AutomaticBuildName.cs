using UnityEngine;
using UnityEngine.Serialization;

namespace Dungbeetle {
[System.Serializable]
public class AutomaticBuildName : ISerializationCallbackReceiver {
    [HideInInspector, SerializeField]
    private string bakedString;

    // These fields are included in the player, because the code might be in a DLL
    [SerializeField]
    private string prefix;
    [HideInInspector, SerializeField]
    private bool includeDate,
                 includeTime,
                 includeMachineName,
                 includeEditorSuffix;

    public override string ToString() => includeEditorSuffix && Application.isEditor ? $"{bakedString} editor" : bakedString;

    public AutomaticBuildName (string prefix, bool includeDate = false, bool includeTime = false, bool includeMachineName = false, bool includeEditorSuffix = false) {
        this.prefix = prefix;
        this.includeDate = includeDate || string.IsNullOrEmpty(prefix) && !includeTime && !includeMachineName;
        this.includeTime = includeTime;
        this.includeMachineName = includeMachineName;
        this.includeEditorSuffix = includeEditorSuffix;
    }

    protected virtual string GetString() {
        var substrings = new System.Collections.Generic.List<string>(5);
        if (!string.IsNullOrEmpty(prefix)) substrings.Add(prefix);
        if (includeDate)                   substrings.Add(System.DateTime.Now.ToString("yyyy-MM-dd"));
        if (includeTime)                   substrings.Add(System.DateTime.Now.ToString("HH:mm"));
        if (includeMachineName)            substrings.Add(System.Environment.MachineName);
        return string.Join(" ", substrings);
    }

    public void Update() => bakedString = GetString();

    public void OnBeforeSerialize () => Update();
    public void OnAfterDeserialize() {}
}
}
