using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Dungbeetle {
public class DungbeetleTcpClient {

    private IPAddress[] addresses;
    private TcpClient client;
    private int port;

    public DungbeetleTcpClient (string domain, int port) {
        this.addresses = Dns.GetHostAddresses(domain);

        var addressFamily = AddressFamily.InterNetworkV6;
        foreach (var a in addresses)
            if (a.AddressFamily == AddressFamily.InterNetwork)
                addressFamily = AddressFamily.InterNetwork;

        var filteredAddresses = new List<IPAddress> (addresses.Length);
        foreach (var a in addresses)
            if (a.AddressFamily == addressFamily)
                filteredAddresses.Add(a);

        this.addresses = filteredAddresses.ToArray();

        this.client = new TcpClient (addressFamily);
        this.port = port;
    }

    public void Connect() => client.Connect(addresses, port);

    public IAsyncResult BeginConnect() => client.BeginConnect(addresses, port, null, null);

    public void EndConnect (IAsyncResult asyncResult) => client.EndConnect(asyncResult);

    public TcpClient TcpClient => client;
}
}
