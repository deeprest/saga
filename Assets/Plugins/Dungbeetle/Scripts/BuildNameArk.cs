using UnityEngine;

namespace Dungbeetle {
public class BuildNameArk : ScriptableObject {
    public static string BuildName => ark.useCustomBuildName ? ark.customBuildName : ark.buildName.ToString();

    public static BuildNameArk ark;

    [SerializeField]
    public AutomaticBuildName buildName;
#pragma warning disable CS0649
    [SerializeField]
    private string customBuildName;
    [SerializeField]
    private bool useCustomBuildName;
#pragma warning restore CS0649

    public void OnEnable() {
        BuildNameArk.ark = this;
    }
}
}
