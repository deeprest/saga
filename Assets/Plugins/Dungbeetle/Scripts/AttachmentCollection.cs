using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Dungbeetle {
public interface IAttachment {
    bool Ready { get; }
}

public class LogAttachment : IAttachment {

    public bool Ready { get; private set; }

    private string content;

    public LogAttachment (bool @short) {
        if (LogReader.CanFindLogFile())
            LogReader.LogAsString(SetLogFileString, @short);
        else if (Application.isEditor) {
            var p    = Application.platform;
            var wp   = RuntimePlatform.WindowsPlayer;
            var osxp = RuntimePlatform.OSXPlayer;
            if (p == wp || p == osxp)
                SetLogFileString("Log file could not be found in editor. Something went wrong.");
            else
                SetLogFileString("Log file in editor is only supported for Windows and OSX! (check LogReader.boo)");
        }
        else  {
            SetLogFileString("Log file not found!");
        }
    }

    private void SetLogFileString (string content) {
        this.content = content;
        Ready = true;
    }

    public override string ToString() => content;
}

public class AttachmentCollection {
    private List<object>      readyAttachments   = new List<object>();
    private List<IAttachment> waitingAttachments = new List<IAttachment>();

    public void AddString (string attachment) {
        readyAttachments.Add(attachment);
    }

    public void AddLog(bool @short) {
        var attachment = new LogAttachment(@short);
        if (attachment.Ready)
            readyAttachments.Add(attachment);
        else
            waitingAttachments.Add(attachment);
    }

    public bool Ready => waitingAttachments.Count == 0;
    public int  Count => readyAttachments.Count;

    private void UpdateReadyStatus() {
        var i = 0;
        while (i < waitingAttachments.Count) {
            if (waitingAttachments[i].Ready) {
                readyAttachments.Add(waitingAttachments[i]);
                waitingAttachments.RemoveAt(i);
            }
            else i += 1;
        }
    }

    public string[] ToStringArray() {
        var result = new string[readyAttachments.Count];
        for (int i = 0; i < result.Length; i++) {
            result[i] = readyAttachments[i].ToString();
        }
        return result;
    }

    public WaitForSource Wait() => new WaitForSource(this);

    public class WaitForSource : IEnumerator {

        private AttachmentCollection source;

        public WaitForSource (AttachmentCollection source) {
            this.source = source;
        }

        public object Current => null;

        public bool MoveNext() {
            source.UpdateReadyStatus();
            return !source.Ready;
        }

        public void Reset() {}
    }
}
}
