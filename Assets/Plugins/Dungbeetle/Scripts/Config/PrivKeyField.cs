namespace Dungbeetle {
public class PrivKeyField : ConfigField {
    [PrivKey]
    public string key;

    public static implicit operator string(PrivKeyField f) => f.key;
}
}
