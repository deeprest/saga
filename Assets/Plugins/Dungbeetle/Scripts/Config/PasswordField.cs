namespace Dungbeetle {
public class PasswordField : ConfigField {
    [Password]
    public string password;

    public static implicit operator string(PasswordField f) => f.password;
    public static implicit operator byte[](PasswordField f) => System.Text.Encoding.ASCII.GetBytes(f.password);
}
}
