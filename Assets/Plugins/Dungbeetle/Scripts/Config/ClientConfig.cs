using System.Collections.Generic;

using UnityEngine;

namespace Dungbeetle {
public class ClientConfig : ScriptableObject {
    public DomainField    Domain;
    public PortField      Port;
    public PubKeyField    PubKey;
    public PasswordField  Password;
    public ProjectIdField ProjectId;

    public IEnumerator<UnityEngine.Object> subAssets {
        get {
            yield return Domain;
            yield return Port;
            yield return PubKey;
            yield return Password;
            yield return ProjectId;
        }
    }
}
}
