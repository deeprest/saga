namespace Dungbeetle {
public class DomainField : ConfigField {
    public string domain;

    public override string ToString() => this;

    public static implicit operator string (DomainField f) => f.domain;
}
}
