namespace Dungbeetle {
public class PubKeyField : ConfigField {
    [PubKey]
    public string key;

    public static implicit operator string (PubKeyField f) => f.key;
    public static implicit operator byte[] (PubKeyField f) => System.Text.Encoding.ASCII.GetBytes(f.key);
}
}
