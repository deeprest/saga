namespace Dungbeetle {
public class ProjectIdField : ConfigField {
    [ProjectId]
    public uint projectId;

    public static implicit operator uint (ProjectIdField f) => f.projectId;
}
}
