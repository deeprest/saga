namespace Dungbeetle {
public class PortField : ConfigField {
    public ushort port;

    public override string ToString() => this;

    public static implicit operator ushort(PortField f) => f.port;
    public static implicit operator    int(PortField f) => (int) f.port;
    public static implicit operator string(PortField f) => f.port.ToString();
}
}
