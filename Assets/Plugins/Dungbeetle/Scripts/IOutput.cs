using System;

namespace Dungbeetle
{
    public interface IOutput
    {
        void Log(string s);
    }
}
