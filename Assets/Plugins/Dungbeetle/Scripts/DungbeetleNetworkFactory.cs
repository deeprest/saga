using System;
using System.IO;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

namespace Dungbeetle {
public class DungbeetleNetworkFactory {

    private RSACryptoServiceProvider asymetricAlgorithm {
        get {
            if (_asymmetricAlgorithm == null || _asymmetricAlgorithmSource != _pubKey.key) {
                _asymmetricAlgorithmSource = _pubKey.key;
                _asymmetricAlgorithm = new RSACryptoServiceProvider();
                _asymmetricAlgorithm.FromXmlString(_pubKey);
            }
            return _asymmetricAlgorithm;
        }
    }
    private RSACryptoServiceProvider _asymmetricAlgorithm;
    private string                   _asymmetricAlgorithmSource;

    private byte[] _symKey;

    private PasswordField  _password;
    private PubKeyField    _pubKey;
    private PrivilegeLevel _privilegeLevel;
    private DateTime       _previousConnectionTime = new DateTime(1970, 1, 1);
    private string         _currentIdentifier = RandomPassword(20);

    public DungbeetleNetworkFactory (PasswordField password, PubKeyField pubKey, PrivilegeLevel privilegeLevel) {
        _password = password;
        _pubKey = pubKey;
        _privilegeLevel = privilegeLevel;

        var aesSource = new AesManaged();
        _symKey = aesSource.Key;
    }

    public DungbeetleNetwork Create (TcpClient client, IOutput log) {
        var likelyExpired = (DateTime.Now - _previousConnectionTime).Minutes > 10f;
        var result = Create(client, _password, this.asymetricAlgorithm, _symKey, _privilegeLevel, _currentIdentifier, likelyExpired, log);
        if (result != null)
            _previousConnectionTime = DateTime.Now;
        return result;
    }

        public static DungbeetleNetwork Create (TcpClient client, string password, string pubKey, PrivilegeLevel privilegeLevel, IOutput log) {
        var pwBytes = System.Text.Encoding.ASCII.GetBytes(password);
        return Create(client, pwBytes, pubKey, privilegeLevel, log);
    }

    private static DungbeetleNetwork Create (TcpClient client, byte[] password, string pubKey, PrivilegeLevel privilegeLevel, IOutput log) {
        using (var tempRSA = new RSACryptoServiceProvider())
        using (var tempAes = new AesManaged()) {
            tempRSA.FromXmlString(pubKey);
            return Create(client, password, tempRSA, tempAes.Key, privilegeLevel, RandomPassword(20), true, log);
        }
    }

    private static DungbeetleNetwork Create (TcpClient client, byte[] password, RSACryptoServiceProvider asym, byte[] symKey, PrivilegeLevel privilegeLevel, string currentIdentifier, bool likelyExpired, IOutput log) {
        var success = false;
        NetworkStream stream = default (NetworkStream);
        BinaryReader  reader = default (BinaryReader);
        BinaryWriter  writer = default (BinaryWriter);
        try {
            stream = client.GetStream();
            reader = new BinaryReader(stream);
            writer = new BinaryWriter(stream);

            writer.Write(likelyExpired ? "GetKey" : "Key");

            string response = default (string);

            if (!likelyExpired) {
                reader.ReadString(); // Always 'Send key'
                writer.Write(currentIdentifier);

                response = reader.ReadString();
                if (response == "Key accepted") {
                    var network = new DungbeetleNetwork(stream, reader, writer, symKey, log);
                    success = true;
                    return network;
                }
            }

            reader.ReadString(); // Always 'Send password'

            password = asym.Encrypt(password, false);
            writer.Write(password.Length);
            writer.Write(password);

            response = reader.ReadString();
            if (response == $"{privilegeLevel} accepted") {
                writer.Write(currentIdentifier);
                var key = asym.Encrypt(symKey, false);
                writer.Write(key.Length);
                writer.Write(key);

                var network = new DungbeetleNetwork(stream, reader, writer, symKey, log);
                success = true;
                return network;
            }

            throw new ProtocolException($"Server did not accept the password for '{privilegeLevel}': {response}");
        }
        finally {
            if (success == false) {
                if (writer != null) ((IDisposable) writer).Dispose();
                if (reader != null) ((IDisposable) reader).Dispose();
                if (stream != null) ((IDisposable) stream).Dispose();
            }
        }
    }

    /*********************************************
    **          Password Generation             **
    *********************************************/
    private static readonly string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    private static System.Random randomProvider => _randomProvider ?? (_randomProvider = new System.Random());
    private static System.Random _randomProvider;

    private static string RandomPassword (int length) {
        var res = new StringBuilder();
        while (0 < length--)
            res.Append(valid[DungbeetleNetworkFactory.randomProvider.Next(valid.Length)]);
        return res.ToString();
    }
}
}
