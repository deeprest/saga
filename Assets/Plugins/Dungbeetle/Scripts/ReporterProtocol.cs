using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Threading;

namespace Dungbeetle {
static class ReporterProtocol {

    private static DungbeetleNetworkFactory networkFactory = new DungbeetleNetworkFactory (ReporterConnectionConfig.Password, ReporterConnectionConfig.PubKey, PrivilegeLevel.Reporter);

    private class UnityDebug : IOutput {
        public void Log(string s) => UnityEngine.Debug.Log(s);
    }

    private static IOutput unityDebug = new UnityDebug();

    public static void SendNewBug (BugReport bug, byte[] imageBytes, Action<string> progressMessageHandler, Action<float> progressHandler, Action<float> attachmentsHandler, CancellationToken cancellationToken, ref string resultMessage, ref bool socketFailed, ref string socketFailMsg) {
        var attachments   = bug.attachments;
        bug.attachments   = new List<string>();
        var serializedBug = bug.ToYaml();
        try {
            using (var client  = ReporterServerConnection.Connection())
            using (var network = networkFactory.Create(client, unityDebug)) {
                client.ReceiveTimeout = 600000;
                network.Write("New2");
                network.Write(BugReport.bugReportFormatVersion);
                network.Write(serializedBug, progressMessageHandler, progressHandler, cancellationToken);
                network.Write(attachments, progressMessageHandler, attachmentsHandler, cancellationToken);
                network.Write(imageBytes);
                progressMessageHandler("Waiting for server");
                while (!network.stream.DataAvailable) {
                    if (cancellationToken.IsCancellationRequested) {
                        socketFailed = true;
                        socketFailMsg = "Cancelled";
                        return;
                    }
                    Thread.Sleep(10);
                }
                resultMessage = network.ReadString();
            }
        }
        catch (SocketException e) {
            socketFailed  = true;
            socketFailMsg = e.Message;
        }
        catch (ProtocolException e) {
            socketFailed  = true;
            socketFailMsg = e.Message;
        }
        catch (EndOfStreamException e) {
            socketFailed  = true;
            socketFailMsg = e.Message;
        }
        catch (IOException e) {
            socketFailed  = true;
            socketFailMsg = e.Message;
        }
    }
}
}
