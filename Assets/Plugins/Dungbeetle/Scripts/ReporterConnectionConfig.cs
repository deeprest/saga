using System;

using UnityEngine;

namespace Dungbeetle {
public class ReporterConnectionConfig : ScriptableObject {

    private static ReporterConnectionConfig config {
        get {
            var report = _config == null;
            if (_config == null)
                LoadConfig();
            if (report && _config != null)
                _config.CheckForBrokenReferences();
            return _config;
        }
    }

    private static ReporterConnectionConfig configUnchecked {
        get {
            if (_config == null)
                LoadConfig();
            return _config;
        }
    }

    private static ReporterConnectionConfig _config;

    private void CheckForBrokenReferences() {
        if (clientConfig == null)
            Debug.LogWarning("ReporterConnectionConfig.asset seems to contain broken references. Check its meta file. Delete ReporterConnectionConfig.asset if you want to regenerate it.");
    }

    private static void LoadConfig() {
        if (Application.isEditor)
            _config = loadConfig();
        else
            _config = (ReporterConnectionConfig) UnityEngine.Resources.Load("ReporterConnectionConfig", typeof(ReporterConnectionConfig));
    }

    public delegate void dirtyEvent (UnityEngine.Object o);
    public static event dirtyEvent setDirty; // Needs to be set from a UnityEditor context.

    static public Func<ReporterConnectionConfig> loadConfig; // Needs to be set from a UnityEditor context.

    static public string        Domain    => ReporterConnectionConfig.config.clientConfig.Domain;
    static public ushort        Port      => ReporterConnectionConfig.config.clientConfig.Port;
    static public PubKeyField   PubKey    => ReporterConnectionConfig.config.clientConfig.PubKey;
    static public PasswordField Password  => ReporterConnectionConfig.config.clientConfig.Password;
    static public uint          ProjectId => ReporterConnectionConfig.config.clientConfig.ProjectId;

    [SerializeField]
    private ClientConfig clientConfig;

    public bool Connected => clientConfig;

    public static void ConnectConfig (ClientConfig cc) {
        var c = ReporterConnectionConfig.configUnchecked;
        c.clientConfig = cc;
        setDirty(c);
    }
}
}
