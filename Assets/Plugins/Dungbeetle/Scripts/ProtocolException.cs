using System;

namespace Dungbeetle {
public class ProtocolException : Exception {
    public ProtocolException (string m) : base(m) {}
}
}
