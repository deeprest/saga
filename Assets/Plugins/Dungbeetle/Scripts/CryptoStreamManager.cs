using System;
using System.IO;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Security.Cryptography;
using System.Text;

using System.Threading;

namespace Dungbeetle {
public class CryptoStreamManager : IDisposable {

    private SymmetricAlgorithm algorithm;
    private BinaryReader binaryReader; 
    private BinaryWriter binaryWriter;

#pragma warning disable 414
    private IOutput output;
#pragma warning restore 414

    public byte[] Key => algorithm.Key;

    public CryptoStreamManager (NetworkStream s, BinaryReader binaryReader, BinaryWriter binaryWriter, byte[] key, IOutput output) {
        this.algorithm     = new AesManaged();
        this.algorithm.Key = key;
        this.binaryReader  = binaryReader;
        this.binaryWriter  = binaryWriter;
        this.output        = output;
    }

    public CryptoStreamManager (NetworkStream s, byte[] key, IOutput output) {
        this.algorithm     = new AesManaged();
        this.algorithm.Key = key;
        this.binaryReader  = new BinaryReader(s);
        this.binaryWriter  = new BinaryWriter(s);
        this.output        = output;
    }

    private void ContentWriter (Action<BinaryWriter> f) {
        ContentWriter(f, SendPlain, null, null, default(CancellationToken));
    }
    private void ContentWriter (Action<BinaryWriter> f, Action<byte[], Action<float>, CancellationToken> sendAction, Action<string> reportMessageAction, Action<float> reportProgressAction, CancellationToken c) {
        algorithm.GenerateIV();

        byte[] output;
        MemoryStream memoryStream;
        using (memoryStream = new MemoryStream())
        using (var cryptoStream = new CryptoStream(memoryStream, algorithm.CreateEncryptor(), CryptoStreamMode.Write))
        using (var tempBWriter  = new BinaryWriter(cryptoStream)) {
            reportMessageAction?.Invoke("Formatting data");
            f(tempBWriter);
            reportMessageAction?.Invoke("Encrypting");
        }

        output = memoryStream.ToArray();

        var IVAndContentLength = new byte[20];
        algorithm.IV                        .CopyTo(IVAndContentLength, 0 );
        BitConverter.GetBytes(output.Length).CopyTo(IVAndContentLength, 16);

        binaryWriter.Write(IVAndContentLength);
        sendAction(output, reportProgressAction, c);
    }

    private void SendPlain(byte[] bytes, Action<float> reportAction, CancellationToken c) {
        binaryWriter.Write(bytes);
    }

    private void SendAndReport(byte[] bytes, Action<float> reportAction, CancellationToken c) {
        var bytesToWrite = bytes.Length;
        var chunk =  new byte[4096 < bytesToWrite ? 4096 : bytesToWrite];
        long bytesWritten = 0;
        while (bytesWritten < bytesToWrite && !c.IsCancellationRequested) {
            long bytesNotWritten = bytesToWrite - bytesWritten;
            var chunkSize = 4096 < bytesNotWritten ? 4096 : bytesNotWritten;
            Array.Copy(bytes, bytesWritten, chunk, 0, chunkSize);

            binaryWriter.Write(chunk, 0, (int) chunkSize);
            bytesWritten += chunkSize;
            reportAction(((float) bytesWritten) / bytesToWrite);
        }
    }

    private void ContentReader (Action<BinaryReader> f) {
        var IVAndContentLength = binaryReader.ReadBytes(20);
        var IVLength = IVAndContentLength.Length-4;
        var IV = new byte[IVLength];
        Array.Copy(IVAndContentLength, 0, IV, 0, IVLength);
        algorithm.IV = IV;
        var numBytes = BitConverter.ToInt32(IVAndContentLength, 16);

        var rawBytes = binaryReader.ReadBytes(numBytes);
        using (var memoryStream = new MemoryStream(rawBytes))
        using (var cryptoStream = new CryptoStream(memoryStream, algorithm.CreateDecryptor(), CryptoStreamMode.Read))
        using (var tempBReader  = new BinaryReader(cryptoStream)) {
            f(tempBReader);
        }
    }

    public void Write (string s, Action<string> m, Action<float> f, CancellationToken t) {
        ContentWriter((BinaryWriter w) => {
            w.Write(s);
        }, SendAndReport, m, f, t);
    }
    public void Write (string s) {
        ContentWriter((BinaryWriter w) => {
            w.Write(s);
        });
    }
    public void Write (List<string> l) {
        ContentWriter((BinaryWriter w) => {
            w.Write(l.Count);
            foreach (var s in l)
                w.Write(s);
        });
    }
    public void Write(List<string> l, Action<string> m, Action<float> f, CancellationToken c) {
        ContentWriter((BinaryWriter w) => {
            w.Write(l.Count);
            for (int i = 0; i < l.Count && !c.IsCancellationRequested; i++)
                w.Write(l[i]);
            if (c.IsCancellationRequested)
                return;
        }, SendAndReport, m, f, c);
    }
    public void Write (List<int> l) {
        ContentWriter((BinaryWriter w) => {
            w.Write(l.Count);
            for (int i = 0; i < l.Count; i++)
                w.Write(l[i]);
        });
    }
    public void Write (byte[] b) {
        ContentWriter((BinaryWriter w) => {
            w.Write(b.Length);
            w.Write(b);
        });
    }
    public void Write (int n) {
        ContentWriter((BinaryWriter w) => {
            w.Write(n);
        });
    }

    public string ReadString () {
        string result = default(string);
        ContentReader((BinaryReader r) => {
            result = r.ReadString();
        });
        return result;
    }

    /***************************************************
    **  WARNING!  If you call this function from any  **
    **  Editor-space script, IEnumerable will be      **
    **  unreliable for this object. (Thanks, Unity).  **
    ***************************************************/
    public string[] ReadStringArray() {
        string[] result = null;
        ContentReader((BinaryReader r) => {
            var count = r.ReadInt32();
            result = new string[count];
            for (int i = 0; i < count; i++)
                result[i] = r.ReadString();
        });
        return result;
    }
    public int[] ReadIntArray() {
        int[] result = null;
        ContentReader((BinaryReader r) => {
            var count = r.ReadInt32();
            result = new int[count];
            for (int i = 0; i < count; i++)
                result[i] = r.ReadInt32();
        });
        return result;
    }

    public byte[] ReadBytes() {
        byte[] result = null;
        ContentReader((BinaryReader r) => {
            var numBytes = r.ReadInt32();
            result = r.ReadBytes(numBytes);
        });
        return result;
    }
    public int ReadInt32() {
        int result = default(int);
        ContentReader((BinaryReader r) => {
            result = r.ReadInt32();
        });
        return result;
    }

    public void Dispose() => Dispose(true);

    private bool disposed;
    protected virtual void Dispose (bool disposing) {
        if (!disposed) {
            ((IDisposable) binaryWriter).Dispose();
            ((IDisposable) binaryReader).Dispose();
            ((IDisposable) algorithm   ).Dispose();
            disposed = true;
        }
    }
}
}
