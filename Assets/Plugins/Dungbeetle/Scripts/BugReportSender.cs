using System;
using System.Threading;

namespace Dungbeetle {
public class BugReportSender {

    public static void Send (byte[] screenshot, string description, string email, string sceneName, string buildName, Action<string, bool, string> bugProgressMessageHandler, Action<float, bool, string> bugProgressHandler, Action<float, bool, string> attachmentsProgressHandler, CancellationToken cancellationToken, Action<string, bool, string> resultHandler, params string[] attachments) {

        // 'screenshot' should be a png image.
        //   A reduced size is recommended.
        //   Refer to 'ScreenshotSource.bytesShrunk' for a good default implementation.

#if DUNGBEETLE_DLL
        var bug = new BugReport((int) ReporterConnectionConfig.ProjectId, email, sceneName, buildName, description, attachments);
#else
        var bug = new BugReport((int) ReporterConnectionConfig.ProjectId, email, sceneName, string.IsNullOrEmpty(buildName) ? BuildtimeInfo.BuildName() : buildName, description, attachments);
#endif

        bool socketFailed    = default(bool);
        string socketFailMsg = default(string);
        string resultMsg     = default(string);

        Action<string> localProgressMessageHandler = (string message) => {
            bugProgressMessageHandler(message, socketFailed, socketFailMsg);
        };
        Action<float> localProgressHandler = (float progress) => {
            bugProgressHandler(progress, socketFailed, socketFailMsg);
        };
        Action<float> localAttachmentsHandler = (float progress) => {
            attachmentsProgressHandler(progress, socketFailed, socketFailMsg);
        };

        ThreadStart t = () => {
            try {
                ReporterProtocol.SendNewBug(bug, screenshot, localProgressMessageHandler, localProgressHandler, localAttachmentsHandler, cancellationToken, ref resultMsg, ref socketFailed, ref socketFailMsg);
            }
            finally {
                resultHandler(resultMsg, socketFailed, socketFailMsg);
            }
        };
        var thread = new Thread(t);
        thread.Start();
    }
}
}
