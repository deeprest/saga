// This script is a major modification of http://wiki.unity3d.com/index.php?title=TextureScale

// Only works on ARGB32, RGB24 and Alpha8 textures that are marked readable

using System.Threading;

using UnityEngine;

namespace Dungbeetle {
public interface TextureScaleProgressListener {
    void Progress (float s);
}

public class TextureScale {

    private class ThreadData {
        public int  start;
        public int  end;
        public bool report;

        public ThreadData (int s, int e, bool r) {
            this.start  = s;
            this.end    = e;
            this.report = r;
        }
    }

    private static bool      outOfMemory;
    private static Color[]   texColors, newColors;
    private static Texture2D texSource;
    private static float     ratioX, ratioY;
    private static int       w, w2, h2, finishCount;

    private static Mutex     mutex;

    private static TextureScaleProgressListener listener;

    public static void Bilinear (Texture2D tex, int newWidth, int newHeight) {
        Bilinear (tex, newWidth, newHeight, null);
    }

    public static void Bilinear (Texture2D tex, Texture2D result, TextureScaleProgressListener l) {
        TextureScale.listener = l;
        ThreadedScale (tex, result.width, result.height, result);
    }

    public static void Bilinear (Texture2D tex, int newWidth, int newHeight, TextureScaleProgressListener l) {
        TextureScale.listener = l;
        ThreadedScale (tex, newWidth, newHeight, tex);
    }

    private static void ThreadedScale (Texture2D tex, int newWidth, int newHeight, Texture2D result) {
        if (TextureScale.mutex == null)
            TextureScale.mutex = new Mutex(false);
        try {
            TextureScale.texColors = tex.GetPixels();
            TextureScale.outOfMemory = false;
        }
        catch (System.OutOfMemoryException) {
            TextureScale.texSource = tex;
            TextureScale.outOfMemory = true;
        }
        TextureScale.newColors = new Color[newWidth * newHeight];
        TextureScale.ratioX = 1.0f / (((float) newWidth ) / (float)(tex.width -1));
        TextureScale.ratioY = 1.0f / (((float) newHeight) / (float)(tex.height-1));
        TextureScale.w = tex.width;
        TextureScale.w2 = newWidth;
        TextureScale.h2 = newHeight;
        TextureScale.finishCount = 0;

        var cores = Mathf.Min(SystemInfo.processorCount, newHeight);
        var slice = newHeight / cores;

        if (cores > 1 && !outOfMemory) {
            ThreadData threadData;
            int i = 0;
            for (; i < cores-1; i++) {
                threadData = new ThreadData(slice*i, slice*(i+1), false);
                var thread = new Thread (BilinearScale);
                thread.Start(threadData);
            }
            threadData = new ThreadData(slice*i, newHeight, true);
            BilinearScale (threadData);
            while (TextureScale.finishCount < cores)
                Thread.Sleep(1);
        }
        else {
            var threadData = new ThreadData (0, newHeight, true);
            BilinearScale (threadData);
        }

        if (tex == result)
            result.Resize(newWidth, newHeight);

        result.SetPixels(newColors);
        result.Apply();

        texColors = null;
        newColors = null;
        texSource = null;
    }

    private static void BilinearScale (object threadData) {
        BilinearScale ((ThreadData) threadData);
    }
    private static void BilinearScale (ThreadData threadData) {
        for (int y = threadData.start; y < threadData.end; y++) {
            var yFloor = Mathf.Floor(y * ratioY);
            var y1     = yFloor * w;
            var y2     = (yFloor+1) * w;

            for (int x = 0; x < w2; x++) {
                var xFloor = Mathf.Floor(x * ratioX);
                var xLerp  = x * ratioX-xFloor;

                if (outOfMemory)
                    TextureScale.newColors[(int) (y * w2 + x)] = texSource.GetPixelBilinear((x + 0.5f) / w2, (y + 0.5f) / h2);
                else
                    TextureScale.newColors[(int) (y * w2 + x)] = ColorLerpUnclamped(ColorLerpUnclamped(texColors[(int) (y1 + xFloor)], texColors[(int) (y1 + xFloor+1)], xLerp),
                                                                                    ColorLerpUnclamped(texColors[(int) (y2 + xFloor)], texColors[(int) (y2 + xFloor+1)], xLerp),
                                                                                    y*ratioY-yFloor);
            }

            if (threadData.report) {
                var d = y - threadData.start;
                if (listener != null)
                    listener.Progress (d / (threadData.end - threadData.start));
            }
        }

        mutex.WaitOne();
        finishCount++;
        mutex.ReleaseMutex();
    }

    private static Color ColorLerpUnclamped (Color c1, Color c2, float value) =>
        new Color (c1.r + (c2.r - c1.r)*value,
                   c1.g + (c2.g - c1.g)*value,
                   c1.b + (c2.b - c1.b)*value,
                   c1.a + (c2.a - c1.a)*value );
}
}
