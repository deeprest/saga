using UnityEngine;

using System;
using System.IO;

namespace Dungbeetle {
public static class LogReader {

    private class ReadOperationData {
        public LogHandler logHandler;
        public byte[] buffer;
        public FileStream fs;
    }

    public static void LogAsString (LogHandler f, bool @short = false) {
        var fileLength = new FileInfo(LogPath()).Length;
        var contentLength = @short ?
                    (1<<16)-1 > fileLength ? fileLength : (1<<16)-1:   // 64KiB for editor convenience
                    (1<<24)-1 > fileLength ? fileLength : (1<<24)-1;   // 16MiB is the max size of MEDIUMTEXT in MySQL

        var opData = new ReadOperationData();
        opData.logHandler = f;
        opData.buffer = new byte[contentLength];
        opData.fs = new FileStream(LogPath(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite, (int) contentLength, true); // FileShare.ReadWrite prevents access violations. Reasons unknown.

#if UNITY_EDITOR
        if (fileLength != contentLength)
#else
        if (@short && fileLength != contentLength)
#endif
            opData.fs.Seek(-contentLength, SeekOrigin.End);

        opData.fs.BeginRead(opData.buffer, 0, (int) contentLength, EndReadLog, opData);
    }

    public static void EndReadLog(IAsyncResult asyncResult) {
        var opData = (ReadOperationData) asyncResult.AsyncState;
        opData.fs.EndRead(asyncResult);
        opData.logHandler(System.Text.Encoding.UTF8.GetString(new MemoryStream(opData.buffer).ToArray()));
    }

    public static string LogPath() {
        if (Application.isEditor) {
            if (Application.platform == RuntimePlatform.WindowsEditor)
                return $"{Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData)}/Unity/Editor/Editor.log";
            else // Assume OSX
                return $"{Environment.GetFolderPath(Environment.SpecialFolder.Personal)}/Library/Logs/Unity/Editor.log";
        }
        else {
            if (Application.platform == RuntimePlatform.WindowsPlayer)
                return $"{Application.persistentDataPath}/output_log.txt";
            else if (Application.platform == RuntimePlatform.OSXPlayer)
                return $"{Environment.GetFolderPath(Environment.SpecialFolder.Personal)}/Library/Logs/Unity/Player.log";
            else if (Application.platform == RuntimePlatform.PS4)
                return "/app0/debug.log";
            else // Assume Linux
                return $"{Application.persistentDataPath}/Player.log";
        }
    }

    public static bool CanFindLogFile() {
        var p    = Application.platform;
        var wp   = RuntimePlatform.WindowsPlayer;
        var osxp = RuntimePlatform.OSXPlayer;
        return Application.isEditor && p != wp && p != osxp || File.Exists(LogPath()); // Shortcut to false if linux editor
    }

    public delegate void LogHandler (string log);

}
}
