using System.IO;

namespace Dungbeetle {
public static class BinaryWriterExtension {
    public static void WriteStringBytesLength (this BinaryWriter extendee, int value) {
        var num = (uint) value;
        while (num >= 128) {
            extendee.Write((byte) ((num | 128) & 255));
            num >>= 7;
        }
        extendee.Write((byte) num);
    }
}
}
