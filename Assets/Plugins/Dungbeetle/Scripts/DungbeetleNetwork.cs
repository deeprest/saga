using System;
using System.IO;
using System.Net.Sockets;

namespace Dungbeetle {
public class DungbeetleNetwork : CryptoStreamManager {

    public NetworkStream stream;

    public DungbeetleNetwork (NetworkStream strm, BinaryReader clrReader, BinaryWriter clrWriter, byte[] symKey, IOutput log) : base(strm, clrReader, clrWriter, symKey, log) {
        stream = strm;
    }

    public new void Dispose() {
        Dispose(true);
    }

    private bool disposed;
    protected override void Dispose(bool disposing) {
        if (!disposed) {
            ((IDisposable) stream).Dispose();
            base.Dispose(disposing);
            disposed = true;
        }
    }
}
}
