
# Saga (working title)
A game prototype. This is mostly a Mega Man X gameplay clone with some additions. Made in Unity.


## Major changes to the Mega Man X formula
1. Player can aim instead of only shooting forward.
1. Arbitrary ground and wall angles
1. Projectiles collide with walls by default
1. platforms
1. front doors in addition to side doors
1. No instant-death (spikes or bottomless pits)
1. Enemies respawn at visible and known locations, not invisible static points.
1. Replace teleportation with airdrop from chopper.


## Requirements

**Unity version: 2019.4.18f1**

[Install Git LFS](https://help.github.com/en/github/managing-large-files/installing-git-large-file-storage)
After checkout, you must run the following to get all of the assets:

    git lfs pull


### Unity Plugins
* [NavMeshComponents](https://github.com/Unity-Technologies/NavMeshComponents)
* [untiy-scene-reference](https://github.com/starikcetin/unity-scene-reference) [(JohannesMP's original gist here)](https://gist.github.com/JohannesMP/ec7d3f0bcf167dab3d0d3bb480e0e07b)
* [UnityLitJson](https://github.com/Mervill/UnityLitJson)
* l18N
* [unity-aseprite-importer](https://github.com/martinhodler/unity-aseprite-importer) (Editor)
* [Unity-BitmapFontImporter](https://github.com/litefeel/Unity-BitmapFontImporter) (Editor)



