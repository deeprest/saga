START A DEVLOG ON ITCH

+ sprite animations: using state machine.
+ replace old sprite anims with unity animators;
+ replace unity animators with sprite library/resolver

+ composite sprite characters.

+ normal maps, so cannot use sprite atlas.

+ Limited use of 2d rigidbodies (bouncygrenade, pickups, stickybomb).

+ 2D lighting

+ new input system. gamepad, linux.

+ customutility: build, multitool, font generator

+ turrets. only aim when target is visible

+ remove pathfinding from lift, just use points/radius

+ control rebinding

+ lifts

+ aiming: cursor look; minimum cursorDelta; cursor off and no shot when under min. Directional aiming scheme when using gamepad

+ talkers and dialogue box

+ marching square nodes

+ camera zones

+ parallax in editor

+ cursor locking. on pause menu, diegetic, app focus change

+ scene list; scene references, scene triggers

+ UI Screens

+ AudioLoops

