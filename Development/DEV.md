# Remember
- Script execution order is the reason liftbots are updated first in the entites list, because they are added to the list on Awake().
- FIXED Linux: glitchy/wrong sprite shader for entities. This is because of texture compression settings on each individual texture.
- Composite sprite render layers. Create a layer for each composite, or how to keep entire composites from interlacing with one another? 
	SOLUTION: Animate a *struct* int for each piece, and update the sortingOrder of the SpriteRenderers from LateUpdate().
- Pixel-perfect animations must be done in Unity for each piece. Snap to pixel density, use constant anim curves.
- 2d skeleton rigs, avatars, and IK can not be used with composites. Skeletons use rotations, and these anims are not rotation-based.
- NOFIX CODE the build number is incorrect in builds. [The build must be made after the relevant commit.]
- Physics2D hits propagate up to the object with a Rigidbody2D. In TakeDamage() the child entity that received the hit
will receive the damage and the owner can react.  Without a Rigidbody2D, in TakeDamage() the GetComponent() call only 
returns the Entity on the collider's GameObject or null. 
- NOFIX parallax in editor only updates when moving a transform. [Just keep the parallax object selected to get updates.]


# Done (next: CHANGELOG)
+ improved crush detection


# Issues
- walljump into smaller spaces
- camera lerp when captain smirk says "im selling these fine leather jackets"
- player gets stuck to wall without input when there's top+side collision

## Needs Work

- stickybomb, explosion
  - stickybomb only does area damage when not stuck to an entity; it only damages the object its stuck to.
  - stickybombs fail to damage sphere turrets.
  
- graphook
  - graphook: cancelling before it hits wall will incorrectly give ava velocity.
  
- CODE Assess need for destructible layer. Firing from within wall.
- CODE remove global references to CurrentPlayer.
- CODE change cursorOuter based on control device. Test with gamepad.
- CODE Character agentType: remove need for Global.instance.AgentType lookup. Just use ints.
  
- hornet with rockets (guided?)

- FIX jumping and sliding up and over a ledge only **sometimes** snaps to ground
- victory sequence needs to follow player position continuously.
- implicit wall grab when liftbot moves away while player is standing next to lift. When standing on the side of a carryobject, it pulls you along.
- full weapon/ability drop upon death
- while victory sequence is playing, pausing an unpausing can mess up the sequence timing.
- gamepad: when not aiming, default aim direction.
- gamepad: graphook-trigger behaviour. sometimes double.
- back arm disappers when pause menu is showing.
- fix the MI style dialogue font. Q's are weird.
- mech clips through wall with tile collision. maybe mech needs a better boxcast while doing a punch-dash.
- flame mammoth stage: move turret. turret vertical, camera size makes situation unfair
- flame mammoth stage: in boss chamber, make airbot wait until music stops before attacking
- feature_shadows scene is borked <- are we using shadows?

  
# New Things

- Music: "now playing" and attribution with link to artist's chosen page.
- Icons: for weapons in a rotary/cycle
- aiming/projectile path for all weapons.
- jetpack
- laser for laser-guided rockets.
- Ability to dash under low clearance. Requires crouch. Cannot run, only dash while crouched.
- Pause for a moment when dying. 


## The Cut (stuff for the next iteration)
- NEW cheats menu... (there are already console commands. Is this necessary?)
- NEW ability to hang/cling
- NEW zip line
+ NEW ladders / climbables. Still very rough.
- NEW water. low gravity. shader with slight refraction. splash.

#### Polish
- POLISH dash vertical offset on incline
- POLISH shooting arm is on a back layer while moving through MMX style doors
- POLISH when speech UI is activated, have the UI width take a second to tween open 
- POLISH make UI speech box options like Iconoclasts
- POLISH the audio falloff for bipeds seems nonexistent
- POLISH landing snaps. possible anim frame adjustment will fix
- POLISH adjust jump anim frame. it pops at the jump arc apex
- POLISH control name text for right stick says "RS"
- POLISH add low health breathing anim
- POLISH Hornet particles should not rotate, and should disappear
- POLISH NEW player death effect/anim

## Unity Bugs
- UNITY bug: [MACOS] cursor movement in last direction when shoot (Fixed in version 2021?)
- UNITY bug: [WEBGL] webgl audio loop
- ENGINE/OS abort signal on exit leads to crash (no recent repro)


## Unity Assets that may be helpful
- IntroLoop
- Shadows: https://assetstore.unity.com/packages/tools/particles-effects/2ddl-pro-2d-dynamic-lights-and-shadows-25933


# Notes

* Cancelled: Enable/Disable Lighting Option.
  Too much is involved to get this right, and it does not seem worth the cost.
    1. Swapping all lit shaders, even on instanced materials, with 
    2. in prefabs and in scenes.
    3. Maintain a list of shaders in global prefab
    4. Some lights are driven by animations/scripts, so we cannot disabled the components (overhead is small though)
    5. Parallax/background layers need to use "Tint" to look right. A list of tints would need to be maintained.
    6. Appropriate replacements for each and every light would need to be made and maintained (sprite cookies).
  

* Dialogue 
  Decided to use string identifiers instead of enum values. 
  Adding or removing enum values is not tracked by the Editor (as fragile as C enums), 
  and Unity Events do not support enum values as parameters (UnityEvents2.0 plugin is possibility). 
  Thought of having a master list of string identifiers, but it would only create another layer of potential mismatches. 
  Also tried using the Editor with the dialogue structures as Scripted Objects, but it makes the YAML much less clean, 
  and would require anyone editing (such as translators) to have the Unity Editor, which is not ideal. 
  Also, YAML comments are not supported in Unity's serialized assets. 
