
# THANK YOU!
"jbarrios" for creating a feedback video on TIGSource feedback forum.
MET for his awesome music.
https://etclundberg.itch.io/ for early feedback
https://gist.github.com/JohannesMP for SceneReference and others.
https://www.youtube.com/channel/UCGVJF2RcWjORfbZtlWvh93w Thank you Lotte.
Monkey Island sprites ripped by Ultimecia
**FF6 sprite variations acknowledgements**
["ZSaladin"](https://github.com/zsaladin/EssentialEditor) for the EssentialEditor inspector script




# Other X Fan Games
["Mega Man X: Corrupted"](http://www.megamanxcorrupted.com/)
["Megaman X: Isolation"](https://www.youtube.com/channel/UC2CSkCJMni7CuRLLmEGpKSw)
["Cancelled/Dead Mega Man X Fangames"](https://www.youtube.com/watch?v=PB8pMBSK8AU)
["Mega Man X Elf Wars (aiming)"](https://youtu.be/xGahhqoooT0?t=109)
["Mega Man X++"](https://www.youtube.com/watch?v=twI3res-obs)
["Apsel Haven Mega Man X fangame"](https://www.youtube.com/watch?v=CwW_cziXs4U)
["Mega Man X AD"](https://reploidsoft.blogspot.com/)
["Mega Man: Assimilation"](https://www.youtube.com/watch?v=y_18Gljz_ec)

# Other Games
["20XX"](https://store.steampowered.com/app/322110/20XX/)
["gunvolt"]()


# where to release/announce demo
When you have a good build, tell these folks:
https://nathanielhoover.weebly.com/  (Flashman85; made the list [here](https://sprites-inc.co.uk/thread-1505-page-8.html))
https://dennisengelhard.com/
look for somecallmejonny sp? on youtube
gamejolt
sprites.co.uk forums
pixeljoint or tigsource (to find artist for full project)
# distribution platforms
itch.io
GOG

# announcements
itch.io
tigsource forums
indiegamesplus
pixelprospector
gamejolt
reddit
twitter
