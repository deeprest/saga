
## Mega Man X Breakdown
*Demonstrates enemy abilities in a safe, controlled environment first.*
Intro level teaches everything you need to know:
* aiming. use cursor to indicate direction etc
* run and jump. basic movement. implicit goal of progression
* take damage. vulnerability
* death cycle
* shoot. a way to overcome vulnerability
* kill enemies. defeat obstacles to progress
* rewards, minibosses
* enemy sweet spots
* arrange for wallslide to teach walljump out of pit
* Vile: you cannot win, saved by zero (demos charge shot and dash)
* Theme: grow stronger / self improvement

    ### Opinion
    Megaman X1-X3 has legacy issues that can be improved with modern techniques: small viewing area, can only shoot forward, enemies respawn immediately out of sight, levels are static.


MMX 4:3 screen ratio
Chamber = 10.5 player widths X 6.5 player heights

16:10 screen ratio
Chamber = 12.5 player widths X 6.5 player heights




- ducking
- diagonal aiming
- zoom camera
- rotating camera
- rotating sprites
- subpixel idle anim (breathing)
- 3-frame turn around anim
- one-way platforms  (Super Mario 3, Contra)
- background doors



You can dash, wall-jump, wall-slide, charge your shots, fight bosses, and collect weapons (Mega Man X)
but you can shoot all directions (Abuse)
and freely explore the world (Metroid)
which is partially generated (Rogue-lite)
and interact/talk with people (RPGs)
and there are one-way platforms and background doors (Super Mario)
---
and you can get allies that follow you around and help (Shadowrunner)
You're airlifted by a helicopter (Contra)

pathfinding
lighting (cosmetic)
