# Saga Design 

## Overview

### Established design decisions:
* 2D gameplay. All interaction happens on a plane. Anything not on the plane of interaction should not be important to gameplay.
* Platformer-style movement. This is an action game. Speed and agility are valued.
* Free-aiming. This classifies the game as a shooter. Controls are consequently less accessible than comparable 2D games, but no more complex than FPS games.
* Big Bosses. 



## Core Mechanics

The Core is combat tactics:  
  * [Defense](#defense) 
    * Movement: evading enemy attacks.
    * "Abilities": use defenses when applicable
    * take cover or retreat when damaged
    * (recovery)
  * [Attack](#attack)
    * Movement: attack position. Ideally this is behind cover.
    * Aim is important
      * Player-relative aiming makes "strafe-shooting" the norm. (move player without moving aim cursor)
    * Shot timing
      * Some enemies are only vulnerable for short periods of time.
      * Hit a moving target while in motion (a skill the player may develop)
    * (Charged shots / weapon alt shot?)
    * (temp powerups?)
  * Boss Strategy
    * exposing enemy vulnerability
    * wait for opportunity
    
## Theme
### What is the theme of this game?
**MMX theme was absorbing enemies' abilities to become stronger.**
#### Ideas: 
* Cannibalize and repurpose enemy parts. 
* Redirect enemy attacks to hurt enemy.



## Principles

#### Accessibility
* Must run smoothly on lower-spec machines [define min spec]
* Single player. No internet connection required to play.
* Control Rebinding, Mouse and Keyboard or Gamepad

#### Does not have overt commercialization
* No ads. You pay with your soul. (fake ads?)
* No microtransactions.
* No in-game currency to purchase.

#### Does not use addictive or abusive mechanics
* Have nothing to compulsively accumulate. (alternatively, make the collectibles useless)
* No achievements to leave you feeling under-achieving.
* No trophies. Your ego is big enough already.
* No Quick Time Events. (Unless used in a parody of button mashing: "Press F", "Oh god don't stop pressing F". Or specifically to make fun of Call of Duty's "Press F to Pay Respects")

#### Not too complex; You have a life
* (hmm) No tedious narrative to ignore (dialogue and presentation, but no narrative). *OR IS THERE?!?!?*
* No complicated technology/skill tree
* The less the player has to "manage", the better.

#### Prefer Diegetic and Minimal Interfaces
* Minimal Screen-Space UI
* Prefer in-world or on-character indicators to bars or numbers. 


# Primary Influences
* Mega Man X (1993) dash, walljump, wallside, charged shot.
* Super Metroid (1994) Open map, non-linear levels.
* Contra; Two-way platforms. Possibly: temporary powerups, going prone.
* Metal Gear; Visibility to enemy. Possibly: communication with team to advance story.
* Abuse (1995) Cursor aiming any direction while running. Game controllers were not supported.

## Major changes to the Mega Man X formula
1. Player can aim in any direction instead of only shooting forward.
1. Arbitrary ground and wall angles.
1. No instant-death spikes or bottomless pits. (no fall damage)
1. Enemies respawn at visible and known locations, not invisible static points.
1. Replace teleportation with airdrop from chopper. (no teleportation)
1. Projectiles collide with walls.
1. Avoid button-mashing with hold-to-fire. This is to save wear-and-tear on game controller triggers, and mouse buttons.
1. Camera Size and Movement.
    * Camera can center on an imaginary point around the player, in the aiming direction. ("cursor influence")
    * Allow for variance in zoom, depending on how cozy the surroundings.
1. Hold dash instead of combo dash+jump to do better walljump. (Improved in MMX2)


### Preserve from MMX games
* special case inertia
* tight controls in air
* vertical jump velocity control
* dash
* wall jump
* wall slide
* charge weapon
* collect weapons from defeated bosses
* safety timer after taking damage
* speed upgrades
* bosses
* lifts

### Additions
* inertia while in-air (this exists in MMX armadillo stage)
* lighting, normal mapped sprites, emissive
* AI: teams, line-of-sight, navigation mesh
* (Possible) level generation

### Cycles
* combat cycle. aware of enemy, visual contact, observe, attack, evade => death or success
* death cycle. possibly: transformation, or respawn, or instant replay.
* stage cycle. HQ to generated level. boss, weapon.

### Notes
* strafe-shooting = removes need for very precise aiming, but timing is still critical.
* swarming enemies with high health = requires escape or wide-radius weapons
* bouncy grenade spread shot = spammy mess, hope for the best
* well-projected enemy shots at long intervals, low health = higher rewards for skill


# Design Decisions

### Will Zones-Loops be used?
  * Benefits
    * Level loading happens seamlessly.
    * Levels do not need artificial boundaries.
    * The world can change dynamically, giving a feeling of anticipation.
  * Costs
    * Everything must consider/support this feature.
      - particle systems
      - pickups
      - Projectiles. Projectiles fired in incoming chunk will (remain) disappear after crossing border.  
      - minimap
      - 'walkers'
    * There is no apparent benefit if the player teleports to different place in the level.
    * Performance hiccups may take development time to smooth out. (Level content optimization)
    * "the train problem": Any object outside of it's parent chunk can potentially "pop" into view.
  * Details
    + The moving bounds that enable zones needs to be significantly larger than viewing area. At least 3x. (DONE)
    + The Nav Mesh must be moved along with focus object. (DONE)
    + Preserve continuity without duplicate objects or multiple cameras. This is done by moving the entire chunk to the new location. (DONE)
  

### (NO) DESIGN (RotationExperiment) Determine if the game will use a perspective camera or not. 
  **If so, fix these issues:**
  - minimap render must go back to ortho
  - any z-fighting
  - backgrounds are flickering, probably due to 2d lighting not expecting perspective camera;
  - ?cursor-outer seems smallish and cannot be changed in menu after changing scenes?
  - FIX world speech does not show
  
### (NO) DESIGN (GLR) Gameplay Layer Render
**What value does add to the experience?**
**Can or should the player make decisions based on what they see in the background?**
  - Is it worth the performance hit?
  - Are there content requirements/changes? If acceptable or minimal, then leave as an option.
  - How to represent cleary on minimap?
  

  
### Other

- DESIGN should pawn inertia add to projectile inertia?
- Should pinch between liftbot and twoway platform push player downward? or cause damage?
- Have narrow vertical shafts? walljumping up narrow shafts now fails too, because of the newer top collision.

* Idea: Use abilities gained from defeating enemies in the stage to defeat the boss.
* (needs an) Easy to Explain Hook (elevator pitch): A Mega Man X inspired shooter with (lighting, sliding, flying, rockets, trains, and chainsaws)... [What is the hook?]
* Is this a Mega Man X clone, remake, or something else?
* Quick Description: "A Run'n'Gun Platformer where one masters weapons to defeat bosses"
* Focus:
  * Defeating Bosses  (learn attacks/"tells", find weak spots)
  * Mastery of Weapons  (use, and configuration/modification?)
  * Mastery of Movement  (dash, wallslide, and abilities)
* Interacting with People and Objects. Examples: Talk, Diegetics, Switches, Lifts
* Exploring / Mapping. Level Design: Bottlenecks and Loops (Thief-style level loops).
* Doom-style fetch-the-key is not much fun
* RPG-style help-the-bystander side quest
* Many smashable things, Ratchet and Clank style.  

- DESIGN ability to automatically break destructibles when pinched by them.
- Bosses/Enemies
  - DESIGN Big wrecking ball bot.
  - DESIGN agile boss bot.
  - DESIGN big boss sequence... chainsaw romance sequence?
  - DESIGN enemy that uses and drops the graphook
- DESIGN chainsaw DPS? Or is damage-per-hit acceptable? Post-damage grace periods prevent the chainsaw from annihilating enemies. 

- DESIGN support reflections for laser?
- DESIGN When reflecting projectiles will cause damage to originator, make it very obvious with color changes and sound.
- DESIGN As projectiles reflect off of walls, make them smaller and the sound pitch higher after each bounce.
- DESIGN weapon heat limit and cooldown, for battle pacing.
- DESIGN align shot with surface when aiming into floor/wall
+ DESIGN stickybomb explosion should break breakable letters
- DESIGN graphook: show valid raycast targets?

- DESIGN Cursor Visibility UX: The cursor can become difficult to locate in a crowded scene.
- DESIGN jumping(without hor move) while auto-wallsliding -> walljump OR jump vertical OR jump away from wall?
- DESIGN shield on back. press down to use shield.
+ DESIGN loading screen
+ DESIGN weapon mod stations?



## Defense

### Evading Attacks
![](design_2dshooters.svg)
A consistently horizontal line of attack increases the ability to predict a hit.
At arbitrary angles, ambiguity changes a simple decision into a more stressful guess.  
It creates a third possibility: unknown.  I imagine that having a clear choice makes either outcome
-success or failure- more satisfying. One approach is to remove the potential for ambiguity by removing the aiming mechanic, but I've already committed to a free-aiming mechanic, so I'll pursue design options in the third case.  
One option is to provide a shield that discards an incoming projectile.  
The player might use it when they cannot avoid being hit, or it might become a higher-priority decision:
"I'll block before I attempt to evade". Evasion is an important mechanic in side-shooters but in this case it would be a secondary option. 
This raises some design questions:
  1. Will characters become damage sponges, relying on armor and shields, or is evasion still the key to success? Less reliance on player skill and more reliance on character stats?
  2. How exactly will the player know which projectiles can be blocked?
  3. How important is projection for incoming enemies? How to give clear warning? Lasers, sounds, turret animations while aiming...
  4. 

### Shields
- DESIGN stickybomb exploding interaction with shield:
  - do raycast to each IDamage within range
  - if raycast hits something else, do a projection of the collider onto a perpendicular vector of the normal
  - use the projected endpoints to do two more raycasts
  - if the raycast hits an IDamage collider, proceed



## Attack

### Aiming

#### Shot timing


## Spam
I want to avoid letting player bullet spam be an effective strategy. This is an action game, so it would diminish the value of improving player skill and reduce gameplay decisions to chance.
Options:
1. Limit ammo. This gives wreckless shooting a consequence, but then finding and rationing ammo becomes an important chore.
1. Limit the shot rate. A longer shot interval would be effective, but greatly increases the importance of shot timing and accuracy;
1. Permit a degree of spam, but keep it limited to preserve other strategies. A cooldown for all weapons is one possibility.
1. Nullify the effectiveness of spam. Maybe some enemies are good at blocking/reflecting projectiles, so aiming precision would be required.
1. Allow spam, but make the consequences prohibitive. Have collateral damage harm progress, or if a weapon overheats then let it be destroyed.


dodge vs dodge = potshots
enemy charge gives player time to move into position
Can only use one ability per each "mount point".
Abilities persist as long as they are installed.
Replacing an ability forces the avatar to drop it.
reflection levels (scatter, normal, directed at instigator)
lift types (simple, elevator, train, etc)
should projectiles inherit instigator inertia?


# Weapons
| WEAPON           | STRENGTH            | EFFECTIVE         | (LIMITATION/WEAKNESS)                              |
|:-----------------|:--------------------|:------------------|:---------------------------------------------------|
| regular shot     | quick               | small enemies     | (limited damage)                                   |
| charged shot     | penetrates          | armour            | (takes time to charge, flies in straight line)     |
| reflect shot     | subversive          | around corners    | (can damage the instigator)                        |
| *shotgun shot*   | multi               | groups            | (short range)                                      |
| stickybomb       | explosion           | armour            | (heavy/gravity, delayed explosion)                 |
| rockets          | guided by reticle   | armour            | (limited manueverablity)                           |
| *seeker missile* | seek target         | flying            | (can be mislead with countermeasures (flares))     |
| laser            | instant             |                   | (must have line of sight)                          |
| chainsaw/melee   | powerful            | armour            | (very short range)                                 |
| *slider/crawler* | follows walls       | static            | (limited direct range)                             |


## projectiles, damage
1. IDamage vs static geometry
2. ignore/take damage
3. blocking hit VS passthrough
4. multiple "hits"
5. overlapping entities
6. reflection: deal damage on each reflection?
7. damage per second of overlap, explosions?


* direct path
* warmup duration
* cooldown duration
* reflected path
* curved path

## Weapon Trade-offs
* speed (slow,med,fast) VS timeout or range
* damage VS count
* damage VS shot velocity
* damage VS spread
* damage VS cooldown
* interval (fire rate) VS cooldown duration
* full auto VS charged secondary
* reflect VS explodes

## Possible Scenarios
* stickybomb: boss retracts a limb into a vulnerable place
* layers of armour. Must peel off layers to find vulnerability.
* bounce grenade: boss hovers above you and has a weak spot on top; you can bounce a grenade off the wall to hit.



## PROJECTILE ATTRIBUTES:
* rate of fire / full auto
* projectile count
* spread
* projectile timeout
* damage per hit
* degree of homing / seek  (slight curve, to full turn around)
* pierce count (hits until projectile is destroyed)
* reflect amount after pierce, before sticky
* sticky amount after bounce
* final explosive amount after stick/bounce
* charge variant

## RAYCAST ATTRIBUTES:
* duration until cool down
* damage per second
* (removed) pierce count (hits until raycast is stopped)
* reflect count; 
* amount of decay per reflect

## MELEE ATTRIBUTES:
* damage per hit 
* damage per second



# Abilities
| ABILITY         | STRENGTH         | EFFECTIVE   | (LIMITATION/WEAKNESS)                           |
|:----------------|:-----------------|:------------|:------------------------------------------------|
| jets/propellar  | flight           |             | (less agile in midair)                          |
| graphook        | quick, tether    |             | (will shock player if hits a live wire?)        |
| shield          | absorbs,reflects |             | (only blocks weaker attacks, absorb has limits) |
| slow-motion     | time dilation    |             | (cannot use other abilities at same time)       |
| *super-jump*    | dodge, mobility  |             | might jump into danger                          |




# Gameplay Elements

## Environment
+ entry (chopdrop)
+ angled floors (dash)
+ walls (can jump up or slide down)
+ side doors (regular doors)
+ background doors (move to a "background" area) (warp doors)
+ simple destructibles (boxes, vents, windows), glass types (brittle, requires charge, requires explosive)
+ hidden areas. (false cover)
+ "two-way" platforms (the type you can choose to fall through) Should pinch between liftbot and twoway platform push player downward? or cause damage?
+ moving platforms "lifts" (simple, elevator, train, etc)
+ camera zones
- climbable background areas. rough-prototype ladders are implemented.
* glass types (brittle, requires charge, requires explosive)


## Interact
+ switches (lights, lifts)
+ diegetic interfaces
+ talk with NPCs

## Enemies
*Enemies have LOS/blind spots and weak spots*
- [moves along a track, like liftbot] (spikes, blades, something deadly on a track)
+ wheelbot; movement: rolls on the ground in one direction, changes direction when it hits a wall
- [seeks player along the ground] (crawling mines??)
+ airbot; movement: seeks in air
+ turret; shoots (line of sight) Projectiles and Raycast
- [track movement + gun]  <= miniboss
- [seeks on ground + gun]  <= miniboss
- [seeks in air + gun]  <= Hornet mini-boss

# Bosses

+ boss dark opening: auto run away from light, ambient light fades out, showing only emissive. (Similar to Spark Mandrill or X2 intro boss)
+ Hornet mini-boss; seeks player, while shooting (line of sight or mounted cannon), and dropping wheelbots
+ boss: ride armor or quickboss prototype
+ boss: giant in the background ("you're a wall now!"), like sigma, but with wrecking ball


## Weapons
+ reflection types (scatter, normal, directed at instigator)
+ buster (variants: rapidfire, spread)
+ bouncy (charge for multi)
+ sticky bombs
+ chainsaw
+ laser beam

## Abilities
+ propellar (flight)
+ graphook
+ shield (directional, does not cover full body)
+ slowmo


## Locations / Scenes
+ Headquarters
+ City Highway + Generated Buildings
+ Generated City


## Boss situational awareness
when boss has low health
has line of sight to player
distance to player
is in specific areas of chamber
if player:
has shield up
has recently shot
is charging shot
is dashing
is running
is jumping
is on wall


## Combat
dodge side attack with jump
dodge aerial attack with dash
block
reflect
(absorb)


# Other Thoughts

### Player perspective *(what does the player need to remember)*
1. primary weapon. xbuster, bounce grenade, sticky bomb, chainsaw, trishot, reflected shot, laser
1. primary weapon can charge. charged form / charged blast
1. secondary "ability". graphook, shield, airbot propeller
1. ? Weapon limit. infinite, multiple, single(Contra).
1. ? maybe the current ability modifies current weapon?
1. ? Abilities: persistent, temporary, limited. Ex: persistent shields, temporary powerup, limited ammo.


### Level Ideas
0. Onboarding:
   Start with just a head and cursor. Hovering over some things in the scene trigger the next step.
   Gain an arm to shoot at the cursor. Shoot something to trigger the next step.
   Get legs, and enable left/right movement.  Move into position and shoot a trigger to activate the next step.
   Now you can jump.  Jump and shoot a trigger for the next step.
   Now maybe something shoots at you! Take damage if hit, but you cannot die.
   You are now provided with health pickups.
   Learn dash to better avoid enemy turret projectiles. Shooting the turret moves to next step.
1. Long Fall. Take a long elevator up (or chopdrop to top of building) and end up falling all the way back.
2. Large Moving Vehicle. Large tank, train, airship.
3. Chase sequence.  Speeder bikes, fighter jets.
4. Riding on Cars! city traffic can hit you, and you can ride on top.


### Possible Scenarios
* A boss cycle: Talk -> Find Boss -> lose, if not prepared -> talk -> find weapon -> practice using weapon -> attempt Boss again.
* A merchant/clerk attempts a HARD sell. Starts subtle, gets a little more agressive, attempts blackmail, threatens you, begs while crying, then finally locks all the doors and fights you. Either way, in the end you buy the thing. Item could be a leather jacket, etc.



### Death possibilities
1. Respawn
2. Parts. reclaim parts
3. Gears. Health can be reclaimed from dropped "gears" ala Sonic. Respawn if empty.
4. Instant Replay. Go back before death and replay with a few seconds leeway.
- Instant Replay: Playback system, use for death cycle:  go back in time to last grace period, play a couple seconds, and have a countdown,
  then let the player resume where they were at that point. If they die within 3 seconds, rewind even further to grace period before that.
5. Tiny




# DESIGN GRAVEYARD

#### spiderbot
collect parts: legs, arms, head
biped body assembly. Get to safety to rebuild?
collecting parts: automatic by proximity. show parts on bot.

#### body parts enable abilities. recollect on death.
build weapons onto biped arm slots
find body upgrades: run speed, jump height, etc

#### pickle rick / tiny when damaged
