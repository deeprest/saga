# Design Practice


# Lens
1. Emotion
2. Essential Experience
3. Venue
4. Surprise
5. Fun
6. Curiosity
7. Endogenous Value
8. Problem Solving
9. Elemental Tetrad
10. Holographic Design
11. Unification
12. Resonance
13. Infinite Inspiration
14. Problem Statement



# Eskil Steenburg's Pivot Model
- https://www.youtube.com/watch?v=xsplKRCL7dA
- sandbox seems like full of possibility
- But real life is dull / dangerous
- experience of going to movie seems more important/relevant than the story within the movie.
- Story of the gameplay is important, the story in the game is not.
- example: sports, such as football.
---
Mechanics, Goal, Acceptable Failure
1. mechanics: toy
2. add a goal: puzzle/challenge
3. acceptable failure.
    * uncertainty
    * prevents planning
- player can distinguish between mechanics and acceptable failure
- player will create contingency plans
- player must feel it's 'fair': AI. Chance. Twitch skills. Complexity.
- Complexity: physics are usually, but not always predictable
---
Cheap satisfaction
but what about the story shown during marketing?
Story-ettes 'nuggets'
4. Situational pivots
"Add Then"
football: start at 0-0. team scores! other team scores! etc
How you get there is important, not the end.
---
player aware of potential changes to situation; prepares contingencies.
multiple situations; one is more advantageous at any given time.
Sacrifice [abilities] to achieve desired situational state
Changing of the situation is something the player can affect.
Examples:
1. Monopoly: There's a Bull market and Bear market, and landing on properties might pay out to the player that lands on them. Then allow the players to somehow flip the market to their advantage. This changes the game story of the game from "Bill acquired properties and then got more later after making deals" to something much more dramatic.
2. Formula One racing: Rain changes advantages based on tire selection, or sacrificing time during a pit stop to change to more advantageous tires. If the rain could be turned on/off by the players multiple times during race, and especially if a sacrifice (of fuel or something) was required, it would create a much more dynamic and player-altered dramatic tension.
4. player needs: zombie game shifts from killing zombies to finding bandages to stop bleeding. A more interesting mechanic would be if the player WANTED to bleed because it became advantageous.
## More Examples:
Magic. pivot on the effectiveness of categories of magic.
Faction control. Unique consequences to different factions controlling the field.
Submarine warfare: slow and silent VS fast and loud. No purely-advantageous states, there is a tradeoff in each decision.
* Level change
* flooding the world
* mechanics change in light VS darkness




# DESIGN pillars (from gdu.io)
1. Mantra:  "You master weapons to defeat bosses."
1. Design pillars, describing *feel* (three phrases):  Nimbly pimbly. Upgrade! Irreverent Distractions.
2. Summary. Talk to people to learn the secrets of weapon mastery, to defeat evil robots and save the city.
3. Features: aiming, many weapons, challenging bosses, procedurally generated city
4. Target platforms: Desktop: Linux, MacOS, etc
5. Target Audience: Any fans of MMX-style platformers that want to see the gameplay evolve somewhat.
6. Game Details (see below)
7. Interface, Controls: m+k, gamepads
8. Artstyle: Pixel art with rotation. Composite sprites with lighting, normal+emissive maps.
9. Sound / Music: rock soundtrack + instrument foley
10. [Development plan]




# Are You Working In A Feature Factory?
https://gdu.io/blog/are-you-working-in-a-feature-factory/ 
1. Find the right metric
Many people do not measure their progress at all. That is why they end up in this deep dark pit of game development where nothing gets done and they’ve been working on a project for two years or so. And before they know it, they have this giant thing that’s not even a full working game but it has all these cool features that are confusing to use and it’s just a nightmare. Therefore, number ONE is picking the right metric.
2. Focus on OUTCOME not OUTPUT
If your output is more important than your outcome, you’ll never get anywhere. I think the outcome for any indie developer is probably the vertical slice, the initial prototype, or the MVP of the game. You want to get a playable game as soon as possible that’s the outcome. Everything that you do should be focused on the outcome, not your output for that day, week, or month. It’s not how long you worked at it.
3. Avoid shiny objects
The third advice I’d like to give is avoid the shiny object syndrome. It’s still related to what I mentioned above wherein you would come up with these ideas and then all of a sudden just go off on a tangent, like “Oh that’s so cool! I want to build that thing!” The shiny object syndrome is when you would get all excited about new features and just abandon the old one in pursuit of the new ones.  Sometimes you would abandon entire games, just like that because you can’t wait to start on a new one. You would just hop from shiny object to shiny object and that is the number one killer of all indie games. And if this happens, so many mistakes could happen in your game, you lose motivation and may just end up hating or quitting your project. You don’t want that to happen.



## GDC Hitchhiker's Guide to Rapid prototypes
Minimum Viable product.
### SMART goals
1. Specific: precise description of what is to be accomplished.
2. Measurable: progress is visible and tracked.
3. Attainable: what actions are needed to make it happen.
4. Relevant: meaningful and realistic.
5. Time-Bound: estimated time for completion.
### Strategy
Carefully designed plan of action. Accounts for:
* Deliverables
* Features
* Functions
* Tasks
* Deadlines
* Costs
**Effectively Scoped**
### Scope
Recognize Barriers.
Bad scope can come from your Ego.
Humility can help feel out your internal barriers:
* Attitude
* Emotions
* Understanding
* Creativity
* Skill
Humility will spot external barriers:
* Time
* Resources
* Location
* Opportunity
* Competition
"Start where you are. Use what you have. Do what you can." - Arthur Ashe
### Vision
Two parts: Game Concept and Design. Focus more on the design iteration.
Core Mechanic. Secondary Mechanics. progression. Narrative.


## Design Advice (from unknown youtube video)
Must: Onboard player with forced-learning mechanics.
Must: Polish lots.
Avoid: Starting Too BIG.
Avoid: Idea commitment.
Avoid: Story up front.
Avoid: Overly rigid design.
Avoid: Arbitrarily adding things.


### From GDU, design mistakes
* (experiment) prototype first instead of design-up-front
* (vision) do not follow trends
* (escapism) avoid making decisions based on what's realistic
* (ease of use) UI / UX, onboarding
* inconsistent design.. (recognizable patterns)
* not having a clear vision or mantra
* ignoring feedback
* not valuing feedback
