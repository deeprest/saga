# Design Analysis


# Megaman

1987 consoles (NES)
Keiji Inafune
Meinami Matsune
Akira Kitamura
Takashi Nishiyama
## Story
- Dr Light and Wily are visually based on Santa Clause and Einstein.
- Rock and Roll are like children to Dr Light and and work as servants, but in Japanese culture hard work is a virtue.
- Wily is jealous of Light and modifies the Robot Masters.
- Absorb Robot Master powers via their 'cores'.
- Mega Man is based on Astro Man, who is based on pinocchio.
- These stories are about a man that builds a son that goes on to have adventures, from which he both grows as a character and establishes his own identity.
- In Japanese version, growth is the defining feature of MM. In America, he's already powerful.
- The story as delivered mostly through external media: interviews, advertisements, magazine.
## Gameplay
1. Level Select Screen: player control over progression.
2. Covert Tutorials: Always let the player experiment with new elements in safe environment first.
3. Introduce two easy enemies/obstacles independently then combine them later.
4. Level of fairness.
1. Small waves of easy enemies. 
1. Does not combine enemy types. 
1. Mix it up with level design. Difficulty curve increases, than falloff for relief. 
1. Sense of completion to each wave.
1. Set player expectations: When player sees easy obstacle, they expect a more difficult version of that obstacle later on.
1. The player must pay attention to avoid being hit, they cannot simply run right and shoot. 
1. Bosses have observable patterns of movement and vulnerability.
1. Rock Paper Scissors: cutman, gutsman(rock), bombman, fireman, iceman, elecman.
1. Readability of each character's design. Movement and attack is immediately understandable. If the player has to think twice to identify what the boss does, then there is a design mistake.
1. Obtaining the powers of one's enemies is a distinctly Japanese philosophy, which shows the hero's growth represented with internal power. Weapons are part of him, not held externally.
1. No crouch because it made it difficult to know if projectiles line up with player horizontally.





# Shovel Knight

## Keeps from older games
1. Strict focus on a single mechanic, and imaginative level design that explores the idea in many ways.
2. Precision and consistency of chunky characters and grid-like levels.
3. Secrets and Bosses
4. Teach mechanics with level design instead of tutorials.
5. Quickstart. Get right into the action!
## Removes
1. Punitive Death System. (holdover from arcade game design)

## Nostalgia
Take from multiple sources.
Emulate what works.
Modernize what doesn't.
Similar but upgraded graphics.
